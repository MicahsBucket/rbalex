@isTest
public with sharing class RetailPurchaseOrderFormControllerTest {
    
    @isTest
    public static void testGetAccId(){
        // need to create vendor account names Renewal by Andersen. need to run method to fetch the new id.
		TestUtilityMethods utility = new TestUtilityMethods();
		utility.setUpConfigs();
//        createCustomSetting(); 
        Account a = createTestAccount();
        test.startTest();
        Id vendorId = RetailPurchaseOrderFormController.getVendorId();
        test.stopTest();
        system.assertEquals(a.id, vendorId);
    }
    
    @isTest
    public static void testGetRecordTypeId(){
        test.startTest();
        Id recordTypeId = RetailPurchaseOrderFormController.getRecordTypeId();
        test.stopTest();
    }
    
    private static Account createTestAccount(){        
        Account a = new Account();
        a.Name = 'Renewal by Andersen';
        a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vendor').getRecordTypeId();
        a.Baan_Business_Partner_Number__c = '12345';
        insert a;
        Return a;
    }
  /*private static void createCustomSetting(){
        RMS_Settings__c customSetting = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        insert customSetting;          
    }*/
}