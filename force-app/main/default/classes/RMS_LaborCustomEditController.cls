public with sharing class RMS_LaborCustomEditController {

    /******* Set up Standard Controller for Labor__c  *****************/
    public boolean displayPopup {get; set;}
    public boolean displaymsg {get; set;}
    private final Labor__c theLabor;
    public class CustomException extends Exception { }

       
    public RMS_LaborCustomEditController(ApexPages.StandardController stdController) {
        this.theLabor = (Labor__c)stdController.getRecord();
        system.debug('@@@@@@'+theLabor);
        if (this.theLabor == null)
            this.theLabor = new Labor__c(); 
            theLabor.RecordTypeId= ApexPages.currentPage().getParameters().get('RecordType');
            theLabor.Store_Location__c = ApexPages.currentPage().getParameters().get('StoreLocation');
            theLabor.Related_FSL_Work_Order__c = ApexPages.currentPage().getParameters().get('RelatedWorkOrder');
    } 
    
    public pagereference popUp() {
    
       pageReference pr;
       WorkOrder theWorkOrder = [SELECT Store_Location__c, Store_Location_Id__c, Store_Allowable_Percentage__c, Order_Retail_Amount__c, Work_Order_Type__c, Sold_Order__r.RecordTypeName__c FROM WorkOrder WHERE Id =: theLabor.Related_FSL_Work_Order__c];
       Decimal allowedpercentage = theWorkOrder.Store_Allowable_Percentage__c; 
       system.debug('allowedpercentage'+allowedpercentage);
       Decimal retailAmount = theWorkOrder.Order_Retail_Amount__c;
       system.debug('retailAmount'+retailAmount);
       Decimal incalculatedAmount = (allowedpercentage/100)*retailAmount;
       Decimal calculatedAmount = incalculatedAmount.setScale(2);
       system.debug('calculatedAmount'+calculatedAmount);
       
        if(theLabor.External_Labor_Amount__c > calculatedAmount && theWorkOrder.Work_Order_Type__c=='Install' && calculatedAmount > 0.00 && theWorkOrder.Sold_Order__r.RecordTypeName__c=='CORO_Record_Type'){
            displayPopup = true;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Maximum Payment Amount allowed based on Allowable labor percent on Store  is ' +calculatedAmount + ' but you entered '+theLabor.External_Labor_Amount__c+ ' Are you sure?')); //
        }
        else{
           try{
            upsert theLabor;
            pr = new pagereference('/' + theLabor.id);
            pr.setRedirect(true);
            }catch(exception ex){
            if(ex.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                String[] errorMsg = ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                String[] errorMsgOnly = errorMsg[1].split(':');
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,+errorMsgOnly[0].trim())); 
                displaymsg = true;            
            }else{
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred. Please contact the support group with the error message'+ex.getmessage()));
                displaymsg = true;
            } 
           }            
        } 
        return pr;      
    } 
    
    public PageReference save(){
    pageReference pr;
      try{
            upsert theLabor;
            pr = new pagereference('/' + theLabor.id);
            pr.setRedirect(true);
            }catch(exception ex){
            if(ex.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                String[] errorMsg = ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                String[] errorMsgOnly = errorMsg[1].split(':');
                displayPopup = false;
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,+errorMsgOnly[0].trim()));    
                displaymsg = true;            
            }else{
                displayPopup = false;
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred. Please contact the support group with the error message'+ex.getmessage()));
                displaymsg = true;
            } 
           }      
        return pr;
     }
     
      public PageReference savenew(){
      pageReference pr;
      try{      
          upsert theLabor Id; 
          pr=new PageReference ('/apex/RMS_LaborCustomEdit?StoreLocation='+theLabor.Store_Location__c+'&RecordType='+theLabor.RecordTypeId+'&RelatedWorkOrder='+theLabor.Related_FSL_Work_Order__c);
          pr.setRedirect(true); 
         }catch(exception ex){
            if(ex.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                String[] errorMsg = ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                String[] errorMsgOnly = errorMsg[1].split(':');
                displayPopup = false;
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,+errorMsgOnly[0].trim())); 
                displaymsg = true;            
            }else{
                displayPopup = false;
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred. Please contact the support group with the error message'+ex.getmessage()));
                displaymsg = true;
            } 
           } 
         return pr;   
      }
   
    public void closePopup()
      {       
       displayPopup = false;   
      } 
      
    public PageReference dosavenew(){
       pageReference pr;
       WorkOrder theWorkOrder = [SELECT Store_Location__c, Store_Location_Id__c, Store_Allowable_Percentage__c, Order_Retail_Amount__c, Work_Order_Type__c, Sold_Order__r.RecordTypeName__c FROM WorkOrder WHERE Id =: theLabor.Related_FSL_Work_Order__c];
       Decimal allowedpercentage = theWorkOrder.Store_Allowable_Percentage__c; 
       system.debug('allowedpercentage'+allowedpercentage);
       Decimal retailAmount = theWorkOrder.Order_Retail_Amount__c;
       system.debug('retailAmount'+retailAmount);
       Decimal incalculatedAmount = (allowedpercentage/100)*retailAmount;
       Decimal calculatedAmount = incalculatedAmount.setScale(2);
       system.debug('calculatedAmount'+calculatedAmount);    
        
       if(theLabor.External_Labor_Amount__c > calculatedAmount && theWorkOrder.Work_Order_Type__c=='Install' && calculatedAmount > 0.00 && theWorkOrder.Sold_Order__r.RecordTypeName__c=='CORO_Record_Type'){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Maximum Payment Amount allowed based on Allowable labor percent on Store  is ' +calculatedAmount + ' but you entered '+theLabor.External_Labor_Amount__c+ ' Are you sure?')); //
            displayPopup = true;
        }
        else{
        try{      
          upsert theLabor Id; 
          pr=new PageReference ('/apex/RMS_LaborCustomEdit?StoreLocation='+theLabor.Store_Location__c+'&RecordType='+theLabor.RecordTypeId+'&RelatedWorkOrder='+theLabor.Related_FSL_Work_Order__c);
          pr.setRedirect(true); 
         }catch(exception ex){
            if(ex.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                String[] errorMsg = ex.getMessage().split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                String[] errorMsgOnly = errorMsg[1].split(':');
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,+errorMsgOnly[0].trim())); 
                displayPopup = false;
                displaymsg = true;            
            }else{
                displayPopup = false;
                apexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,'An unexpected error occurred. Please contact the support group with the error message'+ex.getmessage()));
                displaymsg = true;
            } 
           }
        } 
     return pr;  
   }
 }