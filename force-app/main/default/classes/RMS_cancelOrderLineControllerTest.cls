@isTest 
private with sharing class RMS_cancelOrderLineControllerTest {
    
    static testmethod void createTestRecords() {  
        
        // Turn off the financial trigger to avoid SOQL limits in test class
        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();  
        
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');       
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];       
        insert dwelling1;      
                
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
                
        List<Order> ordersToInsert = new List<Order>();
        Order orderToInsert =  new Order( Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 EffectiveDate= Date.Today(), 
                                 Status ='Draft', 
                                 Pricebook2Id = pricebookId
                                );
        insert orderToInsert;
        
        OrderItem orderItemMaster = new OrderItem(OrderId = orderToInsert.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100);
        insert orderItemMaster;
        Asset asset = new Asset ( Name='Asset1',
                                 Original_Order_Product__c = orderItemMaster.Id,
                                 Product2Id= masterProduct.Id,
                                 AccountId = dwelling1.id, 
                                 Variant_Number__c = '1234ABC',
                                 Unit_Wholesale_Cost__c = 200,
                                 Quantity = 1,
                                 Price = 100,
                                 Status = 'Installed',
                                 Sold_Order__c = orderToInsert.Id,
                                 PurchaseDate = Date.Today()
                                );
        
        insert asset;
        OrderItem serviceOrderItem = new OrderItem(Sold_Order_Product_Asset__c = asset.Id, Installed_Product_Asset__c = asset.Id,
                                                   OrderId = orderToInsert.id, Parent_Order_Item__c = orderItemMaster.id, 
                                                   PricebookentryId = pricebookEntryServ.Id, 
                                                   Quanity_Ordered__c = 2, Service__c = true, Quantity = 2, UnitPrice = 100 );
        insert serviceOrderItem;
    }
    
    static testmethod void cancelOrderLineTest(){
        createTestRecords();
        Test.startTest();
        Order order = [SELECT Id, AccountId FROM Order WHERE Name = 'Sold Order 1' LIMIT 1];       
        System.debug('...order..'+order);
        OrderItem orderItem = [SELECT Id, OrderId, Defect_Code__c, Status__c, PricebookEntry.Product2Id,  PricebookEntry.Product2.Name, 
                               Purchase_Order__c, Quantity, Total_Retail_Price__c, Unit_Wholesale_Cost__c, Total_Wholesale_Cost__c, 
                               Variant_Number__c, UnitPrice, Parent_Order_Item__c, Service__c, Unit_Id__c, Purchase_Order__r.Status__c, 
                               Color_Match_Hinges__c, Corrosion_Resistant_Hinges__c, Corrosion_Resistant_Lock__c, Hardware_Finish_Color__c, 
                               Window_Opening_Control_Device__c, Is_Height_L_or_R_Leg_Height__c, Is_Leg_Height_L_or_R_Leg_Height__c, 
                               Leg_Height_Exterior_Fraction__c, Leg_Height_Exterior_Whole_Inch__c,Leg_Height_Interior_Fraction__c, 
                               Leg_Height_Interior_Whole_Inch__c, Meet_Rail_Dimension__c, EJ_Kerf__c, Cancellation_Reason__c
                               FROM OrderItem WHERE OrderId =: order.id LIMIT 1];        
        System.debug('...orderItem..'+orderItem);
        orderItem.Status__c = 'Cancelled';
        update orderItem;

        Test.setCurrentPage(Page.RMS_CancelOrderLine);
        ApexPages.StandardController sc = new ApexPages.StandardController(OrderItem);
        RMS_CancelOrderLineController cancelOrderCtrl = new RMS_CancelOrderLineController(sc);
        cancelOrderCtrl.save();        
        OrderItem oi = [select id from OrderItem where id =: orderItem.id AND Status__c = 'Cancelled'];
        system.assertEquals(false, ApexPages.hasMessages());
        cancelOrderCtrl.cancel();
        list<OrderItem> oi2 = [select id from OrderItem where id =: orderItem.id AND Status__c != 'Cancelled'];
        system.assertEquals(true, oi2.isEmpty());
        Test.stopTest();              
    }
}