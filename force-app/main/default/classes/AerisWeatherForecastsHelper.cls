// Code coverage provided by: AerisWeatherForecastHelperTest

public without sharing class AerisWeatherForecastsHelper {

	// Configurations set in Aeris_Weather_API_Configuration__mdt
    private static String CLIENT_ID;
    private static String SECRET_KEY;
    private static Integer FORECAST_INTERVAL_HOURS = 3; // default to 3
    
    private static Map<String,AerisWeatherForecastResponse.ThresholdMapping> thresholdFieldMap = AerisWeatherForecastResponse.getThresholdFieldMap();
    private static String urlFieldsParameter = AerisWeatherForecastResponse.getDelimitedForecastPropertyNames('periods.',',');
	private static Integer descriptionCounter = 0;

	static {
		List<Aeris_Weather_API_Configuration__mdt> configs = [SELECT DeveloperName, Value__c FROM Aeris_Weather_API_Configuration__mdt];
		for (Aeris_Weather_API_Configuration__mdt config : configs) {
			if (config.DeveloperName == 'CLIENT_ID' && config.Value__c != null) {
				CLIENT_ID = config.Value__c;
			}
			if (config.DeveloperName == 'SECRET_KEY' && config.Value__c != null) {
				SECRET_KEY = config.Value__c;
			}
			if (config.DeveloperName == 'FORECAST_INTERVAL_HOURS') {
				FORECAST_INTERVAL_HOURS = Integer.valueOf(config.Value__c);
			}
		}
		if (CLIENT_ID == null) { throw new AerisWeatherForecastHelper_InvalidCustomMetadata_CustomException('CLIENT_ID must be set in Aeris Weather API Configuration');}
		if (SECRET_KEY == null) { throw new AerisWeatherForecastHelper_InvalidCustomMetadata_CustomException('SECRET_KEY must be set in Aeris Weather API Configuration');}
	}

	public static List<ServiceAppointment> updateForecasts(List<sObject> appointments) {
		List<ServiceAppointment> sasToUpdate = new List<ServiceAppointment>();

		// Get set of service territory Ids for the service appointments in this batch
        Set<Id> stIds = new Set<Id>();
        for(sObject obj : appointments) {
			ServiceAppointment sa = (ServiceAppointment)obj;
        	stIds.add(sa.ServiceTerritoryId);
        	if (sa.ServiceTerritory.ParentTerritoryId != null) {
        		stIds.add(sa.ServiceTerritory.ParentTerritoryId);
        	}
        }

        // Get any Weather_Forecast_Threshold__c records associated with these appointments' service territories or parent service territories
        List<Weather_Forecast_Threshold__c> thresholds = [
        	SELECT Name,Criteria_Logic__c, Accumulated_Precipitation_in__c,Wind_Speed_MPH__c,Ice_Accumulation_in__c,
	        	Max_Feels_Like_Temp_F__c,Max_Humidity__c,Max_Temp_F__c,Max_Wind_Gust_Speed_MPH__c,Min_Feels_Like_Temp_F__c,
	        	Min_Humidity__c,Min_Temp_F__c,Probability_of_Precipitation__c,Service_Territory__c,Snow_Accumulation_in__c,
	        	UV_Index__c,Work_Order_Record_Types__c
	        FROM Weather_Forecast_Threshold__c
	        WHERE Service_Territory__c IN :stIds
        ];

        // Generate map of territory Ids to their relevant Weather_Forecast_Threshold__c records
        Map<Id,List<Weather_Forecast_Threshold__c>> territoriesToThresholds = new Map<Id,List<Weather_Forecast_Threshold__c>>();
        for (Weather_Forecast_Threshold__c threshold : thresholds) {
        	if (!territoriesToThresholds.containsKey(threshold.Service_Territory__c)) {
        		territoriesToThresholds.put(threshold.Service_Territory__c, new List<Weather_Forecast_Threshold__c>());
        	}
        	territoriesToThresholds.get(threshold.Service_Territory__c).add(threshold);
        }

        // Loop over the service appointments and perform the forecast callout for each appointment
        for(sObject obj : appointments) {
			ServiceAppointment sa = (ServiceAppointment)obj;

            // Generate URL parameters
            Map<String,Object> urlParams = new Map<String,Object>();
            if (sa.Latitude != null && sa.Longitude != null) {
            	urlParams.put('LocationString',(String.valueOf(sa.Latitude) + ',' + String.valueOf(sa.Longitude)));
            } else {
            	urlParams.put('LocationString',(String.valueOf(sa.PostalCode)).Left(5));
            }
            DateTime endTime = sa.SchedEndTime < sa.SchedStartTime.addHours(FORECAST_INTERVAL_HOURS) ? sa.SchedStartTime.addHours(FORECAST_INTERVAL_HOURS) : sa.SchedEndTime;
            urlParams.put('SchedStartTime',String.valueOf(sa.SchedStartTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')));
            urlParams.put('SchedEndTime',String.valueOf(endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')));
            urlParams.put('Hours',FORECAST_INTERVAL_HOURS);
            urlParams.put('ServiceAppointmentId',sa.Id);

            AerisWeatherForecastResponse response;
			try {
				response = AerisWeatherForecastsHelper.performCalloutToForecastAPI(urlParams);
			} catch (Exception ex) { /*response is null*/ }
			
			system.debug(LoggingLevel.WARN, 'xxxxx ' + response);

			String responseErrorDescription;
        	// Handle response
            if (response == null) {
                // Error in performCalloutToForecastAPI
				responseErrorDescription = 'Error occurred during weather API callout attempt.';
				sa.Weather_Forecast_Error__c = true;
                System.debug(LoggingLevel.ERROR,'***** Error (ServiceAppointmentId=' + sa.Id + ') - Null response returned from performCalloutToForecastAPI');
            } else if (response.success == null || response.success == false || response.error != null) {
                // 200 status code, but the API itself returned an error
				responseErrorDescription = 'Error occurred during weather API callout attempt.  Response object: \n';
				sa.Weather_Forecast_Error__c = true;
                System.debug(LoggingLevel.ERROR,'***** Error (ServiceAppointmentId=' + sa.Id + ') - Forecast API returned an error message.  Error details: ' + response.error);
            } else if (response.response == null || response.response.size() == 0 || response.response[0].periods == null){
                // Malformed JSON, missing expected nodes
				responseErrorDescription = 'Error occurred during weather API callout attempt - unable to process response.  Response object: \n';
                sa.Weather_Forecast_Error__c = true;
				System.debug(LoggingLevel.ERROR,'***** Error (ServiceAppointmentId=' + sa.Id + ') - Malformed response.  Response JSON: ' + response);
            } else if (response.response[0].periods.size() == 0) {
            	// No forecast provided by the API
				responseErrorDescription = 'Error occurred during weather API callout attempt - no forecast data returned.  Response object: \n';
            	sa.Weather_Forecast_Error__c = true;
				System.debug(LoggingLevel.ERROR,'***** Warning (ServiceAppointmentId=' + sa.Id + ') - No forecasts were returned from the API during the appointment\'s timeframe.');
            	System.debug(LoggingLevel.ERROR,'***** Details for ServiceAppointmentId=' + sa.Id + ': SchedStartTime=' + sa.SchedStartTime + ' | SchedEndTime=' + sa.SchedEndTime);
        	} else {
        		// Success!
				sa.Weather_Forecast_Error__c = false;

        		// Generate list of all Weather_Forecast_Threshold__c records for the SA's territory and the territory's parent territory, if applicable
        		List<Weather_Forecast_Threshold__c> applicableThresholds = new List<Weather_Forecast_Threshold__c>();
        		if (territoriesToThresholds.get(sa.ServiceTerritoryId) != null) {
        			applicableThresholds.addAll(territoriesToThresholds.get(sa.ServiceTerritoryId));
        		}
        		if (sa.ServiceTerritory.ParentTerritoryId != null && territoriesToThresholds.get(sa.ServiceTerritory.ParentTerritoryId) != null) {
        			applicableThresholds.addAll(territoriesToThresholds.get(sa.ServiceTerritory.ParentTerritoryId));
        		}

				// Process the response against any configured weather forecast thresholds
				if (applicableThresholds.size() > 0) {
					AerisWeatherForecastsHelper.processWeatherForecastForServiceAppointment(sa, response, applicableThresholds,FORECAST_INTERVAL_HOURS); // Process the result
				}
        	}

        	sa.Weather_Forecast_JSON__c = responseErrorDescription + JSON.serialize(response);
            sa.Weather_Timestamp__c = System.now();
			sasToUpdate.add(sa);
        }

		return sasToUpdate;
	}


    // Perform the actual callout to the AerisWeather API
    public static AerisWeatherForecastResponse performCalloutToForecastAPI(Map<String,Object> urlParams) {
        AerisWeatherForecastResponse result;
        
        try {
            String url = String.format('https://api.aerisapi.com/forecasts/{0}?from={1}&to={2}&filter={3}h&fields={4}&client_id={5}&client_secret={6}', 
            	new Object[]{
	            	urlParams.get('LocationString'),
	            	urlParams.get('SchedStartTime'),
	            	urlParams.get('SchedEndTime'),
	            	urlParams.get('Hours'),
	            	urlFieldsParameter,
	            	CLIENT_ID,
	            	SECRET_KEY
            	}
            );
            
			// Make callout
            HttpRequest request = new HttpRequest();
            request.setEndpoint(url);
            request.setMethod('GET');

            Http http = new Http();
            HttpResponse response = http.send(request);

            if (response.getStatusCode() == 200) {
            	// Endpoint successfully received request 
            	try {
            		result = AerisWeatherForecastResponse.parse(response.getBody());
	            } catch(Exception ex) {
	                System.debug(LoggingLevel.ERROR,'***** Error during attempt to deserialize JSON response (ServiceAppointmentId=' + urlParams.get('ServiceAppointmentId') + ').');
	                System.debug(LoggingLevel.ERROR,'***** Response body: ' + response.getBody());
	                throw new AerisWeatherForecastResponse_CustomException(ex);
	            } 
            } else {
            	System.debug(LoggingLevel.ERROR,'***** Error during callout (ServiceAppointmentId=' + urlParams.get('ServiceAppointmentId') + ') - status code ' + response.getStatusCode() + ': ' + response.getStatus());
            	throw new AerisWeatherForecastResponse_CustomException('Error during callout - status code ' + response.getStatusCode());
            }
        } catch(Exception ex) {
            System.debug(LoggingLevel.ERROR,'****** Error occurred during weather forecast callout attempt (ServiceAppointmentId=' + urlParams.get('ServiceAppointmentId') + '); unable to process response.  Details: ' + ex);
            throw new AerisWeatherForecastResponse_CustomException(ex);
        }

        return result;
    }

    public static void processWeatherForecastForServiceAppointment(ServiceAppointment sa, AerisWeatherForecastResponse response, List<Weather_Forecast_Threshold__c> applicableThresholds, Integer intervalHours) {
    	if (applicableThresholds == null || applicableThresholds.size() == 0) {return;}
		descriptionCounter = 0;
    	
    	List<ThresholdViolation> violations = new List<ThresholdViolation>();
    	Boolean inJeopardy = false;

    	List<MultidayWorkOperationalTime> multidayTimes;
    	if (sa.FSL__IsMultiDay__c && sa.FSL__MDT_Operational_Time__c != null) {
    		try {
    			multidayTimes = (List<MultidayWorkOperationalTime>)JSON.deserialize(sa.FSL__MDT_Operational_Time__c, List<MultidayWorkOperationalTime>.class);
    		} catch (Exception ex) {
				System.debug(LoggingLevel.ERROR,'***** Error parsing multiday times: ' + ex);
				System.debug(LoggingLevel.ERROR,'***** sa.FSL__IsMultiDay__c: ' + sa.FSL__IsMultiDay__c);
			}
    	}

    	// Loop over forecast periods in the forecast response
        for (AerisWeatherForecastResponse.Periods period : response.Response[0].periods) {
			DateTime startTime = Datetime.newInstance(period.timestamp * (Long)1000);
        	DateTime endTime = startTime.addHours(intervalHours);

        	// FUTURE ENHANCEMENT: Include logic to calculate precip accumulation in the previous X hours, eg: if 6+in of snow falls in the 12hrs prior, flag it.

        	// Check whether the period overlaps with the appointment's scheduled time
        	Boolean skip = true;
        	if (sa.FSL__IsMultiDay__c && sa.FSL__MDT_Operational_Time__c != null && sa.SchedStartTime.date() < sa.SchedEndTime.date()) {
        		for (MultidayWorkOperationalTime multidayTime : multidayTimes) {
        			if (multidayTime.Type == 'OperationalSlot' && startTime < multidayTime.Finish && endTime > multidayTime.Start) {
        				// This period overlaps with one of the multiday times on-site based on FSL__MDT_Operational_Time__c, so do not skip.
        				skip = false;
        			}
        		}
        	} else {
				skip = false; // not a multiday appointment
			}
        	if (skip) { 
				continue; // Skip this period if it does not overlap a multiday appointment's actual time on-site 
			} 

        	// For each period, loop over the Weather_Forecast_Threshold__c records for this service appointment
        	for (Weather_Forecast_Threshold__c thresholdConfig : applicableThresholds) {
        		
        		// Check whether the relevant work order's record type matches the threshold configuration's record type
        		List<String> woRecordTypeNames = thresholdConfig.Work_Order_Record_Types__c.split(',');
        		if (!(woRecordTypeNames.contains('All') || woRecordTypeNames.contains(sa.Work_Order__r.RecordType.Name))) {
        			continue; // Skip this configuration
        		}

        		Boolean exceedsAnyThreshold = false;
        		Boolean exceedsAllThresholds = true;
        		List<ThresholdViolation> violationsForPeriod = new List<ThresholdViolation>();

        		// For each threshold config, loop over the populated fields
        		for (String configFieldName : thresholdConfig.getPopulatedFieldsAsMap().keySet()) {
        			if (!thresholdFieldMap.containsKey(configFieldName)) { continue; } // Field is not mapped; skip this field.

        			// Get the threshold mapping information for this field
        			AerisWeatherForecastResponse.ThresholdMapping thresholdMapping = thresholdFieldMap.get(configFieldName);
    				
        			// Get the forecast value from the forecast period object 
    				Object forecastValue = period.getPropertyByName(thresholdMapping.propertyName);
    				if (forecastValue == null) { continue; } 

    				// Check whether the forecast value exceeds the relevant threshold based on its threshold type (MIN or MAX)
    				if ((thresholdMapping.thresholdType == AerisWeatherForecastResponse.ThresholdType.MAX
    					&& Double.valueOf(forecastValue) > Double.valueOf(thresholdConfig.get(configFieldName))) ||
    					(thresholdMapping.thresholdType == AerisWeatherForecastResponse.ThresholdType.MIN
    					&& Double.valueOf(forecastValue) < Double.valueOf(thresholdConfig.get(configFieldName)))
    				) {
    					// It does exceed
    					exceedsAnyThreshold = true;
    					violationsForPeriod.add(new ThresholdViolation(
							sa.Id,
							startTime,
							endTime,
							thresholdConfig,
							thresholdMapping.fieldLabel,
							forecastValue,
							thresholdConfig.get(configFieldName)
						));
    				} else {
    					// It does not exceed
    					exceedsAllThresholds = false; // Threshold is configured, but the forecastValue does not meet the threshold
    				}
        		}

        		// If a threshold configuration was exceeded (based on its Criteria Logic), note the descriptions and set the inJeopardy flag
        		if (exceedsAnyThreshold &&
        			(thresholdConfig.Criteria_Logic__c == 'Any Criteria (OR)'
        			|| (thresholdConfig.Criteria_Logic__c == 'All Criteria (AND)' && exceedsAllThresholds))) {
        			inJeopardy = true;
        			violations.addAll(violationsForPeriod);
        		}
        	}
        }

        // Update the service appointment to reflect that it is in jeopardy due to weather
        if (inJeopardy) {
        	sa.FSL__InJeopardy__c = true;
        	sa.Weather_Forecast_Warnings__c = JSON.serialize(violations);
        	if (sa.FSL__InJeopardyReason__c == null) {
        		sa.FSL__InJeopardyReason__c = 'Weather';
        	}
        }
    }

    public class MultidayWorkOperationalTime {
    	public String Type;
    	public DateTime Start;
    	public DateTime Finish;
    }

	private class ThresholdViolation {
		public String saId;
		public Integer descriptionNumber;
		public DateTime startTime;
		public DateTime endTime;
		public String timeIntervalString;
		public String thresholdConfigId;
		public String thresholdConfigName;
		public String description;
		public String shortDescription;

		public ThresholdViolation(
				String saId, 
				DateTime startTime, 
				DateTime endTime, 
				Weather_Forecast_Threshold__c thresholdConfig, 
				String fieldLabel,
				Object forecastValue,
				Object thresholdValue) {

			this.descriptionNumber = ++descriptionCounter;
			this.saId = saId;
			this.startTime = startTime;
			this.endTime = endTime;
			this.timeIntervalString = startTime.format('MMM d h:mm') + ' - ' + endTime.format('h:mm a');
			this.thresholdConfigId = thresholdConfig.Id;
			this.thresholdConfigName = thresholdConfig.Name;
			this.description = String.format('Forecasted {0} value of {1} exceeds limit of {2} between {3} and {4}.', 
    						new Object[]{fieldLabel,forecastValue,thresholdValue,startTime.format(),endTime.format()});
			this.shortDescription = String.format('Forecasted {0} value of {1} exceeds limit of {2}.', 
    						new Object[]{fieldLabel,forecastValue,thresholdValue});
		}
	}

    public Class AerisWeatherForecastResponse_CustomException extends Exception {}

	public Class AerisWeatherForecastHelper_InvalidCustomMetadata_CustomException extends Exception {}
}