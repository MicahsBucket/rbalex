public without sharing class GanttAction_ResourceAbsencesController {
    
    // Properties for Gantt_ResourceAbsencesPage.page
    public Id srId { get; set; }
    public Id stmId { get; set; }
    public DateTime initialStart { get; set; }
    public DateTime initialEnd { get; set; }

    // Constructor for Gantt_ResourceAbsencesPage.page
    public GanttAction_ResourceAbsencesController() {
        srId = ApexPages.currentPage().getParameters().get('id');
        system.debug(srId);
        stmId = ApexPages.currentPage().getParameters().get('stm');
        
        String ganttStartString = ApexPages.currentPage().getParameters().get('start');
        String ganttEndString = ApexPages.currentPage().getParameters().get('end');
        If (String.isNotBlank(ganttStartString)) {
            String[] startParts = ganttStartString.split('-');
            initialStart = Datetime.newInstance(Integer.valueOf(startParts[2]), Integer.valueOf(startParts[0]), Integer.valueOf(startParts[1]),12,0,0);
        }
        
        if (initialStart == null || initialStart <= System.now()) { 
            initialStart = Datetime.newInstance(System.today().addDays(1), Time.newInstance(12, 0, 0, 0)); // tomorrow at noon
        }
        initialEnd = initialStart;
    }

    @AuraEnabled(cacheable=true)
    public static string getTerritoryName(String serviceTerritoryMemberId) {
        return [SELECT ServiceTerritory.Name FROM ServiceTerritoryMember WHERE Id = :serviceTerritoryMemberId].ServiceTerritory.Name;
    }

    @AuraEnabled(cacheable=true)
    public static List<ServiceTerritoryMember> getServiceResourcesForTerritory(String serviceTerritoryMemberId, DateTime startDateTime, DateTime endDateTime) {

        if (startDateTime == null || endDateTime == null) {
            return new List<ServiceTerritoryMember>(); // only show resources after a user enters a start and end time
        }

        Id stId = [SELECT ServiceTerritoryId FROM ServiceTerritoryMember WHERE Id = :serviceTerritoryMemberId].ServiceTerritoryId;

        // Query for all territory members in the same territory as the selected resource during the interval of startDateTime->endDateTime
        List<ServiceTerritoryMember> results = [SELECT Id, ServiceResourceId, ServiceResource.Name, TerritoryType, EffectiveStartDate, EffectiveEndDate 
            FROM ServiceTerritoryMember
            WHERE ServiceTerritoryId = :stId 
                AND ServiceResource.IsActive = true
                AND (EffectiveStartDate = null OR EffectiveStartDate <= :endDateTime)
                AND (EffectiveEndDate = null OR EffectiveEndDate >= :startDateTime)
            ORDER BY ServiceResource.Name ASC];

        return results;
    }

    @AuraEnabled(cacheable=false)
    public static SaveResultsWrapper createAbsenceRecords(List<ResourceAbsence> resourceAbsences, String serviceTerritoryMemberId) {
        SaveResultsWrapper result = new SaveResultsWrapper();
        Id recordTypeId = Schema.SObjectType.ResourceAbsence.getRecordTypeInfosByDeveloperName().get('Non_Availability').getRecordTypeId();
        system.debug(recordTypeId);

        Integer territoryOffset = TimeZone.getTimeZone([SELECT ServiceTerritory.OperatingHours.TimeZone FROM ServiceTerritoryMember WHERE Id = :serviceTerritoryMemberId].ServiceTerritory.OperatingHours.TimeZone).getOffset(system.now());
        Integer userOffset = UserInfo.getTimeZone().getOffset(system.now());

        Integer timeZoneDifference = (userOffset - territoryOffset) / 1000;

        for (ResourceAbsence ra : resourceAbsences) {
            ra.Start = ra.Start.addSeconds(timeZoneDifference);
            ra.End = ra.End.addSeconds(timeZoneDifference);
            ra.RecordTypeId = recordTypeId;
            ra.FSL__Approved__c = true;
        }

        for (Database.SaveResult sr : Database.insert(resourceAbsences, false)) {
            if (sr.isSuccess()) {
                system.debug(sr.getId());
                result.successfulRecords.add(sr.getId());
            } else {
                system.debug(sr.getErrors());
                result.errors.add(sr.getErrors()[0].getMessage());
            }
        }
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static string getServiceTerritoryMemberTimezone(String serviceTerritoryMemberId){
        String timeZone =  [SELECT ServiceTerritory.OperatingHours.TimeZone FROM ServiceTerritoryMember WHERE Id = :serviceTerritoryMemberId].ServiceTerritory.OperatingHours.TimeZone;
        switch on timeZone {
            when 'America/New_York' {
                timeZone = 'Eastern';
            }
            when 'America/Chicago' {
                timeZone = 'Central';
            }
            when 'America/Los_Angeles' {
                timeZone = 'Pacific';
            }
            when 'America/Indiana/Indianapolis' {
                timeZone = 'Eastern';
            }
            when 'America/Denver' {
                timeZone = 'Mountain';
            }
            when else {
                timeZone = 'Unidentified';
            }
        }

        return timeZone;
    }

    public class SaveResultsWrapper{
        @AuraEnabled public List<String> successfulRecords = new List<String>();
        @AuraEnabled public List<String> errors = new List<String>();
    }
}