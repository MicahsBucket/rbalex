/**
 * Created by melindagrad on 3/15/18.
 */



public with sharing class Peak_AnnouncementEmailController {
    /*
     * This method will send emails to Community Users with a certain Profile. The email will be sent to the
     * email address on the Community User's related Contact record, and will use the specified Email Template.
     *
     * To use this method set the emailTemplateName and profileName to the desired Email Template and User Profile
     */
    @InvocableMethod
    public static void sendAnnouncementEmail(List<Id> announcementIDs){

        String emailTemplateName = 'Announcement Email';
        String profileName = 'Partner RMS-Sales';

        //Query Email Template ID
        ID templateId = [SELECT Id FROM EmailTemplate WHERE EmailTemplate.Name =: emailTemplateName].Id;

        //Query Community Users by Profile Name
        List<User> recipientList = [SELECT Id, Name, ContactId, Contact.Email, Email FROM User WHERE Profile.Name =:profileName];
        List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();

        //For each Announcement
        for(Id theAnnouncementId : announcementIDs) {

            //For each recipient, create a single email message and add to to List
            for (User theUser: recipientList) {

                if(theUser.ContactId != NULL) {
                    List<String> toAddress = new List<String>{theUser.Contact.Email != null ? theUser.Contact.Email : theUser.Email};
                    System.debug('toAddress === ' + toAddress);
                    Messaging.SingleEmailMessage singleMessage = new Messaging.SingleEmailMessage();

                    singleMessage.setTemplateId(templateId);

                    //Send email to Contact in order to correctly populate merge fields on template
                    singleMessage.setTargetObjectId(theUser.ContactId);
                    singleMessage.setToAddresses(toAddress);
                    singleMessage.setTreatTargetObjectAsRecipient(false);
                    singleMessage.setWhatId(theAnnouncementId);
                    singleMessage.saveAsActivity = false;

                    //Add to list of messages
                    messageList.add(singleMessage);
                } else {
                    // Dang, not actually a contact
                }
            }
        }

        //Send List of emails
		if(!Test.isRunningTest()){
			Messaging.SendEmailResult[] results = messaging.sendEmail(messageList);
		}

    }
}