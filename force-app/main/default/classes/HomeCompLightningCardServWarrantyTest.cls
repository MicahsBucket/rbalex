/*
* @author Jason Flippen
* @date 11/17/2020
* @description Test Class for the following Classes:
*              - HomeCompLightningCardServiceAndWarranty
*/ 
@isTest
public class HomeCompLightningCardServWarrantyTest {

    @testSetup static void setupData() {
 
        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        List<Product2> testProductList = new List<Product2>();
        // Parent Product
        Product2 testParentProduct = new Product2(Name = 'Test Parent Product',
                                                  RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                                  Vendor__c = testVenderAcct.Id,
                                                  Cost_PO__c = true,
                                                  IsActive = true,
                                                  Account_Number__c =  testFan.Id);
        testProductList.add(testParentProduct);
        // Child Product
 
        insert testProductList;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        // Parent PricebookEntry
        PricebookEntry testParentPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testParentProduct.Id);
        testPBEList.add(testParentPBEStandard);
        PricebookEntry testParentPBE = testUtility.createPricebookEntry(testPricebook.Id, testParentProduct.Id);
        testPBEList.add(testParentPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Install Needed',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;

        Order testOrder1 =  new Order(Name = 'Test Order1', 
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Warranty Rejected',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder1;

    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getServiceCompleted method.
    */
    static testMethod void testGetServiceCompleted() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Install Needed'];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardServiceAndWarranty.ServiceCompletedWrapper> orderList = HomeCompLightningCardServiceAndWarranty.getServiceCompleted(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }

    /*
    * @author Jason Flippen
    * @date 11/17/2020
    * @description Method to test the functionality in the getWarrantyRejected method.
    */
    static testMethod void testGetWarrantyRejected() {

        Test.startTest();

            List<Order> testOrderList = [SELECT Id, OrderNumber FROM Order WHERE Status = 'Warranty Rejected'];

            if (!testOrderList.isEmpty()) {
                List<HomeCompLightningCardServiceAndWarranty.WarrantyRejectedWrapper> orderList = HomeCompLightningCardServiceAndWarranty.getWarrantyRejected(testOrderList[0].OrderNumber);
                System.assertEquals(1,orderList.size());
            }

        Test.stopTest();

    }

}