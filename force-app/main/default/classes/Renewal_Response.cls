public with sharing class Renewal_Response extends Peak_Response{
    @AuraEnabled public List<Renewal_ContentObject> renewalResults{get;set;} // custom results to iterate over - use this when creating a list of custom wrapper classes

    public Renewal_Response(){
        super();
        renewalResults = new List<Renewal_ContentObject>();
    }
}