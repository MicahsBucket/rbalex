/*
* @author Jason Flippen
* @date 02/25/2020 
* @description Class to provide functionality for the confirmPurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - ConfirmPOActionControllerTest
*/
public with sharing class ConfirmPOActionController {

    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Confirmation_Number__c,
                                                  Charge_Cost_To__c,
                                                  Estimated_Ship_Date__c,
                                                  Order__c,
                                                  Order__r.Pricebook2Id,
                                                  Order__r.RecordTypeId,
                                                  Order__r.RecordType.DeveloperName,
                                                  Order__r.Revenue_Recognized_Date__c,
                                                  Order__r.Status,
                                                  RecordTypeId,
                                                  RecordType.DeveloperName,
                                                  Status__c,
                                                  Store_Location__c,
                                                  Store_Location__r.Name,
                                                  Tax__c,
                                                  Vendor__c,
                                                  Vendor__r.Name,
                                                  Vendor__r.Zero_Dollar_Vendor__c,
                                                  (
                                                      SELECT Id,
                                                             Pricebookentry.Product2.Name,
                                                             Charge_Cost_To__c,
                                                             Unit_Wholesale_Cost__c,
                                                             Variant_Number__c,
                                                             Sold_Order_Product_Asset__r.Variant_Number__c
                                                      FROM   Order_Products__r
                                                      ORDER BY Pricebookentry.Product2.Name ASC
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.chargeCostTo = purchaseOrder.Charge_Cost_To__c;
        wrapper.estimatedShipDate = purchaseOrder.Estimated_Ship_Date__c;
        wrapper.status = purchaseOrder.Status__c;
        wrapper.confirmationNumber = purchaseOrder.Confirmation_Number__c;
        String storeLocationId = '';
        String storeLocationName = '';
        if (purchaseOrder.Store_Location__c != null) {
            storeLocationId = purchaseOrder.Store_Location__c;
            storeLocationName = purchaseOrder.Store_Location__r.Name;
        }
        wrapper.storeLocationId = storeLocationId;
        wrapper.storeLocationName = storeLocationName;

        String vendorName = '';
        Boolean zeroDollarVendor = false;
        if (purchaseOrder.Vendor__c != null) {
            vendorName = purchaseOrder.Vendor__r.Name;
            zeroDollarVendor = purchaseOrder.Vendor__r.Zero_Dollar_Vendor__c;
        }
        wrapper.vendorName = vendorName;
        wrapper.zeroDollarVendor = zeroDollarVendor;

        Decimal tax = 0.00;
        if (purchaseOrder.Tax__c != null &&
            vendorName != 'Renewal by Andersen' &&
            purchaseOrder.Charge_Cost_To__c != 'Manufacturing') {
            tax = purchaseOrder.Tax__c;
        }
        wrapper.tax = tax;

        Boolean costPurchaseOrder = false;
        if (purchaseOrder.RecordType.DeveloperName == 'Cost_Purchase_Order') {
            costPurchaseOrder = true;
        }
        wrapper.costPurchaseOrder = costPurchaseOrder;

        Boolean relatedOrderCORO = false;
        Boolean relatedOrderService = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            relatedOrderCORO = true;
        }
        else if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            relatedOrderService = true;
        }
        wrapper.relatedOrderCORO = relatedOrderCORO;
        wrapper.relatedOrderService = relatedOrderService;

        Boolean relatedOrderIsClosed = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.Status == 'Job Closed') {
            relatedOrderIsClosed = true;
        }
        wrapper.relatedOrderIsClosed = relatedOrderIsClosed;

        // If the related Order is a Purchase Order, make sure the Revenue Recognized
        // Date on the Order is null, and the Order Status is a valid Status.
        Boolean revRecNullAndValidStatus = false;
        Set<String> validStatusSet = new Set<String>{'Job in Progress','Install Complete','Install Needed','Install Scheduled','Order Released','Ready to Order'};
        if (relatedOrderCORO == true && purchaseOrder.Order__c != null &&
            purchaseOrder.Order__r.Revenue_Recognized_Date__c == null &&
            validStatusSet.contains(purchaseOrder.Order__r.Status)) {
            revRecNullAndValidStatus = true;
        }
        wrapper.revRecNullAndValidStatus = revRecNullAndValidStatus;

        wrapper.productList = new List<OrderProductWrapper>();
        for (OrderItem oi : purchaseOrder.Order_Products__r) {
            OrderProductWrapper productWrapper = new OrderProductWrapper();
            productWrapper.id = oi.Id;
            productWrapper.productName = oi.Pricebookentry.Product2.Name;
            productWrapper.chargeCostTo = oi.Charge_Cost_To__c;
            Decimal unitWholesaleCost = 0.00;
            if (oi.Unit_Wholesale_Cost__c != null) {
                unitWholesaleCost = oi.Unit_Wholesale_Cost__c;
            }
            productWrapper.unitWholesaleCost = unitWholesaleCost;
            // productWrapper.variantNumber = 'tbd';
            productWrapper.variantNumberOld = oi.Sold_Order_Product_Asset__r.Variant_Number__c;
            
            productWrapper.variantNumber = oi.Variant_Number__c;
            // productWrapper.variantNumberOld = 'a4s4s5s8';
            // productWrapper.confirmationNumber = 'ConfirmNumber';
            wrapper.productList.add(productWrapper);
        }

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Method to update the Purchase Order Status to "Confirmed".
    * @param Purchase Order (Wrapper)
    * @returns String containing the save result (Success/Error)
    */
    @AuraEnabled
    public static String confirmPurchaseOrder(PurchaseOrderWrapper purchaseOrder) {

        String returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;

/*
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Confirmed_Timestamp__c,
                                                  Order__c,
                                                  Order__r.RecordTypeId,
                                                  Order__r.RecordType.DeveloperName,
                                                  Status__c,
                                                  Vendor__c,
                                                  Vendor__r.Name,
                                                  Vendor__r.Zero_Dollar_Vendor__c,
                                                  (
                                                      SELECT Id,
                                                             Charge_Cost_To__c,
                                                             Unit_Id__c,
                                                             Unit_Wholesale_Cost__c,
                                                             Variant_Number__c
                                                      FROM   Order_Products__r
                                                      WHERE  Verify_Item_Configuration__c = false
                                                      AND    NSPR__c = false
                                                      AND    Product_Record_Type__c = 'Master_Product'
                                                      AND    Product_Family__c IN ('Window', 'Specialty')
                                                      ORDER BY Unit_Id__c ASC
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        Boolean relatedOrderCORO = false;
        Boolean relatedOrderService = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            relatedOrderCORO = true;
        }
        else if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            relatedOrderService = true;
        }

        Boolean allOrderItemsHaveWholesaleCost = false;
        Boolean completeUnitsHaveVariants = true;
        if (purchaseOrder.Vendor__c != null && purchaseOrder.Vendor__r.Zero_Dollar_Vendor__c == true) {
            allOrderItemsHaveWholesaleCost = true;
        }
        else {

            for (OrderItem oi : purchaseOrder.Order_Products__r) {
                Boolean isNotServiceManufacturing = (relatedOrderService == false || oi.Charge_Cost_To__c != 'Manufacturing');
                if (oi.Unit_Wholesale_Cost__c == null ||
                    oi.Unit_Wholesale_Cost__c < 0 ||
                    (oi.Unit_Wholesale_Cost__c == 0 && isNotServiceManufacturing)) {
                    allOrderItemsHaveWholesaleCost = false;
                    break;
                }
            }

            // Do a check and make sure all complete units have variants for Renewal by Andersen service POs.
            if (relatedOrderService == true && purchaseOrder.Vendor__r.Name == 'Renewal by Andersen') {
                for (OrderItem oi : purchaseOrder.Order_Products__r) {
                    if (oi.Pricebookentry.Product2.Name == 'Complete Unit' && oi.Variant_Number__c == null) {
                        returnResult += 'Please enter new Variant Number for Unit ' + oi.Unit_Id__c + './n';
                        completeUnitsHaveVariants = false;
                    }
                    else {
                        completeUnitsHaveVariants = true;
                    }
                }
            }

            if (allOrderItemsHaveWholesaleCost == true && completeUnitsHaveVariants == true) {
                purchaseOrder.Status__c = 'Confirmed';
                purchaseOrder.Confirmed_Timestamp__c = System.Now();
            }
*/

        List<OrderItem> updateOIList = new List<OrderItem>();
        for (OrderProductWrapper orderProduct : purchaseOrder.productList) {
            OrderItem updateOI = new OrderItem(Id = orderProduct.id,
                                               Unit_Wholesale_Cost__c = orderProduct.unitWholesaleCost,
                                               Variant_Number__c = orderProduct.variantNumber);
            updateOIList.add(UpdateOI);
        }

        Purchase_Order__c updatePurchaseOrder = new Purchase_Order__c(Id = purchaseOrder.id,
                                                                      Estimated_Ship_Date__c = purchaseOrder.estimatedShipDate,
                                                                      Status__c = 'Confirmed',
                                                                      Tax__c = purchaseOrder.tax,
                                                                      Confirmation_Number__c =  purchaseOrder.confirmationNumber,
                                                                      Confirmed_Timestamp__c = Datetime.now());

        try {

            if (!updateOIList.isEmpty()) {
                update updateOIList;
            }

            // Update the po with the new status.
            update updatePurchaseOrder;
            
            returnResult = 'Confirm PO Success';

        }
        catch (Exception ex) {
            System.debug('************ confirmPurchaseOrder Error: ' + ex);
            returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
        }

        return returnResult;

    }


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 02/25/2020
    * @description Wrapper Class for Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public Date estimatedShipDate {get;set;}

        @AuraEnabled
        public Boolean costPurchaseOrder {get;set;}

        @AuraEnabled
        public Boolean relatedOrderIsClosed {get;set;}

        @AuraEnabled
        public Boolean relatedOrderCORO {get;set;}

        @AuraEnabled
        public Boolean relatedOrderService {get;set;}

        @AuraEnabled
        public Boolean revRecNullAndValidStatus {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String storeLocationId {get;set;}

        @AuraEnabled
        public String storeLocationName {get;set;}

        @AuraEnabled
        public Decimal tax {get;set;}

        @AuraEnabled
        public String vendorName {get;set;}

        @AuraEnabled
        public Boolean zeroDollarVendor {get;set;}

        @AuraEnabled
        public List<OrderProductWrapper> productList {get;set;}

        @AuraEnabled
        public String confirmationNumber {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 02/26/2020
    * @description Wrapper Class for Purchase Order Products.
    */
    @TestVisible
    public class OrderProductWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String productName {get;set;}

        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public Decimal unitWholesaleCost {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}

        @AuraEnabled
        public String variantNumberOld {get;set;}

        

    }

}