public class  WindowLeadTimeController
{


@AuraEnabled(cacheable=true)
    public static   string   getRecordId() {
    string recordid;
    List<Lead_Time__c> leadtimeList = [select id from Lead_Time__c where Lead_Time_Active__c=true];
    
    if(leadtimeList.size()>0)
    {
    recordid=leadtimeList[0].id;
    
    }
    
    return recordid;
    }

}