global class CanvassUnitRecordCountSummationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful 
{
    //database.executebatch(new CanvassUnitRecordCountSummationBatch(),1);
    
    //CanvassUnitRecordCountSummationBatch rsb = new CanvassUnitRecordCountSummationBatch();
    //String sch = '0 0 * * * ?';
    //system.schedule('Canvass Unit Record Count Summation Batch 00', sch, rsb);
    
    //CanvassUnitRecordCountSummationBatch rsb = new CanvassUnitRecordCountSummationBatch();
    //String sch = '0 15 * * * ?';
    //system.schedule('Canvass Unit Record Count Summation Batch 15', sch, rsb);
    
    //CanvassUnitRecordCountSummationBatch rsb = new CanvassUnitRecordCountSummationBatch();
    //String sch = '0 30 * * * ?';
    //system.schedule('Canvass Unit Record Count Summation Batch 30', sch, rsb);
    
    //CanvassUnitRecordCountSummationBatch rsb = new CanvassUnitRecordCountSummationBatch();
    //String sch = '0 45 * * * ?';
    //system.schedule('Canvass Unit Record Count Summation Batch 45', sch, rsb);
    
    global string canvassMarketBatchJobId;
    global decimal NewRecordCountTotal;
    global decimal OldRecordCountTotal;
    global decimal OldRecordsNotUpdatedCountTotal;
    global decimal OldRecordsUpdatedCountTotal;
    
    global CanvassUnitRecordCountSummationBatch(string canvassMarketBatchId)
    {
        canvassMarketBatchJobId = canvassMarketBatchId;
        NewRecordCountTotal = 0;
        OldRecordCountTotal = 0;
        OldRecordsNotUpdatedCountTotal = 0;
        OldRecordsUpdatedCountTotal = 0;
    }

    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // query to get holding jobs from queue
        return Database.getQueryLocator ([SELECT Id, Name, New_Record_Count__c, Old_Record_Count__c, Old_Records_Not_Updated_Count__c, Old_Records_Updated_Count__c
                                          FROM Custom_Data_Layer_Batch_Queue__c 
                                          WHERE Canvass_Market_Batch_Job__c = :canvassMarketBatchJobId]);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Queue__c> scope)
    {
        try
        {
            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
            {
                NewRecordCountTotal = NewRecordCountTotal+queuedJob.New_Record_Count__c;
                OldRecordCountTotal = OldRecordCountTotal+queuedJob.Old_Record_Count__c;
                OldRecordsNotUpdatedCountTotal = OldRecordsNotUpdatedCountTotal+queuedJob.Old_Records_Not_Updated_Count__c;
                OldRecordsUpdatedCountTotal = OldRecordsUpdatedCountTotal+queuedJob.Old_Records_Updated_Count__c;
                
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Completed';
                update queuedJob;
            }
        }
        catch(Exception e)
        {
            system.debug('e****');
            system.debug(e);
            system.debug(e.getLineNumber());
            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
            {
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Failed';
            }
            
            update scope;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        List<Canvass_Market_Batch_Job__c> canvassMarketBatchJobList = [SELECT Id, Name, New_Record_Count__c, Old_Record_Count__c, Old_Records_Not_Updated_Count__c, Old_Records_Updated_Count__c, Status__c, User__r.Email
                                                                       FROM Canvass_Market_Batch_Job__c
                                                                       WHERE Id = :canvassMarketBatchJobId];
        
        boolean sendEmail = false;
        string userEmail = null;                                                               
        for(Canvass_Market_Batch_Job__c canvassMarketJob :canvassMarketBatchJobList)
        {
            canvassMarketJob.Status__c = 'Completed';
            canvassMarketJob.New_Record_Count__c = NewRecordCountTotal;
            canvassMarketJob.Old_Record_Count__c = OldRecordCountTotal;
            canvassMarketJob.Old_Records_Not_Updated_Count__c = OldRecordsNotUpdatedCountTotal;
            canvassMarketJob.Old_Records_Updated_Count__c =OldRecordsUpdatedCountTotal;
            
            if(canvassMarketJob.User__r.Email != null)
            {
                sendEmail = true;
                userEmail = canvassMarketJob.User__r.Email;
            }
        }
        
        update canvassMarketBatchJobList;
        
        if(sendEmail == true)
        {
            sendClientEmail(userEmail);
        }
    }
    
    // used to send email to client
    global void sendClientEmail(string userEmail)
    {
        list<string> emailAddresses = new list<string>();
        emailAddresses.add(userEmail);

        string resBody = 'Canvass Unit Batch Job Has Finished Running';
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.ToAddresses = EmailAddresses;
        message.Subject = 'Canvass Unit Batch Job Has Finished Running';
        message.htmlBody = resBody;
            
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] rslts = Messaging.sendEmail(messages);
            
        if (rslts[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + rslts[0].errors[0].message);
        }
    }
}