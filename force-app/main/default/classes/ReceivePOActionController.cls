/*
* @author Jason Flippen
* @date 02/28/2020 
* @description Class to provide functionality for the receivePurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - ReceivePOActionControllerTest
*/
public with sharing class ReceivePOActionController {

    /*
    * @author Jason Flippen
    * @date 02/28/2020
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Estimated_Ship_Date__c,
                                                  Name,
                                                  Order__c,
                                                  Order__r.AccountId,
                                                  Order__r.BillToContactId,
                                                  Order__r.EffectiveDate,
                                                  Order__r.Pricebook2Id,
                                                  Order__r.RecordTypeId,
                                                  Order__r.RecordType.DeveloperName,
                                                  Order__r.Revenue_Recognized_Date__c,
                                                  Order__r.Status,
                                                  Processing_Type__c,
                                                  RecordTypeId,
                                                  RecordType.DeveloperName,
                                                  Status__c,
                                                  Store_Location__c,
                                                  Store_Location__r.Name,
                                                  Subtotal__c,
                                                  Tax__c,
                                                  Vendor__c,
                                                  Vendor__r.Name,
                                                  Vendor__r.Over_Receiving_Allowed__c,
                                                  Vendor__r.Zero_Dollar_Vendor__c,
                                                  (
                                                      SELECT Id,
                                                             Pricebookentry.Product2Id,
                                                             Pricebookentry.Product2.Name,
                                                             Charge_Cost_To__c,
                                                             Date_Received__c,
                                                             Date_Vendor_Credit__c,
                                                             Date_Written_Off__c,
                                                             Defect_Code__c,
                                                             Description,
                                                             Desired_width__c,
                                                             Discount_Amount__c,
                                                             Installed_Product_Asset__c,
                                                             Quantity,
                                                             Quanity_Ordered__c,
                                                             Quantity_To_Receive__c,
                                                             Quantity_To_Write_Off__c,
                                                             Sold_Order_Product_Asset__c,
                                                             Sold_Order_Product_Asset__r.Product2Id,
                                                             Sold_Order_Product_Asset__r.Product_Name__c,
                                                             Sold_Order_Product_Asset__r.Status,
                                                             Total_Retail_Price__c,
                                                             Unit_Id__c,
                                                             Unit_Wholesale_Cost__c,
                                                             Variant_Number__c,
                                                             Vendor_Credit_To_Receive__c
                                                      FROM   Order_Products__r
                                                      ORDER BY Pricebookentry.Product2.Name ASC
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.estimatedShipDate = purchaseOrder.Estimated_Ship_Date__c;
        wrapper.name = purchaseOrder.Name;
        wrapper.orderId = purchaseOrder.Order__c;
        wrapper.orderAccountId = purchaseOrder.Order__r.AccountId;
        wrapper.orderBillToContactId = purchaseOrder.Order__r.BillToContactId;
        wrapper.orderEffectiveDate = purchaseOrder.Order__r.EffectiveDate;
        wrapper.processingType = purchaseOrder.Processing_Type__c;
        wrapper.status = purchaseOrder.Status__c;
        wrapper.subtotal = purchaseOrder.Subtotal__c;
        wrapper.tax = purchaseOrder.Tax__c;

        String vendorId = '';
        String vendorName = '';
        Boolean overReceivingAllowed = false;
        Boolean zeroDollarVendor = false;
        if (purchaseOrder.Vendor__c != null) {
            vendorId = purchaseOrder.Vendor__c;
            vendorName = purchaseOrder.Vendor__r.Name;
            overReceivingAllowed = purchaseOrder.Vendor__r.Over_Receiving_Allowed__c;
            zeroDollarVendor = purchaseOrder.Vendor__r.Zero_Dollar_Vendor__c;
        }
        wrapper.vendorId = vendorId;
        wrapper.vendorName = vendorName;
        wrapper.vendorOverReceivingAllowed = overReceivingAllowed;
        wrapper.zeroDollarVendor = zeroDollarVendor;

        Boolean costPurchaseOrder = false;
        if (purchaseOrder.RecordType.DeveloperName == 'Cost_Purchase_Order') {
            costPurchaseOrder = true;
        }
        wrapper.costPurchaseOrder = costPurchaseOrder;

/*
        Boolean purchaseOrderComplete = false;
        if (po.Status__c == 'Received' &&
            po.Received_Timestamp__c != null &&
            po.Received_Timestamp__c.date() != Date.today()) {
            purchaseOrderComplete = true;
        }
        wrapper.purchaseOrderComplete = purchaseOrderComplete;
*/

        Boolean relatedOrderCORO = false;
        Boolean relatedOrderService = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            relatedOrderCORO = true;
        }
        else if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            relatedOrderService = true;
        }
        wrapper.relatedOrderCORO = relatedOrderCORO;
        wrapper.relatedOrderService = relatedOrderService;

        Boolean relatedOrderIsClosed = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.Status == 'Job Closed') {
            relatedOrderIsClosed = true;
        }
        wrapper.relatedOrderIsClosed = relatedOrderIsClosed;

        // If the related Order is a Purchase Order, make sure the Revenue Recognized
        // Date on the Order is null, and the Order Status is a valid Status.
        Boolean revRecNullAndValidStatus = false;
        Set<String> validStatusSet = new Set<String>{'Job in Progress','Install Complete','Install Needed','Install Scheduled','Order Released','Ready to Order'};
        if (relatedOrderCORO == true && purchaseOrder.Order__c != null &&
            purchaseOrder.Order__r.Revenue_Recognized_Date__c == null &&
            validStatusSet.contains(purchaseOrder.Order__r.Status)) {
            revRecNullAndValidStatus = true;
        }
        wrapper.revRecNullAndValidStatus = revRecNullAndValidStatus;

        String storeLocationId = '';
        String storeLocationName = '';
        if (purchaseOrder.Store_Location__c != null) {
            storeLocationId = purchaseOrder.Store_Location__c;
            storeLocationName = purchaseOrder.Store_Location__r.Name;
        }
        wrapper.storeLocationId = storeLocationId;
        wrapper.storeLocationName = storeLocationName;

        wrapper.productList = new List<OrderProductWrapper>();
        for (OrderItem oi : purchaseOrder.Order_Products__r) {

            if (oi.Quantity_To_Receive__c == null || oi.Quantity_To_Receive__c != oi.Quanity_Ordered__c) {
                OrderProductWrapper productWrapper = new OrderProductWrapper();
                productWrapper.id = oi.Id;
                productWrapper.productId = oi.Pricebookentry.Product2Id;
                productWrapper.productName = oi.Pricebookentry.Product2.Name;
                productWrapper.chargeCostTo = oi.Charge_Cost_To__c;
                productWrapper.dateReceived = oi.Date_Received__c;
                productWrapper.dateVendorCredit = oi.Date_Vendor_Credit__c;
                productWrapper.dateWrittenOff = oi.Date_Written_Off__c;
                productWrapper.defectCode = oi.Defect_Code__c;
                productWrapper.description = oi.Description;
                productWrapper.desiredWidth = oi.Desired_width__c;
                productWrapper.discountAmount = oi.Discount_Amount__c;
                productWrapper.installedProductAssetId = oi.Installed_Product_Asset__c;

                Decimal quantity = 0;
                if (oi.Quantity != null) {
                    quantity = oi.Quantity;
                }
                productWrapper.quantity = quantity;
/*
                Decimal quantityOrdered = 0;
                if (oi.Quanity_Ordered__c != null) {
                    quantityOrdered = oi.Quanity_Ordered__c;  // Quantity is misspelled in the object
                }
                productWrapper.quantityOrdered = quantityOrdered;
*/
                
                Decimal quantityToReceive = quantity;
                if (oi.Quantity_To_Receive__c != null && oi.Quantity_To_Receive__c != quantity) {
                    quantityToReceive = oi.Quantity_To_Receive__c;
                }
                productWrapper.quantityToReceive = quantityToReceive;

//                productWrapper.quantityToWriteOff = oi.Quantity_To_Write_Off__c;
                productWrapper.soldOrderProductAssetId = oi.Sold_Order_Product_Asset__c;

                String soldOrderProductAssetProdId = '';
                String soldOrderProductAssetProdName = '';
                String soldOrderProductAssetStatus = '';
                if (oi.Sold_Order_Product_Asset__c != null) {
                    soldOrderProductAssetProdId = oi.Sold_Order_Product_Asset__r.Product2Id;
                    soldOrderProductAssetProdName = oi.Sold_Order_Product_Asset__r.Product_Name__c;
                    soldOrderProductAssetStatus = oi.Sold_Order_Product_Asset__r.Status;
                }
                productWrapper.soldOrderProductAssetProdName = soldOrderProductAssetProdName;

                productWrapper.totalRetailPrice = oi.Total_Retail_Price__c;
                productWrapper.unitId = oi.Unit_Id__c;
                productWrapper.unitWholesaleCost = oi.Unit_Wholesale_Cost__c;
                productWrapper.variantNumber = oi.Variant_Number__c;
                productWrapper.vendorCreditToReceive = oi.Vendor_Credit_To_Receive__c;
                wrapper.productList.add(productWrapper);
            }

        }
        
        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 01/23/2020
    * @description Method to update the Purchase Order Status to "Received"
    * @param Purchase Order (Wrapper)
    * @returns String identifying whether or not the update was successful
    */
    @AuraEnabled
    public static String receivePurchaseOrder(PurchaseOrderWrapper purchaseOrder) {
        
        String returnResult = null;

        // Create a savepoint in prior to any updates/inserts in case we need to rollback.
        Savepoint savePoint = Database.setSavepoint();

        // Determine which "Receive" Status should be applied to the Purchase Order.
        String receivedStatus = 'Received';
        for (OrderProductWrapper orderProduct : purchaseOrder.productList) {
            if (orderProduct.quantityToReceive < orderProduct.quantity) {
                receivedStatus = 'Partially Received';
                break;
            }
        }

        // Update the Purchase Order values.
        Purchase_Order__c updatePurchaseOrder = new Purchase_Order__c(Id = purchaseOrder.id,
                                                                      Status__c = receivedStatus,
                                                                      Received_Timestamp__c = Datetime.now());

        // Get the List of OrderItems (aka Order Products) to be updated.
        List<OrderItem> updateOIList = receiveOrderItems(purchaseOrder, receivedStatus);

        try {

            // Get the updated Subtotal.
            updatePurchaseOrder.Subtotal__c = getUpdatedSubTotal(updateOIList);

            // If we have OrderItems (aka Order Products) to update, update them.
            if (!updateOIList.isEmpty()) {
                // Update the Tax for each OrderItem (if applicable).
                updateOIList = getUpdatedOrderItems(updateOIList, updatePurchaseOrder.Subtotal__c, purchaseOrder.tax);
                update updateOIList;
            }

            // Finally, let's update the Purchase Order.
            update updatePurchaseOrder;
            
            returnResult = 'Receive PO Success';
            if (receivedStatus == 'Partially Received') {
                returnResult = 'Partial Receive PO Success';
            }

        }
        catch (Exception ex) {
            System.debug('************ receivePurchaseOrder Error: ' + ex);
            // Rollback the updates/inserts.
            Database.rollback(savePoint);
            returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
        }

        return returnResult;
        
    }

    /*
    * @author Jason Flippen
    * @date 01/23/2020
    * @description Method to update the Products related to the Purchase Order.
    * @param Purchase Order (Wrapper)
    * @returns OrderItem List
    */
    private static List<OrderItem> receiveOrderItems(PurchaseOrderWrapper purchaseOrder, String receivedStatus) {

        List<OrderItem> updateOIList = new List<OrderItem>();

        Set<Id> assetIdSet = getAssetIdSet(purchaseOrder);
        Map<Id,Asset> assetMap = getAssetMap(assetIdSet);
        Map<Id,Received_Product__c> assetReceivedProdMap = getAssetReceivedProdMap(assetIdSet);

        // Build a Map of Assets to be upserted.
        Map<Id,Asset> upsertAssetMap = new Map<Id,Asset>();
        Map<Id,Asset> orderAssetMap = new Map<Id,Asset>();
        for (OrderProductWrapper orderProduct : purchaseOrder.productList) {

            // Is this Product being "received"? -- It will have a value in it Quantity_to_Receive__c field if it is.
            if (orderProduct.quantityToReceive != null) {

                Asset productAsset = null;

                // Is this Product associated to an Asset?
                if (String.isBlank(orderProduct.installedProductAssetId)) {

                    // Get the appropriate Record Type (Id) for the new Asset record.
                    Id assetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Installed_Products').getRecordTypeId();
                    if (purchaseOrder.costPurchaseOrder == true) {
                        assetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Cost_Products').getRecordTypeId();
                    }
                    else if (purchaseOrder.relatedOrderService == true) {
                        assetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Service_Products').getRecordTypeId();
                    }

                    // Create a new instance of an Asset record.
                    productAsset = new Asset(AccountId = purchaseOrder.orderAccountId,
                                             ContactId = purchaseOrder.orderBillToContactId,
                                             Name = orderProduct.productName,
                                             Original_Order_Product__c = orderProduct.id,
                                             Price = orderProduct.totalRetailPrice,
                                             Product2Id = orderProduct.productId,
                                             PurchaseDate = purchaseOrder.orderEffectiveDate,
                                             Received_Purchase_Order__c = purchaseOrder.name,
                                             RecordTypeId = assetRecordTypeId,
                                             SerialNumber = orderProduct.variantNumber,
                                             Sold_Order__c = purchaseOrder.orderId,
                                             Store_Location__c = purchaseOrder.storeLocationId,
                                             Unit_Wholesale_Cost__c = orderProduct.unitWholesaleCost,
                                             Variant_Number__c = orderProduct.variantNumber,
                                             Vendor__c = purchaseOrder.vendorId);

                    // If this is a "Service" PO and it's a complete unit
                    // link the new asset to the original asset.
                    if (purchaseOrder.relatedOrderService == true &&
                        orderProduct.productName.contains('Complete Unit') &&
                        String.isNotBlank(orderProduct.soldOrderProductAssetId)) {

                        productAsset.Original_Asset__c = orderProduct.soldOrderProductAssetId;
                        productAsset.Name = orderProduct.soldOrderProductAssetProdName;
                        productAsset.Product2Id = orderProduct.soldOrderProductAssetProdId;
                        
                    }

                    // If this is a Cost PO, set the Store Location and Purchase Date.
                    if (purchaseOrder.costPurchaseOrder) {
                        productAsset.Store_Location__c = purchaseOrder.storeLocationId;
                        productAsset.PurchaseDate = Date.today();
                    }
                    
                }
                else if (String.isNotBlank(orderProduct.installedProductAssetId)) {

                    // This Product is associated to an Asset, so let's grab it from our Asset Map.
                    productAsset = assetMap.get(orderProduct.installedProductAssetId);

                }

                // Make some additional updates to the new/existing Asset record related to this Product.
                productAsset.Date_Received__c = Date.today();
                productAsset.Status = receivedStatus;

                if (productAsset.Quantity == null) {
                    productAsset.Quantity = orderProduct.quantityToReceive;
                }
                else {
                    productAsset.Quantity = (orderProduct.quantityToReceive + productAsset.Quantity);
                }
                
                // If this Product has already been Received today subtract the previous received quantity.
                if (!assetReceivedProdMap.isEmpty() && assetReceivedProdMap.containsKey(orderProduct.installedProductAssetId)) {
                    productAsset.Quantity = (productAsset.Quantity - assetReceivedProdMap.get(orderProduct.installedProductAssetId).Quantity__c);
                }
                
                // Update our Map of Assets to be upserted.
                if (upsertAssetMap.containsKey(orderProduct.id)) {

                    Asset currentAsset = upsertAssetMap.get(orderProduct.id);

                    // JETT 2083 set Asset Received Date to Today   
                    currentAsset.Date_Received__c = Date.today(); 
                    
                    if (currentAsset.Quantity == null) {
                        currentAsset.Quantity = orderProduct.quantityToReceive;
                    }
                    else {
                        currentAsset.Quantity = (orderProduct.quantityToReceive + currentAsset.Quantity);
                    }
                
                    // If this Product has already been Received today subtract the previous received quantity.
                    if (!assetReceivedProdMap.isEmpty() && assetReceivedProdMap.containsKey(orderProduct.installedProductAssetId)) {
                        currentAsset.Quantity = (currentAsset.Quantity - assetReceivedProdMap.get(orderProduct.installedProductAssetId).Quantity__c);
                    }
                    
                    // Add Asset to our Map of Assets to be upserted.
                    upsertAssetMap.put(orderProduct.id,currentAsset);

                }
                else {

                    // Add Asset to our Map of Assets to be upserted.
                    upsertAssetMap.put(orderProduct.id,productAsset);
                }
                
                // Add the new/updated Asset to our master Order-to-Asset Map. This will
                // be used to retreive the Id from the Asset records being inserted.
                orderAssetMap.put(orderProduct.id,productAsset);

            }

        }

        try {

            // If we have Asset records to upsert, upsert them.
            if (!upsertAssetMap.isEmpty()) {
                upsert upsertAssetMap.values();
            }

            // Get the List of Received Product records to be upserted.
            List<Received_Product__c> upsertReceivedProdList = new List<Received_Product__c>();
            for (OrderProductWrapper orderProduct : purchaseOrder.productList) {

                if (orderProduct.quantityToReceive != null) {

                    Id assetId = orderAssetMap.get(orderProduct.id).Id;

                    // Does a Received Product record already exist for this Asset?
                    if (!assetReceivedProdMap.isEmpty() && assetReceivedProdMap.containsKey(assetId)) {

                        // Update existing Received Product record.
                        Received_Product__c receivedProduct = assetReceivedProdMap.get(assetId);
                        receivedProduct.Quantity__c = orderProduct.quantityToReceive;
                        receivedProduct.Amount__c = orderProduct.unitWholesaleCost;
                        receivedProduct.Date__c = Date.today();
                        upsertReceivedProdList.add(receivedProduct);

                    }
                    else {

                        // Create a new received product record.
                        Received_Product__c receivedProduct = new Received_Product__c(Asset__c = assetId,
                                                                                      Quantity__c = orderProduct.quantityToReceive,
                                                                                      Amount__c = orderProduct.unitWholesaleCost,
                                                                                      Date__c = Date.today());
                        upsertReceivedProdList.add(receivedProduct);

                    }

                    // Update the OrderItem (aka Order Product) and add it to our "Update" List.
                    OrderItem updateOI = new OrderItem(Id = orderProduct.id,
                                                       Date_Received__c = Date.today(),
                                                       Discount_Amount__c = orderProduct.discountAmount,
                                                       Installed_Product_Asset__c = assetId,
                                                       Quantity = orderProduct.quantity,
                                                       Quantity_To_Receive__c = orderProduct.quantityToReceive,
                                                       Unit_Wholesale_Cost__c = orderProduct.unitWholesaleCost);
                    updateOIList.add(updateOI);

                }

            }

            // If we have Received Products to upsert, upsert them.
            if (!upsertReceivedProdList.isEmpty()) {
                upsert upsertReceivedProdList;
            }

        }
        catch (Exception ex) {
            System.debug('************ receiveOrderItems Error: ' + ex);
            updateOIList = null;
        }

        return updateOIList;

    }

    /*
    * @author Jason Flippen
    * @date 03/09/2020
    * @description Method to return a Set of Asset Ids from Products.
    * @param PurchaseOrderWrapper
    * @returns Set of Asset Ids
    */
    private static Set<Id> getAssetIdSet (PurchaseOrderWrapper purchaseOrder) {

        Set<Id> assetIdSet = new Set<Id>();

        for (OrderProductWrapper orderProduct : purchaseOrder.productList) {
            assetIdSet.add(orderProduct.installedProductAssetId);
        }

        return assetIdSet;

    }

    /*
    * @author Jason Flippen
    * @date 03/09/2020
    * @description Method to return a Map of Assets.
    * @param Set of Asset Ids
    * @returns Map of Assets
    */
    private static Map<Id,Asset> getAssetMap(Set<Id> assetIdSet) {

        Map<Id,Asset> assetMap = new Map<Id,Asset>();

        // Make sure we have a Set of Asset Ids before querying for records.
        if (!assetIdSet.isEmpty()) {

            assetMap = new Map<Id,Asset>([SELECT Id,
                                                 Quantity
                                          FROM   Asset
                                          WHERE  Id IN :assetIdSet]);

        }

        return assetMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/09/2020
    * @description Method to return an Asset-to-Received Product Map.
    * @param Set of Asset Ids
    * @returns Asset-to-Received Product Map
    */
    private static Map<Id,Received_Product__c> getAssetReceivedProdMap(Set<Id> assetIdSet) {

        Map<Id,Received_Product__c> assetReceivedProdMap = new Map<Id,Received_Product__c>();

        // If we have Assets, build an Asset-to-Received Product
        // Map for the Products that were received today.
        if (!assetIdSet.isEmpty()) {

            for (Received_Product__c rp: [SELECT Id,
                                                 Quantity__c,
                                                 Amount__c,
                                                 Date__c,
                                                 Asset__c
                                          FROM   Received_Product__c
                                          WHERE  Asset__c IN :assetIdSet
                                          AND    Date__c = TODAY]) {
                assetReceivedProdMap.put(rp.Asset__c, rp);
            }

        }

        return assetReceivedProdMap;

    }

    /*
    * @author Jason Flippen
    * @date 03/09/2020
    * @description Method to update the Tax on each OrderItem record (if applicable).
    * @param OrderItem List; Purchase Order Subtotal; Purchase Order Tax
    * @returns List of OrderItem records
    */
    private static List<OrderItem> getUpdatedOrderItems(List<OrderItem> orderItemList, Decimal purchaseOrderSubtotal, Decimal purchaseOrderTax) {

        List<OrderItem> updatedOIList = new List<OrderItem>();

        if (purchaseOrderSubtotal != null && purchaseOrderSubtotal > 0 &&
            purchaseOrderTax != null && purchaseOrderTax > 0) {
            
            // Grab the List of OrderItems that have a Unit Wholesale Cost value greater than 0.
            Set<Id> eligibleOrderItemIdSet = new Set<Id>();
            for (OrderItem oi : orderItemList) {
                if (oi.Unit_Wholesale_Cost__c != null && oi.Unit_Wholesale_Cost__c > 0) {
                    eligibleOrderItemIdSet.add(oi.Id);
                }
            }

            Decimal remainingTax = purchaseOrderTax;

            Integer item = 1;
            for (OrderItem oi : orderItemList) {

                if (eligibleOrderItemIdSet.contains(oi.Id)) {

                    if (item == eligibleOrderItemIdSet.size()) {
                        oi.Tax__c = remainingTax;
                    }
                    else {
                        oi.Tax__c = purchaseOrderTax * ((oi.Unit_Wholesale_Cost__c * oi.Quantity) / purchaseOrderSubtotal);
                    }

                    item++;
                    remainingTax -= oi.Tax__c;

                }

                updatedOIList.add(oi);

            }

        }
        else {
            updatedOIList = orderItemList;
        }

        return updatedOIList;
        
    }

    /*
    * @author Jason Flippen
    * @date 01/23/2020
    * @description Method to calculate the Subtotal for a Purchase Order.
    * @param OrderItem List
    * @returns Subtotal (Decimal)
    */
    private static Decimal getUpdatedSubTotal(List<OrderItem> orderItemList) {

        Decimal subtotal = 0;

        for (OrderItem oi : orderItemList) {
            
            if (oi.Unit_Wholesale_Cost__c != null || oi.Quantity != null) {
                Decimal itemDiscount = 0;
                if (oi.Discount_Amount__c != null) {
                    itemDiscount = oi.Discount_Amount__c;
                }
                subtotal += ((oi.Unit_Wholesale_Cost__c - itemDiscount) * oi.Quantity);
            }

        }

        return subtotal;
        
    }


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 02/28/2020
    * @description Wrapper Class for Purchase Order
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String name {get;set;}

        @AuraEnabled
        public Date estimatedShipDate {get;set;}

        @AuraEnabled
        public Boolean costPurchaseOrder {get;set;}

        @AuraEnabled
        public String orderId {get;set;}

        @AuraEnabled
        public String orderAccountId {get;set;}

        @AuraEnabled
        public String orderBillToContactId {get;set;}

        @AuraEnabled
        public Date orderEffectiveDate {get;set;}

        @AuraEnabled
        public String processingType {get;set;}

        @AuraEnabled
        public Boolean relatedOrderIsClosed {get;set;}

        @AuraEnabled
        public Boolean relatedOrderCORO {get;set;}

        @AuraEnabled
        public Boolean relatedOrderService {get;set;}

        @AuraEnabled
        public Boolean revRecNullAndValidStatus {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String storeLocationId {get;set;}

        @AuraEnabled
        public String storeLocationName {get;set;}

        @AuraEnabled
        public Decimal subtotal {get;set;}

        @AuraEnabled
        public Decimal tax {get;set;}

        @AuraEnabled
        public String vendorId {get;set;}

        @AuraEnabled
        public String vendorName {get;set;}

        @AuraEnabled
        public Boolean vendorOverReceivingAllowed {get;set;}

        @AuraEnabled
        public Boolean zeroDollarVendor {get;set;}

        @AuraEnabled
        public List<OrderProductWrapper> productList {get;set;}

    }

    /*
    * @author Jason Flippen
    * @date 02/28/2020
    * @description Wrapper Class for Purchase Order Products.
    */
    @TestVisible
    public class OrderProductWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String productId {get;set;}

        @AuraEnabled
        public String productName {get;set;}

        @AuraEnabled
        public String chargeCostTo {get;set;}

        @AuraEnabled
        public Date dateReceived {get;set;}

        @AuraEnabled
        public Date dateVendorCredit {get;set;}

        @AuraEnabled
        public Date dateWrittenOff {get;set;}

        @AuraEnabled
        public String defectCode {get;set;}

        @AuraEnabled
        public String description {get;set;}

        @AuraEnabled
        public Decimal desiredWidth {get;set;}

        @AuraEnabled
        public Decimal discountAmount {get;set;}

        @AuraEnabled
        public String installedProductAssetId {get;set;}

        @AuraEnabled
        public Decimal quantity {get;set;}

        @AuraEnabled
        public Decimal quantityOrdered {get;set;}

        @AuraEnabled
        public Decimal quantityToReceive {get;set;}

//        @AuraEnabled
//        public Integer quantityToWriteOff {get;set;}

        @AuraEnabled
        public String soldOrderProductAssetId {get;set;}

        @AuraEnabled
        public String soldOrderProductAssetProdId {get;set;}

        @AuraEnabled
        public String soldOrderProductAssetProdName {get;set;}

        @AuraEnabled
        public String soldOrderProductAssetStatus {get;set;}

        @AuraEnabled
        public Decimal totalRetailPrice {get;set;}

        @AuraEnabled
        public String unitId {get;set;}

        @AuraEnabled
        public Decimal unitWholesaleCost {get;set;}

        @AuraEnabled
        public String variantNumber {get;set;}

        @AuraEnabled
        public Decimal vendorCreditToReceive {get;set;}

    }

}