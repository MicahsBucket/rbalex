@isTest
private class CustomerHelpControllerTest {
    @testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord2 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        Order ord3 = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = date.today();
        ord2.Order_Processed_Date__c = date.today().addDays(-1);
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1';
        Database.insert(currentUser);
        ord.CustomerPortalUser__c = currentUser.Id;
        ord2.CustomerPortalUser__c = currentUser.Id;
        Store_Configuration__c s = new Store_Configuration__c();
        s.Store__c = storeAccount.Id;
        s.EmailToCustomers__c = true;
        s.Order_Number__c = 0001;
        Insert s;
        List<Order> ordList = new List<Order>{ord, ord2, ord3};
            Database.update(ordList);
    }

    @isTest
    static void submitContactTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
		list<order> ords = [select id from order ];
		string ordId = ords[0].id;
        CustomerHelpController.submitContact('Hello sundeep How are you !..', 'sundeep@gmail.com',ordId, 'WebsiteFeedBack');
            System.assert(ordId!=Null);
        }
    }
    @isTest
    static void getAllOrdersTest() {
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
		CustomerHelpController.FAQWrap orders = CustomerHelpController.getAllOrders();
            System.assert(orders!=Null);
        }
    }

    @isTest
    static void getContactInfoTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
            list<order> ords = [select id from order ];
            string ordId = ords[0].id;
			Contact cont = CustomerHelpController.getContactInfo(true, ordId);
		System.assert(cont!=null);
        }
    }
    
     @isTest
    static void getStoreConfigInfoTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1'];
        System.runAs(currentUser){
			Store_Configuration__c s = CustomerHelpController.getStoreConfigInfo(null);
            System.assertEquals(s.EmailToCustomers__c, false);
        }
    }
 }