/*
* @author Jason Flippen
* @date 06/22/2020 
* @description Test Class provides code coverage for the following classes:
*               - NewCostPOActionController
*/
@isTest
public class NewCostPOActionControllerTest {

    /*
    * @author Jason Flippen
    * @date 06/22/2020 
    * @description Method to create data to be consumed by test methods.
    */ 
    @testSetup static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVendorAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        Order testOrder =  new Order(Name = 'Dummy Cost PO Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     AccountId = testDwellingAcct.Id,
                                     Cancellation_Reason__c = 'Unknown',
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     Status = 'Cancelled',
                                     Store_Number__c = '0077',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;

        RMS_Settings__c testCustomSetting = new RMS_Settings__c(Name = 'Dummy Order Id',
                                                                Value__c = testOrder.Id);
        insert testCustomSetting;

    }
        
    /*
    * @author Jason Flippen
    * @date 06/22/2020 
    * @description Method to test the functionality in the Controller.
    */ 
    private static testMethod void testController() {

        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Dummy Cost PO Order'];
        Account testVendorAcct = [SELECT Id FROM Account WHERE Name = 'Test Vendor AccountTest Account'];

        Test.startTest();

            Purchase_Order__c newCostPO = NewCostPOActionController.getNewCostPOData();
            newCostPO.Vendor__c = testVendorAcct.Id;
            Map<String,String> costPOResultMap = NewCostPOActionController.createCostPO(newCostPO);
            System.assertEquals(true, costPOResultMap.containsKey('New Cost PO Success'), 'Unexpected New Cost PO Result');

        Test.stopTest();

    }

}