/**
* @author Jason Flippen
* @date 01/29/2021
* @description Class to provide functionality for the newWorkOrderRedirect LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - NewWorkOrderRedirectControllerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class NewWorkOrderRedirectController {

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description: Method to retrieve Order data.
    * @param orderId
    * @return OrderWrapper
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled(cacheable=true)
    public static Order getOrderData(String orderId) {

        // Retrieve Order data.
        Order order = [SELECT Id,
                              AccountId,
                              Status,
                              RecordTypeId
                       FROM   Order
                       WHERE  Id = :orderId];        
        
        System.debug('***** order: ' + order);
                                                       
        return order;

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description: Method to retrieve a List of (wrapped) Work
    *               Order Record Types available for an Order.
    * @param order
    * @return List<WorkOrderTypeWrapper>
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static List<WorkOrderRecordTypeWrapper> getWorkOrderRecordTypeList(Order order) {

        List<WorkOrderRecordTypeWrapper> wrapperList = new List<WorkOrderRecordTypeWrapper>();

        // Add default (blank) value to the Wrapper List.
        WorkOrderRecordTypeWrapper wrapper = new WorkOrderRecordTypeWrapper();
        wrapper.label = '---None---';
        wrapper.value = '';
        wrapperList.add(wrapper);

        // Grab eligible Order Record Types and create Sets of
        // the Work Order Record Type Names to be returned.
        Id orderChangeOrderRTId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId();
        Set<String> changeOrderWORTNameSet = new Set<String>{'Collections',
                                                             'HOA',
                                                             'Historical',
                                                             'Install',
                                                             'Job Site Visit',
                                                             'LSWP',
                                                             'Paint/Stain',
                                                             'Permit',
                                                             'Tech Measure'};
        
        Id orderCORORTId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId();
        Set<String> coroWORTNameSet = new Set<String>{'Collections',
                                                      'HOA',
                                                      'Historical',
                                                      'Install',
                                                      'Job Site Visit',
                                                      'LSWP',
                                                      'Paint/Stain',
                                                      'Permit',
                                                      'Tech Measure'};

        Id orderCOROServiceRTId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId();
        Set<String> coroServiceWORTNameSet = new Set<String>{'Collections',
                                                             'HOA',
                                                             'Historical',
                                                             'Install',
                                                             'Job Site Visit',
                                                             'LSWP',
                                                             'Paint/Stain',
                                                             'Permit',
                                                             'Service'};

        Map<String,Schema.RecordTypeInfo> workOrderRecordTypeMap = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName();

        // Iterate through the Work Order Record Types and add
        // the appropriate ones to our (wrapper) List.
        for (String workOrderRTName : workOrderRecordTypeMap.keySet()) {

            // Grab the actual Id for this Record Type.
            Id workOrderRecordTypeId = workOrderRecordTypeMap.get(workOrderRTName).getRecordTypeId();
            
            Boolean addToList = false;
            wrapper = new WorkOrderRecordTypeWrapper();

            // Only certain Work Order Record Types are
            // available for certain Order Record Types.
            if (order.RecordTypeId == orderChangeOrderRTId) {

                // Change Order (Order)

                if (changeOrderWORTNameSet.contains(workOrderRTName)) {
                    wrapper.label = workOrderRTName;
                    wrapper.value = workOrderRecordTypeId;
                    addToList = true;
                }

            }
            else if (order.RecordTypeId == orderCORORTId) {

                // CORO Record Type (Order)

                if (coroWORTNameSet.contains(workOrderRTName)) {
                    wrapper.label = workOrderRTName;
                    wrapper.value = workOrderRecordTypeId;
                    addToList = true;
                }
                
            }
            else if (order.RecordTypeId == orderCOROServiceRTId) {

                // CORO Service (Order)

                if (coroServiceWORTNameSet.contains(workOrderRTName)) {
                    wrapper.label = workOrderRTName;
                    wrapper.value = workOrderRecordTypeId;
                    addToList = true;
                }
                
            }

            // Are we adding this wrapper record to our List?
            if (addToList == true) {
                wrapperList.add(wrapper);
            }

        }
        System.debug('***** wrapperList: ' + wrapperList);

        return wrapperList;

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description: Method to create a new Work Order.
    * @param order
    * @return saveResultMap
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static Map<String,String> createWorkOrder(Order order, String workOrderRecordTypeId) {

        Map<String,String> saveResultMap = new Map<String,String>();

        try {

            WorkOrder newWorkOrder = new WorkOrder(RecordTypeId = workOrderRecordTypeId,
                                                   AccountId = order.AccountId,
                                                   Sold_Order__c = order.Id);

            insert newWorkOrder;
            saveResultMap.put('New Work Order Success', newWorkOrder.Id);

        }
        catch (Exception ex) {
            saveResultMap.put(ex.getMessage(), '');
        }

        return saveResultMap;

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Wrapper Class for Work Order Record Types to be used in a Combobox.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @TestVisible
    public class WorkOrderRecordTypeWrapper {

        @AuraEnabled
        public String label {get;set;}

        @AuraEnabled
        public String value {get;set;}

    }

}