@isTest
private class CustomerOrderWorkers_UnitTest {
     private static final String RESOURCE_NAME = 'Resource';
    private static final String ACCOUNT_NAME = 'Test Account';
     private static final String RESOURCE1_FIRST_NAME = 'Bert';
    private static final String RESOURCE1_LAST_NAME = 'Knerkerts';
        private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();


       @testSetup
    static void setup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
         Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;
         User admin = new User(
            FirstName = 'Test',
            LastName = 'Admin',
            Email = 'test.admin@example.com',
            UserName = 'test.admin@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        System.runAs(admin) {
            
            User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
        insert user1;
          Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1_Test';
        currentUser.Added_to_Public_Groups__c = true;
        Database.insert(currentUser);  
           Account acc = utility.createDwellingAccount('Dwelling Account');
             
        Account storeAcc = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :storeAcc.id ];
        storeConfig1.Sales_Tax__c = 5;
        update storeConfig1;
        acc.Store_Location__c = storeAcc.Id;
            insert acc;
            Resource__c resource = new Resource__c(RbA_User__c=user1.Id, Name = RESOURCE_NAME, Resource_Type__c='Tech Measure', Retail_Location__c=storeConfig1.id, Active__c=TRUE);
        insert resource; 
            ServiceResource resource1 = createServiceResource(user1, storeConfig1);
        insert resource1;
         String pricebookId = Test.getStandardPricebookId();
         Contact primaryContact = TestDataFactoryStatic.createContact(acc.id, 'CalvinPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', acc.id, storeAcc.id, 'Sold', date.today());
                OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        test.starttest();
        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.OpportunityId = opportunity.Id;
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        testOrder.CustomerPortalUser__c = currentUser.Id;
       // testOrder.Store_Location__c = store.Id,
        //testOrder.BillToContactId = contact.Id,
        insert testOrder;
            WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = testOrder.Id;
        wo.AccountId = testOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Install').Id;
        wo.status ='Appt Complete / Closed';
         wo.Primary_Installer_FSL__c = resource1.id;
         insert wo;
         test.stoptest();
         // WorkOrder wo1 = new WorkOrder(RecordTypeId = woRecordTypeMap.get('Tech_Measure').Id, Work_Order_Type__c = 'Tech Measure', Sold_Order__c = testOrder.Id, AccountId = testOrder.AccountId, Status = 'Scheduled & Assigned', Duration = 1, StartDate = System.Now(), Primary_Tech_Measure_FSL__c = resource1.id);
         //insert wo1;
        }
    }
    
     private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;
          Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1
        );
        return storeConfig;
     }
      private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }
     private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }
      private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
    @isTest
    static void getOrderTest(){
        Test.startTest();
       User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1_Test'];
        list<order> ords = [select id from order ];
        string ordId = ords[0].id;
        Order testOrder = CustomerOrderWorkersController.getOrder(ordId);
        test.stopTest();
    }
    
    @IsTest
    static void getResourceTest(){
        Test.startTest();
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1_Test'];
        System.runAs(currentUser){
        list<order> ords = [select id,Sales_Rep__c, Primary_Installer__c, Primary_Tech_Measure__c from order ];
            list<Resource__c> rc = [select Id,name from Resource__c ];
            ords[0].Primary_Installer__c = rc[0].Id;
            update ords[0];
        string ordId = ords[0].id;
      
        // Check the installer
         CustomerOrderWorkersController.getWorker(true, ords[0]);
         CustomerOrderWorkersController.getWorker(false, ords[0]);
        }
        test.stopTest();
    }
    
    @IsTest
    static void getNumberTest(){
        Test.startTest();
       User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1_Test'];
        System.runAs(currentUser){
        list<order> ords = [select id,Sales_Rep__c, Primary_Installer__c, Primary_Tech_Measure__c from order ];
        string ordId = ords[0].id;
       system.debug('ords[0].id Record------->'+ords[0].Sales_Rep__c);
        User u = CustomerOrderWorkersController.getNumber(ordId);
                    User u1 = CustomerOrderWorkersController.getNumber(null);
        }
        test.stopTest();
    }

    @IsTest
    static void getUsersTest(){
        Test.startTest();
        User currentUser = [SELECT Id FROM User WHERE FirstName = 'testUser1_Test'];
        System.runAs(currentUser){
        list<order> ords = [select id,Sales_Rep__c, Primary_Installer__c, Primary_Tech_Measure__c from order ];
        string ordId = ords[0].id;
        
            String u = CustomerOrderWorkersController.getUsers(ordId);
        } 
        test.stopTest();
    }
    
}