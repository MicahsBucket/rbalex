@isTest
public class CanvassWaypointTriggerTest {
    @isTest(SeeAllData=true)
	public static void testWaypointFromDoNotContactCanvassUnit() {
        /* Setup Test Data */
        
        // Create a Canvass Market
        CNVSS_Canvass_Market__c testCanvassMarket = CanvassTestDataUtils.createTestCanvassMarket();
        ID canvassMarketId = testCanvassMarket.Id;
            
        // Create Canvass Units
        CNVSS_Canvass_Unit__c canvassUnit = CanvassTestDataUtils.createTestCanvassUnit(canvassMarketId, true);
        
        // Create Waypoint
        SMA__MARoute__c testRoute = CanvassTestDataUtils.createTestRoute();
        SMA__MAWayPoint__c testWaypoint = CanvassTestDataUtils.createTestWaypoint(canvassUnit, testRoute, '');
        
        // Verify
        system.assertNotEquals(null, testWaypoint.sma__notes__c);
    }
}