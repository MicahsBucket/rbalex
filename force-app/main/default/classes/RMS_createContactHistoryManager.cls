/*******************************************************//**

@class  RMS_createContactHistoryManager

@brief  When a Contact is created, a Contact History record is created. 
Primary Contact and Primary Dwelling for Contact are passed from Contact
to Contact History. Should a Contact have their Account change, a Contact
History record is created as well. This means each Contact has at least one
Contact History record. They should have a Contact History record for each
Account they have had populated in the "Account" field over time.

Note: Contact related list on the Account page is actually the list of Contact Histories
in order to view all Contacts who have lived at that Dwelling. The "New Contact" button; h
however brings user to "Create Contact" page.

@author  Brianne Wilson (Slalom.BLW)
@version    2016-4-19  Slalom.BLW
Created.
@version    2017-04-07  Slalom.CDK
Revised


@copyright  (c)2016 Slalom.  All Rights Reserved. 
Unauthorized use is prohibited.

***********************************************************/
public with sharing class RMS_createContactHistoryManager {
    
    Id customerContactRecordType = UtilityMethods.retrieveRecordTypeId('Customer', 'Contact');
    map<String, RMS_Settings__c> RMS_Settings_map = RMS_Settings__c.getAll(); 
    
    
    /*******************************************************
                    afterContactInsertManager method
    *******************************************************/
    public void afterContactInsertManager(List<Contact> listNew){
                
        // Initialize Collections       
        List<Contact_History__c> contactHistoriesToUpsert = new list<Contact_History__c>();                


        List<Id> relatedAccountIds = new list<Id>();             
        List<Contact> customerContacts = new List<Contact>();
        List<Contact> formerPrimaryContactsToUpdate = new List<Contact>();
        
        // Loop through the contacts and only retrieve the Customer contacts with an account
        for(Contact con : listNew){ 
            if(con.AccountId != null && con.RecordTypeId == customerContactRecordType){
                customerContacts.add(con);
            }
        }
        // Return if no customer contacts were inserted
        if (customerContacts == null) return;
        
        // Loop through the new customer contacts
        for(Contact con : customerContacts){ 
            //Creating a new contact history for each
            Contact_History__c ch = new Contact_History__c(
                    Contact__c = con.Id,
                    Dwelling__c = con.AccountId,
                    Primary_Contact__c = con.Primary_Contact__c,
                    Type__c = 'Current Resident');
                
            contactHistoriesToUpsert.add(ch);
            
            //If it's a Primary Contact, add the account to a list of Ids
            if(con.Primary_Contact__c == True){
                relatedAccountIds.add(con.AccountId);
                }
        }
        
        //Create a map of accounts for all accounts related to primary contacts        
        Map<Id,Account> parentAccounts = new Map<Id,Account>([SELECT Id, Primary_Contact__c FROM Account WHERE Id in :relatedAccountIds]);

        // Loop through the customer contacts again
        for(Contact con : customerContacts){
            
            // If it's a primary contact, put that name on the Account and put the Account in a map
            if(con.Primary_Contact__c == TRUE){
                Account tempAccount = parentAccounts.get(con.AccountId);
                tempAccount.Primary_Contact__c = con.Full_Name__c;
                parentAccounts.put(tempAccount.Id, tempAccount);
            }
        }
        
        
        // Now query for any contacts linked to one of the primary accounts that is not in this trigger         
        for(Contact former : [SELECT Id, Primary_Contact__c, AccountId FROM Contact WHERE AccountId In:relatedAccountIds AND Primary_Contact__c = TRUE AND Id NOT IN : Trigger.newMap.keySet()]){
 
            // Set the primary contact to false for these other contacts
            former.Primary_Contact__c = FALSE;  
            formerPrimaryContactsToUpdate.add(former);
        }  
            
        upsert contactHistoriesToUpsert;
        update parentAccounts.values();
        update formerPrimaryContactsToUpdate;

    }
    
    /*******************************************************
                    afterContactUpdateManager method
    *******************************************************/
    public void afterContactUpdateManager(List<Contact> listOld, List<Contact> listNew, Map<Id, Contact> mapOld, Map<Id, Contact> mapNew){

        // Initialize Collections       
        Set<Id> relatedAccountIds = new Set<Id>();
        Map<Id, List<Contact_History__c>> contactToHistoriesMap = new Map<Id,List<Contact_History__c>>();   
        Map<Id, Account> accountMap = new Map<Id, Account>();   
        Map<Id, Contact> customerContacts = new Map<Id, Contact>();
        List<Contact_History__c> contactHistoriesToUpsert = new List<Contact_History__c>();
        
        // Find all related accounts linked to the Contact if the AccountId or Primary Contact has changed
        for(Contact con : listNew){
            System.Debug('*********con=' +con);
            System.Debug('*********customerContactRecordType=' +customerContactRecordType);
            
            if(con.RecordTypeId == customerContactRecordType && ((mapNew.get(con.Id).Primary_Contact__c != mapOld.get(con.Id).Primary_Contact__c) || (mapOld.get(con.Id).AccountId != mapNew.get(con.Id).AccountId))){
                relatedAccountIds.add(con.AccountId);
                customerContacts.put(con.Id, con);
                relatedAccountIds.add(mapOld.get(con.Id).AccountId);
                
            }
        }
        // Just return if neither the AccountId or Primary Contact has changed for any contact
        if (relatedAccountIds == null) return;
        
        // Now create an account map for all of the related Accounts
        accountMap = new Map<Id,Account>([SELECT Id, Primary_Contact__c FROM Account WHERE Id in :relatedAccountIds]);
        
        // Loop through existing contact histories linked to one of the contacts and one of the accounts and create maps of lists
        list<Contact_History__c> contactHistoryList =  [SELECT Id, Primary_Contact__c, Contact__c, Dwelling__c, Type__c 
                                        FROM Contact_History__c 
                                        WHERE Contact__c IN: customerContacts.keySet() 
                                        AND Dwelling__c in: relatedAccountIds];
        for(Contact_History__c ch :  contactHistoryList)
        {
            // If the contact to History Map contains the contact, update the list of contact histories for that key
            if (contactToHistoriesMap.containsKey(ch.Contact__c)) {
                List<Contact_History__c> tempCHList = contactToHistoriesMap.get(ch.Contact__c);
                tempCHList.add(ch);
                contactToHistoriesMap.put(ch.Contact__c, tempCHList);
            }
            // Otherwise create a new entry in the contact to History Map
            else {
                contactToHistoriesMap.put(ch.Contact__c, new List<Contact_History__c>{ch});
            }
        }

        // Loop through all of the contacts
        for(Contact con : customerContacts.values()){
            if (contactToHistoriesMap.containsKey(con.Id)) {
                // Loop through the child histories for each contact to review
                for(Contact_History__c child : contactToHistoriesMap.get(con.Id)){
                    
                    //If the primary contact has changed on the contact, update it on the contact history
                    if((mapNew.get(con.Id).Primary_Contact__c != mapOld.get(con.Id).Primary_Contact__c) && (mapNew.get(con.Id).AccountId == child.Dwelling__c)){
                        child.Primary_Contact__c = con.Primary_Contact__c;
                        contactHistoriesToUpsert.add(child);                          
                    }
                                          
                    //If the accountId has changed on the contact, create a new contact history linked to the new account
                    if(con.AccountId != null && mapOld.get(con.Id).AccountId != con.AccountId){
                        // IF 6/6/2017: Adding this code to loop over the Contact History records to make sure we are not 
                        // inserting a record if there is already a contact history record for this account/contact combo
                        boolean createHistoryRecord = true;
                        for (Contact_History__c contactHistory: contactHistoryList){
                            if(con.AccountId == contactHistory.Dwelling__c && con.Id == contactHistory.Contact__c){
                                createHistoryRecord = false;
                                break;
                            }
                        }
                        if(createHistoryRecord){
                            //only run this if there is no record for this account and contact:
                        contactHistoriesToUpsert.add(new Contact_History__c(
                        Contact__c = con.Id,
                        Dwelling__c = con.AccountId,
                        Primary_Contact__c = con.Primary_Contact__c,
                        Type__c = 'Current Resident'));
                        }
                    }                    
                }         
            }
              
        }
        for(Contact con : customerContacts.values()){
            
            Account tempAcct = accountMap.get(con.AccountId);
                        
            // If the contact was set as a primary contact then update the account's Primary Contact name to the current
            if(con.Primary_Contact__c == TRUE){
                tempAcct.Primary_Contact__c = con.Full_Name__c;
                    accountMap.put(tempAcct.Id, tempAcct);
            } 
            // If the contact is not a primary contact and the account currently has the contact as the primary
            // contact name, clear the field
            else if (tempAcct.Primary_Contact__c == con.Full_Name__c){
                tempAcct.Primary_Contact__c = '';
                accountMap.put(tempAcct.Id, tempAcct);
            }
        }
        
        upsert contactHistoriesToUpsert;
        update accountMap.values();
    }  

    /*******************************************************
                afterContactHistoryInsertManager method
    *******************************************************/
    public void afterContactHistoryInsertManager(List<Contact_History__c> listOld, List<Contact_History__c> listNew, Map<Id, Contact_History__c> mapOld, Map<Id, Contact_History__c> mapNew){                               
        
        updatePrimaryAndSecondaryContactHistories(listOld, listNew, mapOld, mapNew);
        
    }

    /*******************************************************
                afterContactHistoryUpdateManager method
    *******************************************************/
    public void afterContactHistoryUpdateManager(List<Contact_History__c> listOld, List<Contact_History__c> listNew, Map<Id, Contact_History__c> mapOld, Map<Id, Contact_History__c> mapNew){                               
    
        updateContactsAndAccounts(listOld, listNew, mapOld, mapNew);                   
        updatePrimaryAndSecondaryContactHistories(listOld, listNew, mapOld, mapNew);
        
    }


    /*******************************************************
                updatePrimaryAndSecondaryContactHistories method
    *******************************************************/
    public void updatePrimaryAndSecondaryContactHistories(List<Contact_History__c> listOld, List<Contact_History__c> listNew, Map<Id, Contact_History__c> mapOld, Map<Id, Contact_History__c> mapNew){                               
        
        // Initialize Collections       
        List<Contact_History__c> contactHistoriestoUpdate = new List<Contact_History__c>();
        Map<Id,Set<Id>> mapCurrentPrimary = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapCurrentSecondary = new Map<Id,Set<Id>>();
        
        // Loop through the new contact history records
        for(Contact_History__c conHist : listNew){ 
 
            // If the contact history is the primary current resident, add the dwelling and the list of 
            // contacts linked to the dwelling to the primary map
            if(conHist.Primary_Contact__c == true && conHist.Type__c == 'Current Resident'){
                if (mapCurrentPrimary.containsKey(conHist.Dwelling__c)) {
                    Set<Id> tempContactIds = mapCurrentPrimary.get(conHist.Dwelling__c);
                    tempContactIds.add(conHist.Contact__c);
                    mapCurrentPrimary.put(conHist.Dwelling__c, tempContactIds);
                }
                else {
                    mapCurrentPrimary.put(conHist.Dwelling__c, new Set<Id>{conHist.Contact__c});                    
                }  
            }
 
            // If the contact history is the secondary current resident, add the dwelling and the list of 
            // contacts linked to the dwelling to the secondary map
            if(conHist.Primary_Contact__c == false && conHist.Type__c == 'Current Resident'){
                if (mapCurrentSecondary.containsKey(conHist.Dwelling__c)) {
                    Set<Id> tempContactIds = mapCurrentSecondary.get(conHist.Dwelling__c);
                    tempContactIds.add(conHist.Contact__c);
                    mapCurrentSecondary.put(conHist.Dwelling__c, tempContactIds);
                }
                else {
                    mapCurrentSecondary.put(conHist.Dwelling__c, new Set<Id>{conHist.Contact__c});                    
                }                  
            }
        }
        
        // Loop through all primary current resident Contact Histories related to one of the dwellings in the primary map        
        for(Contact_History__c conHist: [SELECT Id, Primary_Contact__c, Dwelling__c, Contact__c, Type__c FROM Contact_History__c WHERE Type__c = 'Current Resident' AND Primary_Contact__c = true AND (Dwelling__c in: mapCurrentPrimary.keySet())]){            

            // If the contact is not contained within the map, set the contact history to past resident and primary contact to false
            if(!mapCurrentPrimary.get(conHist.Dwelling__c).contains(conHist.Contact__c)) {
                conHist.Type__c = 'Past Resident';
                conHist.Primary_Contact__c = false;
                contactHistoriestoUpdate.add(conHist);   
            }   
        }
        // Loop through all secondary current resident Contact Histories related to one of the dwellings in the secondary map        
        for(Contact_History__c conHist: [SELECT Id, Primary_Contact__c, Dwelling__c, Contact__c, Type__c FROM Contact_History__c WHERE Type__c = 'Current Resident' AND Primary_Contact__c = false AND (Dwelling__c in: mapCurrentSecondary.keySet())]){            

            // If the contact is not contained within the map, set the contact history to past resident
            if(!mapCurrentSecondary.get(conHist.Dwelling__c).contains(conHist.Contact__c)) {
                conHist.Type__c = 'Past Resident';
                contactHistoriestoUpdate.add(conHist);   
            }   
        }
        
        update contactHistoriestoUpdate;

    }
    
    /*******************************************************
                updateContactsAndAccounts method
    *******************************************************/
    public void updateContactsAndAccounts(List<Contact_History__c> listOld, List<Contact_History__c> listNew, Map<Id, Contact_History__c> mapOld, Map<Id, Contact_History__c> mapNew){
        
        Set<Id> contactSet = new Set<Id>();
        Set<Id> accountSet = new Set<Id>();        
        
        // Loop through the contact histories and find only the ones where the Update Contact Account 
        // flag changed. If it is, add the contact and account to sets.
        for(Contact_History__c ch : listNew){
            If(mapOld.get(ch.Id).Update_Contact_Account__c != mapNew.get(ch.Id).Update_Contact_Account__c){
                contactSet.add(ch.Contact__c);   
                accountSet.add(ch.Dwelling__c);
            }   
        }
        
        // Return if the sets are empty
        if (contactSet == null && accountSet == null) return;
        
        // Now create maps from the contact and account sets
        Map<Id,Contact> conMap = new Map<Id,Contact>([Select ID, Primary_Contact__c, AccountId from Contact where Id in :contactSet]);
        Map<Id,Account> accMap = new Map<Id,Account>([Select Id, Primary_Contact__c from Account WHERE Id in :accountSet]);
        Map<Id,List<Contact>> primaryMap = new Map<Id,List<Contact>>();
        
        // Loop through and find contacts that are related to the account, but not in the contact set
        for (Contact cont : [Select Id, AccountId, Primary_Contact__c FROM Contact WHERE AccountId IN :accountSet AND Id NOT IN :contactSet AND Primary_Contact__c = TRUE]) {

            // Populate the primary Map with the Account Id to the List of Contacts
            if (primaryMap.containsKey(cont.AccountId)) {
                List<Contact> tempContList = primaryMap.get(cont.AccountId);
                tempContList.add(cont);
                primaryMap.put(cont.AccountId, tempContList);
            }
            else {
                primaryMap.put(cont.AccountId, new List<Contact>{cont});
            }

        }
        
        // Loop back through the contact histories
        for(Contact_History__c conHist : listNew) {

            // If the contact is in the contact map, update the dwelling and primary contact 
            // and put it back in the map
            if (conMap.containsKey(conHist.Contact__c)) {
                Contact tempContact = conMap.get(conHist.Contact__c);
                tempContact.AccountId = conHist.Dwelling__c;
                tempContact.Primary_Contact__c = conHist.Primary_Contact__c;
                conMap.put(tempContact.Id, tempContact);
            }                                                

            // If the dwelling is in the account map, set the primary contact equal to the contact history name
            // and put it back in the map
            if (accMap.containsKey(conHist.Dwelling__c)) {
                Account tempAccount = accMap.get(conHist.Dwelling__c);
                if (conHist.Primary_Contact__c) tempAccount.Primary_Contact__c = conHist.Full_Name__c;
                accMap.put(tempAccount.Id, tempAccount);
            }      

            // If the contact is in the primary map, set the primary contact equal to false
            // and put it back in the map
            if(primaryMap.size()>0 && primaryMap.containsKey(conHist.Dwelling__c)){
                if (conHist.Primary_Contact__c) {
                    for(Contact cont : primaryMap.get(conHist.Dwelling__c)) {
                        cont.Primary_Contact__c = false;
                        conMap.put(cont.Id, cont);
                    }
                }
            }
        }                                               
        update conMap.values(); 
        update accMap.values();
    }
     
}