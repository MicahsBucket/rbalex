public with sharing class NewBusinessAdjustmentRedirectController {
    @AuraEnabled
    public static BAOrderWrapper getRecordData(String recordId, String objectName) {
        System.debug('recordId: '+ recordId);
        System.debug('objectName: '+ objectName);
        Order baOrder = [SELECT Id, AccountId, BillToContactId, Store_Location__c FROM Order WHERE Id =: recordId];

        BAOrderWrapper wrap = new BAOrderWrapper();
        wrap.OrderId = baOrder.Id;
        wrap.OrderBillToContactId = baOrder.BillToContactId;
        wrap.OrderStoreLocation = baOrder.Store_Location__c;
        wrap.todaysDate = Date.today();
        return wrap;
    }


    public class BAOrderWrapper {
        @AuraEnabled 
        public Id OrderId;
        @AuraEnabled 
        public Id OrderBillToContactId;
        @AuraEnabled 
        public Id OrderStoreLocation;
        @AuraEnabled 
        public Date todaysDate;
    }
}