@RestResource(urlMapping='/postWorker')
global with sharing class WorkerRestAPI  {

    @HttpPost
    global static String getWorkers(Id salesforceId, String statusMedallia) {
        
        List<List<sObject>> returnLists = new List<List<sObject>>();
        List<Id> workerIds = new List<Id>();        
        List<Survey_Worker__c> surveyWorkerList = [SELECT Name, Role__c, Worker_Id__c, Survey_Id__c, Id, Store_Account_Number__c FROM Survey_Worker__c WHERE Survey_Id__c = :salesforceId]; 
        for(Survey_Worker__c sw : surveyWorkerList){
            workerIds.add(sw.Worker_Id__c);
        }
        List<Worker__c> workerList = [SELECT Name, Email__c, Enabled_User_Id__c, 
                                             First_Name__c, Last_Name__c, 
                                             Mobile__c, Pivotal_Employee_Id__c, 
                                             User__c, Source_System__c, Store_Account__c
                                      FROM Worker__c WHERE Id = :workerIds];
        if(workerList.size() == 0){
           RestContext.response.statusCode = 500;
        }
        returnLists.add(workerList);
        returnLists.add(surveyWorkerList);
        String jsonReturnList = JSON.serialize(returnLists);

        Survey__c survey = [SELECT Survey_Status__c, Sent_To_Medallia__c, Id FROM Survey__c WHERE Id = :salesforceId];
        survey.Survey_Status__c = 'Accepted';
        survey.Sent_To_Medallia__c = true;
        Database.update(survey, true);

        return jsonReturnList;
    }

    public class nullException extends Exception {}
}