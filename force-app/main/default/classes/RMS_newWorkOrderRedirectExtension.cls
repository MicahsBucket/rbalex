public with sharing class RMS_newWorkOrderRedirectExtension {

    public string woType {get;set;}
    public string orderId {get;set;}
    public string accountId{get;set;}
    public string shippingStreet{get;set;}
    public string shippingCity{get;set;}
    public string shippingState{get;set;}
    public string shippingPostalCode{get;set;}
    public string shippingCountry{get;set;}
    public string hoa{get;set;}
    public string historical{get;set;}
    public string permit{get;set;}
    public string opportunity{get;set;}
    public string woId{get;set;}
    public string woName{get;set;}
    public string billToContactId{get;set;}
    public string storeId{get;set;}
    public Boolean isConsole{get;set;}
    public Store_Configuration__c storeConfig{get;set;}

    public string woRTid;

    private Apexpages.StandardController standardController;
    private final WorkOrder wo;

    public RMS_newWorkOrderRedirectExtension(ApexPages.StandardController stdController) {
        this.wo = new WorkOrder();

        Map<String, String> urlParam = ApexPages.currentPage().getParameters();
        system.debug('all url params ' + urlParam);

        orderId = urlParam.get('orderId');
        system.debug('order id in constructor ' + orderId);
        storeId = urlParam.get('storeId');
        accountId = urlParam.get('accountId');
        shippingStreet = urlParam.get('shippingStreet');
        shippingCity = urlParam.get('shippingCity');
        shippingPostalCode = urlParam.get('shippingPostalCode');
        shippingCountry = urlParam.get('shippingCountry');
        shippingState = urlParam.get('shippingState');
        billToContactId = urlParam.get('billToContactId');
        woType = urlParam.get('type');
        isConsole = urlParam.get('isConsole') == 'True' ? true : false;

        opportunity = urlParam.get('relatedOpp');

        permit = urlParam.get('permit');
        hoa = urlParam.get('hoa');
        historical = urlParam.get('historical');

        woName = wo.WorkOrderNumber;
        map<id,Id> storeIdToStoreConfigIdMap = new map<id,Id>();
        //        list<id> storeList = new list<id>{storeId};
        for (Account store : [SELECT Id, Active_Store_Configuration__c FROM Account WHERE Id =: storeId]) {
            if (store.Active_Store_Configuration__c != null)
                storeIdToStoreConfigIdMap.put(store.Id, store.Active_Store_Configuration__c);
        }
        map<Id, Store_Configuration__c> storeIdToStoreConfigMap = UtilityMethods.mapStoreConfigs(storeIdToStoreConfigIdMap);


        if (storeIdToStoreConfigMap != null && storeIdToStoreConfigMap.size() > 0)
            storeConfig = storeIdToStoreConfigMap.get(storeId);


    }

    public String testStoreLocation;

    /******* redirect method  *****************/
    public PageReference redirect() {

        // If a test is running just set the store name, otherwise get it from the current user
        String storeLocationName = (Test.isRunningTest()) ?     testStoreLocation :
                                                                [SELECT Default_Store_Location__c FROM User WHERE Id =: UserInfo.getUserId()].Default_Store_Location__c;

        if (String.isBlank(storeLocationName)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_USER)); //
            return null;
        }
        system.debug('order id in redirect method ' + orderId);
        wo.Sold_Order__c = orderId;
        wo.AccountId = accountId;
        wo.Street = shippingStreet;
        wo.City = shippingCity;
        wo.State = shippingState;
        wo.PostalCode = shippingPostalCode;
        wo.Country = shippingCountry;
        wo.OwnerId = UserInfo.getUserId();

        if(woType != null && woType != 'null'){
            if(woType == 'hoa'){
                woRTid = UtilityMethods.retrieveRecordTypeId('HOA', 'WorkOrder');
                woType = 'HOA';
            }else if(woType == 'historical'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Historical', 'WorkOrder');
                woType = 'Historical';
            }else if(woType == 'lswp'){
                woRTid = UtilityMethods.retrieveRecordTypeId('LSWP', 'WorkOrder');
                woType = 'LSWP';
            }else if(woType == 'techMeasure'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
                woType = 'Tech Measure';
            }else if(woType == 'install'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder');
                woType = 'Install';
            }else if(woType == 'paintStain'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Paint_Stain', 'WorkOrder');
                woType = 'Paint/Stain';
                if(storeConfig != null && storeConfig.Paint_Stain_Vendor_Account__c != null && storeConfig.Paint_Stain_Vendor_Contact__c != null){
                    wo.Vendor__c = storeConfig.Paint_Stain_Vendor_Account__c;
                    wo.Vendor_Contact__c = storeConfig.Paint_Stain_Vendor_Contact__c;
                }
            }else if(woType == 'permit'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Permit', 'WorkOrder');
                woType = 'Building Permit';
                wo.Status = 'New';
                if(permit != null && permit != '' && permit != 'null'){
                    wo.Municipality__c = permit;
                }
            }else if(woType == 'collections'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Collections', 'WorkOrder');
                woType = 'Collections';
            } else if(woType == 'jobSiteVisit'){
                woRTid = UtilityMethods.retrieveRecordTypeId('Job_Site_Visit', 'WorkOrder');
                woType = 'Job Site Visit';
            }
            wo.RecordTypeId = woRTid;
            wo.Work_Order_Type__c = woType;
        }

        if(billToContactId != null && billToContactId != 'null'){
            wo.ContactId = billToContactId;
        }
        if(opportunity != null && opportunity != '' && opportunity != 'null'){
            wo.Opportunity__c = opportunity;
        }
        if(hoa != null && hoa != '' && hoa != 'null' && wo.Work_Order_Type__c == 'HOA' ){
            wo.Municipality__c = hoa;
        } else if(historical != null && historical != '' && historical != 'null' && wo.Work_Order_Type__c == 'Historical'){
            wo.Municipality__c = historical;
        }


        system.debug('WORK ORDER ABOUT TO BE GENERATED: ' + wo);
        insert wo;

        if(woType == 'actions' || woType == 'permit'){
            List<Municipality_Contact__c> mc = new List<Municipality_Contact__c>();
            mc = [Select Id, Active__c, Municipality__c FROM Municipality_Contact__c WHERE Municipality__c =: wo.Municipality__c AND Active__c = TRUE ORDER BY CreatedDate DESC LIMIT 1];

            if(mc.size() > 0){
                wo.Municipality_Contact__c = mc[0].Id;

                update wo;
            }

        }

        // Used for redirecting to the view page from javascript
        woId = wo.Id;
        PageReference p;
        if(isConsole) {
            p = new PageReference('/console#%2F' + woId);
        } else {
            p = new PageReference('/' + woId);
        }

        //m.put('nooverride', '1');
        return p;
    }

}