public class CustomerLogoutCtrl {
    /*
* Method to create the url string required to successfully log out of a community.
* MTR - 08-09-18
*/
    @AuraEnabled   
    public static String getSitePrefix(){
        String url;
        String ntwrkId = Network.getNetworkId();
        if(!test.isRunningTest()){
            ConnectApi.Community comm = ConnectApi.Communities.getCommunity(ntwrkId);
            System.debug(comm.siteUrl);
            url = comm.siteUrl;
        }
        if(test.isRunningTest()){
            url = 'testUrl';
        }
        return url;
    }
}