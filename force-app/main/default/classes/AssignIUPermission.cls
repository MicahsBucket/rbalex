global with sharing class AssignIUPermission implements Database.Batchable<sObject>, Database.Stateful,  Database.AllowsCallouts{
    public Set<String> pSetAPINames = new Set<String>();
    public Map<Id, PermissionSet> pSetsById = new Map<Id, PermissionSet>();
    public Set<String> ltPSets = new Set<String>{'LMS_Admin', 'LMS_External_User', 'LMS_ILT_Admin', 'LMS_ILT_Instructor', 'LMS_Player_Admin', 'LMS_Player_User', 'LMS_Publisher', 'LMS_User'};
    public Set<String> vtPSets = new Set<String>{'Player_Admin', 'Player_User'};
    public String ltuiPSet = 'LearnTrac_Lightning_UI_User';
    public String vtuiPSet = 'ViewTrac_Lightning_UI_User';
    public String ltuiPSetId = '';
    public String vtuiPSetId = '';

    public AssignIUPermission(){
        if (![SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'ltui'].isEmpty()) {
            pSetAPINames.addAll(ltPSets);
            pSetAPINames.add(ltuiPSet);
        }
        if (![SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'vtui'].isEmpty()) {
            pSetAPINames.addAll(vtPSets);
            pSetAPINames.add(vtuiPSet);
        }
        pSetsById = new Map<Id, PermissionSet>([SELECT Id, Name FROM PermissionSet WHERE Name IN :pSetAPINames]);
        for (PermissionSet permissionSetItem : pSetsById.values()) {
            if (permissionSetItem.Name == ltuiPSet) {
                ltuiPSetId = permissionSetItem.Id;
            }
            if (permissionSetItem.Name == vtuiPSet) {
                vtuiPSetId = permissionSetItem.Id;
            }
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        AssignIUPermission.WithoutSharingOps wsops = new AssignIUPermission.WithoutSharingOps();
        String soql = 'SELECT Id FROM User WHERE (ContactId != NULL OR UserType = \'Guest\' OR UserType = \'Standard\') AND (Name != \'Integration User\' AND Name != \'Security User\') AND isActive = true' +(Test.isRunningTest()?' LIMIT 200':'');
        return wsops.getWithoutSharingQueryLocator(soql);
    }

    global void execute(Database.BatchableContext BC, List<User> scope){
        List<PermissionSetAssignment> existingAssignments = [
                SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
                FROM PermissionSetAssignment
                WHERE AssigneeId IN :scope AND PermissionSetId IN :pSetsById.keySet()
        ];

        Map<Id, Set<String>> existingAssignmentsByUser = new Map<Id, Set<String>>();
        for (PermissionSetAssignment psa : existingAssignments) {
            existingAssignmentsByUser.put(psa.AssigneeId, new Set<String>());
        }
        for (PermissionSetAssignment psa : existingAssignments) {
            existingAssignmentsByUser.get(psa.AssigneeId).add(psa.PermissionSet.Name);
        }

        List<PermissionSetAssignment> newAssignments = new List<PermissionSetAssignment>();
        for (User u : scope) {
            if (existingAssignmentsByUser.containsKey(u.Id)) {
                Boolean isNeadLTUIPermissionSet = false;
                Boolean isNeadVTUIPermissionSet = false;

                for (String item : ltPSets) {
                    if (existingAssignmentsByUser.get(u.Id).contains(item)) {
                        isNeadLTUIPermissionSet = true;
                    }
                }
                if (existingAssignmentsByUser.get(u.Id).contains(ltuiPSet)) {
                    isNeadLTUIPermissionSet = false;
                }

                for (String item : vtPSets) {
                    if (existingAssignmentsByUser.get(u.Id).contains(item)) {
                        isNeadVTUIPermissionSet = true;
                    }
                }
                if (existingAssignmentsByUser.get(u.Id).contains(vtuiPSet)) {
                    isNeadVTUIPermissionSet = false;
                }

                if (isNeadLTUIPermissionSet && ltuiPSetId != '') {
                    newAssignments.add(new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = ltuiPSetId));
                }
                if (isNeadVTUIPermissionSet && vtuiPSetId != '') {
                    newAssignments.add(new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = vtuiPSetId));
                }
            }
        }
        System.debug('New PS assignments to create count: ' + newAssignments.size());
        if (!newAssignments.isEmpty()) {
            Database.insert(newAssignments, false);
        }
    }

    global void finish(Database.BatchableContext BC){

    }


    private without sharing class WithoutSharingOps{

        public Database.QueryLocator getWithoutSharingQueryLocator(String soql){
            return Database.getQueryLocator(soql);
        }

    }
}