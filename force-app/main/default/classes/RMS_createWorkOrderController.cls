public with sharing class RMS_createWorkOrderController {


    public string woType {get;set;}
    public string orderId {get;set;}
    public string accountId{get;set;}
    public string shippingStreet{get;set;}
    public string shippingCity{get;set;}
    public string shippingState{get;set;}
    public string shippingPostalCode{get;set;}
    public string shippingCountry{get;set;}
    public string hoa{get;set;}
    public string historical{get;set;}
    public string permit{get;set;}
    public string opportunity{get;set;}
    public string woId{get;set;}
    public string woName{get;set;}
    public string billToContactId{get;set;}
    public string storeId{get;set;}

    public string woRTid;

    public string propPickValSelected { get; set; }
    public List<SelectOption> pickLstValue { get; set; }
     private Map<String,List<String>>orderRecordTypesWithWordOrderRT = 
            new Map<String,List<String>>{
                'CORO Record Type' => new List<String>{'Tech Measure','Install','Job Site Visit','HOA','Historical',
                'LSWP','Paint/Stain','Permit','Collections'},
                'Change Order' => new List<String>{'Tech Measure','Install','Job Site Visit','HOA','Historical',
                'LSWP','Paint/Stain','Permit','Collections'},
                'CORO Service' => new List<String>{'Install','Service','Job Site Visit','HOA','Historical',
                'LSWP','Paint/Stain','Permit','Collections'}
            };

    public List<SelectOption>wordOrderRTs {get;set;}  
    public String selectedRecordType {get;set;}  

    /******* Set up Standard Controller for Purchase_Order__c  *****************/
    private Apexpages.StandardController standardController;
    private final WorkOrder wo;
    private Order currentOrder;

    //Constructor
    public RMS_createWorkOrderController(ApexPages.StandardController stdController) {
        this.wo = (WorkOrder)stdController.getRecord();
        map<string, string> urlParam = ApexPages.currentPage().getParameters();

        orderId = urlParam.get('orderId');
        storeId = urlParam.get('storeId');
        accountId = urlParam.get('accountId');
        shippingStreet = urlParam.get('shippingStreet');
        shippingCity = urlParam.get('shippingCity');
        shippingPostalCode = urlParam.get('shippingPostalCode');
        shippingCountry = urlParam.get('shippingCountry');
        shippingState = urlParam.get('shippingState');
        billToContactId = urlParam.get('billToContactId');
        woType = urlParam.get('type');

        opportunity = urlParam.get('relatedOpp');

        permit = urlParam.get('permit');
        hoa = urlParam.get('hoa');
        historical = urlParam.get('historical');

        woName = wo.WorkOrderNumber;
        wordOrderRTs = new List<SelectOption>(); 
        if (String.isNotEmpty(orderId)) {
            currentOrder = [SELECT id,Status, recordTypeId FROM Order
                                  WHERE Id = : orderId];
            getALLWorkOrderRecordTypes(currentOrder.recordTypeId);
        }

        
    }


    private void getALLWorkOrderRecordTypes(String recordTypeId){
        Schema.DescribeSObjectResult describeOrder = Schema.SObjectType.Order; 
        Map<Id,Schema.RecordTypeInfo> orderRtMapById = describeOrder.getRecordTypeInfosById();
        Schema.RecordTypeInfo orderRecordType =  orderRtMapById.get(recordTypeId);
        Schema.DescribeSObjectResult describeWorkOrder = Schema.SObjectType.WorkOrder; 
        Map<String,Schema.RecordTypeInfo> workOrderRtMapByName = describeWorkOrder.getRecordTypeInfosByName();
        wordOrderRTs.add(new SelectOption('','---None---'));
        if (orderRecordTypesWithWordOrderRT.containsKey(orderRecordType.getName())) {
            List<String>allRecordTypes = (orderRecordTypesWithWordOrderRT.get(orderRecordType.getName()));
            allRecordTypes.sort();
            for (String workOrderRecordType :  allRecordTypes) {
                Schema.RecordTypeInfo workOrderRecordTypeInfo =  workOrderRtMapByName.get(workOrderRecordType);  
                wordOrderRTs.add(new SelectOption(workOrderRecordTypeInfo.getRecordTypeId(),workOrderRecordType));
            }
        }
    }

    public PageReference save() {
        try{ 
            if ('Draft'.equalsIgnoreCase(currentOrder.Status)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Can’t create new Work Orders if Status = Draft'));
                return null;
            }
        wo.Sold_Order__c = orderId;
        wo.AccountId = accountId;
        wo.Street = shippingStreet;
        wo.City = shippingCity;
        wo.State = shippingState;
        wo.PostalCode= shippingPostalCode;
        wo.Country = shippingCountry;

        if(billToContactId != null && billToContactId != 'null'){
            wo.ContactId = billToContactId;
        }
        if(opportunity != null && opportunity != '' && opportunity != 'null'){
            wo.Opportunity__c = opportunity;
        }
        if(hoa != null && hoa != '' && hoa != 'null' && wo.Work_Order_Type__c == 'HOA' ){
            wo.Municipality__c = hoa;
        }else if(historical != null && historical != '' && historical != 'null' && wo.Work_Order_Type__c == 'Historical'){
            wo.Municipality__c = historical;
        }

        if (String.isNotEmpty(selectedRecordType)) {
            wo.RecordTypeId = selectedRecordType;   
        }
        

        insert wo;

        if(woType == 'actions'|| woType == 'permit'){
            List<Municipality_Contact__c> mc = new List<Municipality_Contact__c>();
            mc = [Select Id, Active__c, Municipality__c FROM Municipality_Contact__c WHERE Municipality__c =: wo.Municipality__c AND Active__c = TRUE ORDER BY CreatedDate DESC LIMIT 1];

            if(mc.size() > 0){
                wo.Municipality_Contact__c = mc[0].Id;

                update wo;
            }

        }

        // Used for redirecting to the view page from javascript
        woId = wo.Id;

        return new PageReference('/' + woId);
    }
    
    catch(DmlException ex){
        for(integer i=0; i < ex.getNumDml(); i++){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getDmlMessage(i)));
        }
        return null;
    }
    }

}