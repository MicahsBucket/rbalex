/*
* @author Jason Flippen
* @date 11/13/2020
* @description Class to provide support for the homeComponentLightningCardServiceOrdering LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - HomeCompLightningCardServiceOrderingTest
*/ 
public with sharing class HomeCompLightningCardServiceOrdering{

    @AuraEnabled(cacheable=true)
    public static List<ServiceRequestWrapper> getServiceRequests(String searchKey) {

        List<ServiceRequestWrapper> wrapperList = new List<ServiceRequestWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
        String queryString = 'SELECT Id,' +
                             '       Name,' +
                             '       OrderNumber,' +
                             '       Status,' +
                             '       BillToContactId,' +
                             '       BillToContact.Name,' +
                             '       Bill_to_Contact_Text__c,' +
                             '       Service_Type__c,' +
                             '       AccountId,' +
                             '       Account.Name,' +
                             '       Amount_Due__c,' +
                             '       Estimated_Ship_Date__c ' +
                             'FROM   Order ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'WHERE (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey) ';
		}
        
		queryString += 'LIMIT 20';

		for (Order o : Database.query(queryString)) {
            ServiceRequestWrapper wrapper = new ServiceRequestWrapper(o.Id,
                                                                      o.OrderNumber,
                                                                      o.BillToContactId,
                                                                      o.BillToContact.Name,
                                                                      o.Estimated_Ship_Date__c,
                                                                      o.AccountId,
                                                                      o.Account.Name,
                                                                      o.Amount_Due__c);
            wrapperList.add(wrapper);
        }

        return wrapperList;

    }

    @AuraEnabled(cacheable=true)
    public static List<PurchaseOrderWrapper> getPurchaseOrders(String searchKey) {

        List<PurchaseOrderWrapper> wrapperList = new List<PurchaseOrderWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
        
		Set<String> eligibleStatusSet = new Set<String>{'Rejected'};
        String queryString = 'SELECT Id,' +
                             '       Name,' +
                             '       Reference__c,' +
                             '       Comments__c,' +
                             '       Rejection_Date__c,' +
                             '       Status__c ' +
                             'FROM   Purchase_Order__c ' +
                             'WHERE  Status__c IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (Name LIKE :tempSearchKey OR Reference__c LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY Name ' +
                       'LIMIT 20';
        
        for (Purchase_Order__c po : Database.query(queryString)) {
            PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper(po.Id,
                                                                    po.Name,
                                                                    po.Reference__c,
                                                                    po.Comments__c,
                                                                    po.Rejection_Date__c);
            wrapperList.add(wrapper);               
        }
        
        return wrapperList;
        
    }


/** Wrapper Classes **/


     public class ServiceRequestWrapper {
        
        @AuraEnabled
        public String ServiceReqNumber{get;set;}
        
        @AuraEnabled
        public String ServiceReqNumberURL{get;set;}
        
        @AuraEnabled
        public String Customername{get;set;}
        
        @AuraEnabled
        public String CustomernameURL{get;set;}
        
        @AuraEnabled
        public datetime EstShipDate{get;set;}
        
        @AuraEnabled
        public String Accountname{get;set;}
        
        @AuraEnabled
        public String AccountnameURL{get;set;}
        
        @AuraEnabled
        public Decimal AmountDue{get;set;}
        
       
        
        public ServiceRequestWrapper(Id recId,
                                     String OrdNum,
                                     Id customerId,
                                     String customerName1,
                                     Datetime EstimShipDate,
                                     Id accId,
                                     String accName,
                                     Decimal amountDue1) {
        
            ServiceReqNumber = OrdNum;
            ServiceReqNumberURL = '/'+ recId;
            Customername = customerName1;
            if (customerId == null) {
                CustomernameURL = null;
            }
            else {
                CustomernameURL= '/'+ customerId;
            }
            EstShipDate = EstimShipDate;
            Accountname = accName;
            AccountnameURL= '/'+ accId;
            AmountDue = amountDue1;
        }
    
    }
    
     public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String PurchaseOrderNumber{get;set;}
        
        @AuraEnabled
        public String PurchaseOrderNumberURL{get;set;}
        
        @AuraEnabled
        public String Reference{get;set;}             
        
        @AuraEnabled
        public datetime RejectedDate{get;set;}
        
        @AuraEnabled
        public String Comment{get;set;}
        
        public PurchaseOrderWrapper(Id recId,
                                    String PONum,
                                    String ref,
                                    String Comment1,
                                    Datetime rejDate) {
        
            PurchaseOrderNumber = PONum;
            PurchaseOrderNumberURL = '/'+ recId;
            RejectedDate = rejDate;
            Reference = ref;
            Comment = Comment1;
            
        }
    
    }

}