public with sharing class RMS_StoreConfigurationTriggerHandler {
    public void isBeforeInsert(List<Store_Configuration__c> oldList, List<Store_Configuration__c> newList, Map<Id, Store_Configuration__c> oldMap, Map<Id, Store_Configuration__c> newMap) {
        updateComplianceCoordinatorPermissions(oldList, newList, oldMap, newMap);
    }
    
        public void isBeforeUpdate(List<Store_Configuration__c> oldList, List<Store_Configuration__c> newList, Map<Id, Store_Configuration__c> oldMap, Map<Id, Store_Configuration__c> newMap) {
            updateComplianceCoordinatorPermissions(oldList, newList, oldMap, newMap);
        updateContractorInformation(oldList, newList, oldMap, newMap);
    }

    public void updateComplianceCoordinatorPermissions(List<Store_Configuration__c> oldList, List<Store_Configuration__c> newList, Map<Id, Store_Configuration__c> oldMap, Map<Id, Store_Configuration__c> newMap) {
        System.debug('in RMS_StoreConfigurationTriggerHandler/updateComplianceCoordinatorPermissions');
        //determine if the compliance coordinator has changed.
        //add permission set to the new coordinator


        Id addUserId;
        System.debug('newList: ' + newList);
        System.debug('oldList: ' + oldList);

        for(Store_Configuration__c sc : newList) {
            System.debug(sc);
            if(sc.Compliance_Coordinator__c != null) {
                if(Trigger.isUpdate) {
                    if(oldMap.get(sc.Id).Compliance_Coordinator__c != sc.Compliance_Coordinator__c) {
                        addUserId = sc.Compliance_Coordinator__c;
                    }
                }
                else {
                    addUserId = sc.Compliance_Coordinator__c;
                }
            }
        }
        System.debug('User id: ' + addUserId);

        //need to add the permission set in a different transaction context
        //https://developer.salesforce.com/docs/atlas.en-us.210.0.apexcode.meta/apexcode/apex_dml_non_mix_sobjects.htm
        RMS_StoreComplianceUserQueueable updateJob = new RMS_StoreComplianceUserQueueable(addUserId);
        Id jobId = System.enqueueJob(updateJob);
    
    }

    /*@future
    public static void updateComplianceUserPermissions(List<Id> addUserId) {
        PermissionSet pm = [select Id, Name from PermissionSet where Name = 'Store_Configuration_Contract_Editing'];            
            List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
            for(User u : [select Id from User where Id in :addUserId]) {
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = pm.Id, AssigneeId = u.Id);
                permissionSetList.add(psa);
            }
            System.debug('permissionSetList: ' + permissionSetList);
            insert permissionSetList;
        }
    */

    public void updateContractorInformation(List<Store_Configuration__c> oldList, List<Store_Configuration__c> newList, Map<Id, Store_Configuration__c> oldMap, Map<Id, Store_Configuration__c> newMap) {
        System.debug('in RMS_StoreConfigurationTriggerHandler/updateContractorInformation');
        //Determine if the contractor fields have been changed
        //determine if the current user is the Compliance Coordinator, or is an Admin
        //If yes to both, allow the change.
        Id UserProfileId = UserInfo.getProfileId();
        List<Profile> UserProfileList = [SELECT Name from Profile where id = :UserProfileId];
        String UserProfileName =UserProfileList[0].Name;
        //List<Store_Configuration__c> insufficientPerms = new List<Store_Configuration__c>();

        for(Store_Configuration__c sc : newList) {
        //if the field was updated
            if(oldMap.get(sc.Id).Contractor_License_1_Expiration__c != newMap.get(sc.Id).Contractor_License_1_Expiration__c || oldMap.get(sc.Id).Contractor_License_2_Expiration__c != newMap.get(sc.Id).Contractor_License_2_Expiration__c || oldMap.get(sc.Id).Contractor_License_1__c != newMap.get(sc.Id).Contractor_License_1__c || oldMap.get(sc.Id).Contractor_License_2__c != newMap.get(sc.Id).Contractor_License_2__c) {
                if(UserProfileName == 'Super Administrator' || sc.Compliance_Coordinator__c == UserInfo.getUserId())  {
                    System.debug('allowing the update');
                //allow the update
                }
                else {
                //throw an error
                    System.debug('failing the update');
                    sc.addError('You cannot update the Contractor License - you are not the Compliance Coordinator, or an Administrator');
                }
            }
        }
    }
}