/*
* @author Jason Flippen
* @date 10/09/2020 
* @description Test Class for the following Classes:
*
*			   - HomeComponentDashboardFinder
*
*/
@isTest
public with sharing class HomeComponentDashboardFinderTest {

    /*
    * @author Jason Flippen
    * @date 10/09/2020
    * @description Method to test the "getDashboardId" method.
    */
    private static testMethod void testGetDashboardId() {

        Test.startTest();

            Id dashboardId = HomeComponentDashboardFinder.getDashboardId();
            
            // 20201030 - Jason Flippen - Removed Assert Equals because the person
            // deploying this code may not have accessed a Dashboard  recently.
            //System.assertEquals(false, dashboardId==null, 'Unexpected Dashboard Id returned');

        Test.stopTest();
    }

}