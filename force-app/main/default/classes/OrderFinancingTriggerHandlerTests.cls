@isTest
private class OrderFinancingTriggerHandlerTests {

  public static final String dwellingName = 'Dwelling Account';
  public static final String postInsertDwellingName = '11111, ' + dwellingName;
  
   static testMethod void testupdateFinanceExpirationDateonOrder(){
   TestUtilityMethods utility = new TestUtilityMethods();
    
    utility.setUpConfigs();
    
    Account account1 = utility.createVendorAccount('Vendor Account 1 ');
    insert account1;

    Account account2 = new Account(  Name = 'RbA',
                    AccountNumber = '1234567890',
                    Phone = '(763) 555-2000'
                  );
    insert account2;

    Account dwelling = utility.createDwellingAccount(dwellingName);

    Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
    dwelling.Store_Location__c = store.Id;
    insert dwelling;

    
    Opportunity opp1 = utility.createOpportunity(dwelling.id, 'Closed - Won');
    insert opp1;
  
    Store_Configuration__c stconfig = new Store_Configuration__c(Store__c = store.Id, Order_Number__c= 1234);
    insert stconfig;
    
    Store_Finance_Program__c storefinaprog = new Store_Finance_Program__c(Store_Configuration__c = stconfig.Id, Name = '12 Months Same as Cash', Option_Order__c = '17', Active__c = True);
    insert storefinaprog;
    
        
    Financial_Account_Number__c fan = new Financial_Account_Number__c(Name ='Finan Acc', Account_Type__c = 'Cost PO');
    insert fan;
    Product2 product1 = new Product2(
      Name='Test Product',
      Vendor__c = account1.id,
      Cost_PO__c = true,
      isActive = true,
      Account_Number__c =  fan.Id
    );
    
    insert product1;
    
    PricebookEntry pricebookEntry1 = utility.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
    insert pricebookEntry1;
    
    List<Order> orderList =  new List<Order>{ new Order(  Name='Order1', 
                  AccountId = dwelling.id, 
                  EffectiveDate= Date.Today(), 
                  Store_Location__c = store.Id,
                  Opportunity = opp1,                  
                  Status ='Draft', 
                  Tech_Measure_Status__c = 'New',
                  Pricebook2Id = Test.getStandardPricebookId()
                ),  new Order(  Name='Order2', 
                  AccountId = dwelling.id, 
                  EffectiveDate= Date.Today(), 
                  Store_Location__c = store.Id,
                  Opportunity = opp1,                   
                  Status ='Draft',
                  Tech_Measure_Status__c = 'New', 
                  Pricebook2Id = Test.getStandardPricebookId()
                ),  new Order(Name='Order3', 
                  AccountId = dwelling.id, 
                  EffectiveDate= Date.Today(), 
                  Store_Location__c = store.Id,
                  Opportunity = opp1,                   
                  Status ='Draft', 
                  Install_Order_Status__c = 'New',
                  Pricebook2Id = Test.getStandardPricebookId()
                )};    
    insert orderList;

    List<OrderItem> oiList = new List<OrderItem>{new OrderItem(OrderId = OrderList[0].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 ),
        new OrderItem(OrderId = OrderList[1].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 ),
        new OrderItem(OrderId = OrderList[2].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100 )};
    insert oiList;
   
    List<Order_Financing__c> OrderFinanceList = new List<Order_Financing__c>();
    Order_Financing__c testof;
    testof = new Order_Financing__c(Related_Order__c = OrderList[0].Id, Expiration_Date__c = system.today(),Store_Location__c = store.Id, Store_Finance_Program__c = storefinaprog.Id);
    OrderFinanceList.add(testof);
    testof = new Order_Financing__c(Related_Order__c = OrderList[0].Id, Expiration_Date__c = system.today()-1,Store_Finance_Program__c = storefinaprog.Id, Store_Location__c = store.Id);
    OrderFinanceList.add(testof);
    testof = new Order_Financing__c(Related_Order__c = OrderList[0].Id, Expiration_Date__c = system.today()-2,Store_Finance_Program__c = storefinaprog.Id, Store_Location__c = store.Id);
    OrderFinanceList.add(testof);
    insert OrderFinanceList;
      
    List<Order_Financing__c> oflist = [Select Id, name, Expiration_Date__c from Order_Financing__c where Related_Order__c = :OrderList[0].Id];      
    oflist[0].Expiration_Date__c = system.today()+10;
    update oflist[0];
    List<Order_Financing__c> oflistd = [Select Id, name, Expiration_Date__c from Order_Financing__c where Related_Order__c = :OrderList[0].Id];  
    delete oflistd[1];  

    }

 }