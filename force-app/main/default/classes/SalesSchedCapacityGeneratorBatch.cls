/**
 * @File Name          : SalesSchedAppointmentCtrl.cls
 * @Description        : Lightning Web Component controller for viewing appointment information.
 * @Author             : James Loghry
 * @Group              : Demand Chain
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 4/16/2019, 12:13:59 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    2/15/2019, 1:18:04 PM   Demand Chain (James Loghry)     Initial Version
**/
public with sharing class SalesSchedCapacityGeneratorBatch implements Database.Batchable<sObject>, Database.Stateful{

    //List of sales capacities to create
    public List<Sales_Capacity__c> capacities {get; set;}

    //Determines the month of capacities to generate (in yyyy-MM-dd format, where the day is the 1st of the month)
    private Date monthStart {get; set;}
    private Date monthEnd {get; set;}


    //Populated if onboarding or adding capacity for a specific set of users.
    private Set<Id> userIds {get; set;}

    //The query used to generate the list of users for capacity creation.
    private String query {get; set;}

    //List of time slots used to create capacities
    private Map<String,List<Slot__c>> slotMap {get; set;}

    public SalesSchedCapacityGeneratorBatch(Date monthStart, Set<Id> userIds){
        this.userIds = userIds;
        Date today = Date.today().addMonths(2);
        this.monthStart = Date.newInstance(today.year(), today.month(), 1);
        if(monthStart != null){
            this.monthStart = monthStart;
        }
        this.monthEnd = this.monthStart.addMonths(1).addDays(-1);


        this.query = 
            'Select ' +
            	+'Id,' +
            	'Contact.Account.Active_Store_Configuration__r.Slot_Group__c ' +
            'From ' +
            	'User ' + 
            'Where ' +
            	'IsActive = true ' +
            	'And Profile.Name = \'Partner RMS-Sales\'';
        if(this.userIds != null){
            this.query += ' And Id in :userIds';
        }
        
        if(Test.isRunningTest()){
            this.query += ' And Enabled_User_Id__c LIKE \'EnabledTest123%\'';
        }

        slotMap = new Map<String,List<Slot__c>>{'Three Slots'=>new List<Slot__c>(),'Five Slots'=>new List<Slot__c>()};
        for(Slot__c slot : [Select Id,Slot_Group__c,Day__c From Slot__c]){
            slotMap.get(slot.Slot_Group__c).add(slot);
        }

        this.capacities = new List<Sales_Capacity__c>();
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(this.query);
    }


    public void execute(Database.BatchableContext bc, List<sObject> scope){
        Map<String,Sales_Capacity__c> capacityMap = new Map<String,Sales_Capacity__c>();
        Date startDate = this.monthStart;
        while(startDate < this.monthEnd){
            String d = ((Datetime)startDate).formatGmt('EEEE');
            String yyyymmdd = ((Datetime)startDate).formatGmt('yyyy-MM-dd');
            for(User u : (List<User>)scope){
                if(u.Contact.Account.Active_Store_Configuration__r.Slot_Group__c != null && this.slotMap.containsKey(u.Contact.Account.Active_Store_Configuration__r.Slot_Group__c)){
                    for(Slot__c slot : this.slotMap.get(u.Contact.Account.Active_Store_Configuration__r.Slot_Group__c)){
                        if(slot.Day__c == d){
                            String key = u.Id + '_' + yyyymmdd + '_' + slot.Id;
                            capacityMap.put(
                                key,
                                new Sales_Capacity__c(
                                    Slot__c = slot.Id,
                                    Date__c = startDate,
                                    User__c = u.Id,
                                    Available__c = true,
                                    Status__c = 'Available',
                                    OwnerId = u.Id,
                                    External_Id__c = key
                                )
                            );
                        }
                    }
                }
            }
            startDate = startDate.addDays(1);
        }
        this.capacities.addAll(capacityMap.values());
	}

    public void finish(Database.BatchableContext bc){
        System.debug('JWL: capacities size: ' + this.capacities.size());
        Database.executeBatch(new SalesSchedInsertBatch(this.capacities),2000);
    }
}