public class RMS_LaborEditRedirectController {

    public RMS_LaborEditRedirectController(ApexPages.StandardController controller) {
        this.controller = controller;
    }

    public PageReference getRedir() {

        Labor__c la = [Select Id, RecordTypeId, Store_Location__c, Related_FSL_Work_Order__c From Labor__c Where Id = :ApexPages.currentPage().getParameters().get('Id')];
        system.debug('RecordTypeId'+la.RecordTypeId);
        Id rdId = Schema.SObjectType.Labor__c.RecordTypeInfosByName.get('Labor').RecordTypeId;
        Id rdtrimId = String.valueOf(rdId).substring(0,15);
        system.debug('#########'+rdId);
        system.debug('@@@@@@@@@'+rdtrimId);

        PageReference newPage;
        if (la.RecordTypeId == rdId || la.RecordTypeId == rdtrimId) {
            newPage = new PageReference('/apex/RMS_LaborCustomEdit?StoreLocation='+la.Store_Location__c+'&RecordType='+la.RecordTypeId+'&RelatedWorkOrder='+la.Related_FSL_Work_Order__c);
        } else {
            newPage = new PageReference('/' + la.Id + '/e');
            newPage.getParameters().put('nooverride', '1');
        }
        newPage.getParameters().put('Id', la.id);
        return newPage.setRedirect(true);
    }

    private final ApexPages.StandardController controller;
 }