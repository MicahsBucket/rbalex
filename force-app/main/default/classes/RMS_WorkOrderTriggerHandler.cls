public with sharing class RMS_WorkOrderTriggerHandler {
    public void updateOrderAfterWorkOrderComplete(List<RbA_Work_Order__c> listNew, Map<Id, RbA_Work_Order__c> mapNew) {
        //if type is tech measure work order and new status is Appt Complete/Closed
        //then change the status of the Order to 'Ready to Order'
        //and add the timestamp to the Time_Ready_to_order__c field on the order

        System.debug('in UpdateOrderAfterWorkOrderComplete');
        Map<Id, Id> ordersToWorkOrderMap = new Map<Id, Id>();
        for(RbA_Work_Order__c r : listnew) {
            ordersToWorkOrderMap.put(r.Sold_Order__c, r.Id);
        }

        System.debug('ordersToWorkOrderMap: ' + ordersToWorkOrderMap);

        List<Order> ordersToUpdate = new List<Order>();

        //Added by Geetha Katta to exclude Orders which has open actvities
        integer openActivitiesCount = 0;
       
         String recordTypeName = 'On Hold'; 
         Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Task.getRecordTypeInfosByName();
         Schema.RecordTypeInfo rtInfo =  rtMapByName.get(recordTypeName);
         Id recordtypeid = rtInfo.getRecordTypeId();
         System.debug('#####'+recordtypeid);

        for(Order ord : [SELECT Id, Status FROM Order WHERE Id IN: ordersToWorkOrderMap.keySet()]) {
        List<Task> tsk = [Select Id,Status from Task where What.Id IN: ordersToWorkOrderMap.keySet() and RecordTypeId = :recordtypeid and Status='Open'];
        system.debug('@@@@@'+recordtypeid);
            openActivitiesCount = openActivitiesCount + tsk.size();
            Id rbaOrderId = ordersToWorkOrderMap.get(ord.Id);
            RbA_Work_Order__c rbaOrder = mapNew.get(rbaOrderId);
            if(rbaOrder.Work_Order_Status__c == 'Appt Complete / Closed' && rbaOrder.Work_Order_Type__c == 'Tech Measure' && openActivitiesCount == 0 && ord.Status!='On Hold') {
                ord.status = 'Ready to Order';
                ord.apex_context__c = true;
                //ord.Time_Ready_to_Order__c = datetime.now();  //should be done in RMS_OrderTriggerHandler.onBeforeUpdate method
                ordersToUpdate.add(ord);
            }
        }

        System.debug('ordersToUpdate: ' + ordersToUpdate);

        if(ordersToUpdate != null && !ordersToUpdate.isEmpty()){
            Database.update(ordersToUpdate);
        }
    }
}