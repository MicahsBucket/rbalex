@isTest
public with sharing  class BatchPortalStoreUserNotifyTest 
{
    @testSetUp
    static void setUpTestData()
    {
        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name = 'Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();               
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Opportunity testOpp=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp;
        Opportunity testOpp2=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp2;
        Account store1 = [SELECT Id,Active_Store_Configuration__c from Account Where Name = '77 - Twin Cities, MN'];
        System.debug('_____store1______'+store1);        
        Store_Configuration__c storeConfig1 = [SELECT id,Invitation_Delay_Days__c,Activate_Community__c FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        storeConfig1.Sales_Tax__c = 5;
        storeConfig1.Activate_Community__c=true;
        storeConfig1.Invitation_Delay_Days__c=0;
        storeConfig1.Portal_Activation_Date__c=System.today()-1;
        storeConfig1.Remit_to_Company_Name__c ='TestLocation';
        update storeConfig1;
        dwelling1.Store_Location__c = store1.Id;
        dwellingsToInsert.add(dwelling1);
        insert dwellingsToInsert;        
        
        ID contactRType=[select id from RecordType where SobjectType='Contact' and name='Customer'].id;
        ID contactCustomerRType=[select id from RecordType where SobjectType='Contact' and name='Customer Contacts'].id;
        List<Contact> contactsToInsert = new List<Contact>();       
         
        Contact contact1 = new contact (Email='RatnaTest@Ratnatest.com', FirstName='Contact',LastName='1',AccountId=dwelling1.id, Primary_Contact__c =true,RecordTypeId=contactRType);
        contactsToInsert.add(contact1);
        
        Contact contact2 = new contact (Email='RatnaTest2@Ratnatest.com', FirstName='Contact',LastName='2',AccountId=dwelling1.id, Primary_Contact__c =false,RecordTypeId=contactRType);
        contactsToInsert.add(contact2);
        
        Contact contact3 = new contact (Email='RatnaTest3@Ratnatest.com', FirstName='Contact',LastName='3',AccountId=dwelling1.id, Primary_Contact__c =true,RecordTypeId=contactRType);
        contactsToInsert.add(contact3);
        insert contactsToInsert;
        
        List<OpportunityContactRole> oppRoles=new List<OpportunityContactRole>();
        OpportunityContactRole role1=new OpportunityContactRole(contactId=contact1.id,OpportunityId=testOpp.Id,Role='Decision Maker');
        OpportunityContactRole role2=new OpportunityContactRole(contactId=contact2.id,OpportunityId=testOpp.Id,Role='Maker');
        OpportunityContactRole role3=new OpportunityContactRole(contactId=contact3.id,OpportunityId=testOpp2.Id,Role='Decision Maker');
        OpportunityContactRole role4=new OpportunityContactRole(contactId=contact2.id,OpportunityId=testOpp2.Id,Role='Maker');
        oppRoles=new List<OpportunityContactRole>{role1,role2,role3,role4};
        insert oppRoles;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        Pricebook2 pricebook1 =  utility.createPricebook2Name('Standard Price Book');
        insert pricebook1;
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
            
        List<Order> ordersToInsert = new List<Order>();
        ID orderRType=[select id from RecordType where SobjectType='Order' and name='CORO Record Type'].id;
        Order order =  new Order(   Name='Sold Order', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 Primary_Contact__c=contact1.id,
                                 Secondary_Contact__c=contact2.id,
                                 OpportunityId=testOpp.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order);
        Order order2 =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact3.id,
                                 Primary_Contact__c=contact3.id,
                                 Secondary_Contact__c=contact2.id,
                                 OpportunityId=testOpp2.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order2);
        Order order3 =  new Order(   Name='Sold Order 2', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact3.id,
                                 Primary_Contact__c=contact1.id,
                                 Secondary_Contact__c=contact2.id,
                                 OpportunityId=testOpp2.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order3);
        test.startTest();
        insert ordersToInsert;
        List<OrderItem> orderItems = new List<OrderItem>();
        
        OrderItem orderItemMaster = new OrderItem(OrderId = Order.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100,Quote_Accepted__c = FALSE );
        orderItems.add(orderItemMaster);
        
        OrderItem orderItemMaster2 = new OrderItem(OrderId = Order2.id, PricebookentryId = pricebookEntryMaster.Id, Quantity = 2, UnitPrice = 100,Quote_Accepted__c = FALSE );
        orderItems.add(orderItemMaster2);
        
        insert orderItems;
        test.stopTest();
        
        /*List<Asset> assetsToInsert = new List<Asset>();
        Asset asset = new Asset (     Name='Asset1',
                                 Original_Order_Product__c = orderItemMaster.Id,
                                 Product2Id= masterProduct.Id,
                                 AccountId = dwelling1.id,
                                 ContactId = contact1.id,
                                 Variant_Number__c = '1234ABC',
                                 Unit_Wholesale_Cost__c = 200,
                                 Store_Location__c = store1.id,
                                 Quantity = 1,
                                 Price = 100,
                                 Status = 'Installed',
                                 Sold_Order__c = order.Id,
                                 PurchaseDate = Date.Today()
                                );
       
        assetsToInsert.add(asset);     
        */
    }
    @isTest
    static void createTestRecords() {      
        Test.startTest();
        //Id batchjobId = Database.executeBatch(new BatchPortalStoreUserNotify());
        //system.debug('Job Id :'+batchJobId);
        
        String CRON_EXP = '0 0 0 15 3 ? *';
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ScheduleBatchPortalUserNotify());
        
        
        List<Order> orderRecords=[select id,Apex_Context__c,Status,RecordTypeId,RecordTypeName__c,Primary_Contact__c,Secondary_Contact__c from order];
        
        for(Order orderRecord:orderRecords) {
            System.debug('_____orderRecord_____'+JSON.serialize(orderRecord));
            orderRecord.Apex_Context__c=true;
            orderRecord.Status='Cancelled'; 
        }
        Test.stopTest();
    }
    @isTest
    static void cancelSoldOrderTest()
    {
        Test.startTest();
        Order orderRecord=[select id,Apex_Context__c,Status,RecordTypeId,RecordTypeName__c,Primary_Contact__c,Secondary_Contact__c from order where Name='Sold Order' limit 1];
        System.debug('_____orderRecord_____'+orderRecord);
        orderRecord.Apex_Context__c=true;
        orderRecord.Status='Cancelled';
        
        update orderRecord;
        Test.stopTest();        
    }
	@isTest
    static void cancelSoldOrder1Test()
    {
        Test.startTest();
        Order orderRecord=[select id,Apex_Context__c,Status,RecordTypeId,RecordTypeName__c,Primary_Contact__c,Secondary_Contact__c from order where Name='Sold Order 1' limit 1];
        System.debug('_____orderRecord_____'+orderRecord);
        orderRecord.Apex_Context__c=true;
        orderRecord.Status='Cancelled';
        
        update orderRecord;
        Test.stopTest();
        
    }
}