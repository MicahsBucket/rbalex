/*******************************************************//**

@class    RMS_newWorkOrderRedirectExtensionTest

@brief    Tests the Work Order creation redirect page. Adapted from RMS_newMiscLaborExtensionTest.

@author  Creston Kuenzi (Slalom.CDK)

@version    2015-7-13  Slalom.CDK Created.
@version    2018-3-31  Gabe Rholl, Penrod. Edited.

@see        RMS_newWorkOrderRedirectExtension

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/
@isTest
private class RMS_newWorkOrderRedirectExtensionTest {

    /*******************************************************
                    setupData
    *******************************************************/
    @testSetup static void setupData(){

            WorkType measureWT = new WorkType();
            measureWT.Name = 'Measure';
            measureWT.DurationType = 'Hours';
            measureWT.EstimatedDuration = 2;
            measureWT.ShouldAutoCreateSvcAppt = true;
            measureWT.FSL__Exact_Appointments__c = true;
            insert measureWT;

            WorkType installWT = new WorkType();
            installWT.Name = 'Install';
            installWT.DurationType = 'Hours';
            installWT.EstimatedDuration = 8;
            installWT.ShouldAutoCreateSvcAppt = true;
            installWT.FSL__Exact_Appointments__c = true;
            insert installWT;

            WorkType serviceWT = new WorkType();
            serviceWT.Name = 'Service';
            serviceWT.DurationType = 'Hours';
            serviceWT.EstimatedDuration = 1;
            serviceWT.ShouldAutoCreateSvcAppt = true;
            serviceWT.FSL__Exact_Appointments__c = false;
            insert serviceWT;

            WorkType jobSiteVisitWT = new WorkType();
            jobSiteVisitWT.Name = 'Job Site Visit';
            jobSiteVisitWT.DurationType = 'Hours';
            jobSiteVisitWT.EstimatedDuration = 1;
            jobSiteVisitWT.ShouldAutoCreateSvcAppt = true;
            jobSiteVisitWT.FSL__Exact_Appointments__c = false;
            insert jobSiteVisitWT;
    }

    static testmethod void redirectHOAWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'hoa';
        customController.redirect();
        
    }

    static testmethod void redirectHistoricalWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'historical';
        customController.redirect();
        
    }

    static testmethod void redirectLSWPWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'lswp';
        customController.redirect();
        
    }

    static testmethod void redirectTechMeasureWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'techMeasure';
        customController.redirect();
        
    }

    static testmethod void redirectInstallWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'install';
        customController.redirect();
        
    }

    static testmethod void redirectPaintStainWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'paintStain';
        customController.redirect();
        
    }

    static testmethod void redirectPermitWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'permit';
        customController.redirect();
        
    }

    static testmethod void redirectCollectionsWorkOrderTest(){
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;

        PageReference pageRef = Page.RMS_newWorkOrderRedirect;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(new WorkOrder());
        RMS_newWorkOrderRedirectExtension customController  = new RMS_newWorkOrderRedirectExtension(stdController);
                             
        customController.testStoreLocation = '77 - Twin Cities, MN';
        customController.woType = 'collections';
        customController.redirect();
        
    }
    
}