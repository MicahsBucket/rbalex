@isTest
public with sharing class Peak_FeaturedGroupsControllerTest {

    // Note: For Group Membership, ensure that the type of user matches the type of group. Salesforce prevents
    // Internal Chatter Group (No Network Id) <=> Internal User (Has Standard/Internal Profile)
    // Community Chatter Group (Has Network Id) <=> Community User (Has Community/Partner profile)


    @isTest
    public static void testGetFeaturedGroups() {

        // Insert two  collab groups
        CollaborationGroup testGroup1 = Peak_TestUtils.createChatterGroup(Peak_TestConstants.TEST_GROUPNAME+'A','Public');
        insert testGroup1;
        CollaborationGroup testGroup2 = Peak_TestUtils.createChatterGroup(Peak_TestConstants.TEST_GROUPNAME+'B','Public');
        insert testGroup2;


        // Call action and cast result as Collab Group List
        Peak_Response peakResponse = Peak_FeaturedGroupsController.getFeaturedGroups((String)testGroup1.Id+','+(String)testGroup2.Id);
        List<CollaborationGroup> foundGroups = (List<CollaborationGroup>)peakResponse.results;

        System.assertEquals(foundGroups.size(), 2);

        // Test that the collab group name is accurate
        System.assertEquals(0, (foundGroups[0].Name).indexOf(Peak_TestConstants.TEST_GROUPNAME));
        System.assertEquals(0, (foundGroups[1].Name).indexOf(Peak_TestConstants.TEST_GROUPNAME));
    }



    @isTest
    public static void testGetPrefix() {
        system.assert(Peak_FeaturedGroupsController.getSitePrefix() != null);
    }



}