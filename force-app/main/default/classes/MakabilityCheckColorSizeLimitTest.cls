/**
 * @File Name          : MakabilityCheckColorSizeLimitTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/1/2019, 11:14:48 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/30/2019, 2:12:19 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public  class MakabilityCheckColorSizeLimitTest {


    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createColorSizeConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,55,55,'Even','Even','White','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorSizeLimit.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Size Limit - passed');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createColorSizeConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,60,60,'Even','Even','White','1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorSizeLimit.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Size Limit - passed - No records found.');
    }
    @isTest
    public static void noMatchingInteriorColorTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createColorSizeConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,55,55,'Even','Even','Blue','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorSizeLimit.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingInteriorColorTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Size Limit - passed');        
    }   
    @isTest
    public static void sizeExceedsLimitTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createColorSizeConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,65,65,'Even','Even','White','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorSizeLimit.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingExteriorColorTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsgTwo = results[0].errorMessages[1];
        system.assertEquals(assertMsg, 'Color Size Limit - The unit exceeds the Maximum Width of 60 Even inches for the interior color you have selected.');        
        system.assertEquals(assertMsgTwo, 'Color Size Limit - The unit exceeds the Maximum Height of 60 Even inches for the interior color you have selected.');        
    }   
     
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createColorSizeConfig(Product_Configuration__c pc){
        Color_Size_Limits__c csl = new Color_Size_Limits__c();
            csl.Interior_Color__c = 'White';
            csl.Maximum_Height_Fractions__c = 'Even';
            csl.Maximum_Height__c = 60;
            csl.Maximum_Width_Fractions__c = 'Even';
            csl.Maximum_Width__c = 60;
            csl.Product_Configuration__c = pc.id;
            insert csl;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, string wFrac, string hFrac,String inColor, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
		ProductConfiguration pc = new ProductConfiguration();
        pc.interiorColor = incolor;
        pc.widthInches = wInch;
        pc.widthFractions = wFrac;
        pc.heightInches = hInch;
        pc.heightFractions = hFrac;           
        pc.productId = prodId;
        oi.orderItemId = oiId;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
    
}