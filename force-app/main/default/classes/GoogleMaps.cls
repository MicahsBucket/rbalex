public with sharing class GoogleMaps {

	public static String duration {get;set;}
	public static Integer travelTime {get;set;}
	public static Decimal distance {get;set;}

	private static String apiKey {get;set;}

	@future(callout=true)
	public static void setRouteDistance(Id oId) {

			Map<String, RMS_Settings__c> RMS_Settings_map = RMS_Settings__c.getAll(); 
			RMS_Settings__c googleApiKey = RMS_Settings_map.get('Google API Server Key');
  		apiKey = googleApiKey != null ? googleApiKey.Value__c : null;

  		if(apiKey == null){
  			//QueryException e = new QueryException();
				//e.setMessage('Google API Server Key is missing from RMS Settings');
				//throw e;
				System.debug('Google API Server Key is missing from RMS Settings');
  		}

  		Order o = [Select Id, Route_Mileage__c, Store_Location__c, AccountId from Order where Id = :oId];
  		Decimal oldMileage = o.Route_Mileage__c;

  		Id storeLocationId = o.Store_Location__c;
      Id dwellingAccountId = o.AccountId;

  		List<Id> accountIds = new List<Id>{storeLocationId, dwellingAccountId};
			List<Account> distanceAccounts = [Select Id, ShippingStreet,
			    ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry
			    from Account where Id in :accountIds];

  		String address1 = encodeAccountAddress(distanceAccounts[0]);
  		String address2 = encodeAccountAddress(distanceAccounts[1]);

			String jsonResults = getJsonResults(address1, address2);
			jsonResults = formatJsonResults(jsonResults);
			
			updateJsonSections(jsonResults);

			if(distance != null && oldMileage != distance){
				o.Route_Mileage__c = distance;
				update o;
			}
			//return distance;
	}

	public static String getJsonResults(String address1, String address2) {
				
		String url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
				+ '?origins=' + address1
				+ '&destinations=' + address2
				+ '&mode=driving'
				+ '&sensor=false'
				+ '&language=en'
				+ '&units=imperial';

		if(apiKey != null){
			url += '&key=' + apiKey;
		}

		HTTPResponse resp = getGoogleMapsDistance(url);
		String jsonResults = resp.getBody().replace('\n', '');
		return jsonResults;

	}

	public static HttpResponse getGoogleMapsDistance(String url){
		
		HttpRequest req = new HttpRequest();
		Http http = new Http();
		req.setMethod('GET');
		req.setEndPoint(url);

 		return http.send(req);

	}

	public static String formatJsonResults(String value) {
		
		value = value.replace('{', ', ');
		value = value.replace('}', ', ');
		value = value.replace('[', ', ');
		value = value.replace(']', ', ');
		value = value.replace('"', '');
		
		return value;
	}

	public static String encodeAccountAddress(Account a) {
		return EncodingUtil.urlEncode(
			+ a.ShippingStreet + ' '
			+ a.ShippingCity + ', '
			+ a.ShippingState + ' '
			+ a.ShippingPostalCode + ' '
			+ a.ShippingCountry,
			'UTF-8');
	}

	public static void updateJsonSections(String jsonResults) {
				
			List<String> jsonSections = jsonResults.split(', ');
			
			for (Integer i = 0; i < jsonSections.size(); i++) {
					jsonSections[i] = jsonSections[i].trim();
					
					if (jsonSections[i].contains('duration :') || jsonSections[i].contains('duration:')) {
							duration = parseDuration(jsonSections[i + 1]);
							travelTime = parseTravelTime(duration);
					}
					
					if (jsonSections[i].contains('distance :') || jsonSections[i].contains('distance:')) {
							distance = parseDistance(jsonSections[i + 1]);
					}
			}
	}

	public static Decimal parseDistance(String value) {
			value = value.replace('text:', '');
			value = value.replace('text : ', '');
			value = value.replace(' mi', '');
			value = value.replace(' ft', '');
			value = value.replace(',', '');
			value = value.trim();
			
			return Decimal.valueOf(value);
	}

	public static String parseDuration(String value) {
			value = value.replace('text:', '');
			value = value.replace('text : ', '');
			
			return value;
	}

	public static Integer parseTravelTime(String value) {
	
			Integer tmpMinutes = 0;
	
			List<String> durationNodes = value.split(' ');
			String prevDurationNode = '';
			
			for (String durationNode : durationNodes) {
					if (durationNode == 'day' || durationNode == 'days') {
							tmpMinutes += Integer.valueOf(prevDurationNode) * 1440;
					}
					if (durationNode == 'hour' || durationNode == 'hours') {
							tmpMinutes += Integer.valueOf(prevDurationNode) * 60;
					}
					if (durationNode == 'min' || durationNode == 'mins') {
							tmpMinutes += Integer.valueOf(prevDurationNode);
					}
					
					prevDurationNode = durationNode;
			}
	
			return tmpMinutes;  
	}

}