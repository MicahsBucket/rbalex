/**
* @description  Getting ErrorLog latest record to display warning messages on opportunity detail
*/ 
public class FileUploaderController_ltng {
    
    
    @auraenabled
    public static ErrorLog__c returnErrorMessage(Id recordId)
    {
        ErrorLog__c error;
        List<ErrorLog__c> errorList=[select ErrorMessage__c,WarningMessage__c,CreatedDate from  ErrorLog__c where RelatedTo__c=:recordId ORDER BY CreatedDate DESC];
        if(errorList.size()>0)
        {
            error=  errorList[0];
        }
        return error;
        
    }
    
    
}