public with sharing class EditPOCommentsActionController {
    @AuraEnabled
    public static Purchase_Order__c getPOComments(Id recordId) {
        System.debug('EditPOCommentsActionController recordId: '+ recordId);
        Purchase_Order__c poToReturn = [SELECT Id, Status__c, Name, Comments__c, Confirmation_Number__c, 
                                            Requested_Ship_Date__c, Invoice_Number__c, Estimated_Ship_Date__c, Tax__c, 
                                            Processing_Type__c
                                            FROM Purchase_Order__c 
                                            WHERE Id =: recordId];

        return poToReturn;
    }

    @AuraEnabled
    public static string savePOComments(Id recordId, string comments, date requestedShipDate, date estimatedShipDate, string confirmationNumber, string invoiceNumber, decimal tax, string processingType) {
        System.debug('EditPOCommentsActionController recordId: '+ recordId);
        System.debug('EditPOCommentsActionController recordId: '+ processingType);
        Purchase_Order__c poToUpdate = [SELECT Id, Status__c, Name, Comments__c, Confirmation_Number__c, Requested_Ship_Date__c, Invoice_Number__c, 
                                            Estimated_Ship_Date__c, Tax__c, Processing_Type__c
                                            FROM Purchase_Order__c 
                                            WHERE Id =: recordId];
        poToUpdate.Comments__c = comments;
        poToUpdate.Requested_Ship_Date__c = requestedShipDate;
        poToUpdate.Confirmation_Number__c = confirmationNumber;
        poToUpdate.Invoice_Number__c = invoiceNumber;
        poToUpdate.Estimated_Ship_Date__c = estimatedShipDate;
        poToUpdate.Tax__c = tax;
        poToUpdate.Processing_Type__c = ProcessingType;
        update poToUpdate;
        return 'Success';
    }
}