public with sharing class newRefundRedirectExtension {
    private final Refund__c theRefund;

    public newRefundRedirectExtension(ApexPages.StandardController stdController) {
        this.theRefund = (Refund__c)stdController.getRecord();
        if (this.theRefund == null)
            this.theRefund = new Refund__c(); 
    }

    public String testStoreLocation;

    public PageReference redirect() { // Retrieve the key prefix for the payment object and insert it into the page reference
        Schema.DescribeSObjectResult r = Refund__c.sObjectType.getDescribe();
        String keyPrefix = r.getKeyPrefix();        
        PageReference p = new PageReference('/' +keyPrefix +'/e');

        // Get all of the url parameters from the current url and put them in the new url
        Map<String, String> m = p.getParameters();
//        m.putAll(ApexPages.currentPage().getParameters());
        m.put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        m.put('CF00N6100000Br7Xu', ApexPages.currentPage().getParameters().get('CF00N6100000Br7Xu'));
        m.put('CF00N6100000Br7Xu_lkid', ApexPages.currentPage().getParameters().get('CF00N6100000Br7Xu_lkid'));


        // If there is an order, redirect to deposit, otherwise redirect to misc cash receipt
        if (theRefund.Order__c == null) {
            m.put('RecordType', UtilityMethods.RecordTypeFor('Payment__c', 'Misc_Cash_Receipt'));

            // If a test is running just set the store name, otherwise get it from the current user
            String storeLocationName = (Test.isRunningTest()) ?     testStoreLocation :
                                                                    [SELECT Default_Store_Location__c FROM User WHERE Id =: UserInfo.getUserId()].Default_Store_Location__c;


            if (String.isBlank(storeLocationName)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_USER)); //
                return null;
            }
            
            for (Account thisStore : [SELECT Id, Name FROM Account WHERE Name =: storeLocationName]) {
                    // TODO:   This parameter will need to be updated after pushing to build and production
                    m.put('CF00N6100000Br7Y0', thisStore.Name); 
                    m.put('CF00N6100000Br7Y0_lkid', thisStore.Id);  
            }
            
            if (m.get('CF00N6100000Br7Y0') == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_USER)); //
                return null;
            }
            


        } 
        else {
            m.put('RecordType', UtilityMethods.RecordTypeFor('Payment__c', 'Misc_Cash_Receipt'));
            for (Order theOrder : [SELECT Id, AccountId, Primary_Contact__c, Store_Location__c FROM Order WHERE Id =: theRefund.Order__c]) {
                if (theOrder.Store_Location__c == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.STORE_LOCATION_IS_BLANK_PAYMENT)); //
                    return null;
                } 
                else {
                    // TODO:   These parameter will need to be updated after pushing to build and production
                    m.put('CF00N6100000Br7Y0', theOrder.AccountId);    
                    m.put('CF00N6100000Br7Y0_lkid', theOrder.Store_Location__c);    
                    m.put('CF00N6100000H70kB', theOrder.Primary_Contact__c);    
                    // m.put('CF00N6100000H70kB_lkid', theOrder.BillToContactId);  
                }
            }       
        }
        m.put('nooverride', '1');
        return p;
    }
}