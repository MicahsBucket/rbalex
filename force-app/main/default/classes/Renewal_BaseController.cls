public with sharing class Renewal_BaseController {
    @AuraEnabled
    public static Renewal_Response checkIfSuperAdmin(Boolean testThrowException){
        Renewal_Response renewalResponse = new Renewal_Response();
        try {
            if(Test.isRunningTest() && testThrowException) {
                CalloutException testException = new CalloutException();
                testException.setMessage('This is a constructed exception for testing and code coverage');
                throw testException;
            }
            Renewal_ContentObject renewalContentObject = new Renewal_ContentObject();
            List<String> permissionLabels = new List<String>();
            for(Renewal_Super_Admin_Permission_Set__mdt adminPermissionSet : [SELECT Id, Label FROM Renewal_Super_Admin_Permission_Set__mdt]){
                permissionLabels.add(adminPermissionSet.Label);
            }
            renewalContentObject.rforceAdmin = false;
            renewalContentObject.dashboardAdmin = false;
            for(PermissionSetAssignment permissionAssignment : [SELECT Id, PermissionSet.Name, PermissionSet.Label, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Label IN : permissionLabels]){
                if(permissionAssignment.PermissionSet.Label.containsIgnoreCase('rforce')){
                    renewalContentObject.rforceAdmin = true;
                }
                if(permissionAssignment.PermissionSet.Label.containsIgnoreCase('dashboard')){
                    renewalContentObject.dashboardAdmin = true;
                }
            }
            renewalResponse.renewalResults.add(renewalContentObject);
            renewalResponse.success = true;
        } catch (Exception e){
            renewalResponse.success = false;
            renewalResponse.messages.add(e.getMessage());
        }
        return renewalResponse;
    }

}