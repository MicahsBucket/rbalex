@isTest
public class BatchJobServiceResourceTest2 {

    public static List<Contact> contactList;
    
    @testSetup
    private static void setup() {
    
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        insert customSetting1;
        
        TestUtilityMethods util = new TestUtilityMethods();
        Account a = util.createAccount('Acme');
        insert a;
    
        RMS_Settings__c customSetting2 = new RMS_Settings__c(Value__c = a.Id, Name='Unassigned Account Id');
        insert customSetting2;
        
        Account storeAccount = new Account();
        storeAccount.Name = 'StoreAccount';
        storeAccount.Type = 'CORO';
        storeAccount.recordTypeID = getRecordTypeId('Account', 'Store');
        insert storeAccount;
        
        group partnergroup = [select id , name, developername from  group Where developername = 'All_Partner_Users_RbA'];
        AccountShare accountShare = new AccountShare();

            accountShare.AccountId = storeAccount.Id;
            accountShare.UserOrGroupId = partnergroup.id;
            accountShare.AccountAccessLevel = 'Edit';
            accountShare.OpportunityAccessLevel = 'Edit';

        insert accountShare;
        
        
        List<Account> accountList = new List<Account>();
        contactList = new List<Contact>();
        String myLastNamePrefix = 'LastName';
        Integer myLastNameCounter = 1;
        
        
        // Create Accounts
        for(Integer i = 0; i < 5; i++) {
            accountList.add(new Account(
                Name = 'contractorAccount' + i,
                recordTypeID = getRecordTypeId('Account', 'Contractor'),
                WorkComp_EmplLiab_ExpireDate__c = system.today()-25
            ));
        }
        
        insert accountList;
        
        // Create Contacts
        for(Account acct : accountList) {
            contactList.add(new Contact(
                AccountId = acct.Id, 
                LastName = myLastNamePrefix + '' + myLastNameCounter
                ));
            myLastNameCounter++;
        }
        insert contactList;
        System.debug('Contacts: ' + contactList);
        
        //create store configuration
        Store_Configuration__c storeConfig = new Store_Configuration__c();
        storeConfig.Order_Number__c = 1;
        storeConfig.Store__c = storeAccount.Id;
        insert storeConfig;
    }
    
    @isTest
    public static void testBatch() {
        
        test.starttest();
        
        //create store configuration
        Store_Configuration__c storeConfig = [select id, Store__c from Store_Configuration__c  Order By Createddate Desc Limit 1];
        
        // Assign PermissionSet to Users
       // list<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        list<user> commlistuser = createCommUser(4);
        User runUser = commlistuser [0];
        /*PermissionSet ps = [select Id, Name from PermissionSet where Name = 'FSL_Resource_License' limit 1];
        PermissionSet psAgent = [select Id, Name from PermissionSet where Name = 'FSL_Resource_Permissions' limit 1];
        psaList.add(new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = runUser.Id));
        psaList.add(new PermissionSetAssignment (PermissionSetId = psAgent.Id, AssigneeId = runUser.Id));
        insert psaList;*/

        System.runAs(runUser){
        
            List<ServiceResource> serviceResourceList = new List<ServiceResource>();
            
                serviceResourceList.add(new ServiceResource(
                Name = runUser.lastName,
                ResourceType = 'T',
                RelatedRecordId = runUser.Id,
                Retail_Location__c = storeConfig.Id,
                IsActive = true,
                User_Type__c = 'External'
                ));
            
            insert serviceResourceList;
           
            BatchJobServiceResource batchSR = new BatchJobServiceResource();
            Database.executeBatch(batchSR);
            
        }
        Test.stopTest();
    }
    
    private static List<User> createCommUser(Integer numberOfAccounts) {
    
        list<user> myUserList = new list<user>();
        
        Map<String, Id> profileMap = getProfileMap();
      //  UserRole userRole_1 = [SELECT Id FROM UserRole limit 1];
       
        //Create Users
        for(Contact c : [select id , lastName from contact order by createddate Desc Limit 5]) {
            myUserList.add(new User(
                ContactId = c.Id,
                Username = c.lastName + '@example.com',
                Email = c.lastName + '@example.com',
                LastName = c.lastName,
                Alias = c.lastName.right(6),
                ProfileID = profileMap.get('Partner RMS-RSR'),
               // UserRoleId = userRole_1.Id,
                PortalRole = 'Manager',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago',
                EmailEncodingKey='UTF-8'
                ));
        }
        insert myUserList;
        return myUserList;
    }
     
     
     
    private static String getRecordTypeId(String sObjectType, String sObjectTypeDeveloperName) {
        //assuming only one sObjectType is being passed in.

        // sObject types to describe
        String[] types = new String[]{sObjectType};

        // Make the describe call
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);

        // For each returned result, get some info
        for(Schema.DescribeSobjectResult res : results) {
            // Get child relationships
            for(Schema.RecordTypeInfo rti : res.getRecordTypeInfosByDeveloperName().values()) {
                if(rti.getDeveloperName() == sObjectTypeDeveloperName) {
                    return rti.getRecordTypeId();
                }
            }
        }
        return '';
    }
    
    private static Map<String, Id> getProfileMap() {
        List<Profile> profileList = [select Id, Name from Profile];
        Map<String, Id> profileMap = new Map<String, Id>();
        for(Profile p : profileList) {
            profileMap.put(p.Name, p.Id);
        }
        return profileMap;
    }
}
/* @isTest

private class BatchJobServiceResourceTest {

    public static List<User> userList;
    
    @testSetup
    private static void setup() {

        //Creating RMS_Settings__c
		RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        System.debug([SELECT Id from RMS_Settings__c]);
        
        TestUtilityMethods util = new TestUtilityMethods();
        Account a = util.createAccount('Acme');
		insert a;
        System.debug('Accounts: ' + [Select Id from Account]);

		RMS_Settings__c customSetting2 = new RMS_Settings__c(
			Value__c = a.Id, Name='Unassigned Account Id');
		insert customSetting2;
        System.debug('RMS_Settings: ' + [SELECT Id from RMS_Settings__c]);
        
        Account storeAccount = new Account();
        storeAccount.Name = 'StoreAccount';
        storeAccount.Type = 'CORO';
        storeAccount.recordTypeID = getRecordTypeId('Account', 'Store');
        insert storeAccount;
        System.debug('Accounts: ' + [Select Id from Account]);
        
        
        List<Account> accountList = createContractorAccounts(4);

        //create store configuration
        Store_Configuration__c storeConfig = new Store_Configuration__c();
        storeConfig.Order_Number__c = 1;
        storeConfig.Store__c = storeAccount.Id;

        insert storeConfig;
        System.debug('Store Configs: ' + [Select Id from Store_Configuration__c]);

        //create users
        userList = createCustomerUsers(accountList);

        //create serviceResources
        List<ServiceResource> serviceResourceList = new List<ServiceResource>();
        
        
        
        System.debug('Users: ' + [select Id from User]);

        insert serviceResourceList;
    }

    @isTest
    public static void testBatch() {
        Test.startTest();
        
        permissionSetAssignment();
        
        createExternalServiceResources(userList, [Select Id from Store_Configuration__c]);

        //find how many service resource accounts have a close date within 15 days
        //if none, rig it so there are some.

        List<ServiceResource> mySRList = [SELECT Auto_Insurance_Expiration_Date__c,General_Insurance_Expiration_Date__c,Id,Date_To_Make_Inactive__c,Work_Comp_Insurance_Expiration_Date__c FROM ServiceResource];
        Integer acctsModified = 0;
        List<ServiceResource> within15days = new List<ServiceResource>();
        List<ServiceResource> after15days = new List<ServiceResource>();
        for(ServiceResource i : mySRList) {
            if(i.Date_To_Make_Inactive__c < Date.Today()) {
                within15days.add(i);
            }
            else(after15days.add(i));
        }

        if(within15days.size() == 0) {
            List<Account> myAccount = [select Id, Auto_Liab_Expire_Date__c from Account limit 1];
            myAccount[0].Auto_Liab_Expire_Date__c = Date.Today() -30;
            acctsModified++;
        }
        if(acctsModified > 0) {  //if we modified some accounts, update these accounts
            update after15days;
        }

        BatchJobServiceResource batchSR = new BatchJobServiceResource();
        Database.executeBatch(batchSR);
        Test.stopTest();

        //assert that we have either the number of accounts within 15 days that are listed as inactive, or that the number of accounts that we modified are inactive.
        //if acctsModified == 0, then within15days size is whatever it is, otherwise within15days is 0, so whatever acctsModified is what it is.
        System.assertEquals(acctsModified + within15days.size(), [SELECT count() FROM ServiceResource WHERE IsActive = False]);    
        
        
    }

    private static Map<String, Id> getProfileMap() {
        List<Profile> profileList = [select Id, Name from Profile];
        Map<String, Id> profileMap = new Map<String, Id>();
        for(Profile p : profileList) {
            profileMap.put(p.Name, p.Id);
        }
        return profileMap;
    }

    private static String getDeveloperName(String sObjectType) {
        //assuming only one sObjectType is being passed in.

        // sObject types to describe
        String[] types = new String[]{sObjectType};

        // Make the describe call
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);

        // For each returned result, get some info
        for(Schema.DescribeSobjectResult res : results) {
            // Get child relationships
            for(Schema.RecordTypeInfo rti : res.getRecordTypeInfosByDeveloperName().values()) {
                return rti.getDeveloperName();
                
            }
        }
        return null;
    }

    private static String getRecordTypeId(String sObjectType, String sObjectTypeDeveloperName) {
        //assuming only one sObjectType is being passed in.

        // sObject types to describe
        String[] types = new String[]{sObjectType};

        // Make the describe call
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);

        // For each returned result, get some info
        for(Schema.DescribeSobjectResult res : results) {
            // Get child relationships
            for(Schema.RecordTypeInfo rti : res.getRecordTypeInfosByDeveloperName().values()) {
                if(rti.getDeveloperName() == sObjectTypeDeveloperName) {
                    return rti.getRecordTypeId();
            	}
        	}
        }
        return '';
    }

    private static List<User> createCustomerUsers(List<Account> accountList) {
        //for each account, do this
        List<Contact> contactList = new List<Contact>();
        List<User> myUserList = new List<User>();
        String myLastNamePrefix = 'LastName';
        Integer myLastNameCounter = 1;
        Map<String, Id> profileMap = getProfileMap();

        for(Account acct : accountList) {
            contactList.add(new Contact(
                AccountId = acct.Id, 
                LastName = myLastNamePrefix + '' + myLastNameCounter
                ));
            myLastNameCounter++;
        }
        insert contactList;
        System.debug('Contacts: ' + contactList);
        
        for(Contact c : contactList) {
            myUserList.add(new User(
                ContactId = c.Id,
                Username = c.lastName + '@example.com',
                Email = c.lastName + '@example.com',
                LastName = c.lastName,
                Alias = c.lastName.right(6),
                ProfileID = profileMap.get('Partner RMS-RSR'),
                PortalRole = 'Manager',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago',
                EmailEncodingKey='UTF-8'
                ));
        }
        insert myUserList;       

        return myUserList;
    }

    
    private static void permissionSetAssignment() {
        //find FSL Agent License permission set
        
        System.debug(userList);
        PermissionSet ps = [select Id, Name from PermissionSet where Name = 'FSL_Agent_License' limit 1];
        System.debug(ps);
        
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        for(User u : userList) {
            System.debug('User: ' + u);
            psaList.add(new PermissionSetAssignment (PermissionSetId = ps.Id, AssigneeId = u.Id));
        }
        
        insert psaList;    
    }
    
    private static List<Account> createContractorAccounts(Integer numberOfAccounts) {
        List<Account> accountList = new List<Account>();

        for(Integer i = 0; i < numberOfAccounts; i++) {
            accountList.add(new Account(
                Name = 'contractorAccount' + i,
                recordTypeID = getRecordTypeId('Account', 'Contractor')
            ));
        }
        
        insert accountList;
        
        return accountList;
    }

    private static List<ServiceResource> createExternalServiceResources(List<User> userList, Store_Configuration__c sc) {
        List<ServiceResource> serviceResourceList = new List<ServiceResource>();
        System.debug('UserList size: ' + userList.size());
        for(User u : userList) {
            serviceResourceList.add(new ServiceResource(
                Name = u.lastName,
                ResourceType = 'T',
                RelatedRecordId = u.Id,
                Retail_Location__c = sc.Id,
                IsActive = true,
                User_Type__c = 'External'
            ));
        }

        insert serviceResourceList;
        
        return serviceResourceList;

    }
}
*/