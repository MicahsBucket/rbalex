@isTest(SeeAllData=false)
global class CanvassUnitQueueableDeletionBatchTest {

    public static testMethod void test1()
    {   
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Queued';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789';
        insert canvassDataLayerQueue;
        
        Test.startTest();
        database.executebatch(new CanvassUnitQueueableBatch(),1);
        
        CanvassUnitQueueableDeletionBatch rsb = new CanvassUnitQueueableDeletionBatch();
        String sch = '0 0 * * * ?';
        system.schedule('Canvass Unit Queueable Deletion Batch 999', sch, rsb);
    
        Test.stopTest();
    }
}