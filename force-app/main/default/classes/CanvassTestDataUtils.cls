@isTest
public class CanvassTestDataUtils {
    public static CNVSS_Canvass_Market__c createTestCanvassMarket() {
        
        CNVSS_Canvass_Market__c testMarket = new CNVSS_Canvass_Market__c(
            Name='TestMarket', 
            CNVSS_ISC_Phone_Number__c = '555-555-5555', 
            CNVSS_ISC_Email__c = 'harryh@slalom.com',
            CNVSS_Canvass_Market_Manager__c = UserInfo.getUserId()
        );
        insert testMarket;
        
        User u = new User();
        u.Id = UserInfo.getUserId();
        u.Canvass_Market__c = 'TestMarket';
        update u;
        
        return testMarket;
    }
    
    public static CNVSS_Canvass_Unit__c createTestCanvassUnit(ID canvassMarketId, boolean doNotKnock) {
        CNVSS_Canvass_Unit__c cu = new CNVSS_Canvass_Unit__c();
        cu.CNVSS_Canvass_Market__c = canvassMarketId;
        double randomNumber = math.round(math.random()*10000);
        cu.CNVSS_Homeowner_First_name__c = 'FName_' + randomNumber;
        cu.CNVSS_Homeowner_Last_Name__c = 'LName_' + randomNumber;
        cu.CNVSS_Is_Sole_Owner__c = true;
        cu.CNVSS_House_No__c = string.valueOf(randomNumber);
        cu.CNVSS_Street__c = 'TestTest Main St.';
        cu.CNVSS_City__c = 'TestCity';
        cu.CNVSS_State__c = 'TX';
        cu.CNVSS_Zip_Code__c = '00000';
        cu.CNVSS_Address_Source__c = 'RbA';
        cu.CNVSS_PK_Full_Address__c = cu.CNVSS_House_No__c + ' ' + cu.CNVSS_Street__c + ', ' + cu.CNVSS_City__c + ', ' + cu.CNVSS_State__c + ', ' + cu.CNVSS_Zip_Code__c;
        if(doNotKnock) {
            cu.CNVSS_DO_NOT_CONTACT__c = true;
        }
        insert cu;
        return cu;
    }
    
    public static SMA__MARoute__c createTestRoute() {
        SMA__MARoute__c testRoute = new SMA__MARoute__c(Name = 'T3st_R0ut3',
                                                       sma__Date__c = date.today()
                                                       );
        testRoute.OwnerId = UserInfo.getUserId();
        insert testRoute;
        return testRoute;
    }
    
    public static SMA__MAWaypoint__c createTestWaypoint(CNVSS_Canvass_Unit__c cu, sma__maroute__c route, string notes) {
        SMA__MAWaypoint__c testWp = new SMA__MAWaypoint__c(Name = cu.CNVSS_Homeowner_First_name__c + ' ' + cu.CNVSS_Homeowner_Last_Name__c,
                                     SMA__Address__c = cu.CNVSS_PK_Full_Address__c,
                                     SMA__Route__c = route.Id,
                                     SMA__BaseObject__c = 'CNVSS_Canvass_Unit__c',                       
                                     SMA__LinkId__c = cu.Id,
                                     SMA__HasAddress__c = true,                     
                                     SMA__Notes__c = notes);
        insert testWp;
        return testWp;
    }
    
    public static List<CNVSS_Canvass_Unit__c> createTestCanvassUnits(ID canvassMarketId) {
        List<CNVSS_Canvass_Unit__c> canvassUnits = new List<CNVSS_Canvass_Unit__c>();
        for(integer i = 0; i < 4; i++) {
            CNVSS_Canvass_Unit__c cu = new CNVSS_Canvass_Unit__c();
            cu.CNVSS_Canvass_Market__c = canvassMarketId;
            double randomNumber = math.round(math.random()*10000);
            cu.CNVSS_Homeowner_First_name__c = 'FName_' + randomNumber;
            cu.CNVSS_Homeowner_Last_Name__c = 'LName_' + randomNumber;
            cu.CNVSS_Is_Sole_Owner__c = true;
            cu.CNVSS_House_No__c = string.valueOf(randomNumber);
            cu.CNVSS_Street__c = 'TestTest Main St.';
            cu.CNVSS_City__c = 'TestCity';
            cu.CNVSS_State__c = 'TX';
            cu.CNVSS_Zip_Code__c = '00000';
            cu.CNVSS_Address_Source__c = 'RbA';
            cu.CNVSS_PK_Full_Address__c = cu.CNVSS_House_No__c + ' ' + cu.CNVSS_Street__c + ', ' + cu.CNVSS_City__c + ', ' + cu.CNVSS_State__c + ', ' + cu.CNVSS_Zip_Code__c;
            canvassUnits.add(cu);
        }
        insert canvassUnits;
        return canvassUnits;
    }
    
    public static CNVSS_Canvass_Lead_Sheet__c createTestCanvassLeadSheet(CNVSS_Canvass_Unit__c cu, string result) {
        CNVSS_Canvass_Lead_Sheet__c testLeadSheet = new CNVSS_Canvass_Lead_Sheet__c(
            Canvass_Result__c = 'Not Home',
            CNVSS_Canvass_Unit__c = cu.Id,
            CNVSS_Home_Owner_1__c = cu.CNVSS_Homeowner_First_name__c + ' ' + cu.CNVSS_Homeowner_Last_Name__c
        );
        insert testLeadSheet;
        return testLeadSheet;
    }
    
    public static List<selectoption> testGetWindowsDrafty() {           
        list<selectoption> options = new list<selectoption>();              
        //Window Drafty is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Windows_Drafty__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }        
        return options; 
    }
    
    public static List<selectoption> testGetWindowTypes() {           
        list<selectoption> options = new list<selectoption>();        
        //Window Type is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Window_Type__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }        
        return options; 
    }

    public static List<selectoption> testGetWindowStyles() {           
        list<selectoption> options = new list<selectoption>();         
        //Window Style is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Window_Style__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }         
        return options; 
    }    

    public static List<selectoption> testGetPaneTypes() {           
        list<selectoption> options = new list<selectoption>();              
        //Pane Type is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Pane_Information__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }         
        return options; 
    }

    public static List<selectoption> testGetWindowsRotting() {           
        list<selectoption> options = new list<selectoption>();        
        //Window Rotting is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Windows_Rotting__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }        
        return options; 
    } 

    public static List<selectoption> testGetWindowsWarping() {           
        list<selectoption> options = new list<selectoption>();            
        //Window Warping is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Windows_Warping__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }         
        return options; 
    }
    
    public static List<selectoption> testGetWindowsCondensation() {           
        list<selectoption> options = new list<selectoption>();             
        //Window Condensation is a MultiSelect Picklist               
        Schema.DescribeFieldResult fieldResult = CNVSS_Canvass_Lead_Sheet__c.CNVSS_Windows_Condensation__c.getDescribe();
        
        list<schema.picklistentry> values = fieldResult.getPickListValues();               
        for (Schema.PicklistEntry a : values) 
        {                  
            options.add(new SelectOption(a.getLabel(), a.getValue()));
        }         
        return options; 
    }
}