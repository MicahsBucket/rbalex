/*
* @author Jason Flippen
* @date 01/07/2020 
* @description Class to provide functionality for the rejectPurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - RejectPOActionControllerTest
*/
public with sharing class RejectPOActionController {

    /*
    * @author Jason Flippen
    * @date 01/07/2020
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        String currentUserProfileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.status = purchaseOrder.Status__c;
        wrapper.currentUserProfileName = currentUserProfileName.toLowerCase();

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 01/07/2020
    * @description Method to update the Purchase Order Status to "In Process".
    * @param purchaseOrderId
    * @returns String containing the save result (Success/Error)
    */
    @AuraEnabled
    public static String rejectPurchaseOrder(Id purchaseOrderId) {
        
        String returnResult = null;

        // Get Purchase Order.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Released_Timestamp__c,
                                                  Submitted_Timestamp__c,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];
        
        // If Purchase Order is rejected its Status is set to "In Process".
        if (purchaseOrder.Status__c == 'Released') {

            purchaseOrder.Status__c = 'In Process';
            purchaseOrder.Released_Timestamp__c = null;
            purchaseOrder.Submitted_Timestamp__c = null;
            purchaseOrder.Rejection_Date__c = System.Now();

            try {
                update purchaseOrder;
                returnResult = 'Reject PO Success';
            }
            catch (Exception ex) {
                System.debug('***** PO Update Error: ' + ex);
                returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
            }

        }
        
        return returnResult;
        
    }


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 01/07/2020
    * @description Wrapper Class for Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String currentUserProfileName {get;set;}

    }

}