global class BatchSchedulePostInstallSurvey implements Schedulable {
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        BatchJobPostInstallationSurvey obj = new BatchJobPostInstallationSurvey ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(obj, 5);
    }
   
}