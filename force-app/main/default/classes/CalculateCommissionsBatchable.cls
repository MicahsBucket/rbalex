global class CalculateCommissionsBatchable implements Database.Batchable<sObject> {

	global final Comm_Payment_Event__c cpe;

    global CalculateCommissionsBatchable(){
        
    	List<Comm_Payment_Event__c> cpeList = [Select Id, Pay_Period_End_Date__c, 
		  	Pay_Period_Start_Date__c, Payment_Type__c 
		    from Comm_Payment_Event__c 
		    where Pay_Period_End_Date__c < Today and Payment_Calculated__c = false
		    order by Pay_Period_End_Date__c asc 
		    limit 1];
        
        if(!cpeList.isEmpty())
            cpe = cpeList[0];

	}
		
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//String query = 'Select Id, ' +
		//	'Payment_Calculated__c, Pay_Period_End_Date__c, Payment_Type__c,' +
		//	'Pay_Period_Start_Date__c ' +
		//	'from Comm_Payment_Event__c ' + 
		//	'where Pay_Period_End_Date__c < Today and Payment_Calculated__c = false ' +
		//	'order by Pay_Period_End_Date__c asc';
		String query = 'Select Id, Name, isActive, ManagerId from User where Commission_User__c = true';

		if(Test.isRunningTest())
			query += ' order by lastmodifieddate desc limit 100';

		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<User> scope) {

  	System.debug('Execute commissions');
  	System.debug(scope);
    System.debug(cpe);
      
    if(cpe == null)
        return;

  	CommissionsManagement.createRetroLogs(cpe, scope);

  	//for(Comm_Payment_Event__c cpe: scope){
  	//	cpe.Payment_Calculated__c = true;
			//CommissionsManagement.calculatePaymentEvent(cpe);
  	//}
  	//update scope;

	}
	
	global void finish(Database.BatchableContext BC) {
		CommissionsManagement.calculatePaymentEvent(cpe);
	}
	
}