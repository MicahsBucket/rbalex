@isTest
public without sharing class SkillRequirementTriggerHandlerTest {

    @testSetup static void setupData(){

            WorkType measureWT = new WorkType();
            measureWT.Name = 'Measure';
            measureWT.DurationType = 'Hours';
            measureWT.EstimatedDuration = 2;
            measureWT.ShouldAutoCreateSvcAppt = true;
            measureWT.FSL__Exact_Appointments__c = true;
            insert measureWT;

            WorkType installWT = new WorkType();
            installWT.Name = 'Install';
            installWT.DurationType = 'Hours';
            installWT.EstimatedDuration = 8;
            installWT.ShouldAutoCreateSvcAppt = true;
            installWT.FSL__Exact_Appointments__c = true;
            insert installWT;

            WorkType serviceWT = new WorkType();
            serviceWT.Name = 'Service';
            serviceWT.DurationType = 'Hours';
            serviceWT.EstimatedDuration = 1;
            serviceWT.ShouldAutoCreateSvcAppt = true;
            serviceWT.FSL__Exact_Appointments__c = false;
            insert serviceWT;

            WorkType jobSiteVisitWT = new WorkType();
            jobSiteVisitWT.Name = 'Job Site Visit';
            jobSiteVisitWT.DurationType = 'Hours';
            jobSiteVisitWT.EstimatedDuration = 1;
            jobSiteVisitWT.ShouldAutoCreateSvcAppt = true;
            jobSiteVisitWT.FSL__Exact_Appointments__c = false;
            insert jobSiteVisitWT;
    }

    @isTest
    public static void testValidateScheduledWorkOrders() {
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        insert customSetting1;

        Id techMeasureRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId();
        
        WorkOrder wo = new WorkOrder();
        wo.RecordTypeId = techMeasureRTId;
        wo.Status = 'Scheduled & Assigned';
        insert wo;

        Skill skill = [SELECT Id FROM Skill LIMIT 1];

        SkillRequirement skillReq = new SkillRequirement();
        skillReq.RelatedRecordId = wo.Id;
        skillReq.SkillId = skill.Id;
        try {
            insert skillReq;    
        } catch(Exception e) {
            System.assertEquals('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION' +
                                ', This Work Order has already been scheduled, you may not add additional skills to this Work Order.' +
                                ' If you need to adjust the skills, cancel the appointment first.: []', e.getMessage());
        }
        
    }

    @isTest
    public static void testValidateNonscheduledWorkOrders() {
        RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
        insert customSetting1;

        Id techMeasureRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId();

        WorkOrder wo = new WorkOrder();
        wo.Status = 'Canceled';
        wo.Cancel_Reason__c = 'Duplicate';
        wo.RecordTypeId = techMeasureRTId;
        insert wo;

        Skill skill = [SELECT Id FROM Skill LIMIT 1];

        SkillRequirement skillReq = new SkillRequirement();
        skillReq.RelatedRecordId = wo.Id;
        skillReq.SkillId = skill.Id;
        insert skillReq;

        List<SkillRequirement> insertedSkillReqs = [SELECT Id FROM SkillRequirement WHERE SkillId =: skill.Id];
        System.assert(insertedSkillReqs.size() > 0);

    }    
}