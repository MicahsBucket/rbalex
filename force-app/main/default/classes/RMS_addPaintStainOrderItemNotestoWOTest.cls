@isTest

public with sharing class RMS_addPaintStainOrderItemNotestoWOTest{

	public static final String dwellingName = 'Dwelling Account';
	public static final String postInsertDwellingName = '11111, ' + dwellingName;

 	static testMethod void testAddingPaintStainOrderItemNotes(){

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
       
       	id miscJobChargeRecordTypeID = UtilityMethods.retrieveRecordTypeId('Misc_Job_and_Unit_Charges', 'Product2');
        
       	Account account1 = utility.createVendorAccount('Vendor Account 1 ');
		insert account1;

		Account account2 = new Account(	Name = 'RbA',
										AccountNumber = '1234567890',
										Phone = '(763) 555-2000'
									);
		insert account2;

		Account dwelling = utility.createDwellingAccount(dwellingName);

		Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
		dwelling.Store_Location__c = store.Id;
		insert dwelling;

	
		Opportunity opp1 = utility.createOpportunity(dwelling.id, 'Closed - Won');
		insert opp1;
	
		//Store_Configuration__c stconfig = new Store_Configuration__c(Store__c = store.Id);
		//insert stconfig;
		
		Financial_Account_Number__c fan = new Financial_Account_Number__c(Name ='Finan Acc', Account_Type__c = 'Cost PO');
		insert fan;
		Product2 product1 = new Product2(
			Name='Paint/Stain',
			Vendor__c = account1.id,
			Cost_PO__c = true,
			isActive = true,
			RecordTypeId = miscJobChargeRecordTypeID,
			Account_Number__c =  fan.Id
		);
		
		insert product1;
		
		PricebookEntry pricebookEntry1 = utility.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
		insert pricebookEntry1;
		
		List<Order> orderList =  new List<Order>{ new Order(	Name='Order1', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,
									Opportunity = opp1,									
									Status ='Draft', 
									Tech_Measure_Status__c = 'New',
									Pricebook2Id = Test.getStandardPricebookId()
								),	new Order(	Name='Order2', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,
									Opportunity = opp1, 									
									Status ='Draft',
									Tech_Measure_Status__c = 'New', 
									Pricebook2Id = Test.getStandardPricebookId()
								),	new Order(Name='Order3', 
									AccountId = dwelling.id, 
									EffectiveDate= Date.Today(), 
									Store_Location__c = store.Id,
									Opportunity = opp1, 									
									Status ='Draft', 
									Install_Order_Status__c = 'New',
									Pricebook2Id = Test.getStandardPricebookId()
								)};		
		insert orderList;

		List<OrderItem> oiList = new List<OrderItem>{new OrderItem(OrderId = OrderList[0].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100, Notes__c = 'Instructions for stain'),
				new OrderItem(OrderId = OrderList[1].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100, Notes__c = 'Instructions for stain' ),
				new OrderItem(OrderId = OrderList[1].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100, Notes__c = 'Instructions for stain' )};
		insert oiList;
        

        Id paintstainWORecordTypeId = UtilityMethods.retrieveRecordTypeId('Paint_Stain', 'WorkOrder');
        
        
        List<WorkOrder> woList = new List<WorkOrder>();
        WorkOrder rwo1 = new WorkOrder(
            recordTypeId = paintstainWORecordTypeId,
            Sold_Order__c = OrderList[0].Id,
            Work_Order_Type__c = 'Paint/Stain',
            Opportunity__c = opp1.Id
        );
        woList.add(rwo1);

        WorkOrder rwo2 = new WorkOrder(
            recordTypeId = paintstainWORecordTypeId,
            Sold_Order__c = OrderList[1].Id,
            Work_Order_Type__c = 'Paint/Stain',
            Opportunity__c = opp1.Id
        );
        woList.add(rwo2);

        WorkOrder rwo3 = new WorkOrder(
            recordTypeId = paintstainWORecordTypeId,
            Sold_Order__c = OrderList[2].Id,
            Work_Order_Type__c = 'Paint/Stain',
            Opportunity__c = opp1.Id
        );
        woList.add(rwo3);

        insert woList;
        
    }
    


}