/**
* @author Bill Hodena
* @group Customer Portal
* @date 5/1
* @description Unit tests for OrderTrackerController
**/
@isTest 
private class OrderTrackerControllerTest {

	@testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();
		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		Database.insert(storeAccount, true);
		Id storeAccountId = storeAccount.Id;
		Id storeConfigId = storeAccount.Active_Store_Configuration__c;
		Id accountId = TestDataFactoryStatic.createAccount('TestAcct', storeAccountId).Id;
		Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'TestPrimaryContact');
		Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Test Opportunity', accountId, storeAccountId, 'Sold', date.today());

		System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
		
		OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
		Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        ord.Order_Processed_Date__c = Date.today().addDays(-1);
        ord.Tech_Measure_Date__c = Date.today().addDays(-3);
        Database.update(ord); 
    }

	@isTest
	private static void getOrderTest(){
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Order testOrder = OrderTrackerController.getOrder(ord.Id);
        System.assertEquals(ord.Id, testOrder.Id);
	}

}