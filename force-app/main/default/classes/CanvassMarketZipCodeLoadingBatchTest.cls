@isTest(SeeAllData=false)
global class CanvassMarketZipCodeLoadingBatchTest {

    public static testMethod void test1()
    {        
        CreateTestData();

        // Call the method that performs the callout
        Test.startTest();
        database.executeBatch(new CanvassMarketZipCodeLoadingBatch(),1);
        
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test 2';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        set<string> zipSet = new set<string>();
        zipSet.add('12345');
        
        set<string> marketSet = new set<string>();
        marketSet.add(canvassMarket.Id);
        
        database.executeBatch(new CanvassMarketZipCodeLoadingBatch(canvassMarket.Id, zipSet, marketSet),1);
        Test.stopTest();
    }
    
    private static void CreateTestData()
    {
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        CNVSS_Canvass_Unit__c canvassUnit = new CNVSS_Canvass_Unit__c();
        canvassUnit.CNVSS_Canvass_Market__c = canvassMarket.Id;
        canvassUnit.CNVSS_House_No__c = '123';
        canvassUnit.CNVSS_Street__c = 'Fake Fake ';
        canvassUnit.Street_Type__c = 'Dr';
        canvassUnit.CNVSS_City__c = 'Charlotte';
        canvassUnit.CNVSS_State__c = 'NC';
        canvassUnit.CNVSS_Zip_Code__c = '12345';
        canvassUnit.Fullstreetaddress__c = '123 Fake Fake Dr, Charlotte, NC 12345';
        canvassUnit.Auto_Created__c = true;
        canvassUnit.Auto_Modified__c = false;
        insert canvassUnit;
    }
}