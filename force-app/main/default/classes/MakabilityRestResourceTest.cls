@isTest

public class MakabilityRestResourceTest {
    
    @isTest
    private static void testMakabilityService(){
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        List<MakabilityRestResource.OrderRequest> orders = createTestOrders(pc); 
        createTestColorConfig(pc);
        createTestScreenConfig(pc);
        createTestGlazingConfig(pc); 
        createTestGrilleConfig(pc);
        createTestSizeDetailConfig(pc);
        createTestFrameNotchConfig(pc);
        createTestSpecialtyShapeGrilleConfig(pc);
        createColorSizeConfig(pc);
        createEjColorConfig(pc);
        mockUpProductFieldControls(pc);
        test.startTest();
 	    MakabilityRestResource.ResponseWrapper res = MakabilityRestResource.doPost(orders);
        test.stopTest();
        system.debug('response in unit test? ' + res);
    }
    
    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createTestColorConfig(Product_Configuration__c pc){
        Color_Configuration__c cc = new Color_Configuration__c();
        cc.Product_Configuration__c = pc.id;
        cc.Exterior_Color__c = 'blue';
        cc.Interior_Color__c = 'blue;white;red';
        insert cc;
    }
    
    private static void createTestScreenConfig(Product_Configuration__c pc){
        Screen_Configuration__c sc= new Screen_Configuration__c();
        sc.Screen_Type__c = 'TruScene';
        sc.Full_Screen_Max_Height_Fraction__c = 'Even';
        sc.Full_Screen_Max_Height_Inches__c = 50;
        sc.Max_Height_Fraction__c = 'Even';
        sc.Max_Height_Inches__c = 96;
        sc.Max_Width_Fraction__c = 'Even';
        sc.Max_Width_Inches__c = 50;
        sc.Product_Configuration__c = pc.id;
        insert sc;
    }
    private static void createTestGlazingConfig(Product_Configuration__c pc){
        Glazing_Configuration__c gc = new Glazing_Configuration__c();
        gc.Glass_Pattern__c = 'Fern';
        gc.Glazing__c = 'High Performance';
        gc.Max_Height_Fraction__c = 'Even';
        gc.Max_Height_Inches__c = 96;
        gc.Max_Width_Inches__c = 50;
        gc.Max_Width_Fraction__c = 'Even';
        gc.Product_Configuration__c = pc.id;
        insert gc;
    }
    private static void createTestGrilleConfig(Product_Configuration__c pc){
        Grille_Pattern_Configuration__c gc = new Grille_Pattern_Configuration__c();
        gc.Grille_Pattern__c = 'Colonial';
        gc.Min_Height_Fraction__c = 'Even';
        gc.Min_Height_Inches__c = 45;
        gc.Min_Width_Inches__c = 25;
        gc.Min_Width_Fraction__c = 'Even';
        gc.Product_Configuration__c = pc.id;
        insert gc;
    }   
    private static void createTestSizeDetailConfig(Product_Configuration__c pc){
      Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();  
        sdc.Product_Configuration__c = pc.id;
        sdc.Performance_Category__c = 'DP Upgrade';
        sdc.Extended_Max_Height_Inches__c = 80;
        sdc.Extended_Max_Height_Fraction__c = 'Even';
        sdc.Extended_Max_Width_Inches__c = 80;
        sdc.Extended_Max_Width_Fraction__c = 'Even';
        sdc.Positive_Force__c = 50;
        sdc.Frame_Type__c = 'Flat Sill';
        sdc.Hardware_Options__c = 'Normal Hinge';
        sdc.Negative_Force__c = 50;
        sdc.Max_Height_Inches__c = 40;
        sdc.Max_Height_Fraction__c = 'Even';
        sdc.Max_Width_Inches__c = 40;
        sdc.Max_Width_Fraction__c = 'Even';
        sdc.Min_Height_Inches__c = 40;
        sdc.Min_Height_Fraction__c = 'Even';        
        sdc.Min_Width_Inches__c = 40;
        sdc.Min_Width_Fraction__c = 'Even';
        sdc.Sash_Operation__c = 'Right';
        sdc.Sash_Ratio__c = '1:1';
        sdc.Specialty_Shape__c = null;
        sdc.United_Inch_Maximum__c = 120;
        sdc.United_Inch_Minimum__c = 30;
        insert sdc;

    }

     private static void createTestFrameNotchConfig(Product_Configuration__c pc){
       Frame_Notch_Configuration__c fnc = new Frame_Notch_Configuration__c(); 
        fnc.ProductConfigurationLookup__c = pc.id;
        fnc.Pocket_Notch__c = true;
		fnc.Frame_Type__c = 'Flat Sill';
        fnc.Exterior_Trim__c = 'Chamfered Edge Overfit Flange';
        insert fnc;		
    }

   private static void createTestSpecialtyShapeGrilleConfig(Product_Configuration__c pc){
       Specialty_Grille_Configuration__c sgc = new Specialty_Grille_Configuration__c(); 
        sgc.Product_Configuration__c = pc.id;
        sgc.Grille_Pattern__c = 'Gothic';
		sgc.Grille_Style__c = 'Full Divided Light (FDL with spacer)';
        sgc.Hubs__c = 0;
        sgc.Lites_High__c = 0;
        sgc.Lites_Wide__c = 0;
        sgc.Specialty_Shape__c ='Circle Top';
        sgc.Spokes__c = 0;
        insert sgc;											  
    }    
    
    private static void createEjColorConfig(Product_Configuration__c pc){
            EJ_Color_Configuration__c ejc = new EJ_Color_Configuration__c();
            ejc.EJ_Color__c = 'blue;red;White';
            ejc.EJ_Species__c = 'Oak';
            ejc.Product_Configuration__c = pc.id;
            insert ejc;

    }

    private static void createColorSizeConfig(Product_Configuration__c pc){
        Color_Size_Limits__c csl = new Color_Size_Limits__c();
            csl.Interior_Color__c = 'White';
            csl.Maximum_Height_Fractions__c = 'Even';
            csl.Maximum_Height__c = 60;
            csl.Maximum_Width_Fractions__c = 'Even';
            csl.Maximum_Width__c = 60;
            csl.Product_Configuration__c = pc.id;
            insert csl;
    }

    private static Product_Field_Control__c createProductFieldControl(Product_Configuration__c pc, Field_Control_List__c fcl ){
        Product_Field_Control__c pfc = new Product_Field_Control__c();
        pfc.Name = fcl.Name;
        pfc.Field_Control_ID__c = fcl.id;
        pfc.Product_Configuration_ID__c = pc.id;
        pfc.Required__c = true;
        return pfc;
    }    

    private static void createProductFieldControlDependencies(String controllingValue, Product_Field_Control__c controllingField, Product_Field_Control__c dependentField ){
        Product_Field_Control_Dependency__c pfcd = new Product_Field_Control_Dependency__c();
        pfcd.Action_Taken__c = 'Disable';
        pfcd.Controlling_Field__c = controllingField.id;
        pfcd.Controlling_Value__c = controllingValue;
        pfcd.Dependent_Field__c = dependentField.id;
        insert pfcd;
        
    }   

    private static Field_Control_List__c createFieldControlLists(String fieldName){
        Field_Control_List__c fcl = new Field_Control_List__c();
        fcl.Name = fieldName;
        fcl.rForce__c = true;
        fcl.Sales__c = true;
        fcl.Tech__c = true;
        return fcl;
    }     

    private static void mockUpProductFieldControls(Product_Configuration__c pc){
      List <Field_Control_List__c> fclList = new List<Field_Control_List__c>();
      List <Product_Field_Control__c> pfcList = new List<Product_Field_Control__c>();
      List <Product_Field_Control_Dependency__c> pfcdList = new List <Product_Field_Control_Dependency__c>();
      Product_Field_Control__c specialShape;
      Product_Field_Control__c rightLeg;
      Product_Field_Control__c leftLeg; 
      List <String> fields = new List <String>{'Right Leg Inches', 
                                               'Left Leg Inches', 
                                               'Locks/Sash', 
                                               'Specialty Shape'};

      for(String f:fields ){
          Field_Control_List__c fcl = createFieldControlLists(f);
          fclList.add(fcl);
      }

      insert fclList;
      
      for(Field_Control_List__c fc : fclList){
         Product_Field_Control__c pfc =  createProductFieldControl(pc,fc);
         pfcList.add(pfc);
      }

      insert pfcList;

      for(Product_Field_Control__c pfc : pfcList){
          if(pfc.Name == 'Specialty Shape'){
              specialShape = pfc;
          }
          if(pfc.Name == 'Right Leg Inches'){
              rightLeg = pfc;
          }
          if(pfc.Name == 'Left Leg Inches'){
              leftLeg = pfc;
          }
      }
      createProductFieldControlDependencies('Chord',specialShape, rightLeg);
      createProductFieldControlDependencies('Chord',specialShape, leftLeg);

    }          
    
    private static List<MakabilityRestResource.OrderRequest> createTestOrders(Product_Configuration__c proc){
        List<MakabilityRestResource.OrderRequest> orders = new List<MakabilityRestResource.OrderRequest>();
        List<MakabilityRestResource.OrderItem> orderItems = new List<MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderRequest o = new MakabilityRestResource.OrderRequest();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = 45;
        pc.widthFractions = 'Even';
        pc.heightInches = 50;
        pc.heightFractions = 'Even';        
        pc.specialtyShape= null;
        pc.grillePattern = 'Colonial';
        pc.grilleStyle = null;
        pc.spokes = null;
        pc.hubs = null;
        pc.litesHigh = null;
        pc.litesWide = null;
        pc.screenType = 'TruScene';
        pc.screenSize = '';
        pc.exteriorColor = 'blue';
        pc.interiorColor = 'blue';
        pc.frame = 'Flat Sill';
        pc.hardwareOption = 'Normal Hinge';
        pc.sashOperation = 'Right';
        pc.sashRatio = '1:1';
        pc.externalForce = 50;
        pc.internalForce = 50;
        pc.highPerformance = true;
        pc.s1Tempering = false;
        pc.s2Tempering = false;
        pc.s1Pattern = 'Fern';
        pc.s2Pattern = 'Fern'; 
        pc.insertFrame = false;
        pc.exteriorTrim = null;
        pc.leftLegInches = null;
        pc.leftLegFraction = null;
        pc.rightLegInches = null;
        pc.rightLegFraction = null;
        pc.ejSpecies = 'Oak';
        pc.ejColor = 'blue';
        pc.locks = 1;       
        pc.productId = proc.Product__c;
        oi.orderItemId = '1';
        oi.productConfiguration = pc;
        orderItems.add(oi);
        o.orderId = '1';
        o.orderItems = orderItems;
        orders.add(o);        
        return orders;
    }
}