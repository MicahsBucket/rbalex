/*Author:Ramakrishna Manchala
* Purpose :Contains logic Related to Release Po Button 
* 
* 
*/
public class PurchaseOrderButtonController
{
    
    @AuraEnabled()
    public static   String  setPurchaseOrderToReleased(String purchaseOrderId) {
        try
        {
            
            
            Retail_Purchase_Order__c po = [select id ,  Recordtype.developername,(select Verify_Item_Configuration__c ,NSPR__c, id from Order_Products__r where Verify_Item_Configuration__c=false AND Product2.Run_Makability__c!=null),order__r.id from Retail_Purchase_Order__c where id =:purchaseOrderId limit 1];
            system.debug(po.Order_Products__r.size());
            if(po.recordtype.developername=='RbA_Purchase_Order')
            {
            
            
              for(OrderItem oi :po.Order_Products__r)
              {
              if(oi.Verify_Item_Configuration__c=false && oi.Product2.Run_Makability__c!=null)
              {
               
                   if(oi.NSPR__c!=true)
                   {
                    return 'Purchase Order cannot be released until all Purchase Order line items have been verified' ;
                   }
                
              }
              
              
              }
                
                Order orderPo= new Order(id=po.order__r.id);
                 orderPo.Apex_Context__c=true;
                 orderPo.Status='Order Released';
                 update orderPo;
                 po.Status__c = 'Released';    
                 po.Released_Timestamp__c = System.Now();
                 update po;   
                
            }
            else if(po.recordtype.developername=='Cost_Purchase_Order')
            {  
                
                
                po.Status__c = 'Released';    
                po.Released_Timestamp__c = System.Now();
                update po;
            } 
            
            
        }
        catch(Exception e)
            
        {
            
        }
        
        return null; 
        
    }
    
    
    @AuraEnabled(cacheable=true)
    public static   String  getStatus(String purchaseId) {
        
        Retail_Purchase_Order__c po = [select id ,Recordtype.developername,status__c  from Retail_Purchase_Order__c where id =:purchaseId limit 1];  
        
        return po.Status__c;
    }
    
}