/*
* @author Jason Flippen
* @date 01/13/2021
* @description Test Class for the following Classes:
*              - ReimbursablePartTriggerHandler
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/ 
@isTest
public class ReimbursablePartTriggerHandlerTest {

    /*
    * @author Jason Flippen
    * @date 01/13/2021 
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        List<Account> testAccountList = new List<Account>();

        Account testStoreAccount = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];

        Account testAccount01 = testUtility.createVendorAccount('Test Vendor Account');
        testAccount01.Name = 'Test Vendor Account';
        testAccount01.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testAccount01);
        Account testDwellingAccount = testUtility.createDwellingAccount('Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testDwellingAccount);
        insert testAccountList;

        Contact testContact = testUtility.createContact(testAccount01.Id, 'BillToTest');
        insert testContact;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Door_Components').getRecordTypeId(),
                                            Family = 'Parts and Accessories',
                                            Vendor__c = testAccount01.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c = testFan.Id,
                                            Reimbursable_Part__c = true);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;

    }

    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to test the "After Insert" functionality.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testAfterInsert() {

        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order'];
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Product'];

        Test.startTest();

            Reimbursable_Part__c testReimbursablePart = new Reimbursable_Part__c(Order__c = testOrder.Id,
                                                                                 Part__c = testProduct.Id,
                                                                                 Unit_Price__c = 100.00);
            insert testReimbursablePart;
            
        Test.stopTest();

        List<Reimbursable_Part__c> reimbursablePartList = [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :testOrder.Id];
        System.assertEquals(1, reimbursablePartList.size(), 'Unexpected Reimbursable Part List');

    }
    
    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to test the "After Delete" functionality.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testAfterDelete() {

        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order'];
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Product'];

        Reimbursable_Part__c testReimbursablePart = new Reimbursable_Part__c(Order__c = testOrder.Id,
                                                                             Part__c = testProduct.Id,
                                                                             Unit_Price__c = 100.00);
        insert testReimbursablePart;
        
        Test.startTest();

            delete testReimbursablePart;
            
        Test.stopTest();

        List<Reimbursable_Part__c> reimbursablePartList = [SELECT Id FROM Reimbursable_Part__c WHERE Order__c = :testOrder.Id];
        System.assertEquals(0, reimbursablePartList.size(), 'Unexpected Reimbursable Part List');

    }
    
}