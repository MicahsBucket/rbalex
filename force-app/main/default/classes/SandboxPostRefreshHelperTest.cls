@isTest
public class SandboxPostRefreshHelperTest {
    
    
    @testSetup static void setup() {
        TestDataFactoryStatic.setUpConfigs();
    }
    
    @isTest
    static void testResetAdminEmailAddresses() {
        User u = new User(Alias = 'standt', Email= System.currentTimeMillis()+'standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = [SELECT Id FROM Profile WHERE Name='Super Administrator'].id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser=testorg.com@example.com');
        
        User u2 = new User(Alias = 'standt1', Email= System.currentTimeMillis()+'standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1=testorg.com@example.com');
        
        User u3 = new User(Alias = 'standt2', Email= System.currentTimeMillis()+'standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = [SELECT Id FROM Profile WHERE Name='Standard User'].id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2=testorg.com@example.com');
        
        List<User> myUserList =  new List<User>{u, u2, u3}; 
            insert myUserList;
        
        
        Test.startTest();
        SandboxPostRefreshHelper.resetAdminEmailAddresses();
        for(User myUser : [SELECT Email, Profile.Name FROM User WHERE Alias LIKE 'standt%']) { 
            if( myUser.Profile.Name != 'Standard User'){
                System.assert(!myUser.Email.Contains('@example.com'));
            } 
            
        }
        Test.stopTest();
    }
    
    @isTest
    static void testPopulateStoreAccountRecords() {
        Test.startTest();
        SandboxPostRefreshHelper.populateStoreAccountRecords();
        Test.stopTest();
        
        System.assertEquals(2, [SELECT Count() FROM Account WHERE Name = '0077 - Twin Cities' OR Name = 'Renewal by Andersen']);
        System.assertEquals(2, [SELECT Count() FROM Back_Office_Checklist_Configuration__c]);
        System.assertEquals(2, [SELECT Count() FROM Store_Configuration__c]);
        System.assertEquals(13, [SELECT Count() FROM Financial_Transaction__c]);
        System.assertEquals(1, [SELECT Count() FROM Store_Vendor__c]);
        
    }
    
    @isTest
    static void testPopulateWorkTypes() {
        Test.startTest();
        SandboxPostRefreshHelper.populateWorkTypes();
        Test.stopTest();
     //   System.assertEquals(3,[SELECT Count() FROM WorkType]);        
        
    }
    
    @isTest
    static void testPopulateServiceTerritories() {
         Account twinCities = new Account(
            Name = '0077 - Twin Cities',
            Type = 'CORO',
            RecordTypeId = UtilityMethods.retrieveRecordTypeId('Store', 'Account'),
            BillingStreet  = '1920 County Road C West,',
            BillingCity = 'Roseville',
            BillingState  =  'Minnesota',
            BillingPostalCode  = '55113',
            BillingCountry = 'United States',
            BillingStateCode  =  'MN',
            BillingCountryCode = 'US',
            BillingLatitude = 45.020743,
            BillingLongitude =   -93.182536,
            ShippingStreet  = '1920 County Road C West,',
            ShippingCity  =  'Roseville',
            ShippingState  = 'Minnesota',
            ShippingPostalCode = '55113',
            ShippingCountry  = 'United States',
            ShippingStateCode  = 'MN',
            ShippingCountryCode = 'US',
            ShippingLatitude  =  45.02083747,
            ShippingLongitude  = -93.18272125,
            Phone  = '+1 (651) 264-4777',
            Fax = '+1 (651) 264-4079',
            Active__c  = TRUE,
            Pivotal_Id__c =  'AAAAAAAAAAg=Store',
            Enabled_StoreId__c = '77',
            Over_Receiving_Allowed__c =  FALSE,
            Account_Balance__c = 0,
            Store_Number__c = '77',
            Contractor_License_1__c = 'BC130983 - MN',
            DBA__c = 'Renewal by Andersen of the Twin Cities',
            Survey_Holds__c = 119,
            Legal_Name__c  = 'Renewal by Andersen, LLC',
            Electronic_Orders__c  =  FALSE,
            Contractor_License_2__c = '266951 - WI',
            Applications_Used__c  = 'rForce;rSuite w/o Tech Mapp;Enabled Plus;Ground Force;CFW;Signature Service OCEM Auto'
        );
        
        insert twinCities;
        
        Test.startTest();
        SandboxPostRefreshHelper.populateServiceTerritories();
        Test.stopTest();
        
        System.assertEquals(1, [SELECT Count() FROM OperatingHours]);
        System.assertEquals(27, [SELECT Count() FROM TimeSlot]);
        System.assertEquals(1, [SELECT Count() FROM ServiceTerritory]);
        System.assertEquals(80, [SELECT Count() FROM Service_Territory_Zip_Code__c]);
    }
    
    
}