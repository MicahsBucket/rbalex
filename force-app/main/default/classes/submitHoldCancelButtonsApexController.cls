/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description displays, hides and provides functionality for buttons based on record type 
* and whether source system requires Manual or Auto process
**/

public with sharing class submitHoldCancelButtonsApexController {

//returns true if the record type is post install, false if post appointment
@AuraEnabled
	public static List<Boolean> determineRecordType(Id recordId){
		Boolean isPostInstall = false;
		Boolean isIncomplete = false;
        
       // adding the Service Survey record type  - Service
       
        Boolean isService = false;
        
		List<Boolean> boolList = new List<Boolean>();
		
		Survey__c surv = [SELECT Id, RecordType.Name, Survey_Status__c FROM Survey__c WHERE Survey__c.Id = :recordId LIMIT 1];
		String status = surv.Survey_Status__c;
		String recordType = surv.RecordType.Name;
		if(recordType == 'Post Install'){
			isPostInstall = true;
		} else if(recordType == 'Post Appointment'){
			isPostInstall = false;
		} else if((recordType != 'Service')){
			isPostInstall = null;
		}
		if(status == 'Incomplete Data'){
			isIncomplete = true;
		}
        
        //Checking for Service RecordType
        isService = recordType == 'Service';
        
		boolList.add(isPostInstall);
		boolList.add(isIncomplete);
        boolList.add(isService);
        
		return boolList;
        
    }

/*@AuraEnabled
	public static String determineIncompleteStatus(Id recordId){
		return 'words';
	}*/

//gets survey based on record Id
@AuraEnabled
	public static Survey__c getSurvey(Id recordId){
		Survey__c survey = [SELECT Id, Primary_Contact_Email__c, Secondary_Contact_email__c, Name, Appointment_Date__c FROM Survey__c WHERE Id = :recordId LIMIT 1];
		return survey;
	}

///updates email information for survey
@AuraEnabled
	public static void editRecord(String email, String recordId){
		Survey__c survey = [SELECT Primary_Contact_Email__c, Secondary_Contact_email__c, Name FROM Survey__c WHERE Id = :recordId LIMIT 1];
		survey.Primary_Contact_Email__c = email;
		Database.update(survey, true);
	}

//updates status to hold or send based on which button was pressed, if survey is already on hold survey is unchanged
@AuraEnabled
	public static String updateStatus(String status, Id recordId){
		Survey__c survey = [SELECT Survey_Status__c, Name FROM Survey__c WHERE Survey__c.Id = :recordId LIMIT 1];
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
		if(status.equals('Send')){
			if(survey.Survey_Status__c == 'Processing' || survey.Survey_Status__c == 'Accepted'){
				return 'already sent';
			} else {
				survey.Send_to_Medallia__c = true;
                try{
                    Database.update(survey, true);
					return 'send';
                } catch (Exception e){
                    return 'locked';
                }
			}
		} else if(survey.Survey_Status__c != 'Hold' && survey.Survey_Status__c != 'Processing' && survey.Survey_Status__c != 'Accepted'){
            Id surveyRecordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
			Integer surveysOnHold = [SELECT Id FROM Survey__c WHERE (Survey_Status__c = 'Hold' OR Survey_Status__c = 'Incomplete Data') AND RecordTypeID = :surveyRecordTypeId].size();
			Integer totalHolds = [SELECT Survey_Holds__c, RecordTypeId FROM Account WHERE RecordTypeId = :recordTypeId LIMIT 1].Survey_Holds__c.intValue();

			Boolean holdsLeft = false;
			if(totalHolds - surveysOnHold > 0) holdsLeft = true;
			if(survey.Survey_Status__c != 'Send' && survey.Survey_Status__c != 'Sent'){
				if(holdsLeft){
					survey.Survey_Status__c = 'Hold';
				} else {
					return 'hold limit';
				}
			} else {
				return 'already sent';
			}
			Database.update(survey, true);
			return 'hold';
        } else if(survey.Survey_Status__c == 'Processing' || survey.Survey_Status__c == 'Accepted'){
            return 'already sent';
        } else return 'already held';
	}
}