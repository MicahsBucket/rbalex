/**
  * @author Ramya Bangalore
  * @Description This SchedulerBatch class is setting time for the "batchUpdateRBACustomer" -GroundForce App
  * 
  * 
  */

global class batchUpdateRBACustomer implements Database.Batchable<sObject> {
    
Static string errorString;
Static boolean hasError= false;    

global Database.QueryLocator start(Database.BatchableContext BC) {
 
    errorString = '';
     String query = 'SELECT AccountId, ID, Account.Canvass_Unit_Id__c FROM Order ';
    
    if (!Test.isRunningTest()) {
           query += ' WHERE status in (\'Install Complete\', \'Job Closed\', \'Closed\')  and ' +
            ' Account.Canvass_Unit_Id__c  != null and Installation_Date_Complete__c <= today and ' +
            ' Account.Canvass_Unit_Id__r.CNVSS_Previous_RbA_Customer__c = false';
    }
    
     	return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext BC, List<Order> scope) {
      List<ID> CanvasIDList = new List<ID>(); // List that contains canvas id units where canvas unit was  updated and order is closed/completed
    
     for (Order o: scope)
       CanvasIDList.add(o.Account.Canvass_Unit_Id__c);
         
      List <CNVSS_Canvass_Unit__c> canvasUnitList = new List <CNVSS_Canvass_Unit__c>();		
      canvasUnitList = [select id, CNVSS_Previous_RbA_Customer__c from CNVSS_Canvass_Unit__c where id in :CanvasIDList];
    
      for (CNVSS_Canvass_Unit__c cu :canvasUnitList){
        cu.CNVSS_Previous_RbA_Customer__c = true;
      }
  
  	  Database.SaveResult [] srList = Database.update(canvasUnitList, FALSE);
         
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                errorString+=sr.getErrors()[0].getStatusCode() + ': ' + sr.getErrors()[0].getMessage();
                hasError = true;
            }
        } 
         
     } 
     
     global void finish(Database.BatchableContext BC) {
       /*  
         String emailSubject;
         String emailBody;
         String emailRecipient;
         String emailSender;
         boolean sendEmail;
         
         if (hasError){
             emailSubject = 'Failures while updating Previous RBA Customer  flag on Canvas Unit';
             emailBody = errorString;
         }
         else{
             emailSubject = 'Success: Updating Previous RBA Customer flag on Canvas Unit';
             emailBody = 'Success !';
         }
         
         // Fetch sendEmail flag & emailReceipient from custom settings 
         
         // Send email */
     
     }
}