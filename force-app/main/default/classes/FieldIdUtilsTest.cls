@isTest
private class FieldIdUtilsTest {

    @isTest static void testGetFormattedLKID() {
        Test.setMock(WebServiceMock.class, new ToolingAPIWSDLMock.MockQuery());
        List<String> testFields = new List<String> { 'DummyTest.DummyTest' };
        FieldIdUtils.runInstall(testFields);
        String relatedFSLWorkOrderLKID = FieldIdUtils.getFormattedLKIDFromName('DummyTest', 'DummyTest');
        System.assertEquals('CFDummyTest_lkid', relatedFSLWorkOrderLKID);
    }

    @isTest static void testGetFormattedId() {
        Test.setMock(WebServiceMock.class, new ToolingAPIWSDLMock.MockQuery());
        List<String> testFields = new List<String> { 'DummyTest.DummyTest' };
        FieldIdUtils.runInstall(testFields);
        String relatedFSLWorkOrderId = FieldIdUtils.getFormattedLookupIdFromName('DummyTest', 'DummyTest');
        System.assertEquals('CFDummyTest', relatedFSLWorkOrderId);
    }

}