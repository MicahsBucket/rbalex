/**
 * @File Name          : MakabilityCheckColorConfigTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 4/30/2019, 2:12:45 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/30/2019, 1:19:06 PM   mark.rothermal@andersencorp.com     Initial Version
**/

@isTest
public class MakabilityCheckColorConfigTest {


    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'blue','blue','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Config - passed');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'blue','blue','1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Color Configurations found for this productId.');
    }
    @isTest
    public static void noMatchingInteriorColorTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'blue','PURPLE','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingInteriorColorTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Config. Incorrect Interior Color PURPLE is not an option for an exterior color of blue . available options are: White;blue;red');        
    }   
    @isTest
    public static void noMatchingExteriorColorTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,'PURPLE','PURPLE','1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingExteriorColorTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Color Config. Incorrect Exterior Color, available options are: {blue}');        
    }   
    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestColorConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = new Map<string,MakabilityRestResource.OrderItem>();
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckColorConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingAnythingTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Something bad happened with the color config');        
    }        
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createTestColorConfig(Product_Configuration__c pc){
        Color_Configuration__c cc = new Color_Configuration__c();
        cc.Product_Configuration__c = pc.id;
        cc.Exterior_Color__c = 'blue';
        cc.Interior_Color__c = 'blue;white;red';
        insert cc;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, String exColor,String inColor, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
		ProductConfiguration pc = new ProductConfiguration();
        pc.exteriorColor = excolor;
        pc.interiorColor = incolor;   
        pc.productId = prodId;
        oi.orderItemId = oiId;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}