/******************************************************************
 * Author : Pavan Gunna
 * Class Name :BatchJobOrderHistoryTracking
 * Createdate :
 * Description : Batch Job For Updating Order Status History For Reporting. 
 * Change Log
 * ---------------------------------------------------------------------------
 * Date      Name          Description
 * ---------------------------------------------------------------------------
 * 
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/

global class BatchJobOrderHistoryTracking implements Database.Batchable<sObject> {
    
    String query;
    Date TodaysDate = date.today();
    Date YesterdaysDate = Date.today().addDays(-1);
    Id serviceRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
    string CustomDate = '2019-12-31T13:04:19.000Z';

    global BatchJobOrderHistoryTracking() {
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
     String newstr = '8014M000001XkywQAC'; //'8014M000002hZL2QAM'; //'8014M000001X06mQAC';
        Query = 'SELECT Id, status, recordTypeId, lastmodifieddate, Time_New__c ,Time_Product_Ordered__c ,Time_Service_Scheduled__c ,Time_Warranty_Submitted__c ,Time_Warranty_Rejected__c ,Time_Service_on_Hold__c ,Time_Customer_Call_Back__c ,Time_Service_to_be_Scheduled__c ,Time_Service_Complete__c ,Time_To_Be_Ordered__c ,Time_Seasonal_Service__c ,Time_Quote__c ,Time_Draft__c,Time_On_Hold__c,Time_Tech_Measure_Needed__c ,Time_Tech_Measure_Scheduled__c ,Time_Ready_To_Order__c ,Time_Order_Released__c ,Time_Install_Needed__c ,Time_Install_Scheduled__c ,Time_Install_Complete__c ,Time_Job_In_Progress__c ,Time_Job_Close__c ,Job_Close_Date__c ,Time_Pending_Cancellation__c ,Time_Cancelled__c, Order_Processed_Date__c  FROM Order  WHERE lastmodifieddate > 2019-12-31T13:04:19.000Z'; // WHERE id =: newstr Lastmodofieddate => :YesterdaysDate' ;
    
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Order> scope) {
        
         list<order> ordList = new list<order>();
         Set<order> ordToUpdate = new Set<order>();
         Map<id, list<OrderHistory>> OrderToHistory = new Map<id,list<OrderHistory>>();
         
        // Get all Order records
        for(order o:scope){
            ordList.add(o);
        }
        system.debug('@pavan ordList'+ordList);
        // Get all the OrderHistory Records 
        if(!ordList.isEmpty()){
            
            for(OrderHistory history: [SELECT CreatedById,CreatedDate,Field,Id,NewValue,OldValue,OrderId FROM OrderHistory WHERE Orderid = : ordList AND Field='Status' ORDER By CreatedDate Desc]){
            
                if(OrderToHistory.get(history.orderid) == null){
                
                    OrderToHistory.put(history.orderid, new list<OrderHistory>());                
                }
                    OrderToHistory.get(history.orderid).add(history);
            }
        }
       system.debug('@pavan OrderToHistory'+OrderToHistory);
       // Evaluate Order Status Fields from the Order History
        
        for(Order ord :ordList ){
        
            if( OrderToHistory.containsKey(ord.id) && !OrderToHistory.get(ord.id).isEmpty() && OrderToHistory.get(ord.id).size() >0){
            
                for(OrderHistory history: OrderToHistory.get(ord.id)) {
                
                    if( ord.recordTypeId !=  serviceRecordTypeId 
                         && (  (history.NewValue == 'Draft' && ((ord.Time_Draft__c < history.CreatedDate && ord.Time_Draft__c != history.CreatedDate) || ord.Time_Draft__c== null))
                             ||(history.NewValue == 'On Hold' && ((ord.Time_On_Hold__c < history.CreatedDate && ord.Time_On_Hold__c != history.CreatedDate) || ord.Time_On_Hold__c == null))
                             ||(history.NewValue == 'Tech Measure Needed' && ((ord.Time_Tech_Measure_Needed__c < history.CreatedDate && ord.Time_Tech_Measure_Needed__c != history.CreatedDate) || ord.Time_Tech_Measure_Needed__c== null))
                             ||(history.NewValue == 'Tech Measure Scheduled' && ((ord.Time_Tech_Measure_Scheduled__c < history.CreatedDate && ord.Time_Tech_Measure_Scheduled__c != history.CreatedDate) || ord.Time_Tech_Measure_Scheduled__c ==null))
                             ||(history.NewValue == 'Ready to Order' && ((ord.Time_Ready_To_Order__c < history.CreatedDate && ord.Time_Ready_To_Order__c != history.CreatedDate) || ord.Time_Ready_To_Order__c== null))
                             ||(history.NewValue == 'Order Released' && ((ord.Time_Order_Released__c < history.CreatedDate && ord.Time_Order_Released__c != history.CreatedDate) || ord.Time_Order_Released__c == null))
                             ||(history.NewValue == 'Install Needed' && ((ord.Time_Install_Needed__c < history.CreatedDate && ord.Time_Install_Needed__c != history.CreatedDate) ||ord.Time_Install_Needed__c == null))
                             ||(history.NewValue == 'Install Scheduled' && ((ord.Time_Install_Scheduled__c < history.CreatedDate && ord.Time_Install_Scheduled__c != history.CreatedDate) || ord.Time_Install_Scheduled__c == null))
                             ||(history.NewValue == 'Install Complete' && ((ord.Time_Install_Complete__c < history.CreatedDate && ord.Time_Install_Complete__c != history.CreatedDate) || ord.Time_Install_Complete__c == null))
                             ||(history.NewValue == 'Job in Progress'  && ((ord.Time_Job_In_Progress__c < history.CreatedDate && ord.Time_Job_In_Progress__c != history.CreatedDate) || ord.Time_Job_In_Progress__c== null))
                             ||(history.NewValue == 'Job Closed' && ((ord.Time_Job_Close__c < history.CreatedDate && ord.Time_Job_Close__c != history.CreatedDate) || ord.Time_Job_Close__c== null)) 
                             ||(history.NewValue == 'Pending Cancellation' && ((ord.Time_Pending_Cancellation__c < history.CreatedDate && ord.Time_Pending_Cancellation__c != history.CreatedDate) || ord.Time_Pending_Cancellation__c== null))
                             ||(history.NewValue == 'Cancelled' && ((ord.Time_Cancelled__c < history.CreatedDate && ord.Time_Cancelled__c != history.CreatedDate) || ord.Time_Cancelled__c == null))
                             ||(history.NewValue != 'Draft' && history.OldValue == 'Draft' && ((ord.Order_Processed_Date__c < history.CreatedDate && ord.Order_Processed_Date__c != history.CreatedDate) || ord.Order_Processed_Date__c== null))
                            )
                    ){
                        system.debug('@pavan Entered into If');
                           if(history.NewValue == 'Draft' && ((ord.Time_Draft__c < history.CreatedDate && ord.Time_Draft__c != history.CreatedDate) || ord.Time_Draft__c== null)){
                              
                              ord.Time_Draft__c = history.CreatedDate; 
                           }  
                          
                           if(history.NewValue == 'On Hold' && ((ord.Time_On_Hold__c < history.CreatedDate && ord.Time_On_Hold__c != history.CreatedDate) || ord.Time_On_Hold__c == null)){
                              
                              ord.Time_On_Hold__c = history.CreatedDate;
                            }  
                          
                            if(history.NewValue == 'Tech Measure Needed' && ((ord.Time_Tech_Measure_Needed__c < history.CreatedDate && ord.Time_Tech_Measure_Needed__c != history.CreatedDate) || ord.Time_Tech_Measure_Needed__c== null)){
                              
                              ord.Time_Tech_Measure_Needed__c = history.CreatedDate;
                            }
                          
                            if(history.NewValue == 'Tech Measure Scheduled' && ((ord.Time_Tech_Measure_Scheduled__c < history.CreatedDate && ord.Time_Tech_Measure_Scheduled__c != history.CreatedDate) || ord.Time_Tech_Measure_Scheduled__c ==null)){
                              
                              ord.Time_Tech_Measure_Scheduled__c = history.CreatedDate;
                            }
                          
                            if(history.NewValue == 'Ready to Order' && ((ord.Time_Ready_To_Order__c < history.CreatedDate && ord.Time_Ready_To_Order__c != history.CreatedDate) || ord.Time_Ready_To_Order__c== null)){
      
                              ord.Time_Ready_To_Order__c = history.CreatedDate;
                            }
                            
                            if(history.NewValue == 'Order Released' && ((ord.Time_Order_Released__c < history.CreatedDate && ord.Time_Order_Released__c != history.CreatedDate) || ord.Time_Order_Released__c == null)){
                              
                              ord.Time_Order_Released__c = history.CreatedDate;
                            }
                            
                            if(history.NewValue == 'Install Needed' && ((ord.Time_Install_Needed__c < history.CreatedDate && ord.Time_Install_Needed__c != history.CreatedDate) ||ord.Time_Install_Needed__c == null)){
                              
                              ord.Time_Install_Needed__c = history.CreatedDate;                              
                            }
                            
                            if(history.NewValue == 'Install Scheduled' && ((ord.Time_Install_Scheduled__c < history.CreatedDate && ord.Time_Install_Scheduled__c != history.CreatedDate) || ord.Time_Install_Scheduled__c == null)){
                              
                              ord.Time_Install_Scheduled__c = history.CreatedDate;
                            }
                            system.debug('@pavan check for install ord.Time_Install_Complete__c'+ord.Time_Install_Complete__c);
                            system.debug('@pavan check for install history.CreatedDate'+history.CreatedDate);
                            system.debug('@pavan check for install history.NewValuec'+history.NewValue);
                            if(history.NewValue == 'Install Complete' && ((ord.Time_Install_Complete__c < history.CreatedDate && ord.Time_Install_Complete__c != history.CreatedDate) || ord.Time_Install_Complete__c == null)){
                              
                              ord.Time_Install_Complete__c = history.CreatedDate;
                            }
                            
                            if(history.NewValue == 'Job Closed' && ((ord.Time_Job_Close__c < history.CreatedDate && ord.Time_Job_Close__c != history.CreatedDate) || ord.Time_Job_Close__c== null)){
                              
                              ord.Time_Job_Close__c = history.CreatedDate;
                            }
                            if(history.NewValue == 'Job Closed' && ((ord.Job_Close_Date__c < history.CreatedDate && ord.Job_Close_Date__c != history.CreatedDate) || ord.Job_Close_Date__c== null)){
                              
                              ord.Job_Close_Date__c = Date.valueof(history.CreatedDate);
                            }
                            
                            if(history.NewValue == 'Pending Cancellation' && ((ord.Time_Pending_Cancellation__c < history.CreatedDate && ord.Time_Pending_Cancellation__c != history.CreatedDate) || ord.Time_Pending_Cancellation__c== null)){
                              
                              ord.Time_Pending_Cancellation__c = history.CreatedDate;
                            }
                            if(history.NewValue == 'Cancelled' && ((ord.Time_Cancelled__c < history.CreatedDate && ord.Time_Cancelled__c != history.CreatedDate) || ord.Time_Cancelled__c == null)){
                              
                              ord.Time_Cancelled__c = history.CreatedDate;
                            }
                            if(history.NewValue == 'Job in Progress'  && ((ord.Time_Job_In_Progress__c < history.CreatedDate && ord.Time_Job_In_Progress__c != history.CreatedDate) || ord.Time_Job_In_Progress__c== null)){
                                      
                              ord.Time_Job_In_Progress__c = history.CreatedDate;
                            }
                            
                            if(history.NewValue != 'Draft' && history.OldValue == 'Draft' && ((ord.Order_Processed_Date__c < history.CreatedDate && ord.Order_Processed_Date__c != history.CreatedDate) || ord.Order_Processed_Date__c== null)){
                              
                              ord.Order_Processed_Date__c = Date.valueof(history.CreatedDate);
                            }
                        ordToUpdate.add(ord);
                        system.debug('@pavan ordToUpdate ...'+ordToUpdate.size() );
                        system.debug('@pavan ordToUpdate ...'+ordToUpdate );
                    }  else if(ord.recordTypeId ==  serviceRecordTypeId && 
                                ( (history.NewValue == 'New'  && ((ord.Time_New__c < history.CreatedDate && ord.Time_New__c != history.CreatedDate || ord.Time_New__c== null)))
                                ||(history.NewValue == 'Product Ordered'  && ((ord.Time_Product_Ordered__c < history.CreatedDate && ord.Time_Product_Ordered__c != history.CreatedDate) || ord.Time_Product_Ordered__c== null))
                                ||(history.NewValue == 'Service Scheduled'  && (( ord.Time_Service_Scheduled__c < history.CreatedDate && ord.Time_Service_Scheduled__c != history.CreatedDate) || ord.Time_Service_Scheduled__c == null))
                                ||(history.NewValue == 'Warranty Submitted'  && ((ord.Time_Warranty_Submitted__c < history.CreatedDate && ord.Time_Warranty_Submitted__c != history.CreatedDate) || ord.Time_Warranty_Submitted__c==null))
                                ||(history.NewValue == 'Warranty Rejected'  && ((ord.Time_Warranty_Rejected__c < history.CreatedDate && ord.Time_Warranty_Rejected__c != history.CreatedDate) || ord.Time_Warranty_Rejected__c == null))
                                ||(history.NewValue == 'Service On Hold'  && ((ord.Time_Service_on_Hold__c < history.CreatedDate && ord.Time_Service_on_Hold__c != history.CreatedDate) || ord.Time_Service_on_Hold__c == null))
                                ||(history.NewValue == 'Customer Call Back'  && (( ord.Time_Customer_Call_Back__c < history.CreatedDate && ord.Time_Customer_Call_Back__c != history.CreatedDate) || ord.Time_Customer_Call_Back__c== null))
                                ||(history.NewValue == 'Service To Be Scheduled'  && ((ord.Time_Service_to_be_Scheduled__c < history.CreatedDate && ord.Time_Service_to_be_Scheduled__c != history.CreatedDate) || ord.Time_Service_to_be_Scheduled__c== null))
                                ||(history.NewValue == 'Service Complete'  && ((ord.Time_Service_Complete__c < history.CreatedDate && ord.Time_Service_Complete__c != history.CreatedDate) || ord.Time_Service_Complete__c == null))
                                ||(history.NewValue == 'To Be Ordered'  && ((ord.Time_To_Be_Ordered__c < history.CreatedDate && ord.Time_To_Be_Ordered__c != history.CreatedDate) || ord.Time_To_Be_Ordered__c == null))
                                ||(history.NewValue == 'Seasonal Service'  && ((ord.Time_Seasonal_Service__c < history.CreatedDate && ord.Time_Seasonal_Service__c != history.CreatedDate) || ord.Time_Seasonal_Service__c == null))
                                ||(history.NewValue == 'Quote'  && ((ord.Time_Quote__c < history.CreatedDate && ord.Time_Quote__c != history.CreatedDate) || ord.Time_Quote__c== null))
                                )
                            ){  
                            system.debug('@pavan Entered into else');
                                if(history.NewValue == 'New'  && ((ord.Time_New__c < history.CreatedDate && ord.Time_New__c != history.CreatedDate || ord.Time_New__c== null))){

                                  ord.Time_New__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Product Ordered'  && ((ord.Time_Product_Ordered__c < history.CreatedDate && ord.Time_Product_Ordered__c != history.CreatedDate) || ord.Time_Product_Ordered__c== null)){
                                  
                                  ord.Time_Product_Ordered__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Service Scheduled'  && (( ord.Time_Service_Scheduled__c < history.CreatedDate && ord.Time_Service_Scheduled__c != history.CreatedDate) || ord.Time_Service_Scheduled__c == null)){
                                  
                                  ord.Time_Service_Scheduled__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Warranty Submitted'  && ((ord.Time_Warranty_Submitted__c < history.CreatedDate && ord.Time_Warranty_Submitted__c != history.CreatedDate) || ord.Time_Warranty_Submitted__c==null)){
                                  
                                  ord.Time_Warranty_Submitted__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Warranty Rejected'  && ((ord.Time_Warranty_Rejected__c < history.CreatedDate && ord.Time_Warranty_Rejected__c != history.CreatedDate) || ord.Time_Warranty_Rejected__c == null)){
                                  
                                  ord.Time_Warranty_Rejected__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Service On Hold'  && ((ord.Time_Service_on_Hold__c < history.CreatedDate && ord.Time_Service_on_Hold__c != history.CreatedDate) || ord.Time_Service_on_Hold__c == null)){
                                  
                                  ord.Time_Service_on_Hold__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Customer Call Back'  && (( ord.Time_Customer_Call_Back__c < history.CreatedDate && ord.Time_Customer_Call_Back__c != history.CreatedDate) || ord.Time_Customer_Call_Back__c== null)){
                                  
                                  ord.Time_Customer_Call_Back__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Service To Be Scheduled'  && ((ord.Time_Service_to_be_Scheduled__c < history.CreatedDate && ord.Time_Service_to_be_Scheduled__c != history.CreatedDate) || ord.Time_Service_to_be_Scheduled__c== null)){
                                  
                                  ord.Time_Service_to_be_Scheduled__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Service Complete'  && ((ord.Time_Service_Complete__c < history.CreatedDate && ord.Time_Service_Complete__c != history.CreatedDate) || ord.Time_Service_Complete__c == null)){
                                  
                                  ord.Time_Service_Complete__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'To Be Ordered'  && ((ord.Time_To_Be_Ordered__c < history.CreatedDate && ord.Time_To_Be_Ordered__c != history.CreatedDate) || ord.Time_To_Be_Ordered__c == null)){
                                  
                                  ord.Time_To_Be_Ordered__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Seasonal Service'  && ((ord.Time_Seasonal_Service__c < history.CreatedDate && ord.Time_Seasonal_Service__c != history.CreatedDate) || ord.Time_Seasonal_Service__c == null)){
                                  
                                  ord.Time_Seasonal_Service__c = history.CreatedDate;
                                }
                                if(history.NewValue == 'Quote'  && ((ord.Time_Quote__c < history.CreatedDate && ord.Time_Quote__c != history.CreatedDate) || ord.Time_Quote__c== null)){
                                  
                                  ord.Time_Quote__c = history.CreatedDate;
                                }
                                    
                             ordToUpdate.add(ord);
                            } 
                }            
            }
        }
        //system.debug('@pavan ordToUpdate '+ordToUpdate );
        if(!ordToUpdate.isEmpty()){
         List<order> ordToUpdateList = new List<order>( new Set<order>(ordToUpdate)) ;
         ordToUpdate.clear();
        Update ordToUpdateList ;
        }
    }

    
    global void finish(Database.BatchableContext BC) {
        
        
    }
    
   
}