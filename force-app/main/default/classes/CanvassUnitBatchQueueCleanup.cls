global class CanvassUnitBatchQueueCleanup implements Database.Batchable<string>, Database.AllowsCallouts, Database.Stateful, Schedulable
{
    //database.executebatch(new CanvassUnitBatchQueueCleanup(),1);
    
    //CanvassUnitBatchQueueCleanup rsb = new CanvassUnitBatchQueueCleanup();
    //String sch = '0 0 22 * * ?';
    //system.schedule('Canvass Unit Custom DataLayer Batch Queue Cleanup', sch, rsb);
    
    global CanvassUnitBatchQueueCleanup()
    {

    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitBatchQueueCleanup(),1);
    }
    
    global list<string> start (Database.BatchableContext BC)
    {
        list<string> runtimeInstanceList = new list<string>();
        string runtimeInstance = 'run';
        runtimeInstanceList.add(runtimeInstance);
        return runtimeInstanceList;
    }
    
    global void execute (Database.BatchableContext BC, list<string> scope)
    {
        Date day = (Date.Today()).addDays(-14);
        
        for(string queue :scope)
        {
            list<Custom_Data_Layer_Batch_Queue__c> queueList = [SELECT Id 
                                                                FROM Custom_Data_Layer_Batch_Queue__c 
                                                                WHERE DAY_ONLY(CreatedDate) < :day
                                                                AND Status__c = 'Completed'
                                                                LIMIT 9999];
            
            delete queueList;
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}