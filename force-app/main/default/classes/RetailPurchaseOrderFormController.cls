public with sharing class RetailPurchaseOrderFormController {

    @AuraEnabled(cacheable=true)
    public static Id getVendorId(){
        Id accId = [Select Id 
                    FROM Account 
                    WHERE Name = 'Renewal by Andersen'
                    AND RecordType.DeveloperName = 'Vendor'
                    Limit 1].Id;
    
   //     system.debug('Rba Vendor Id' + accId);
        return accId;
    }
    
    @AuraEnabled(cacheable=true)
    public static Id getRecordTypeId (){
        Id recordTypeId = Schema.SObjectType.Retail_Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('RbA_Purchase_Order').getRecordTypeId();    
   //     system.debug('Rpo Record type id' + recordTypeId);
        return recordTypeId;
    }

}