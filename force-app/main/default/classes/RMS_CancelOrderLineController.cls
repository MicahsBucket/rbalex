public with sharing class RMS_CancelOrderLineController {
    
    public OrderItem oi;
    ApexPages.StandardController controller;
    public RMS_CancelOrderLineController(ApexPages.StandardController stdController) {
        controller = stdController;
        oi = (OrderItem)stdController.getRecord();
        oi.Status__c = 'Cancelled';
        oi.Purchase_Order__c = null;
    }

     public void save(){
        controller.save();
        OrderItem o =[Select id, Status__c from OrderItem where id = :oi.id ];
        system.debug('@@@' + oi.Status__c);
        delete [Select id from Warranty__c where Service_Product__c =:oi.Id];
        List<OrderItem> relatedorderitemstocancel = new list<OrderItem>();
        if (o.Status__c == 'Cancelled'){
        for(OrderItem orderitemstocancel : [select Id, PricebookEntry.Product2Id,  PricebookEntry.Product2.Name, Purchase_Order__c, Quantity, Total_Retail_Price__c, 
                         Unit_Wholesale_Cost__c, Total_Wholesale_Cost__c, Variant_Number__c, UnitPrice, Parent_Order_Item__c, Service__c, Unit_Id__c, Status__c,
                         Purchase_Order__r.Status__c, Color_Match_Hinges__c, Corrosion_Resistant_Hinges__c, Corrosion_Resistant_Lock__c, Hardware_Finish_Color__c, Window_Opening_Control_Device__c,
                         Is_Height_L_or_R_Leg_Height__c, Is_Leg_Height_L_or_R_Leg_Height__c, Leg_Height_Exterior_Fraction__c, Leg_Height_Exterior_Whole_Inch__c,Leg_Height_Interior_Fraction__c, 
                         Leg_Height_Interior_Whole_Inch__c, Meet_Rail_Dimension__c, EJ_Kerf__c, Cancellation_Reason__c
                         from OrderItem 
                         where OrderId =:oi.OrderId
                         and Parent_Order_Item__c = :oi.id 
                         and Status__c != 'Cancelled'] )
                         {
                         orderitemstocancel.Cancellation_Reason__c = oi.Cancellation_Reason__c;
                         orderitemstocancel.Status__c = 'Cancelled';
                         orderitemstocancel.DeleteWarranty__c=False;
                         relatedorderitemstocancel.add(orderitemstocancel);
                          }
                          system.debug('@@@'+ relatedorderitemstocancel );
          
      
      try{                     
       Update relatedorderitemstocancel;
       }
      Catch(exception e){
      Apexpages.addmessages(e);
         }
      }
        //return new PageReference('/'+oi.OrderId);
    }
    
    public void cancel(){
        controller.cancel();
        //return new PageReference('/'+oi.OrderId);
    
    }
}