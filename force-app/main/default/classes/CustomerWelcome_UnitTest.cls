@isTest
private class CustomerWelcome_UnitTest {
	@testSetup
    static void setup(){
		TestDataFactoryStatic.setUpConfigs();
		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		Database.insert(storeAccount, true);
		Id storeAccountId = storeAccount.Id;
		Id storeConfigId = storeAccount.Active_Store_Configuration__c;
		Id accountId = TestDataFactoryStatic.createAccount('Calvins', storeAccountId).Id;
		Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'PrimaryContact');
        Contact secondaryContact = TestDataFactoryStatic.createContact(accountId, 'SecondaryContact');
		Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
		System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
		OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        OpportunityContactRole oppContJunc2 = TestDataFactoryStatic.createOppCon('Decision Maker', false, secondaryContact, opportunity);
        
		Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        System.debug(LoggingLevel.DEBUG, 'ord: ' + ord);
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator' LIMIT 1].Id;
        User currentUser = TestDataFactoryStatic.createUser(profileID);
        currentUser.FirstName = 'testUser1';
        Database.insert(currentUser);
        User fakeUser = TestDataFactoryStatic.createUser(profileID);
        fakeUser.FirstName = 'testUser2';
        Database.insert(fakeUser);
        ord.CustomerPortalUser__c = currentUser.Id;
        Database.update(ord);         
    }

    @isTest
    static void getPrimaryTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstNAme = 'testUser1' LIMIT 1];
        String userName;
        System.runAs(currentUser){
            userName = CustomerWelcomeController.getPrimary();
        }
        System.assertEquals('PrimaryContactTest PrimaryContactContact', userName);
    }
    
    @isTest
    static void getSecondaryTest(){
        User currentUser = [SELECT Id FROM User WHERE FirstNAme = 'testUser1' LIMIT 1];
        String userName;
        System.runAs(currentUser){
            userName = CustomerWelcomeController.getSecondary();
        }
        System.assertEquals('SecondaryContactTest SecondaryContactContact', userName);
    }
}