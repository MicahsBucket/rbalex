/*
* @author Jason Flippen
* @date 03/11/2020
* @description Test Class for the following Classes:
*              - ServiceReqRecordPageHeaderController
*/ 
@isTest 
public class ServiceReqRecordPageHeaderControllerTest {

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Method to create data to be consumed by test methods.
    */ 
    @testSetup
    public static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        Account testVenderAcct = testUtility.createVendorAccount('Test Vendor Account');
        insert testVenderAcct;

        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAcct = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAcct.Store_Location__c = testStoreAcct.Id;
        insert testDwellingAcct;
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAcct.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testVenderAcct.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAcct.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAcct.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        OrderItem testOI = new OrderItem(OrderId = testOrder.Id,
                                         PricebookentryId = testPBEStandard.Id,
                                         Quantity = 2,
                                         UnitPrice = 100);
        insert testOI;

    }

    /*
    * @author Jason Flippen
    * @date 03/11/2020
    * @description Method to test the functionality in the Controller.
    */ 
    public static testMethod void testController() {

        Test.startTest();

            Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];

            // Retrieve the Test record. There should only be one record.
            ServiceReqRecordPageHeaderController.OrderWrapper order = ServiceReqRecordPageHeaderController.getOrder(testOrder.Id);
            System.assertEquals(true, (order.id != null));

            // Retrive the Map of "Job in Progress" Service Types.
            Map<String,String> serviceTypeJIPMap = ServiceReqRecordPageHeaderController.getServiceTypeOptionMap(true);
            System.assertEquals(true,serviceTypeJIPMap.containsKey('Job in Progress'));

            // Retrive the Map of Service Types that do NOT contain "Job in Progress".
            Map<String,String> serviceTypeMap = ServiceReqRecordPageHeaderController.getServiceTypeOptionMap(false);
            System.assertEquals(false,serviceTypeMap.containsKey('Job in Progress'));

            // Retrieve the flag indicating whether or not we're in a Community.
            ServiceReqRecordPageHeaderController.CongaPrintWrapper congaPrintWrapper = ServiceReqRecordPageHeaderController.getCongaPrintData();
            System.assertEquals(false, congaPrintWrapper.sourceIsCommunity);

            // Retrieve the flag indicating whether or not the "Reject" button is to be displayed.
            Boolean showRejectButton = ServiceReqRecordPageHeaderController.getShowRejectRequestButton(testOrder.Status);
            System.assertEquals(true, showRejectButton);

            // Reject the Order - This will fail because we have no financial transaction record.
            String rejectResult = ServiceReqRecordPageHeaderController.updateStatusRejected(testOrder.Id);
            System.assertEquals(false, (rejectResult == 'Success'));

            // Update the Service Type - this will succeed.
            testOrder.Service_Type__c = 'Legacy';
            String updateResult = ServiceReqRecordPageHeaderController.updateServiceType(testOrder);
            System.assertEquals('Success', updateResult);

            // Cancel the Order - This will succeed.
            String cancelResult = ServiceReqRecordPageHeaderController.updateStatusCancelled(testOrder.Id);
            System.assertEquals('Success', cancelResult);

        Test.stopTest();

    }

}