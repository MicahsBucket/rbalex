/**

* @Julie Brueggeman / Rajani Cheruku

* @description: This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization. 
* @high level order of execution: starts at bottom method(s) createUser or updateUser, moves to handleJit, moves to handleAccount, handleContact, then handleUser. handleUser will call processPermissions and processStores.
* @as of Jan 2020, this is only for the RbA Salesforce Community of RenewU. 
* @test class is SSoAutoProvisioningTest which calls testUtilitySSOcode

*/

global class SsoAutoProvisioning implements Auth.SamlJitHandler {
    //public string result = 'Failure';  //FOR possible future Exception handling 
    //public string className = 'SSoAutoProvisioing'; //For possible future exception handling
    //public string Application ='SSO Auto-Provisioning'; //For possible future exception handling
    private class JitException extends Exception{}

/**

* @author Julie Brueggeman/Rajani Cheruku
* Method: handleUser
* @description: Standard handleUser from Salesforce auto-provisioning boiler-plate code. This creates or updates the community user.

* @param boolean create: true/false on whether to create the user.
* @param User u: the Id of the existing user
* @param Map<String,String> attributes are thevalues from the saml assertion. 
* @param String federationIdentifier: the FederationId (unique identifier) of the user from ADFS or Azure B2C.
* @param boolean isCommunityUser: true/false if the user has access to the community being logged into.

*/
           
    private void handleUser(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isCommunityUser) { 
        Boolean hasRenewU = false; 
            System.debug('Running handleUser');
            if(create && attributes.containsKey('User.email')) {
                u.Username = attributes.get('User.email');
            }
            
            //if(create) { //removed the if create statement to attempt to update a user that has a federation id in the saml response, but not yet on the user.
                System.Debug('handleUser - if(create)');
                if(attributes.containsKey('User.FederationID')) {
                    u.FederationIdentifier = attributes.get('User.FederationID');
                    System.debug('User Federation ID: ' + attributes.get('User.FederationID'));
                } else {
                    u.FederationIdentifier = federationIdentifier;
                  }
            //}


            if(attributes.containsKey('User.email')) {
                u.Email = attributes.get('User.email');
                System.debug('Finding the user email; ' + attributes.get('User.email'));
            }
            if(attributes.containsKey('User.FirstName')) {
                u.FirstName = attributes.get('User.FirstName');
                System.debug('Finding the user first name: ' + attributes.get('User.FirstName'));
            }
            if(attributes.containsKey('User.Lastname')) {
                u.LastName = attributes.get('User.Lastname');
            }
            if(attributes.containsKey('User.Title') && attributes.containsKey('User.Title') != null) {
                u.Title = attributes.get('User.Title');
				u.LMS_Business_Role__c = attributes.get('User.Title');
            }   
                else{
                  u.Title = attributes.get('User.Title');  
                 }  
             
            if(attributes.containsKey('User.Department') && attributes.containsKey('User.Department') != null) {
                u.Department = attributes.get('User.Department');
				u.LMS_Business_function__c = attributes.get('User.Department');
            }
            else{
                u.Department = attributes.get('User.Department');
            }

            if(attributes.containsKey('User.IsActive')) {
                String IsActiveVal = attributes.get('User.IsActive');
                u.IsActive = '1'.equals(IsActiveVal) || Boolean.valueOf(IsActiveVal);
            }

            // new users should get the External Apps profile
            if(create && isCommunityUser) { 
                u.ProfileId = '00e61000001Qrtm'; // This is External Apps Profile
            } //else {
              //u.ProfileId = '00e61000000nVeU'; // This is full license Read only for now. 
             // }
            
            //this is the boolean/attribute which determines if the user has the permissions to the application requested. may not be needed.
            if(attributes.containsKey('User.RenewUIsAllowed')){   
                System.debug('RenewU value' + attributes.get('User.RenewUIsAllowed')); 
                String renewuuser = attributes.get('User.RenewUIsAllowed');
                if(renewuuser == 'true'){
                    hasRenewU = true;
                } 
            }   
        
            // originally attempted to set the permissions on the user to trigger off a declarative way to provide permissions to a user. may no longer be needed.
            if(hasRenewU){
                System.debug('try to update has RenewU Permissions on user');
                u.has_RenewU_Permissions__c = 'true';
                 //update u;
                System.debug('setting renewu permissions on user');
                System.debug('call processpermissions');
                //call processPermissions method to provide permissions to user.
                processPermissions(u, create);
                
            }
            if(!hasRenewU){
               throw new JitException ('You do not have permission to access this site. Contact rSupport for assistance');
            }

            if(attributes.containsKey('User.TimeZoneSidKey')) {
                u.TimeZoneSidKey = attributes.get('User.TimeZoneSidKey');
            } else if(create) {
                u.TimeZoneSidKey = 'America/Chicago';
            }

            //if(attributes.containsKey('User.RenewUIsAllowed')){   
                //System.debug('RenewU value' + attributes.get('User.RenewUIsAllowed')); 
                //String renewuuser = attributes.get('User.RenewUIsAllowed');
                //if(renewuuser == 'true'){
                    //hasRenewU = true;
                //} 
            //} 
        
		    //DefaultStore is the store which the user is located. This is necessary for UserTrigger methods. 
            if(attributes.containsKey('User.DefaultStore') && u.Default_Store_Location__c == null) {
                Account a;
                System.debug('try to set store locations text');
                String account = attributes.get('User.DefaultStore');
                System.debug('Running handleAccount method');
                a = [SELECT Id, Name FROM Account WHERE Enabled_StoreId__c=:account];
                u.Default_Store_Location__c = a.Name;
            }
			 
            // Set other Mandatory Fields
            System.debug('setting mandatory fields');
            u.Alias = u.LastName.left(5);
            u.LocaleSidKey = 'en_US'; 
            u.EmailEncodingKey = 'ISO-8859-1';
            u.LanguageLocaleKey = 'en_US';
            System.debug('mandatory fields set');

            //set the List of Stores in processStores method.
            
            System.debug('request to run processStores method');
			if (u.Store_Locations__c == null) {
            processStores(u, attributes);
			}

            // when the user exists, update the user's fields from the attributes
            if(!create){
                System.debug('update user in handleUser');
                update (u);
            }
 }

 /**

* @author Julie Brueggeman/Rajani Cheruku
* Method: handleContact
* @description: Standard handleContact from Salesforce auto-provisioning boiler-plate code. This creates or updates the contact tied to the user record.

* @param boolean create: true/false if the contact needs to be created. 
* @param String accountId: the accountID which the contact would be created or found.
* @param User u: the userId passed if the user already exists.
* @param Map<String,String> attributes are values from the saml assertion. 

*/

    private void handleContact(boolean create, String accountId, User u, Map<String, String> attributes) {
        Contact c;
        boolean newContact = false;
        System.debug('Running handleContact');
        boolean errorLog = false;


        if(attributes.containsKey('User.email')) {
            String contact = attributes.get('User.email');
            List<User> userContact = [select Id, Username, ContactId from User where Username = :contact];
            List<Contact> lstContact = new List<Contact>();
            if(userContact.size() > 0){
             lstContact = [SELECT Id, AccountId FROM Contact WHERE Id = :userContact[0].ContactId];
            }
            else{
             lstContact = [SELECT Id, AccountId FROM Contact WHERE Email=:contact AND AccountId = :accountId];
            }
            
            System.debug('Number of 1stContacts: ' + lstContact.size());
            System.debug('List is: ' + lstContact);


            if(!lstContact.isEmpty()) {
                c = lstContact[0];
                System.debug('Contact ID ' + c.Id);
                System.debug('Contact list is not empty');
                u.ContactId = c.Id;
            } 
            
            else {
                c = new Contact();
                newContact = true;
                System.debug('in handleContact the contact list empty, set newContact = true');
                }
         
         } 
         
         else {
               c = new Contact();
               newContact = true;
               System.debug('in handleContact no contact found, set newContact = true');
                }

        if(!newContact && c == null) {
            System.debug('in handleContact, newContact = false and there is no contact found based on email');
            throw new JitException('Could not find Contact based on Email: ' + attributes.get('User.email'));
            }

        if(attributes.containsKey('User.email')) {
            c.Email = attributes.get('User.email');
            }
        if(attributes.containsKey('User.FirstName')) {
            c.FirstName = attributes.get('User.FirstName');
            }
        if(attributes.containsKey('User.Lastname')) {
            c.LastName = attributes.get('User.Lastname');
            }

        if(newContact ) {
            System.debug('in handleContact, if newContact = true, insert contact');
            c.AccountId = accountId;
            insert(c);
            u.ContactId = c.Id;
            System.debug('contact created: ' + c.Id);
            }
    }

	/**

* @author Julie Brueggeman/Rajani Cheruku
* Method: handleAccount

* @Standard handleAccount from Salesforce auto-provisioning boiler-plate code. This is meant to create/update the Account where the contact/user should belong.
* @however in this case it's looking up the default store of the user as the account record to utilize. 
*
* @param boolean create: true/false if the Account will need to be new.
* @param User u: the user Id being updated or created.
* @param Map<String, String> values from the saml assertion. 

*/
    private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        Account a;
                              
        // NEVER GOING TO CREATE AN ACCOUNT, ONLY USE EXISTING

        if(attributes.containsKey('User.DefaultStore')) { //the Default store value will be the E+ Store Id on the Account of type Store.
          String account = attributes.get('User.DefaultStore');
          System.debug('Running handleAccount method');
          a = [SELECT Id, Name FROM Account WHERE Enabled_StoreId__c=:account]; //look for the E+ store ID to find the correct store. 
          System.debug('Account Name is ' + a.Name);
          System.debug('User First Name: ' + u.Firstname);
          System.debug(' User Last Name: ' + u.LastName);
          }

        if(a == null) {
            throw new JitException('Store Location does not exist in Salesforce: ' + attributes.get('User.DefaultStore'));
            }
                              
            return a.Id;
     }

/**
* @author Julie Brueggeman/Rajani Cheruku
* Method: processStores(User u, String allStores)
* @Description: converts user Stores into picklist values on a user.  These picklist values will be used to assign appropriate sharing in
                    the User after trigger
                    
* @param User u - the user being created
* @param Map<String,String> attributes: values from the saml assertion.
 */
    private void processStores(User u, Map<String, String> attributes) {
        // grab the stores. 

        System.debug('running processStores');
        system.debug('User.AllStores: ' + attributes.get('User.AllStores'));
        String stores = attributes.get('User.AllStores');
        List<string> storeList = stores.split(',');
        System.debug('storeList: ' + storeList);

        // process the store list and assign the attributes to the user
        u.Store_Locations__c = null;
        for (String st: storeList) {
            System.debug('what store: ' + storeList + 'st = ' + st);
            for (Account a : [select Name, Enabled_StoreId__c from Account where Enabled_StoreId__c = :st]) {
                if (st.equals(a.Enabled_StoreId__c)) {
                    System.debug('st = ' + st);
                    if (u.Store_Locations__c == null) {
                        u.Store_Locations__c = a.Name;
                    } else {
                        u.Store_Locations__c = u.Store_Locations__c + ';' + a.Name;
                    }  
                }
            }
        }
    }
 /**
* @author Julie Brueggeman/Rajani Cheruku
* Method: processPermissions(User u, boolean create)
* @Description: this method compares the users current permissions with what they should have for the community
* It gets the list of the necessary permissions for the community and sets them on the user for a new user or user that does not have the permissions.
* if the user exists, then it compares the list of necessary permission sets for the community to what they have on their user record and adds/removes accordingly.
* the update of the Permissions_List__c in this method will set off the User Trigger and the methods that add the permissions to the user. 
*                
* @param User u - the user being created or updated.
* @param boolean create - true or false if the user is new or not. 
*/

    private void processPermissions(User u, boolean create) {
       // get the list of permissions required for the community from custom metadata.
       List<SSO_AutoProvisioning_Setting__mdt> renewUMdtList = [select Permission_Set_Name__c from SSO_AutoProvisioning_Setting__mdt where AD_Group_Name__c = 'RenewU'];
       system.debug('RenewU List of Permissions: ' + renewUMdtList);

       // get the list of the user Permissions from the custom field Permissions_List__c
       List<User> upsList = [select Permissions_List__c from User where Id = :u.Id];
	   System.debug('user permissions List upsList: ' + upsList + 'List size: ' + upsList.size());

       // if the user is new or they have no values in the Permissions_List__c, no comparison is needed. Set the permission set values on the user.
       if(create || u.Permissions_List__c == null){
        //create set of the names of Permission Sets needed for this community from custom metadata.
        Set<string> renewUMdtSet = new Set<String>();
            //loop through all of the values from custom metadata to create the set.
            for(SSO_AutoProvisioning_Setting__mdt renewUMdt : renewUMdtList){
            renewUMdtSet.add(renewUMdt.Permission_Set_Name__c);
            }
            system.debug('user is new or the user permission list = 0, renewUMdtSet: ' + renewUMdtSet);

        //create a string of the values from the custom metatdata to use to set it on the user.

        String renewUStrings = String.join(new List<string>(renewUMdtSet), ';');
        system.debug('renewUStrings: ' + renewUStrings);

        //set that string to the value on the user's Permissions_List__c
        u.Permissions_List__c = renewUStrings;
        system.debug('Updated User Permission_Assignments__c: ' + u.Permissions_List__c);
        return;
       }
       // if the user already exists, the comparison of permissions is necessary. 
       else{
        System.debug('UserID: ' + u.Id);
        // get the current list from the user record of permissions
        //User upsList = [select Id, Permissions_List__c from User where Id = :u.Id]; moved to beginning of method to account for existing user but no permissions
        // make that list into a string for processing
        //String[] userPermissions = upsList.Permissions_List__c.split(';');
        // get the list of permission sets assigned to the user
        List<PermissionSetAssignment> psa = [select PermissionSetId from PermissionSetAssignment where AssigneeId = :u.Id];
        System.debug('Permission SEt assignment list: ' + psa);
         // make the permission set assignment list into a set
         Set<string> psaSet = new Set<String>();
        // for each of the permission set assignements for that user, loop through and get the permision set name/label and add to new list.   
        for(PermissionSetAssignment renewUpsa : psa){
            PermissionSet ps = [select Label, Name from PermissionSet where Id = :renewUpsa.PermissionSetId];
			System.debug('permission set label: ' + ps.Label + ' ' + ps.Name);
            psaSet.add(ps.Label);

        }
        System.debug('psaSet: ' + psaSet);
        String renewUStrings = String.join(new List<string>(psaSet), ';');
        System.debug('psaSet: ' + psaSet);
        System.debug('renewUStrings: ' + renewUStrings);
        //for each permission found in custom metadata check against the user's current list of permissions
        for(SSO_AutoProvisioning_Setting__mdt renewUMdt : renewUMdtList){ 
          System.debug('renewUMdt value: ' + renewUMdt);
          // if the user's list of permissions on Permissions_List__c = the list of Permissions found in metadata. do nothing. check only.
          if(upsList[0].Permissions_List__c.contains(renewUMdt.Permission_Set_Name__c) && renewUStrings.contains(renewUMdt.Permission_Set_Name__c)){
            System.debug('Inside contains check');
          }
          // checking if the metadata permision set is not contained in the user's Permissions_List__c and not contained in the Permission Set Assignment for user.
          else if(!upsList[0].Permissions_List__c.contains(renewUMdt.Permission_Set_Name__c) && !renewUStrings.contains(renewUMdt.Permission_Set_Name__c)){
            System.debug('add missing permission set value to custom field in user object');
            // add permission names to the existing permissions
            u.Permissions_List__c = u.Permissions_List__c + ';' + renewUMdt.Permission_Set_Name__c;
            System.debug('User Permission List: ' + u.Permissions_List__c);
    
          }
          // checking if the metata permission set is in the user's Permissions_List__c but not contained in the Permission Set Assignment for the user.
          else if(upsList[0].Permissions_List__c.contains(renewUMdt.Permission_Set_Name__c) && !renewUStrings.contains(renewUMdt.Permission_Set_Name__c)){
            System.debug('first remove custom field value from user object then add field value back to user object');
            // if Permissions_List__c on the user has a value, but the user does not have that permission set assigned to them, remove that value from the user's Permissions_List__c.
            u.Permissions_List__c = u.Permissions_List__c.remove(renewUMdt.Permission_Set_Name__c);
            // updating the user after the removal of the permission set name from the user Permissions_List__c to user so that the user trigger comparison runs.
            update u;
            System.debug('User Permission List after removal: ' + u.Permissions_List__c + ' removed: ' + renewUMdt.Permission_Set_Name__c);
            // adding the permission set name back to Permissions_List__c on user so that the user trigger will run and add the permission set to the user.
            u.Permissions_List__c = u.Permissions_List__c + ';' + renewUMdt.Permission_Set_Name__c;
            System.debug('adding permission back: ' + renewUMdt.Permission_Set_Name__c + ' Updated List: ' + u.Permissions_List__c);
          }
        }
      }
    }

/**

* @author Julie Brueggeman/Rajani Cheruku

* @description: Standard handleJit from Salesforce auto-provisioning boiler-plate code. This will determine the order of methods. 

* @param boolean create: true/false on whether or not the user is new. 
* @param User u: the user Id passed.
* @param Id samlSsoProviderId: the SSO provider Id.
* @param Id communityId: the community Id which they are logging into.
* @param Id portalId: the portalId which they are logging into (likely not used in this org).
* @param String federationIdentifier: the FederationId (unique identifier) for the user. From ADFS or Azure B2C.
* @param Map<String, String> attributes: the values from the saml assertion.
* @param String assertion: the string of the saml assertion. 

*/
    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        //need to parse the samle for the list of stores and add them to the stores locations picklist
          
            if(communityId != null || portalId != null) {
                System.debug('running handleJit and logging in to a community');
                String account = handleAccount(create, u, attributes);
                System.debug('running handleAccount from handleJit');
				//only run handleContact if the user is new. handlecontact was running and failing for full licensed users attempting to login.
			  if (create ){
                handleContact(create, account, u, attributes);
                System.debug('running handleContact from handleJit');
				}
                handleUser(create, u, attributes, federationIdentifier, true);
                System.debug('running handleUser from handleJit');
            } 
            
            else {
                System.debug('login not to a community - handleJit');
                handleUser(create, u, attributes, federationIdentifier, false);
                System.debug('running handleUser from handleJit no community');

                }
   }
 /*
 * @author Julie Brueggeman/Rajani Cheruku
 * Method: createUser
 * @Description: createUser will be the first method hit when the user is not found by the federationId or contact's email address. Standard SF boilerplate code.
 *  Modifications made to find when a user may already exist but does not yet have the federationID on the user record. 

 * @param Id samlSsoProviderId: the SSO provider id
 * @param Id communityId: the community Id to get logged into. To start in Jan 2020, RenewU community.
 * @param Id portalId: the portalID to get logged into (likely not used in this org)
 * @param String federationIdentifier: the federationID passed for the user in the saml. their unique identifier. can be from ADFS or Azure B2C.
 * @param Map<String,String> attributes: the attribute values in the saml assertion.
 * @param String assertion: the full string of the saml assertion. 
 */

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug('hitting createUser');
        User u = new User();
        // added a search for a potential existing user based on the user email.
        List<User> existingUser = [select Id, Username, FirstName,LastName, FederationIdentifier, ContactId, Permissions_List__c, Default_Store_Location__c, 
		     Store_Locations__c from User WHERE (Username = :attributes.get('User.email')) OR (FederationIdentifier = :attributes.get('User.FederationID'))];
        System.debug('existingUser: ' + existingUser.size());
        // if a user exists with the same email, call handleJit with existing user Id.
        if(existingUser.size() > 0){
          System.debug('user exists' + existingUser[0]);
          System.debug('calling handleJit from createUser with existingUser');
          //handle jit parameter of false means not to create a user. 
          handleJit(false, existingUser[0], samlSsoProviderId, communityId, portalId,
          federationIdentifier, attributes, assertion);
          //the user Id = the existingUser[0].
          u = existingUser[0];
        }
        //when an existing user is not found, call handleJit. 
        else{
        
        System.debug('calling handleJit from createUser, new user needed');
        //hanldeJit parameter of true means to create a new user.
        handleJit(true, u, samlSsoProviderId, communityId, portalId,
        federationIdentifier, attributes, assertion);
        System.debug('returning new user: ' + u.Id);
        
        }
        return u;
    }
  /*
 * @author Julie Brueggeman/Rajani Cheruku
 * Method: updateUser
 * @Description: updateUser will be the first method hit when the user is identified by the federationId or contact's email address. Standard SF boilerplate code.
 *  
 * @param Id userID: the Id of the user matched. 
 * @param Id samlSsoProviderId: the SSO provider id
 * @param Id communityId: the community Id to get logged into. To start in Jan 2020, RenewU community.
 * @param Id portalId: the portalID to get logged into (likely not used in this org)
 * @param String federationIdentifier: the federationID passed for the user in the saml. their unique identifier. can be from ADFS or Azure B2C.
 * @param Map<String,String> attributes: the attribute values in the saml assertion.
 * @param String assertion: the full string of the saml assertion. 
 */  
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug('hitting updateUser');
        User u = [SELECT Id, FirstName, LastName, ContactId, Permissions_List__c, Default_Store_Location__c, Store_Locations__c FROM User WHERE Id=:userId];
        System.debug('calling handleJit from updateUser');
        handleJit(false, u, samlSsoProviderId, communityId, portalId,
        federationIdentifier, attributes, assertion);
    }
}