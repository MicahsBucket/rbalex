@isTest
private class OnHoldComponentController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	@isTest static void canGetHoldsAndStores() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c survey = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		test.startTest();
			Integer holds = OnHoldComponentController.getHolds();
			Integer holdsUsed = OnHoldComponentController.holdsUsed()[0];
			system.assertEquals(5, holds);
			system.assertEquals(0, holdsUsed);
			survey.Survey_Status__c = 'Hold';
			Database.update(survey, true);
			Integer holdsUsed1 = OnHoldComponentController.holdsUsed()[0];
		test.stopTest();
	}
}