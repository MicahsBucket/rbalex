global class ServiceRequestSchedulable implements Schedulable {

	global void execute(SchedulableContext sc) {
		ServiceRequestBatchable srb = new ServiceRequestBatchable();
		database.executeBatch(srb, 20);
	}

}