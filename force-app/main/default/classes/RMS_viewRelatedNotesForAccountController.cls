public with sharing class RMS_viewRelatedNotesForAccountController {

public String strurl;
public String baseUrl{get;set;}
public final Account acct;
public List<Note> attachments {get;set;}
public List<Note> attachmentsOpp {get;set;}
public List<Note> attachmentsContact {get;set;}
public List<Note> attachmentsOrder {get;set;}
public List<Note> attachmentsServiceOrder {get;set;}
public List<Note> attachmentsChangeOrder {get;set;}
public List<Note> attachmentsActionWorkOrder {get;set;}
public List<Note> attachmentsVisitWorkOrder {get;set;}
public List<Note> attachmentsFSLWorkOrder {get;set;}
public List<Id> ids {get;set;}

public RMS_viewRelatedNotesForAccountController(ApexPages.StandardController stdController) {

  strurl = ApexPages.currentPage().getUrl();
  strurl = strurl.split('apex/')[1];
  baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

   this.acct = (Account)stdController.getRecord();
   loadOppNotes();
   loadContactNotes();
   loadOrderNotes();
   loadServiceOrderNotes();
   loadChangeOrderNotes();
   loadActionWorkOrderNotes();
   loadVisitWorkOrderNotes();
   loadFSLWorkOrderNotes();
}

public List<Note> loadOppNotes(){

    ids = new List<id>();
    attachmentsOpp = new List<Note>();
    attachments = new List<Note>();

    for (Opportunity curr: [SELECT ID, AccountId FROM Opportunity WHERE AccountId =: acct.id]){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsOpp.add(attachments);
    }
    return attachmentsOpp;
}
public List<Note> loadContactNotes(){

    ids = new List<id>();
    attachmentsContact = new List<Note>();
    attachments = new List<Note>();

    for (Contact curr: [SELECT ID, AccountId FROM Contact WHERE AccountId =: acct.id]){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsContact.add(attachments);
    }
    return attachmentsContact;
}
public List<Note> loadOrderNotes(){

    ids = new List<id>();
    attachmentsOrder = new List<Note>();
    attachments = new List<Note>();

    for (Order curr: [SELECT ID, AccountId, RecordType.Name FROM Order WHERE AccountId =: acct.id AND RecordType.Name = 'CORO Record Type']){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsOrder.add(attachments);
    }
    return attachmentsOrder;
}
public List<Note> loadServiceOrderNotes(){

    ids = new List<id>();
    attachmentsServiceOrder = new List<Note>();
    attachments = new List<Note>();

    for (Order curr: [SELECT ID, AccountId, RecordType.Name FROM Order WHERE AccountId =: acct.id AND RecordType.Name = 'CORO Service']){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsServiceOrder.add(attachments);
    }
    return attachmentsServiceOrder;
}
public List<Note> loadChangeOrderNotes(){

    ids = new List<id>();
    attachmentsChangeOrder = new List<Note>();
    attachments = new List<Note>();

    for (Order curr: [SELECT ID, AccountId, RecordType.Name FROM Order WHERE AccountId =: acct.id AND RecordType.Name = 'Change Order']){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsChangeOrder.add(attachments);
    }
    return attachmentsChangeOrder;
}
public List<Note> loadActionWorkOrderNotes(){

    ids = new List<id>();
    attachmentsActionWorkOrder = new List<Note>();
    attachments = new List<Note>();

    for (RbA_Work_Order__c curr: [SELECT ID, Account__c, RecordType.Name, Work_Order_Type__c 
                                  FROM RbA_Work_Order__c 
                                  WHERE Account__c =: acct.id AND (Work_Order_Type__c = 'Paint/Stain' OR Work_Order_Type__c = 'LWSP' OR Work_Order_Type__c = 'Building Permit' OR Work_Order_Type__c = 'HOA')]){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsActionWorkOrder.add(attachments);
    }
    return attachmentsActionWorkOrder;
}
public List<Note> loadVisitWorkOrderNotes(){

    ids = new List<id>();
    attachmentsVisitWorkOrder = new List<Note>();
    attachments = new List<Note>();

    for (RbA_Work_Order__c curr: [SELECT ID, Account__c, RecordType.Name, Work_Order_Type__c 
                                  FROM RbA_Work_Order__c 
                                  WHERE Account__c =: acct.id AND (Work_Order_Type__c = 'Service' OR Work_Order_Type__c = 'Tech Measure' OR Work_Order_Type__c = 'Install')]){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsVisitWorkOrder.add(attachments);
    }
    return attachmentsVisitWorkOrder;
}
public List<Note> loadFSLWorkOrderNotes(){

    ids = new List<id>();
    attachmentsFSLWorkOrder = new List<Note>();
    attachments = new List<Note>();

    for (WorkOrder curr: [SELECT ID,  AccountId, RecordType.Name, WorkType.Name 
                                  FROM WorkOrder 
                                  WHERE   AccountId =: acct.id ]){
      ids.add(curr.id);
    }
    for(Note attachments: [SELECT Title, Id, ParentID, Body, LastModifiedDate FROM Note WHERE ParentID IN: ids ORDER BY LastModifiedDate DESC]){
      attachmentsFSLWorkOrder.add(attachments);
    }
    return attachmentsFSLWorkOrder;
}

}