/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Customer Portal
* @date 1/18
* @description Finds the individual Payments associated to the current Order and Important Dates from the Order
**/
public without sharing class CustomerPaymentController {

    //finds current order from Order Id passed in from the Collapsable Content Parent Component 
    @AuraEnabled
    public static Order getOrder(Id orderId){
        Order currentOrder = [SELECT Retail_Total__c, Retail_Subtotal__c, Payments_Received__c, Amount_Financed__c, Amount_Due__c, Discount_Amount__c, Amount_Refunded__c, Business_Adjustments__c, Id FROM Order WHERE Id = :orderId LIMIT 1];
        return currentOrder;
    }

    //returns a list of payments associated to the order
    @AuraEnabled
    public static List<Payment__c> getPayments(Id orderId){ 
        List<Payment__c> paymentList = [SELECT Id, Payment_Date__c, Payment_Amount__c, Payment_Method__c FROM Payment__c WHERE Order__c = :orderId ORDER BY Payment_Date__c DESC];
        return paymentList;
    }
    //returns a list of refunds associated to the order
    @AuraEnabled
    public static List<Refund__c> getRefunds(Id orderId){ 
        List<Refund__c> refundList = [SELECT Id, Amount__c, Date__c,Refund_Method__c FROM Refund__c WHERE Order__c = :orderId];
        return refundList;
    }


    //finds all the important Dates associated to the current Order
    @AuraEnabled
    public static Order getOrderDates(Id orderId){
        Order ord = [SELECT Id, EffectiveDate,Status, Order_Processed_Date__c, Tech_Measure_Date__c, Time_Ready_to_Order__c, Time_Install_Scheduled__c, Install_Complete_Date__c, Job_Close_Date__c FROM Order WHERE Id = :orderId LIMIT 1];
        return ord;
    }
    
    @AuraEnabled
    public static Id getOrderId() {
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        List<Order> orders = [SELECT Id,CreatedDate,CustomerPortalUser__c, Primary_Contact__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY CreatedDate DESC];
        return orders[0].Id;
    }	
/*	@AuraEnabled
    public static MilestoneDates getAllMileStonesForOrder(Id orderId){
        MilestoneDates mdates = new MilestoneDates();
        Order ord = [Select Id,Opportunity.CloseDate,EffectiveDate,	Date_Sat_Survey_Run__c,Survey_Sent__c from Order where Id= :orderId];
        
		//Date consultationDate = ord.Opportunity.CloseDate;
       	// Date orderPlaceDate = ord.EffectiveDate;
       
       //Swapping consultationDate and orderPlaceDate for CPP- 274
       Date consultationDate = ord.EffectiveDate;
       Date orderPlaceDate = ord.Opportunity.CloseDate;
           
        mdates.consultationDate = consultationDate;
        mdates.orderPlaceDate = orderPlaceDate;
       
        List<DateTime> techMeasureDates = new List<DateTime>();
        List<DateTime> installDateList = new List<DateTime>(); //To display multiple Installations
        
        List<WorkOrder>wkds = [Select Id,Scheduled_Start_Time__c,RecordType.Name from WorkOrder where Sold_Order__c = :orderId and (RecordType.Name='Install' OR RecordType.Name ='Tech Measure')];
        for (WorkOrder wk : wkds){
            if (wk.RecordType.Name=='Install'){
                if(wk.Scheduled_Start_Time__c!=null){
                	installDateList.add(wk.Scheduled_Start_Time__c);
                }
                 mdates.installDate = wk.Scheduled_Start_Time__c;  // To display one Installation             
            } 
            else if (wk.RecordType.Name=='Tech Measure'){
                if(wk.Scheduled_Start_Time__c!=null){
                    System.debug('The start date is '+wk.Scheduled_Start_Time__c);
                	techMeasureDates.add(wk.Scheduled_Start_Time__c);
                }
            }
        }
        mdates.techMeasureDate = techMeasureDates;
        mdates.installDates = installDateList;
          
       List<Survey__c> srv = [SELECT id, Date_Sent__c FROM Survey__c where order_name__c= :orderId order by CreatedDate desc ];
		
        Date surveySentDate;
         if (srv.size()> 0 && srv.get(0).Date_Sent__c!=null) {
        	surveySentDate = srv.get(0).Date_Sent__c;
        }
        mdates.surveySentDate = surveySentDate; 
       
        return mdates;
    } */
}