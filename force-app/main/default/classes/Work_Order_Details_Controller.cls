public with sharing class Work_Order_Details_Controller {

    @AuraEnabled(cacheable=true)
    public static WorkOrder getWorkOrder(String workOrderId) {
        return [SELECT Id,Status,Scheduled_Start_Time__c,Scheduled_End_Time__c,Estimated_Ship_Date__c,AccountId,ContactId,
                       Windows__c,Doors__c,WorkTypeId,WorkType.Name,Account.Name,ServiceTerritoryId,ServiceTerritory.Name,Arrival_Date__c,
                       Contact.Name,Sold_Order__c,Sold_Order__r.Name,Sold_Order__r.OrderNumber,Special_Details_from_Sales__c,Completion_Notes__c,Opportunity__r.Name,
                       WorkOrderNumber,Opportunity__c,Series_1__c,Series_2__c,Bay_Bow__c,Patio_Doors__c,Entry_Doors__c,Primary_Resource_Name__c,Service_Request_Description__c
                FROM WorkOrder 
                WHERE Id = :workOrderId];    
    }
	
    @AuraEnabled(cacheable=true)
    public static WorkOrder getServiceAppointment(String serviceAppointmentId){
      return [SELECT Id,Status,Scheduled_Start_Time__c,Scheduled_End_Time__c,Estimated_Ship_Date__c,AccountId,ContactId,
                       Windows__c,Doors__c,WorkTypeId,WorkType.Name,Account.Name,ServiceTerritoryId,ServiceTerritory.Name,Arrival_Date__c,
                       Contact.Name,Sold_Order__c,Sold_Order__r.Name,Sold_Order__r.OrderNumber,Special_Details_from_Sales__c,Completion_Notes__c,Opportunity__r.Name,
                       WorkOrderNumber,Opportunity__c,Series_1__c,Series_2__c,Bay_Bow__c,Patio_Doors__c,Entry_Doors__c,Primary_Resource_Name__c,Service_Request_Description__c
                FROM WorkOrder 
                WHERE Id IN (Select ParentRecordId From ServiceAppointment Where id= :serviceAppointmentId)];
    }

    @AuraEnabled(cacheable=true)
    public static String getCurrentLoggedInUserTimeZone() {
        TimeZone tz = UserInfo.getTimeZone();
        return tz.getID();
    }
    
}