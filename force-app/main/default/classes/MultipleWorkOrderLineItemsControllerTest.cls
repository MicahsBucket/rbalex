@isTest
private class MultipleWorkOrderLineItemsControllerTest {

    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String MUNICIPALITY_NAME = 'Unit Test Municipality';
    private static final String APPLICATION_NOTES = 'Test Notes';
    private static final String MUNICIPALITY_CONTACT_NAME = 'Unit Test Municipality Contact';
    private static final String RESOURCE1_FIRST_NAME = 'Bert';
    private static final String RESOURCE1_LAST_NAME = 'Knerkerts';
    private static final String SERVICE_TERRITORY_NAME = 'Test Territory';
    private static final String OPP_NAME = 'Test Opportunity';
    private static final String SERVICE_APPT_SUBJECT = 'Test Service Appt';
    private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();

   @TestSetup
    static void testSetup() {

        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Store_Configuration__c storeConfig = createStoreConfig();
        insert storeConfig;

        User admin = new User(
            FirstName = 'Test',
            LastName = 'Admin',
            Email = 'test.admin@example.com',
            UserName = 'test.admin@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Super Administrator'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );

            User user1 = createUser(RESOURCE1_FIRST_NAME, RESOURCE1_LAST_NAME);
            insert user1;

            Account acc = TestDataFactory.createStoreAccount(ACCOUNT_NAME);
            insert acc;

            String pricebookId = Test.getStandardPricebookId();

            Product2 prod = new Product2();
            prod.Name = 'Test Product 1';
            prod.IsActive = true;
            insert prod;

            Product2 pro2 = new Product2();
            pro2.Name = 'Test Product 2';
            pro2.IsActive = true;
            insert pro2;

            List<Skill> mySkills = [SELECT Id FROM Skill];

            PricebookEntry pe = new PricebookEntry();
            pe.IsActive = true;
            pe.Pricebook2Id = pricebookId;
            pe.Product2Id = prod.Id;
            pe.UnitPrice = 45;
            insert pe;

            PricebookEntry pe2 = new PricebookEntry();
            pe2.IsActive = true;
            pe2.Pricebook2Id = pricebookId;
            pe2.Product2Id = pro2.Id;
            pe2.UnitPrice = 45;
            insert pe2;

            OperatingHours oh = new OperatingHours();
            oh.Name = 'Test Operating Hours';
            insert oh;

            ServiceTerritory st1 = new ServiceTerritory();
            st1.Name = 'Test Service Territory';
            st1.OperatingHoursId = oh.Id;
            st1.IsActive = true;
            insert st1;

            ServiceResource resource1 = createServiceResource(user1, storeConfig);
            insert resource1;

            ServiceTerritoryMember stMember = new ServiceTerritoryMember (
                OperatingHoursId = oh.Id,
                ServiceResourceId = resource1.Id,
                ServiceTerritoryId = st1.Id,
                EffectiveStartDate = Date.today().addDays(-1)
            );
            insert stMember;

            Service_Territory_Zip_Code__c serviceZipCode1 = new Service_Territory_Zip_Code__c();
            serviceZipCode1.Name = '53211';
            serviceZipCode1.Service_Territory__c = st1.Id;
            serviceZipCode1.WorkType__c = 'Other';
            insert serviceZipCode1;

            Service_Territory_Zip_Code__c serviceZipCode2 = new Service_Territory_Zip_Code__c();
            serviceZipCode2.Name = '53132';
            serviceZipCode2.Service_Territory__c = st1.Id;
            serviceZipCode2.WorkType__c = 'Service';
            insert serviceZipCode2;

            WorkType wtInstall = new WorkType();
            wtInstall.Name = 'Install';
            wtInstall.EstimatedDuration = 8.0;
            wtInstall.DurationType = 'Hours';
            insert wtInstall;

            WorkType wtMeasure = new WorkType();
            wtMeasure.Name = 'Measure';
            wtMeasure.EstimatedDuration = 2.0;
            wtMeasure.DurationType = 'Hours';
            insert wtMeasure;

            WorkType wtService = new WorkType();
            wtService.Name = 'Service';
            wtService.EstimatedDuration = 1.0;
            wtService.DurationType = 'Hours';
            insert wtService;


            Order testOrder =  new Order();
            testOrder.Name ='Sold Order 1';
            testOrder.AccountId = acc.Id;
            testOrder.EffectiveDate = Date.Today();
            testOrder.Status = 'Install Complete';
            testOrder.Pricebook2Id = pricebookId;
            testOrder.Customer_Pickup_All__c = FALSE;
            testOrder.Installation_Date__c = system.today()-1;
            insert testOrder;

            OrderItem item = new OrderItem();
            item.PricebookEntryId = pe.Id;
            item.OrderId = testOrder.Id;
            item.Product2Id = prod.Id;
            item.Quantity = 2;
            item.UnitPrice = 45;
            insert item;

            OrderItem item2 = new OrderItem();
            item2.PricebookEntryId = pe2.Id;
            item2.OrderId = testOrder.Id;
            item2.Product2Id = pro2.Id;
            item2.Quantity = 2;
            item2.UnitPrice = 45;
            insert item2;
            
            RbA_Skills__c mySkill = new RbA_Skills__c();
            myskill.Install_Duration__c = 5;
            mySkill.Tech_Measure_Duration__c = 5;
            insert myskill;        

            Product_Skill__c ps = new Product_Skill__c();
            ps.Product__c = prod.Id;
            ps.FSL_Skill_ID__c = mySkills[0].Id;
            ps.RBA_Skill__c = mySkill.id;
            insert ps;

            Product_Skill__c ps2 = new Product_Skill__c();
            ps2.Product__c = pro2.Id;
            ps2.FSL_Skill_ID__c = mySkills[0].Id;
            ps2.RBA_Skill__c = mySkill.id;
            insert ps2;

            Product_Skill__c ps3 = new Product_Skill__c();
            ps3.Product__c = pro2.Id;
            ps3.FSL_Skill_ID__c = mySkills[0].Id;
            ps3.RBA_Skill__c = mySkill.id;
            insert ps3;

            Municipality__c m1 = new Municipality__c();
            m1.Name = MUNICIPALITY_NAME;
            m1.Application_Notes__c = APPLICATION_NOTES;
            insert m1;

            Municipality_Contact__c mc1 = new Municipality_Contact__c();
            mc1.Name = MUNICIPALITY_CONTACT_NAME;
            mc1.Active__c = true;
            mc1.Municipality__c = m1.Id;
            insert mc1;


            Id recordId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Service').getRecordTypeId();

            Order mainOrder = [SELECT Id, AccountId FROM Order WHERE Account.Name = :ACCOUNT_NAME];
    }

    /**************************
    *      RUN TESTS
    **************************/
    
    @isTest static void test_getWorkOrderLineItems() {
        Order mainOrder = [SELECT Id, AccountId FROM Order WHERE Account.Name = :ACCOUNT_NAME];

        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = mainOrder.Id;
        wo.AccountId = mainOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Tech_Measure').Id;
        insert wo;

        WorkOrderLineItem woli = new WorkOrderLineItem(
            WorkOrderId = wo.Id
        );
        insert woli;

        // TEST
        Test.startTest();
        List<WorkOrderLineItem> results = MultipleWorkOrderLineItemsController.getWorkOrderLineItems(wo.Id);
        Test.stopTest();

        // ASSERT
        List<WorkOrderLineItem> expectedWOLI = new List<WorkOrderLineItem>(
            [SELECT Id
                FROM WorkOrderLineItem
                LIMIT 1
            ]);

        Id expected = results[0].Id;
        Id actual = expectedWOLI[0].Id;

        System.assertEquals(expected, actual);

    }
    
    @isTest static void test_setWorkOrderLineItems() {
        Order mainOrder = [SELECT Id, AccountId FROM Order WHERE Account.Name = :ACCOUNT_NAME];

        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = mainOrder.Id;
        wo.AccountId = mainOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Tech_Measure').Id;
        insert wo;

        WorkOrderLineItem woli = new WorkOrderLineItem(
            WorkOrderId = wo.Id
        );

        String serializedJSON = JSON.serialize(woli);

        //TEST
        Test.startTest();
        MultipleWorkOrderLineItemsController.setWorkOrderLineItems(wo.Id, '[' + serializedJSON + ']');
        List<WorkOrderLineItem> woliResult = [SELECT Id,
                                            WorkOrderId
                                            FROM WorkOrderLineItem
                                            LIMIT 1];
        Test.stopTest();

        //ASSERT
        Id expected = wo.Id;
        Id actual = woliResult[0].WorkOrderId;

        System.assertEquals(expected, actual);
    }

    @isTest
    static void test_deleteLineItem() {
        Order mainOrder = [SELECT Id, AccountId FROM Order WHERE Account.Name = :ACCOUNT_NAME];

        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = mainOrder.Id;
        wo.AccountId = mainOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Tech_Measure').Id;
        insert wo;

        WorkOrderLineItem woli = new WorkOrderLineItem(
            WorkOrderId = wo.Id
        );
        insert woli;

        //TEST
        Test.startTest();
        Boolean result = MultipleWorkOrderLineItemsController.deleteLineItem(woli.Id);
        Test.stopTest();

        //ASSERT
        Boolean expected = true;
        Boolean actual = result;

        System.assertEquals(expected, actual);
    }

    @isTest
    static void test_getWOLIStatuses() {
        //TEST
        Test.startTest();
            List<String> result = MultipleWorkOrderLineItemsController.getWOLIStatuses();
        Test.stopTest();

        //ASSERT
        Boolean expectedGreaterThanZero = true;
        Boolean actual;

        if(result.size() > 0){
            actual = true;
        } else {
            actual = false;
        }

        System.assertEquals(expectedGreaterThanZero, actual);
    }


    /**************************
    *      SETUP METHODS
    **************************/

    private static User createUser(String firstName, String lastName) {
        User u = new User(
            FirstName = firstName,
            LastName = lastName,
            Email = firstName + '.' + lastName + '@example.com',
            UserName = firstName + '.' + lastName + '@example.com' + (Math.random() * 1000000).intValue(),
            CompanyName = 'ABC Company',
            Alias = 'tuser',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
            TimeZoneSidKey = 'America/Chicago',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        return u;
    }

    private static Store_Configuration__c createStoreConfig() {
        Account store = new Account(
            Type = 'South',
            Name = 'Atlanta Office',
            ShippingCity = 'Atlanta',
            ShippingState = 'Georgia',
            ShippingPostalCode = '30328',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Store'].Id,
            ShippingStreet = '10 Glenlake Pkwy',
            ShippingLongitude = -84.361648,
            ShippingLatitude = 33.936008
        );
        insert store;

        Store_Configuration__c storeConfig = new Store_Configuration__c(
            Store__c = store.Id,
            Order_Number__c = 1
        );
        return storeConfig;
    }

    private static ServiceResource createServiceResource(User u, Store_Configuration__c storeConfig) {
        ServiceResource sr = new ServiceResource(
            Name = u.LastName,
            RelatedRecordId = u.Id,
            Retail_Location__c = storeConfig.Id,
            IsActive = true
        );
        return sr;
    }

    private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
    
}