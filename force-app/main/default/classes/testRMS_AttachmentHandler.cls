@isTest
public class testRMS_AttachmentHandler {
	public static testMethod void testReattachmentOrder(){
		Test.startTest();
		TestUtilityMethods utility = new TestUtilityMethods();
        utility.createOrderTestRecords();
        
		//grab the opportunity created in createOrderTestRecords
		Opportunity o= [select id, name from Opportunity limit 1];
		//get the quote created in createOrderTestRecords
		Order order =  [select id, Name from Order];
        order.Name = '6D17B6E9-8199-4523-A6C9-47E624B01B6E';
        order.rSuite_Id__c = '6D17B6E9-8199-4523-A6C9-47E624B01B6E';
        order.OpportunityId = o.id;
        update order;

		//create an attachment and make sure it goes to the quote
		Attachment anAttachment=new Attachment(ParentId=o.id,
			Name='Order-6D17B6E9-8199-4523-A6C9-47E624B01B6E-Agreement.pdf', 
			body=blob.toPDF('test string'));
		insert anAttachment;
		
		Test.stopTest();
		Attachment theAttachment = [select parentId, id from Attachment where id=:anAttachment.Id limit 1];

		//now make sure the attachment stuff gets updatedcorrectly
		system.assertEquals(theAttachment.ParentId, order.Id, 'The Attachment is not on the correct record');
		
	}

	public static testMethod void testAccountAttachments(){
		
		TestUtilityMethods utility = new TestUtilityMethods();
        //this method should set up all the data I need
        utility.createOrderTestRecords();
        
		//grab the opportunity from createOrderTestRecords()
		Opportunity o = [select id, name from Opportunity limit 1];
		Account theAccount = [select id, name from Account limit 1];

        //grab the order from createOrderTestRecords()
        Order order =  [select id, Name, OpportunityId, rSuite_Id__c from Order];
        order.Name = '6D17B6E981994523A6C947E624B01B6E';
        order.rSuite_Id__c = '6D17B6E9-8199-4523-A6C9-47E624B01B6E';
        order.OpportunityId = o.Id;
        update order;
        
		Test.startTest();
		//create an attachment and make sure it goes to the quote
		Attachment anAttachment=new Attachment(ParentId = theAccount.id,
			Name='Quote-6D17B6E9-8199-4523-A6C9-47E624B01B6E-Agreement.pdf', 
			body=blob.toPDF('test string'));
		insert anAttachment;
		
		Test.stopTest(); 

		Attachment theAttachment = [select id, ParentId, Name from attachment where id=: anAttachment.id limit 1];
		
		//now make sure the attachment stuff gets updatedcorrectly
		//the attachment should remain on the account
		system.assertNotEquals(theAttachment.ParentId, o.Id, 'The Attachment should not be on the Quote object' );
		system.assertEquals(theAttachment.ParentId, theAccount.Id, 'The Attachment should be on the Account object' );
	}
}