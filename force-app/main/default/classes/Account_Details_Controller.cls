public with sharing class Account_Details_Controller {
    
    @AuraEnabled(cacheable=true)
    public static Account getAccount(String accountId) {
        return [SELECT Id,Name,Sales_Rep_User__c,Sales_Rep_User__r.Name,Dwelling_Style__c,
                Store_Location__c,Store_Location__r.Name,Year_Built__c,HOA__c,HOA__r.Name,
                Account_Balance__c,Historical__c,Historical__r.Name,No_Service__c,
                Building_Permit__c,Building_Permit__r.Name,No_Service_Notes__c
                FROM Account 
                WHERE Id = :accountId];
    }
    
    @AuraEnabled(cacheable=true)
    public static List<OrderWrapper> getOrders(String accountId, String strurl) {

        Id serviceRecordTypeId;
        Id CORORecordTypeId;
        id changeOrderTypeId;

        changeOrderTypeId = UtilityMethods.retrieveRecordTypeId('Change_Order', 'Order');
        serviceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        CORORecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Record_Type', 'Order');
        Set<ID> recordTypeIds = new set<ID>();
        if(strurl.Contains('viewService')){
            recordTypeIds.add(serviceRecordTypeId);
        }else {
            recordTypeIds.add(CORORecordTypeId);
            recordTypeIds.add(changeOrderTypeId);   
        }

        List<OrderWrapper>orderWrapList = new List<OrderWrapper>();
        for(Order ord : [SELECT Id,Name,OrderNumber,Status,EffectiveDate,
                         Retail_Total__c,Amount_Due__c,Sales_Rep__c,
                         Estimated_Ship_Date__c,Job_Close_Date__c,
                         BillToContact.Name,Order_Processed_Date__c,
                         Time_Ready_to_Order__c,Adjusted_Arrival_Date__c,
                         Revenue_Recognized_Date__c
                         FROM  Order
                         WHERE RecordTypeId in: recordTypeIds
                         AND AccountId =: accountId 
                         Order By EffectiveDate DESC]){
                             String editLink = ord.Id;
                             OrderWrapper ordWrap = new OrderWrapper(ord.Id, ord );
                             orderWrapList.add(ordWrap);
                         }
        return orderWrapList;
    }
    
    public class OrderWrapper{
        @AuraEnabled
        public String editUrl{get;set;}
        @AuraEnabled
        public String orderNumberUrl{get;set;}
        @AuraEnabled
        public String OrderNumber{get;set;}
        @AuraEnabled
        public String status{get;set;}
        @AuraEnabled
        public Date effectiveDate{get;set;} 
        @AuraEnabled
        public Decimal retailTotal{get;set;}    
        @AuraEnabled
        public Decimal amountDue{get;set;}
        @AuraEnabled
        public Date jobCloseDate{get;set;}
        @AuraEnabled
        public Date estimatedShipDate{get;set;}
        @AuraEnabled
        public String salesRep{get;set;}
        @AuraEnabled
        public String billToContact{get;set;}
        @AuraEnabled
        public Date orderProcessedDate{get;set;} 
        @AuraEnabled
        public Datetime timeReadytoOrder{get;set;}
        @AuraEnabled
        public Date adjustedArrivalDate{get;set;}
        @AuraEnabled
        public Date revenueRecognitionDate{get;set;}
        @AuraEnabled
        public String orderId{get;set;}

        public OrderWrapper(String editLink, Order orderRec){
            editUrl = '/'+ editLink+'/e?retURL='+orderRec.Id+'&nooverride=1';
            orderNumberUrl = '/'+editLink+'?retURL='+orderRec.Id+'&nooverride=1';         
            OrderNumber = orderRec.OrderNumber;
            orderId = orderRec.Id ;
            status= orderRec.Status;
            effectiveDate = orderRec.EffectiveDate;
            retailTotal = orderRec.Retail_Total__c;
            amountDue = orderRec.Amount_Due__c;
            salesRep = orderRec.Sales_Rep__c;
            estimatedShipDate = orderRec.Estimated_Ship_Date__c;
            jobCloseDate = orderRec.Job_Close_Date__c;
            billToContact = orderRec.BillToContact.Name;
            orderProcessedDate = orderRec.Order_Processed_Date__c;
            timeReadytoOrder = orderRec.Time_Ready_to_Order__c;
            adjustedArrivalDate = orderRec.Adjusted_Arrival_Date__c;
            revenueRecognitionDate = orderRec.Revenue_Recognized_Date__c;
        }
    }
    
}