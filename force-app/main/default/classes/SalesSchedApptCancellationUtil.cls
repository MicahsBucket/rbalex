/**
 * @File Name          : SalesSchedApptCancellationUtil.cls
 * @Description        : 
 * @Author             : Brian Tremblay (Demand Chain)
 * @Group              : 
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 4/24/2019, 10:22:48 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    4/24/2019, 10:20:54 AM   James Loghry (Demand Chain)     Initial Version
**/
public class SalesSchedApptCancellationUtil{

    public static void sendApptCancellationEmails(List<Sales_Appointment__c> lCanceledAppts){
        list<string>  status = new list<string> {'Confirmed', 'Accepted', 'Checked In', 'Checked Out', 'Resulted'};
        for(Sales_Appointment_Resource__c resource :
            [Select
                Sales_Capacity__r.User__r.Contact.Email,
				Sales_Capacity__r.User__r.Contact.Secondary_Email__c,
                Sales_Appointment__r.Sales_Order__r.Opportunity__r.Name,
                Sales_Appointment__r.Appointment_Date_Time__c, Primary__c, Finalized_By__r.Contact.Email
             From
                Sales_Appointment_Resource__c
             Where
                Sales_Appointment__c IN :lCanceledAppts
                And Sales_Capacity__r.User__r.Contact.Email != null
            	And Status__c =: status]){
			system.debug('line 28 ' + resource);
            String oppName  = resource.Sales_Appointment__r.Sales_Order__r.Opportunity__r.Name;
            Datetime apptDate = resource.Sales_Appointment__r.Appointment_Date_Time__c;
            String dtStr = apptDate.format('KK:mm a');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               List<String> emailIDs = new List<String>();
                    if(String.isNotBlank(resource.Sales_Capacity__r.User__r.Contact.Email)){
                        emailIDs.add(resource.Sales_Capacity__r.User__r.Contact.Email);
                    }
                    if(String.isNotBlank(resource.Sales_Capacity__r.User__r.Contact.Secondary_Email__c)){  
                        emailIDs.add(resource.Sales_Capacity__r.User__r.Contact.Secondary_Email__c);
                    }   
            mail.setToAddresses(emailIDs);
                 if(resource.Primary__c){ 
                     if((resource.Finalized_By__r.Contact.Email)!= null){
                         mail.setCcAddresses(new List<String>{resource.Finalized_By__r.Contact.Email});
                     }
                 
           }
            mail.setSubject('Appointment Cancelled');
        	String body = 'Your Appointment, '+ oppName +', set for ' + dtStr +' Today has been cancelled. Check your schedule for any replacement Appointments.';
    		mail.setPlainTextBody(body);
        	Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
        }
    }
}