public class LeadTimeTriggerHandler {

    
    public static void makeLeadTimeInactive()
        
    {
     List<Lead_Time__c> oldLeadTimeRecords = [Select id,Lead_Time_Active__c from Lead_Time__c where Lead_Time_Active__c=true];
        for(Lead_Time__c activeLeadtimes :oldLeadTimeRecords )
        {
         activeLeadtimes.Lead_Time_Active__c=false;   
        }
         if(oldLeadTimeRecords.size()>0)
         update  oldLeadTimeRecords;        
    }
    
    /*  public static void cloneLeadTimeRecord(List<Lead_Time__c> updatedLeadTimes)
     
    { 
        List<Lead_Time__c> cloneLeadtimes = new List<Lead_Time__c>();
     
       
        for(Lead_Time__c updateLead :updatedLeadTimes)
        {
           Lead_Time__c cloneLeadTime = new Lead_Time__c();
            cloneLeadTime.CS_Closed_Through__c=updateLead.CS_Closed_Through__c;
            cloneLeadTime.CS_Updated__c=updateLead.CS_Updated__c;
            cloneLeadTime.CS_LeadTime_Days__c=updateLead.CS_LeadTime_Days__c;
            cloneLeadTime.DB_Closed_Through__c=updateLead.DB_Closed_Through__c;
            cloneLeadTime.DB_Updated__c=updateLead.DB_Updated__c;
            cloneLeadTime.DB_LeadTime_Days__c=updateLead.DB_LeadTime_Days__c;
            cloneLeadTime.DG_Closed_Through__c=updateLead.DG_Closed_Through__c;
            cloneLeadTime.DG_Updated__c=updateLead.DG_Updated__c;
            cloneLeadTime.DG_LeadTime_Days__c=updateLead.DG_LeadTime_Days__c;
            cloneLeadTime.GL_Closed_Through__c=updateLead.GL_Closed_Through__c;
            cloneLeadTime.GL_Updated__c=updateLead.GL_Updated__c;
            cloneLeadTime.GL_LeadTime_Days__c=updateLead.GL_LeadTime_Days__c;
            cloneLeadTime.CS2_Closed_Through__c=updateLead.CS2_Closed_Through__c;
            cloneLeadTime.CS2_Updated__c=updateLead.CS2_Updated__c;
            cloneLeadTime.CS2_LeadTime_Days__c=updateLead.CS2_LeadTime_Days__c;
            cloneLeadTime.GL2_Closed_Through__c=updateLead.GL2_Closed_Through__c;
            cloneLeadTime.GL2_Updated__c=updateLead.GL2_Updated__c;
            cloneLeadTime.GL2_LeadTime_Days__c=updateLead.GL2_LeadTime_Days__c;
            cloneLeadTime.PW_Closed_Through__c=updateLead.PW_Closed_Through__c;
            cloneLeadTime.PW_Updated__c=updateLead.PW_Updated__c;
            cloneLeadTime.PW_LeadTime_Days__c=updateLead.PW_LeadTime_Days__c;
            cloneLeadTime.SPW_Closed_Through__c=updateLead.SPW_Closed_Through__c;
            cloneLeadTime.SPW_Updated__c=updateLead.SPW_Updated__c;
            cloneLeadTime.SPW_LeadTime_Days__c=updateLead.SPW_LeadTime_Days__c;
            cloneLeadTime.Legacy_Closed_Through__c=updateLead.Legacy_Closed_Through__c;
            cloneLeadTime.Legacy_Updated__c=updateLead.Legacy_Updated__c;
            cloneLeadTime.Legacy_LeadTime_Days__c=updateLead.Legacy_LeadTime_Days__c;
            
            
            cloneLeadTime.Lead_Time_Active__c=false;
            if(updateLead.Lead_Time_Active__c)
            {
            cloneLeadtimes.add(cloneLeadTime);
            }
        }
     insert cloneLeadtimes;
    }*/
}