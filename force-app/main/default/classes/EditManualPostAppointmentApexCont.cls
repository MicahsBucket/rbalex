public with sharing class EditManualPostAppointmentApexCont {

@AuraEnabled
	public static Survey__c getSurvey(Id recordId){
		Survey__c survey = [SELECT Id, Name, Primary_Contact_First_Name__c,
			Primary_Contact_Last_Name__c,
			Primary_Contact_Email__c,
			Primary_Contact_Mobile_Phone__c,
			Survey_Status__c,
			Appointment_Result__c, 
			Appointment_Date__c,
			Date_Sent__c FROM Survey__c WHERE Id = :recordId LIMIT 1];
		return survey;
	}

//returns a list of workers based on their worker role
@AuraEnabled 
	public static List<Worker__c> getWorkers(String workerRole) {	
		//finds all workers of the specified role (workerRole) and excludes inactive users
		List<Worker__c> workers = [SELECT Id, First_Name__c, Last_Name__c, Role__c 
									FROM Worker__c 
									WHERE Worker__c.Role__c 
									INCLUDES (:workerRole) AND Worker__c.Role__c EXCLUDES ('Inactive') ORDER BY Worker__c.Last_Name__c ASC];
		if(workers.size() == 0){
			workers = new List<Worker__c>();
		}
		return workers;
	}

//returns the original sales rep or tech measurer associated with a survey
@AuraEnabled
	public static Worker__c getOriginalWorker(Id recordId, String role){
		Id workerId = [SELECT Id, Survey_Id__c, Worker_Id__c, Role__c FROM Survey_Worker__c 
					   WHERE Survey_Id__c = :recordId AND Role__c = :role LIMIT 1].Worker_Id__c;
		Worker__c worker = [SELECT Id, First_Name__c, Last_Name__c , Role__c
									FROM Worker__c 
									WHERE Worker__c.Id = :workerId LIMIT 1];
									system.debug('worker: ' + worker);
		return worker;
	}

//creates junction objects for all workers that have been associated with the survey
@AuraEnabled
	public static String savePAS(Id recordId, String firstName, String lastName, String email, 
									String phone, 
									Id salesRep, Worker__c originalSalesRep, String appointmentDate, String result
									){
        List<Survey_Worker__c> junctionList = new List<Survey_Worker__c>();
        Set<Survey_Worker__c> removeJunctionList = new Set<Survey_Worker__c>();
        if(phone != null){
        	if(phone.length() == 10){
        	String first3Digits = phone.substring(0,3);
        	String last9Characters = phone.substring(3,10);
        	String first5Characters = '(' + first3Digits + ')';
        	phone = first5Characters + last9Characters;
        	}
        }
        

        //updates survey information (not including workers)
		Survey__c survey = [SELECT Id, Primary_Contact_First_Name__c,
			Primary_Contact_Last_Name__c,
			Primary_Contact_Email__c,
			Primary_Contact_Mobile_Phone__c
		    FROM Survey__c WHERE Id = :recordId LIMIT 1];
		
		survey.Primary_Contact_First_Name__c = firstName;
		survey.Primary_Contact_Last_Name__c = lastName;
		survey.Primary_Contact_Email__c = email;
		survey.Primary_Contact_Mobile_Phone__c = phone;
		survey.Appointment_Result__c = result;	

		survey.Appointment_Date__c = date.valueOf(appointmentDate);

		//creates a list of original sales rep junctions to delete
		if(originalSalesRep != null){
			Id osrId = originalSalesRep.Id;
			Survey_Worker__c junctionToDelete = [SELECT Id, Worker_Id__c FROM Survey_Worker__c 
												WHERE Survey_Worker__c.Worker_Id__c = :osrId 
												AND Survey_Worker__c.Survey_Id__c = :recordId LIMIT 1];
			removeJunctionList.add(junctionToDelete);
		}

		//builds a list of sales rep junctions to create
		if(salesRep != null){
			Survey_Worker__c salesRepJunction = new Survey_Worker__c(
			Survey_Id__c = survey.Id,
			Worker_Id__c = salesRep,
			Role__c = 'Sales Rep'
			);
			junctionList.add(salesRepJunction);	
		}
		
		//deletes all old junction objects for all old workers that have been removed from survey
		if(removeJunctionList.size() > 0){
			List<Survey_Worker__c> deleteList = new List<Survey_Worker__c>(removeJunctionList);
			Database.delete(deleteList, true);
		}

		//upserts all new information
		if(junctionList.size() > 0){
			Database.upsert(junctionList, true);
		}
		
		//sends correct error information to controller to give user correct error message
		try{
			Database.UpsertResult upsertError = Database.upsert(survey, true);
		} catch(DmlException dml){
			if(String.valueOf(dml).contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
				return 'Validation Error';
			}
		}
		return 'No Errors';
		
	}

@AuraEnabled
	public static void editRecord(String email, String recordId){
		Survey__c survey = [SELECT Primary_Contact_Email__c, Secondary_Contact_email__c, Name FROM Survey__c WHERE Survey__c.Id = :recordId LIMIT 1];
		survey.Primary_Contact_Email__c = email;
		Database.update(survey, true);
	}

//updates status to hold or send based on which button was pressed, if survey is already on hold survey is unchanged
@AuraEnabled
	public static String updateStatus(String status, Id recordId){
		Survey__c survey = [SELECT Sent_To_Medallia__c, Name FROM Survey__c WHERE Survey__c.Id = :recordId LIMIT 1];
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
		if(survey.Sent_To_Medallia__c == true){
			return 'already sent';
		} else {
			survey.Send_to_Medallia__c = true;
			Database.update(survey, true);
			return 'send';
		}
	}
}