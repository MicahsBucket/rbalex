/**
 * @File Name          : SalesSchedUtilsWithoutSharing.cls
 * @Description        : Utility for doing "without sharing" stuffs like changing opportunity ownership.
 * @Author             : Demand Chain (James Loghry)
 * @Group              :
 * @Last Modified By   : James Loghry (Demand Chain)
 * @Last Modified On   : 5/23/2019, 11:35:25 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    2/27/2019, 12:14:56 PM   Demand Chain (James Loghry)     Initial Version
**/
public without sharing class SalesSchedUtilsWithoutSharing {

    public static void createResource(Sales_Appointment__c sa, Sales_Capacity__c sc, Id resourceId){
        update new Opportunity(Id=sa.Sales_Order__r.Opportunity__c,OwnerId=sc.User__c);

        String enabledId = sc.User__r.Enabled_User_Id__c + '_' + sc.Slot__r.Name +'_' + ((Datetime)sc.Date__c).format('YYYY-MM-DD');

        upsert new Sales_Appointment_Resource__c(
            Enabled_Id__c = enabledId,
            Sales_Capacity__c = sc.Id,
            Sales_Appointment__c = sa.Id,
            Status__c = 'Assigned'
        ) Enabled_Id__c;
    }

    public static Map<Id,Sales_Capacity__c> getSalesCapacityMap(Set<Id> capacityIds){
        return new Map<Id,Sales_Capacity__c>(
            [Select
                Id,
                User__r.Id,
                User__r.Name,
                User__r.Email,
                User__r.Phone,
                User__r.MobilePhone,
                User__r.SmallPhotoUrl,
                User__r.Contact.MobilePhone,
                User__r.Contact.Phone,
             	User__r.ContactId
             From
                Sales_Capacity__c
             Where
                Id in :capacityIds]
        );
    }

    public static void addEvents(Map<Id,List<Sales_Appointment_Resource__c>> opportunityToUsersMap, boolean addEvent){
        Sales_Schedule_Setting__mdt sss = [Select Rba_Integration_User_Id__c From Sales_Schedule_Setting__mdt Where QualifiedApiName='Default'];
        List<Event> eventsToUpsert = new List<Event>();
        List<Event> eventsToDelete = new List<Event>();

        Map<Id,List<Event>> opportunityToEventMap = new Map<Id,List<Event>>();
        for(Event e :
            [Select
                WhatId,
                OwnerId,
                Dwelling__c,
                StartDateTime,
                EndDateTime,
                ReminderDateTime,
                Type
             From
                Event
             Where
                WhatId in :opportunityToUsersMap.keySet()
             Order By
                CreatedDate Asc]){
            List<Event> events = opportunityToEventMap.get(e.WhatId);
            if(events == null){
                events = new List<Event>();
                opportunityToEventMap.put(e.WhatId, events);
            }
            events.add(e);
        }

        for(Id opportunityId : opportunityToUsersMap.keySet()){
            Event opportunityEvent = null;
            List<Event> events = opportunityToEventMap.get(opportunityId);

            if(events != null && !events.isEmpty()){
                for(Sales_Appointment_Resource__c sar : opportunityToUsersMap.get(opportunityId)){
                    boolean foundAssignedEvent = false;
                    for(Integer i=0; i < events.size(); i++){
                        Event e = events.get(i);
                        if(!addEvent && e.OwnerId == sar.Sales_Capacity__r.User__c){
                            if(i == 0){
                                e.OwnerId = sss.Rba_Integration_User_Id__c;
                                eventsToUpsert.add(e);
                            }else{
                                eventsToDelete.add(e);
                            }
                        }
                        foundAssignedEvent |= (e.OwnerId == sar.Sales_Capacity__r.User__c);
                    }

                    if(addEvent && !foundAssignedEvent){
                        Event e = events.get(0);
                        System.debug('JWL: sar: primary: ' + sar.Primary__c);
                        //e.Primary_Resource__c = sar.Primary__c;
                        if(!sar.Primary__c){
                            e = e.clone(false, false, true, false);
                        }
                        System.debug('JWL: e: ' + e);
                        e.OwnerId = sar.Sales_Capacity__r.User__c;
                        eventsToUpsert.add(e);
                    }
                }
            }
        }

        upsert eventsToUpsert Id;
        delete eventsToDelete;
    }

    //Used by sales manager auto assignment, because sometimes they wont have visibility to other manager's capacities, which
    //will throw a record security related error.
    public static void insertResources(List<Sales_Appointment_Resource__c> resources){
        insert resources;
    }

    //Used by sales manager auto assignment, because sometimes they wont have visibility to other manager's capacities, which
    //will throw a record security related error.
    public static void updateAppointments(List<Sales_Appointment__c> appointments){
        update appointments;
    }
}