@isTest
public with sharing class cancelOrderButtonControllerTEST {
    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String ACCOUNT_NAME2 = 'Test Account2'; 
    private static final Map<String, RecordType> woRecordTypeMap = createWorkOrderRecordTypeMap();
    private static Id salesProfileId;

    //@isTest
    public static void testSetup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account acc = TestDataFactory.createStoreAccount(ACCOUNT_NAME);
        insert acc;
        
        Account acc2 = TestDataFactory.createStoreAccount(ACCOUNT_NAME2);
        insert acc2;
        
        String pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2();
        prod.Name = 'Test Product 1';
        prod.IsActive = true;
        insert prod;
        
        Product2 pro2 = new Product2();
        pro2.Name = 'Test Product 2';
        pro2.IsActive = true;
        insert pro2;
        
        List<Skill> mySkills = [SELECT Id FROM Skill];
        
        
        PricebookEntry pe = new PricebookEntry();
        pe.IsActive = true;
        pe.Pricebook2Id = pricebookId;
        pe.Product2Id = prod.Id;
        pe.UnitPrice = 45;
        insert pe;
        
        PricebookEntry pe2 = new PricebookEntry();
        pe2.IsActive = true;
        pe2.Pricebook2Id = pricebookId;
        pe2.Product2Id = pro2.Id;
        pe2.UnitPrice = 45;
        insert pe2;

        Opportunity myOpp = new Opportunity(
            Name = 'testOpp' + Date.today().format(),
            StageName = 'New',
            Pricebook2Id = pricebookId,
            CloseDate = Date.today()
        );
        insert myOpp;

        
        
        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        testOrder.OpportunityId = myOpp.Id;
        insert testOrder;
        
        Order testOrder2 =  new Order();
        testOrder2.Name ='Sold Order 1';
        testOrder2.AccountId = acc2.Id;
        testOrder2.EffectiveDate = Date.Today();
        testOrder2.Status = 'Install Complete';
        testOrder2.Pricebook2Id = pricebookId;
        testOrder2.Customer_Pickup_All__c = FALSE;
        testOrder2.Installation_Date__c = system.today()-1;
        
        insert testOrder2;
        
        OrderItem item = new OrderItem();
        item.PricebookEntryId = pe.Id;
        item.OrderId = testOrder.Id;
        item.Product2Id = prod.Id;
        item.Quantity = 2;
        item.UnitPrice = 45;
        insert item;
        
        OrderItem item2 = new OrderItem();
        item2.PricebookEntryId = pe2.Id;
        item2.OrderId = testOrder.Id;
        item2.Product2Id = pro2.Id;
        item2.Quantity = 2;
        item2.UnitPrice = 45;
        insert item2;

        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = testOrder.Id;
        wo.AccountId = testOrder.AccountId;
        wo.RecordTypeId = woRecordTypeMap.get('Tech_Measure').Id;
        insert wo;

        Task myTask1 = new Task();
        myTask1.Subject = 'test';
        myTask1.WhatId = testOrder.Id;
        myTask1.Status = 'Open';
        myTask1.Service_Type__c = 'Service Task';
        insert myTask1;

        Account store1 = [select Id, Active_Store_Configuration__c from Account 
                  where Name = '77 - Twin Cities, MN' Limit 1];

        Store_Discount__c sd = new Store_Discount__c();
        sd.rSuite_Discount_Name__c='PromotionalPerUnitPriceView';
        sd.Store_Configuration__c=store1.Active_Store_Configuration__c;
        insert sd;

        Decimal originalDiscountAmt = 50;
        Store_Discount__c sd1 = [Select Id from Store_Discount__c limit 1];
        Order_Discount__c od1 = new Order_Discount__c(Order__c = testOrder.Id, 
            Discount_Amount__c = 0,
            Store_Discount__c = sd1.Id,
            Discount_Processed_Date__c = Date.today());
        insert od1;
        // Testing discount change in same period
        od1.Discount_Amount__c = originalDiscountAmt;

        update od1;

        Decimal discountAmt = 200;
        // Update the Discount again and test that change
        od1.Discount_Amount__c = discountAmt;
        update od1;

        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Order__c = testOrder.Id,
                                                                    RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId(),
                                                                    Status__c = 'In Process');
        insert testPurchaseOrder;
    }

    @isTest
    public static void getOrderTEST() {
        UtilityMethods.disableAllFslTriggers();
        testSetup();
        Order testOrder = [SELECT Id FROM Order Limit 1];
        system.debug(testOrder);
        cancelOrderButtonController.getOrder(testOrder.Id);
    }

    @isTest
    public static void cancelOrderTEST() {
        UtilityMethods.disableAllFslTriggers();
        testSetup();
        Order testOrder = [SELECT Id FROM Order Limit 1];
        system.debug(testOrder);
        cancelOrderButtonController.cancelOrder(testOrder.Id, 'Unknown');
    }

    private static Map<String, RecordType> createWorkOrderRecordTypeMap() {
        UtilityMethods.disableAllFslTriggers();
        List<RecordType> woRecordTypes = [select Id, DeveloperName from RecordType where sObjectType = 'WorkOrder'];
        Map<String, RecordType> woRecordTypeMap = new Map<String, RecordType>();
        for (RecordType rt : woRecordTypes) {
            woRecordTypeMap.put(rt.DeveloperName, rt);
        }
        return woRecordTypeMap;
    }
}