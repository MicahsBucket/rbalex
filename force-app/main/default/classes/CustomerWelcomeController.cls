/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Customer Portal
* @date 1/18
* @description Finds the Customers First and Last Name
**/
public without sharing class CustomerWelcomeController  {

    //finds the associated Contact and returns the first and last name of the Contact/User
    @AuraEnabled
    public static String getPrimary(){
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        Order ord = [SELECT Id, OpportunityId, CustomerPortalUser__c, Primary_Contact__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC LIMIT 1];
        try{
            OpportunityContactRole oppCon = [SELECT Id, OpportunityId, ContactId 
                                    FROM OpportunityContactRole 
                                    WHERE OpportunityId = :ord.OpportunityId AND isPrimary = true LIMIT 1];
            Contact cont = [SELECT FirstName, LastName FROM Contact WHERE Id = :oppCon.ContactId LIMIT 1];
            String name = cont.FirstName + ' ' + cont.LastName;
            return name;
        } catch(NullPointerException npe){
            return '';
        }
    }

    //finds the associated Contact and returns the first and last name of the Contact/User
    @AuraEnabled
    public static String getSecondary(){
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        Order ord = [SELECT Id, OpportunityId, CustomerPortalUser__c,Primary_Contact__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC LIMIT 1];
        try{
            OpportunityContactRole oppCon = [SELECT Id, OpportunityId, ContactId 
                                    FROM OpportunityContactRole 
                                    WHERE OpportunityId = :ord.OpportunityId AND isPrimary = false LIMIT 1];
            Contact cont = [SELECT FirstName, LastName FROM Contact WHERE Id = :oppCon.ContactId LIMIT 1];
            String name = cont.FirstName + ' ' + cont.LastName;
            return name;
        } catch(NullPointerException npe){
            return '';
        }
    }
}