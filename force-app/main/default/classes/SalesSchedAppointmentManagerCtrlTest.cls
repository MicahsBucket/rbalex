@isTest
private class SalesSchedAppointmentManagerCtrlTest {

    static testmethod void testGetMyAppointmentsResulted(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
         
        //Create a past appointment that has not been resulted yet.
        sstu.createPastAppointments(1);
       	update new Sales_Order__c(
            Id=sstu.salesOrder.Id,
            Result__c = 'Sale',
        	Time_Arrived__c = Datetime.now().time(),
        	Time_Left__c = Datetime.now().time(),
        	Payment_Type__c = 'Cash',
        	Deposit_Type__c = 'Cash',
			Deposit__c = 1,
        	Construction_Department_Notes__c = 'Notes',
        	Why_Customer_Purchased__c = 'Because',
        	Concerns__c = 'Yes',
        	Address_Concerns__c = 'Yes',
        	Partial_Sale__c = 'No',
        	Historical_Area__c = 'No',
        	Mail_and_Canvass_Neighborhood__c = 'Yes',
        	Were_All_Owners_Present__c = 'Yes',
        	Key_Contact_Person__c='Person');
        
		resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.pastCapacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Resulted',
                Assignment_Reason__c = 'Manual'
            )
        );
      	insert resources;
        
        SalesSchedAppointmentManagerCtrl.MyAppointmentWrapper maw = null;
        Test.startTest();
        System.runAs(sstu.reps.get(0)){
        	maw = SalesSchedAppointmentManagerCtrl.getMyAppointments(((Datetime)Date.today()).formatGmt('yyyy-MM-dd'));
            SalesSchedAppointmentManagerCtrl.getAppointmentWrapperNotResulted(new Set<ID>{sstu.appointments.get(0).Id},maw);
        }
        Test.stopTest();
        
        System.assert(!maw.available.isEmpty());
        System.assert(maw.showResultedAppointments);
    }
    static testmethod void testFinanceCompanies()
    {
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
    	RecordType rt = [Select Id From RecordType Where DeveloperName='Finance_Company'];
        Account finance = new Account(Name='Finance',RecordTypeId=rt.Id);
        insert finance;

        RecordType rt2 = [Select Id From RecordType Where DeveloperName='Cash_Payment' And sObjectType = 'Store_Finance_Program__c'];
        Store_Finance_Program__c sfp = new Store_Finance_Program__c(
			RecordTypeId = rt2.Id,
			Store_Configuration__c = sstu.store.Active_Store_Configuration__c,
			Active__c = true,
			Name = 'TestStoreFinanceProgram',
            Program_Fee__c = 0,
            Finance_Company__c = finance.Id
		);
        insert sfp;
        
        insert new Store_Finance_Program__share(UserOrGroupId=sstu.reps.get(0).Id,ParentId=sfp.Id,AccessLevel='Read',RowCause=Store_Finance_Program__share.RowCause.Manual);
        SalesSchedAppointmentManagerCtrl.MyAppointmentWrapper maw = null;
        Test.startTest();
        System.runAs(sstu.reps.get(0)){
        	SalesSchedAppointmentManagerCtrl.getFinanceCompanies(sstu.opportunity.Store_Location__c,'test');
            SalesSchedAppointmentManagerCtrl.getFinancePlans(sstu.opportunity.Store_Location__c,finance.Id,'Test');
        }
        Test.stopTest();
    }
    
    static testmethod void testGetMyAppointmentsNotResulted(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        
       	List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.capacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Assigned',
                Assignment_Reason__c = 'Manual'
            )
        );
         
        //Create a past appointment that has not been resulted yet.
        sstu.createPastAppointments(1);
        sstu.salesOrder.Result__c = 'Sale';
        sstu.salesOrder.Time_Arrived__c = Datetime.now().time();
        sstu.salesOrder.Time_Left__c = Datetime.now().time();
        update sstu.salesOrder;
        
		resources.add(
            new Sales_Appointment_Resource__c(
        		Sales_Capacity__c = sstu.pastCapacities.get(0).Id,
           		Sales_Appointment__c = sstu.appointments.get(0).Id,
        		Status__c = 'Confirmed',
                Assignment_Reason__c = 'Manual'
            )
        );
      	insert resources;
        
        SalesSchedAppointmentManagerCtrl.MyAppointmentWrapper maw = null;
        Test.startTest();
        System.runAs(sstu.reps.get(0)){
        	maw = SalesSchedAppointmentManagerCtrl.getMyAppointments(((Datetime)Date.today()).formatGmt('yyyy-MM-dd')); 
            maw = SalesSchedAppointmentManagerCtrl.getMyAppointments(null);
        }
        Test.stopTest();
        
        System.assert(!maw.available.isEmpty());
        //System.assert(!maw.showResultedAppointments);
    }
}