@isTest 
private class OpportunityTriggerHandlerTest {

	@testSetup static void setupOpportunityTriggerHandlerTest() {

		// Set up default configs
		TestDataFactoryStatic.setUpConfigs();

	}

	@isTest
	private static void testOnBeforeInsert() {
		// Create dwelling and store accounts
		Account dwelling = TestDataFactoryStatic.createDwellingAccount('OpptyBeforeInsertDwelling');
		Account store = TestDataFactoryStatic.createStoreAccount('OpptyBeforeInsertStore');
		upsert dwelling;
		upsert store;

		// Create related contacts
		Contact resident = TestDataFactory.createCustomerContact(dwelling.id);
		upsert resident;

		// Create opportunity
		Opportunity opt = new Opportunity();
		opt.Name = 'OpptyAfterInsertOppty123';
		opt.AccountId = dwelling.Id;
		opt.StageName = 'New';
		opt.Store_Location__c = store.Id;
		opt.CloseDate = Date.newInstance(2018, 12, 31);
		
		// Insert opportunity, this will create the Opportunity
		Test.startTest();		
		insert opt;
		Test.stopTest();

		//Assert that the pricebookId on the opportunity is not null
		System.assertNotEquals(null, [select pricebook2Id from Opportunity where Id =: opt.Id]);
	}
	@isTest
	private static void testOnAfterInsert() {

		// Create dwelling and store accounts
		Account dwelling = TestDataFactoryStatic.createDwellingAccount('OpptyAfterInsertDwelling');
		Account store = TestDataFactoryStatic.createStoreAccount('OpptyAfterInsertStore');
		upsert dwelling;
		upsert store;

		// Create related contacts
		Contact resident = TestDataFactory.createCustomerContact(dwelling.id);
		upsert resident;

		// Create opportunity
		Opportunity opt = new Opportunity();
		opt.Name = 'OpptyAfterInsertOppty123';
		opt.AccountId = dwelling.Id;
		opt.StageName = 'New';
		opt.Store_Location__c = store.Id;
		opt.CloseDate = Date.newInstance(2018, 12, 31);

		// Insert opportunity, this will create the OpportunityContactRole
		Test.startTest();		
		insert opt;
		Test.stopTest();

		// Assert that we created the opportunity contact role
		//System.assertEquals(1, (Integer)[SELECT COUNT() FROM OpportunityContactRole WHERE ContactId=:resident.Id AND OpportunityId=:opt.id LIMIT 1]);

	}
}