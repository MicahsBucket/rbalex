/******************************************************************
 * Author : Sundeep
 * Class Name :BatchJobPostServiceSurvey
 * Createdate :
 * Description : Batch Job For Creating Service Surveys. 
 * Change Log
 * ---------------------------------------------------------------------------
 * Date:- 11-1-2019      Name :- Sundeep Goddety         Description
 * ---------------------------------------------------------------------------
 * 
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/

global class BatchJobPostServiceSurvey implements Database.Batchable<sObject>{
    String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
         Date d = Date.today().addDays(-1);
        
        String query;     
        list<string>  status = new list<string>();
        status.add('Closed');
        status.add('Warranty Submitted'); 
        Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('CORO Service').getRecordTypeId();
        query = 'SELECT Id,Revenue_Recognized_Date__c,of_Visits__c	,BillToContactid,AccountId,Sold_Order__c,Service_Type__c, OpportunityId,Survey_Created__c,Description,Warranty_Date_Submitted__c, EffectiveDate, Store_Location__c FROM Order WHERE Survey_Created__c=false and Service_Type__c != \'Job in Progress\' and Status in: status and  Revenue_Recognized_Date__c = :d  and RecordTypeId = :recordTypeId';
        system.debug('query string printing --'+query);
        return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC, List<Order> scope) {
        list<order> ordList = new list<order>();
        for(order o:scope){
            ordList.add(o);
        }
        if(!ordList.isEmpty()){
            PostServiceSurveyHandler.surveyServiceProcessCreation(ordList);
        }
       
    }
    
    global void finish(Database.BatchableContext BC) {
        
        
    }
    
}