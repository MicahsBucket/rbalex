/**
* @author Jason Flippen
* @date 01/28/2021
* @description Class to provide functionality for the NewPaymentRedirect Aura Component.
*
*              Test code coverage provided by the following Test Class:
*			   - NewPaymentRedirectControllerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class NewPaymentRedirectController {

    /**
    * @author Jason Flippen
    * @date 01/28/2021
    * @description: Method to retrieve data from the object.
    * @param recordId
    * @param objectName
    * @return RecordWrapper
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @AuraEnabled
    public static RecordWrapper getRecordData(String recordId, String objectName) {

        RecordWrapper wrapper = new RecordWrapper();

        // Default Wrapper values.
        wrapper.id = '';
        wrapper.contactId = '';
        wrapper.recordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Misc_Cash_Receipt').getRecordTypeId();
        wrapper.storeLocationId = '';

        System.debug('***** recordId: ' + recordId);
        System.debug('***** objectName: ' + objectName);

        if (String.isNotBlank(recordId) && objectName == 'Order') {

            // Retrieve Order data and update Wrapper.
            
            Order order = [SELECT Id,
                                  BillToContactId,
                                  Store_Location__c
                           FROM   Order
                           WHERE  Id = :recordId];
            
            wrapper.id = order.Id;
            wrapper.contactId = order.BillToContactId;
            wrapper.recordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Deposit').getRecordTypeId();
            wrapper.storeLocationId = order.Store_Location__c;

        }
        else {

            String storeLocationName = '77 - Twin Cities, MN';

            if (!Test.isRunningTest()) {

                // If we aren't running a Test, retrieve the Default
                // Store Location from the current User.
                storeLocationName = [SELECT Default_Store_Location__c
                                     FROM   User
                                     WHERE  Id = :UserInfo.getUserId()].Default_Store_Location__c;
                
            }
            
            // Retrieve the (Account) Id of the Store Location.
            Map<Id,Account> storeAccountMap = new Map<Id,Account>([SELECT Id
                                                                   FROM   Account
                                                                   WHERE  Name = :storeLocationName]);

            // Update the Wrapper.
            if (!storeAccountMap.isEmpty()) {
                wrapper.storeLocationId = storeAccountMap.values()[0].Id;
            }
            
        }
        System.debug('***** wrapper: ' + wrapper);

        return wrapper;
        
    }

    /**
    * @author Jason Flippen
    * @date 01/28/2021
    * @description: Generic Class to wrap Record data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @TestVisible
    public class RecordWrapper {

        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public String contactId {get;set;}

        @AuraEnabled
        public String storeLocationId {get;set;}

        @AuraEnabled
        public String recordTypeId {get;set;}

    }

}