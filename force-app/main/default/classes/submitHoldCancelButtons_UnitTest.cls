@isTest
private class submitHoldCancelButtons_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	@isTest static void canDetermineRecordType() {
		User testUser = [SELECT Id FROM User WHERE FirstName = 'Test' LIMIT 1];
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c surveyPIS = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Survey__c surveyPAS = TestDataFactory.createSurveys('Post Appointment', 1, accountId, 'Pending')[0];
        Survey__c surveyS = TestDataFactory.createSurveys('Service', 1, accountId, 'Pending')[0];
		surveyPIS.Survey_Status__c = 'Incomplete Data';
		update surveyPIS;
		test.startTest();
			Boolean pisBool = submitHoldCancelButtonsApexController.determineRecordType(surveyPIS.Id)[0];
			Boolean pasBool = submitHoldCancelButtonsApexController.determineRecordType(surveyPAS.Id)[0];
			Boolean incomplete = submitHoldCancelButtonsApexController.determineRecordType(surveyPIS.Id)[1];
			system.assertEquals(true, pisBool);
			system.assertEquals(false, pasBool);
			system.assertEquals(true, incomplete);
		test.stopTest();
	}
	
	@isTest static void canGetSurvey() {
		User testUser = [SELECT Id FROM User WHERE FirstName = 'Test' LIMIT 1];
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c surveyPIS = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		test.startTest();
			Survey__c returnSurvey = submitHoldCancelButtonsApexController.getSurvey(surveyPIS.Id);
			system.assertEquals(surveyPIS.Id == returnSurvey.Id, true);
		test.stopTest();
	}
	@isTest static void canEditRecord() {
		User testUser = [SELECT Id FROM User WHERE FirstName = 'Test' LIMIT 1];
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c surveyPIS = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Id surveyId = surveyPIS.Id;
		String newEmail = 'calvin@gmail.com';
		String oldEmail = surveyPIS.Primary_Contact_Email__c;
		test.startTest();
			submitHoldCancelButtonsApexController.editRecord(newEmail, surveyPIS.Id);
			String newEmailSurvey = [SELECT Primary_Contact_Email__c FROM Survey__c WHERE Id = :surveyId LIMIT 1].Primary_Contact_Email__c;
			system.assertEquals(newEmailSurvey == newEmail, true);
			system.assertEquals(newEmailSurvey == oldEmail, false);
		test.stopTest();
	}
	@isTest static void canUpdateStatus() {
		User testUser = [SELECT Id FROM User WHERE FirstName = 'Test' LIMIT 1];
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		List<Survey__c> surveysPending = TestDataFactory.createSurveys('Post Install', 2, accountId, 'Pending');
		Survey__c surveyPending1 = surveysPending[0];
		Survey__c surveyPending2 = surveysPending[1];
		Survey__c surveySend = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Send')[0];
		Survey__c surveyHold = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Hold')[0];
		test.startTest();
			String statusSendtoSend = submitHoldCancelButtonsApexController.updateStatus('Send', surveySend.Id);
			system.assertEquals('send', statusSendtoSend);
			String statusHoldtoSend = submitHoldCancelButtonsApexController.updateStatus('Send', surveyHold.Id);
			system.assertEquals('send', statusHoldtoSend);
			List<Survey__c> surveyHoldList = TestDataFactory.createSurveys('Post Install', 4, accountId, 'Hold');
			String statusPendingtoHold = submitHoldCancelButtonsApexController.updateStatus('Hold', surveyPending1.Id);
			String statusPendingtoHoldFail = submitHoldCancelButtonsApexController.updateStatus('Hold', surveyPending2.Id);
			system.assertEquals('hold', statusPendingtoHold);
			system.assertNotEquals('hold', statusPendingtoHoldFail);
			system.assertEquals('hold limit', statusPendingtoHoldFail);
		test.stopTest();
	}
	
	
}