public class CanvassWaypointTriggerHandler {
    public static void UpdateWaypointNotesDoNotKnock(List<sma__mawaypoint__c> waypoints) {
        // Create the list to add the waypoints to update
        List<sma__mawaypoint__c> listWp = new List<sma__mawaypoint__c>();
        // List to store the canvass unit ids from the waypoints
        List<string> listLinkId = new List<string>();
        // List to store the instantiated canvass units from the listLinkIds
        List<CNVSS_Canvass_Unit__c> listCU = new List<CNVSS_Canvass_Unit__c>();
        // Map to store the key (canvassunit id) and value (waypoint object) 
        Map<string, sma__mawaypoint__c> waypointCanvassMap = new Map<string, sma__mawaypoint__c>();
        for(sma__mawaypoint__c wp : waypoints) {
            // Cloning waypoint record with note field that needs to be updated. Done because this is in after insert.
            //SMA__MAWaypoint__c waypoint = [Select ID, sma__notes__c From SMA__MAWaypoint__c Where ID =: wp.Id];
            waypointCanvassMap.put(wp.sma__LinkId__c, wp);
            listLinkId.add(wp.sma__LinkId__c);
            //CNVSS_Canvass_Unit__c cu = CanvassUnitUtils.getCanvassUnitById(wp.sma__linkid__c);
            //if(cu.CNVSS_DO_NOT_CONTACT__c) {
            //    wp.sma__notes__c = 'Do Not Knock';
            //    listWp.add(wp);
            //}
        }
        
        // below will find all canvass units from the listLinkId generated from the waypoints
        if((!waypointCanvassMap.isEmpty() && !listLinkId.isEmpty()) && (waypointCanvassMap.size() == listLinkId.size())) {
            listCu = [SELECT Id, CNVSS_DO_NOT_CONTACT__c FROM CNVSS_Canvass_Unit__c WHERE Id IN :listLinkId];
            if(!listCu.isEmpty()) {
                for(CNVSS_Canvass_Unit__c cu : listCu) {
                    // if the canvass unit has the do not contact == true, use the map to get the waypoint from that canvass unit id and store it in the listWp
                    if(cu.CNVSS_DO_NOT_CONTACT__c) {
                        listWp.add(waypointCanvassMap.get(cu.Id));
                    }
                }
            }
        }
        
        if(!listWp.isEmpty()) {
            // update each of the waypoints's note field in listWp to 'Do Not Knock' since we've filtered them already in the canvass unit functionality at the top
            for(sma__mawaypoint__c wp : listWp) {
                wp.sma__Notes__c = 'Do Not Knock';
            }
        }
        update listWp;
    }
}