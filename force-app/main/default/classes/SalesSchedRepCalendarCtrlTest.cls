@isTest
private class SalesSchedRepCalendarCtrlTest {

    static testmethod void testGetScheduleWrapperSalesRep(){
        
        Date d = Date.today().toStartOfMonth();
        
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        Test.startTest();
        SalesSChedRepCalendarCtrl.ScheduleWrapper sw = null;
        System.runAs(sstu.reps.get(0)){
        	sw = SalesSchedRepCalendarCtrl.getScheduleWrapper(((Datetime)d).formatGmt('yyyy-MM-dd'),sstu.reps.get(0).Id);
        }
        
        System.assertEquals(false,sw.days.isEmpty());
        boolean foundStore = false;
        for(SalesSchedRepCalendarCtrl.DayWrapper dw : sw.days){
            for(SalesSchedRepCalendarCtrl.CapacityWrapper cw : dw.capacityWrappers){
                foundStore |= (cw.capacity.Store__c != null);
            }
        }
        System.assertEquals(true, foundStore);
    }
    
    //Tests a manager viewing a calendar for one of his or her sales reps
    static testmethod void testGetScheduleWrapperSalesManager(){
        
        Date d = Date.today().toStartOfMonth();
        
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        
        Test.startTest();
        
        SalesSChedRepCalendarCtrl.ScheduleWrapper sw = null;
        System.runAs(sstu.managers.get(0)){    
        	sw = SalesSchedRepCalendarCtrl.getScheduleWrapper(((Datetime)d).formatGmt('yyyy-MM-dd'),sstu.reps.get(0).Id);
        }
        
        System.assertEquals(false,sw.days.isEmpty());
        boolean foundStore = false;
        for(SalesSchedRepCalendarCtrl.DayWrapper dw : sw.days){
            for(SalesSchedRepCalendarCtrl.CapacityWrapper cw : dw.capacityWrappers){
                foundStore |= (cw.capacity.Store__c != null);
            }
        }
        System.assertEquals(true, foundStore);
    }
    
    //Tests a rep attempting to view a calendar of another rep (should throw an exception)
    static testmethod void testGetScheduleWrapperInvalidSalesRep(){
        
        Date d = Date.today().toStartOfMonth();
        
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,2);
        
        Test.startTest();
        SalesSchedRepCalendarCtrl.ScheduleWrapper sw = null;
        boolean exceptionThrown = false;
        System.runAs(sstu.reps.get(0)){
            try{
        		sw = SalesSchedRepCalendarCtrl.getScheduleWrapper(((Datetime)d).formatGmt('yyyy-MM-dd'),sstu.reps.get(1).Id);
            }catch(Exception e){
                exceptionThrown = true;
            }
        }
        
        System.assertEquals(true,exceptionThrown,'No exception thrown, a rep should not be able to see another reps calendar');
    }
    
    static testmethod void testSaveCapacities(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
            
        Sales_Capacity__c s = sstu.capacities.get(0);
        s.Status__c = 'Deferred';
        SalesSchedRepCalendarCtrl.CapacityWrapper cw = new SalesSchedRepCalendarCtrl.CapacityWrapper(s);
        
        Test.startTest();
        System.runAs(sstu.reps.get(0)){
            SalesSchedRepCalendarCtrl.saveCapacities(new List<SalesSchedRepCalendarCtrl.CapacityWrapper>{cw},sstu.reps.get(0).Id);
            cw.store = sstu.store.Id;
            SalesSchedRepCalendarCtrl.saveCapacities(new List<SalesSchedRepCalendarCtrl.CapacityWrapper>{cw},sstu.reps.get(0).Id);
            cw.store = 'Unavailable';
            SalesSchedRepCalendarCtrl.saveCapacities(new List<SalesSchedRepCalendarCtrl.CapacityWrapper>{cw},sstu.reps.get(0).Id);
        }
        Test.stopTest();
        
        s = [Select Status__c,Store__r.Name From Sales_Capacity__c Where Id =:s.Id];
        System.assertEquals('Unavailable',s.Status__c);
        System.assertEquals('77 - Twin Cities, MN',s.Store__r.Name);
    }
}