public class MakabilityValidatorController {

@auraEnabled
Public static LightningResponse getAllOrderItemsApex(string orderId){
	LightningResponse lr = new LightningResponse();
	string oId = orderId;
	try{
		List<OrderItem> oi = [SELECT id,
		                      product2.name,
		                      product2.id,
		                      Height_Inches__c,
		                      Height_Fraction__c,
		                      Width_Inches__c,
		                      Width_Fraction__c,
		                      Tempered_S1__c,
		                      Tempered_S2__c,
		                      Screen_Type__c,
		                      Screen_Size__c,
		                      Interior_Color__c,
		                      Exterior_Color__c,
		                      Glass_Pattern_S1__c,
		                      Glass_Pattern_S2__c,
		                      Grille_Pattern__c,
		                      Frame_Type__c,
		                      Hardware_Option__c,
		                      Sash_Operation__c,
		                      Sash_Ratio__c,
		                      Specialty_Shape__c,
                              Lites_High_S1__c,
                              Lites_Wide_S1__c,
                              Spokes__c,
                              Hubs__c,
           					  Pocket_Notch__c,
				              Exterior_Trim__c,   							  
                              Grille_Style__c,
							  Right_Leg_Inches__c,
							  Right_Leg_Fraction__c,
							  Left_Leg_Inches__c,
							  Left_Leg_Fraction__c,
							  Locks_Sash__c
		                      FROM OrderItem
		                      WHERE order.OrderNumber = :oId
                              AND (product2.Family = 'Window' OR product2.Family = 'Specialty')
                              ORDER By CreatedDate desc ];
		lr.jsonResponse = JSON.serialize(oi);
	}catch(exception e) {
		lr = new LightningResponse(e);
		system.debug('lightning response in catch block for checkMakability' + lr);
	}
	system.debug('lightning response in get all order items '+ lr);
	return lr;
}

@AuraEnabled
Public Static LightningResponse makabilityOrderItem(Id orderItemId){
	LightningResponse lr = new lightningResponse();
	system.debug('orderItem id ' + orderItemId);
	OrderItem oi;
	try{
		oi = [Select Id,
		      Order.Id,
		      product2.Id,
		      Height_Inches__c,
		      Height_Fraction__c,
		      Width_Inches__c,
		      Width_Fraction__c,
		      Tempered_S1__c,
		      Tempered_S2__c,
		      Screen_Type__c,
		      Screen_Size__c,
		      Interior_Color__c,
		      Exterior_Color__c,
		      Glass_Pattern_S1__c,
		      Glass_Pattern_S2__c,
		      Grille_Pattern__c,
		      Frame_Type__c,
		      Hardware_Option__c,
		      Sash_Operation__c,
		      Sash_Ratio__c,
		      Specialty_Shape__c,
		      Lites_High_S1__c,
		      Lites_Wide_S1__c,
		      Spokes__c,
		      Hubs__c,
           	  Pocket_Notch__c,
			  Exterior_Trim__c,   			  
              Grille_Style__c,
			  Right_Leg_Inches__c,
			  Right_Leg_Fraction__c,
			  Left_Leg_Inches__c,
			  Left_Leg_Fraction__c,
			  EJ_Color__c,
			  EJ_Species__c,
			  Locks_Sash__c			  
		      from OrderItem
		      WHERE id = :orderItemId
		      Limit 1];
	} catch(exception e) {
		system.debug('error with orderItem query '+ e.getMessage());
	}
	/////// set up for calling makability ////////
	Set<id> productIds = new set<id>();
	string uniKey;
	Map<string,MakabilityRestResource.OrderItem> uniKeyToOrderItemMap = new map<String,MakabilityRestResource.OrderItem>();
	MakabilityRestResource.OrderRequest ord = new MakabilityRestResource.OrderRequest();
	List<MakabilityRestResource.OrderRequest> ords = new list<MakabilityRestResource.OrderRequest>();
	ProductConfiguration pc = new ProductConfiguration();
	List<MakabilityRestResource.OrderItem> mOis = new List<MakabilityRestResource.OrderItem>();
	MakabilityRestResource.OrderItem mOi = new MakabilityRestResource.OrderItem();
	List<MakabilityRestResource.MakabilityResult> allMakability = new List<MakabilityRestResource.MakabilityResult> ();
	List<ApexClass> mclasses = MakabilityUtility.fetchMakabilityClasses();


	// build callout

	/////////////// todo - should move all below into a validation method that returns errors based on missing fields. //////
	productIds.add(oi.product2.Id);

	/* optional fields*/
	if(oi.Locks_Sash__c != null){
		pc.locks = decimal.valueOf(oi.Locks_Sash__c);
	}
	if(oi.EJ_Color__c != null) {
		pc.ejColor = oi.EJ_Color__c;
	}   	
	if(oi.EJ_Species__c != null) {
		pc.ejSpecies = oi.EJ_Species__c;
	}   		
	if(oi.Right_Leg_Inches__c != null) {
		pc.rightLegInches = oi.Right_Leg_Inches__c;
	}   	
	if(oi.Right_Leg_Fraction__c != null) {
		pc.rightLegFraction = oi.Right_Leg_Fraction__c;
	}   	
	if(oi.Left_Leg_Inches__c != null) {
		pc.leftLegInches = oi.Left_Leg_Inches__c;
	}   	
	if(oi.Left_Leg_Fraction__c != null) {
		pc.leftLegFraction = oi.Left_Leg_Fraction__c;
	}   	
	if(oi.Pocket_Notch__c != null) {
		pc.insertFrame = oi.Pocket_Notch__c;
	}   	
	if(oi.Exterior_Trim__c != null) {
		pc.exteriorTrim = oi.Exterior_Trim__c;
	}   	
	if(oi.Grille_Style__c != null) {
		pc.grilleStyle = oi.Grille_Style__c;
	}        
	if(oi.Lites_High_S1__c != null) {
		pc.litesHigh = oi.Lites_High_S1__c;
	}    
	if(oi.Lites_Wide_S1__c != null) {
		pc.litesWide = oi.Lites_Wide_S1__c;
	}    
	if(oi.Spokes__c != null && oi.Spokes__c.isNumeric()) {
		pc.spokes = Decimal.valueOf(oi.Spokes__c);
	}    
	if(oi.Hubs__c != null && oi.Hubs__c.isNumeric()) {
		pc.hubs = Decimal.valueOf(oi.Hubs__c);
	}    
	if(oi.Frame_Type__c != null) {
		pc.frame = oi.Frame_Type__c;
	}
	if(oi.Hardware_Option__c != null) {
		pc.hardwareOption = oi.Hardware_Option__c;
	}
	if(oi.Sash_Operation__c != null) {
		pc.sashOperation = oi.Sash_Operation__c;
	}
	if(oi.Sash_Ratio__c != null) {
		pc.sashRatio = oi.Sash_Ratio__c;
	}
	if(oi.Specialty_Shape__c != null) {
		pc.specialtyShape = oi.Specialty_Shape__c;
	}
	if(oi.Grille_Pattern__c != null) {
		pc.grillePattern = oi.Grille_Pattern__c;
	}
	if(oi.Tempered_S1__c) {
		pc.s1Tempering = oi.Tempered_S1__c;
	} else {
		pc.s1Tempering = false;
	}
	if(oi.Tempered_S2__c) {
		pc.s2Tempering = oi.Tempered_S2__c;
	} else{
		pc.s2Tempering = false;
	}
	if(oi.Glass_Pattern_S1__c != null) {
		pc.s1Pattern = oi.Glass_Pattern_S1__c;
	}
	if(oi.Glass_Pattern_S2__c != null) {
		pc.s2Pattern = oi.Glass_Pattern_S2__c;
	}
	if(oi.Exterior_Color__c != null) {
		pc.exteriorColor = oi.Exterior_Color__c;
	}
	if(oi.Interior_Color__c != null) {
		pc.interiorColor = oi.Interior_Color__c;
	}
	if(oi.Screen_Type__c != null) {
		pc.screenType = oi.Screen_Type__c;
	}
	if(oi.Screen_Size__c != null) {
		pc.screenSize = oi.Screen_Size__c;
	}
	// mandatory fields
	// high performance doesnt exist on order product yet..
	// but needs to be set for makability. update to set from oi when field is created.
	// will also need to add external force and internal force fields.
	pc.highPerformance = false;
	/////////////////////////////
	if(oi.Width_Inches__c != null) {
		pc.widthInches = oi.Width_Inches__c;
	}
	if(oi.Width_Fraction__c != null) {
		pc.widthFractions = oi.Width_Fraction__c;
	}
	if(oi.Height_Inches__c != null) {
		pc.heightInches = oi.Height_Inches__c;
	}
	if(oi.Height_Fraction__c != null) {
		pc.heightFractions = oi.Height_Fraction__c;
	}
	if(oi.product2.Id != null) {
		pc.productId = oi.product2.Id;
	}

	////////////////////////////
	mOi.orderId = oi.Order.Id;
	mOi.orderItemId= oi.Id;
	mOi.productConfiguration = pc;
    mOi.makabilityCalculator = MakabilityUtility.validateFields(pc);
	mOis.add(mOi);
	ord.orderId = oi.Order.Id;
	ord.orderItems = mOis;
	ords.add(ord);
	uniKeyToOrderItemMap.put(mOi.orderId + mOi.orderItemId, mOi);

	/// run checks ///
	for(ApexClass m:mclasses) {
		try{
			Type t = Type.forName(m.Name);
			MakabilityService ms = (MakabilityService)t.newInstance();
            system.debug('class i am tesing ' + ms + m.Name);
			List<MakabilityRestResource.MakabilityResult> result = ms.checkCompatibility(uniKeyToOrderItemMap, productIds);
			allMakability.addAll(result);
		} catch (exception e) {
			lr = new LightningResponse(e);
			system.debug('lightning response in catch block for makabilityOrderItem' + e.getMessage() + e.getStackTraceString());
			system.debug('lightning response in catch block for makabilityOrderItem' + lr);

		}
	}
	/// end checks ///
	lr.jsonResponse = JSON.serialize(allMakability);
	system.debug('response in makabilityOrderItem '+lr);
	return lr;
}

}