@isTest(SeeAllData=false)
global class CanvassMarketBatchTest {

    public static testMethod void test1()
    {        
        CNVSS_Canvass_Market__c canvassMarket = new CNVSS_Canvass_Market__c();
        canvassMarket.Name = 'Test';
        canvassMarket.CNVSS_Canvass_Market_Manager__c = userInfo.getUserId();
        canvassMarket.CNVSS_ISC_Phone_Number__c = '5555555555';
        insert canvassMarket;
        
        Canvass_Market_Zip_Code__c canvassMarketZip = new Canvass_Market_Zip_Code__c();
        canvassMarketZip.Name = '28217';
        canvassMarketZip.Zip_Code__c = '28217';
        canvassMarketZip.Canvass_Market__c = canvassMarket.id;
        insert canvassMarketZip;

        Test.startTest();
        database.executebatch(new CanvassMarketBatch(canvassMarket.Id),1);
        database.executebatch(new CanvassMarketBatch(canvassMarket.Id,canvassMarketZip.Zip_Code__c),1);
        Test.stopTest();
    }
}