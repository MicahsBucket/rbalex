@isTest
private class SurveyMerger_UnitTest {
	
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
	}

	@isTest static void surveyMergerNullTest() {
		Date today = date.today();
		Date threeDaysAgo = date.today().addDays(-3);
		Account acct = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(acct);
		List<Survey__c> oldSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		oldSurveys[0].Enabled_Appointment_Id__c = '123456789';
		oldSurveys[0].Source_System__c ='rForce';
		Test.setCreatedDate(oldSurveys[0].Id, threeDaysAgo);
		oldSurveys[0].Installation_Date__c = threeDaysAgo;
		oldSurveys[1].Source_System__c ='rForce';
		Id oldSurveyId1 = oldSurveys[0].Id;
		Id oldSurveyId2 = oldSurveys[1].Id;
		Database.update(oldSurveys);

		List<Survey__c> newSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		newSurveys[0].Enabled_Appointment_Id__c = '123456789';
		newSurveys[0].Installation_Date__c = today;
		Test.setCreatedDate(newSurveys[0].Id, today);
		Test.setCreatedDate(newSurveys[1].Id, today);
		newSurveys[0].Source_System__c ='rForce';
		newSurveys[1].Source_System__c ='rForce';
		newSurveys[1].Enabled_Appointment_Id__c = '111111111';
		newSurveys[1].Installation_Date__c = threeDaysAgo;
		Id newSurveyId1 = newSurveys[0].Id;
		Id newSurveyId2 = newSurveys[1].Id;
		Database.update(newSurveys);		

		test.startTest();
			SchedulableContext sc = null;
			SurveyMerger tsc = new SurveyMerger();
			tsc.execute(sc);
		test.stopTest();
		Survey__c deletedSurvey;
		Survey__c updatedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId1 LIMIT 1];
		Survey__c oldSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId2 LIMIT 1];
		try{
			deletedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId1 LIMIT 1];
		} catch(QueryException qe){
			deletedSurvey = null;
		}
		Survey__c newSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId2 LIMIT 1];
		System.assertEquals(null, deletedSurvey);
		System.assertEquals(today, updatedSurvey.Installation_Date__c);
		System.assertNotEquals(true, oldSurvey2.Installation_Date__c == updatedSurvey.Installation_Date__c);

	}

	@isTest static void surveyMergerTest() {
		Date today = date.today();
		Date threeDaysAgo = date.today().addDays(-3);
		Account acct = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(acct);
		List<Survey__c> oldSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		oldSurveys[0].Enabled_Appointment_Id__c = '123456789';
		oldSurveys[0].Source_System__c ='rForce';
		Test.setCreatedDate(oldSurveys[0].Id, threeDaysAgo);
		oldSurveys[0].Installation_Date__c = threeDaysAgo;
		oldSurveys[1].Source_System__c ='rForce';
		Id oldSurveyId1 = oldSurveys[0].Id;
		Id oldSurveyId2 = oldSurveys[1].Id;
		Database.update(oldSurveys);

		List<Survey__c> newSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		newSurveys[0].Enabled_Appointment_Id__c = '123456789';
		newSurveys[0].Installation_Date__c = today;
		Test.setCreatedDate(newSurveys[0].Id, today);
		Test.setCreatedDate(newSurveys[1].Id, today);
		newSurveys[0].Source_System__c ='rForce';
		newSurveys[1].Source_System__c ='rForce';
		newSurveys[1].Enabled_Appointment_Id__c = '111111111';
		newSurveys[1].Installation_Date__c = threeDaysAgo;
		Id newSurveyId1 = newSurveys[0].Id;
		Id newSurveyId2 = newSurveys[1].Id;
		Database.update(newSurveys);

		TestDataFactory.createWorkers('Installer', 2, acct.Id, newSurveys[0].Id);
		TestDataFactory.createWorkers('Installer', 2, acct.Id, oldSurveys[0].Id);

		test.startTest();
			SchedulableContext sc = null;
			SurveyMerger tsc = new SurveyMerger();
			tsc.execute(sc);
		test.stopTest();
		Survey__c deletedSurvey;
		Survey__c updatedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId1 LIMIT 1];
		Survey__c oldSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId2 LIMIT 1];
		try{
			deletedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId1 LIMIT 1];
		} catch(QueryException qe){
			deletedSurvey = null;
		}
		Survey__c newSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId2 LIMIT 1];
		System.assertEquals(null, deletedSurvey);
		System.assertEquals(today, updatedSurvey.Installation_Date__c);
		System.assertNotEquals(true, oldSurvey2.Installation_Date__c == updatedSurvey.Installation_Date__c);

	}

	@isTest static void surveyMergerSameWorkersTest() {
		Date today = date.today();
		Date threeDaysAgo = date.today().addDays(-3);
		Account acct = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(acct);
		List<Survey__c> oldSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		oldSurveys[0].Enabled_Appointment_Id__c = '123456789';
		oldSurveys[0].Source_System__c ='rForce';
		Test.setCreatedDate(oldSurveys[0].Id, threeDaysAgo);
		oldSurveys[0].Installation_Date__c = threeDaysAgo;
		oldSurveys[1].Source_System__c ='rForce';
		Id oldSurveyId1 = oldSurveys[0].Id;
		Id oldSurveyId2 = oldSurveys[1].Id;
		Database.update(oldSurveys);

		List<Survey__c> newSurveys = TestDataFactory.createSurveys('Post Install', 2, acct.Id, 'Pending');
		newSurveys[0].Enabled_Appointment_Id__c = '123456789';
		newSurveys[0].Installation_Date__c = today;
		Test.setCreatedDate(newSurveys[0].Id, today);
		Test.setCreatedDate(newSurveys[1].Id, today);
		newSurveys[0].Source_System__c ='rForce';
		newSurveys[1].Source_System__c ='rForce';
		newSurveys[1].Enabled_Appointment_Id__c = '111111111';
		newSurveys[1].Installation_Date__c = threeDaysAgo;
		Id newSurveyId1 = newSurveys[0].Id;
		Id newSurveyId2 = newSurveys[1].Id;
		Database.update(newSurveys);

		List<Worker__c> worker = TestDataFactory.createWorkers('Installer', 1, acct.Id, newSurveys[0].Id);
		Survey_Worker__c oldSurveyWorker = new Survey_Worker__c(Survey_Id__c = oldSurveys[0].Id, Worker_Id__c = worker[0].Id, Role__c = 'Installer');
		Database.insert(oldSurveyWorker);

		test.startTest();
			SchedulableContext sc = null;
			SurveyMerger tsc = new SurveyMerger();
			tsc.execute(sc);
		test.stopTest();
		Survey__c deletedSurvey;
		Survey_Worker__c deletedSurveyWorker;
		Survey__c updatedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId1 LIMIT 1];
		Survey__c oldSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :oldSurveyId2 LIMIT 1];
		try{
			deletedSurvey = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId1 LIMIT 1];
		} catch(QueryException qe){
			deletedSurvey = null;
		}
		try{
			deletedSurveyWorker = [SELECT Id FROM Survey_Worker__c WHERE Survey_Id__c = :newSurveyId1 LIMIT 1];
		} catch(QueryException qe){
			deletedSurveyWorker = null;
		}
		Survey__c newSurvey2 = [SELECT Id, Enabled_Appointment_Id__c, Installation_Date__c FROM Survey__c WHERE Id = :newSurveyId2 LIMIT 1];
		System.assertEquals(null, deletedSurvey);
		System.assertEquals(null, deletedSurveyWorker);
		System.assertEquals(today, updatedSurvey.Installation_Date__c);
		System.assertNotEquals(true, oldSurvey2.Installation_Date__c == updatedSurvey.Installation_Date__c);


	}
}