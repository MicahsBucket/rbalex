global class GlazingConfiguration {
    public Boolean s1Tempering {get;set;}
    public Boolean s2Tempering {get;set;}
    public String s1Pattern {get;set;}
    public String s2Pattern {get;set;}
}