public class CustomerReferralsCtrl {
    
    	/*
		This Class created by MTR 8/15/18 to be used initially in the community portal. 
		It is currently incomplete and will need to be re-written before go-live. the purpose of
		this class is to send referral information from a form in the community to another system that as of 8-15-18
		has not yet been determined. Note- the test class CustomerREferralsCtrlTest will also need to be refactored.
		*/
    @AuraEnabled  
    public static String sendReferral(String fname, String lname, String phone, String email){
        Referral ref = new Referral();        
        ref.firstName = fname;
        ref.lastName = lname;
        ref.phone = phone;        
        ref.email = email;
        // returning string for now.. this should be a callout to another system. 
        String success = 'success';
        system.debug(ref);    
        return success;
    }
    
    public class Referral{  
        @AuraEnabled
        public String firstName {get;set;}
        @AuraEnabled    
        public String lastName {get;set;}
        @AuraEnabled
        public String email {get;set;}
        @AuraEnabled
        public String phone {get;set;}	
    }
}