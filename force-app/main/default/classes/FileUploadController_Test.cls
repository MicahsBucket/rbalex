@isTest
private class FileUploadController_Test {
    public static Account store1;
    
    static testmethod void testReturnErrorMessage(){
        
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        Product_Config_Settings__c pcs = new Product_Config_Settings__c(
            Hardware_Color_Mappings__c = 'White,White;Canvas,Canvas;Terratone,Stone;Sandtone,Stone;Dark Bronze,Dark Bronze;Black,Black;Pine,Stone;Oak,Stone;Maple,Stone'
        );
        insert pcs;
        store1 = [select Id, Active_Store_Configuration__c from Account 
                  where Name = '77 - Twin Cities, MN' Limit 1];
        Pricebook2 pb = utility.createPricebook2('RSTestDT', store1.Active_Store_Configuration__c);
        insert pb;
        
        Opportunity myOpp = new Opportunity(
            Name = 'testOpp' + Date.today().format(),
            StageName = 'New',
            Pricebook2Id = pb.Id,
            CloseDate = Date.today()
        );
        insert myOpp;
        ErrorLog__c errorLog = new ErrorLog__c();
        errorLog.ErrorMessage__c='Test error';
        errorLog.FileName__c='test';
        errorLog.RelatedTo__c=myopp.id;
        
        insert errorLog;
        
        FileUploaderController_ltng.returnErrorMessage(myopp.id);
        
        
    }
}