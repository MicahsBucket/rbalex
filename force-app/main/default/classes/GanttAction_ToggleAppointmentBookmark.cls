/*
Test class required to achieve code coverage:
- GanttAction_Test
*/

global without sharing class GanttAction_ToggleAppointmentBookmark implements FSL.CustomGanttServiceAppointmentAction {
    global String action(List<Id> serviceAppointmentsIds, Datetime ganttStartDate, Datetime ganttEndDate, Map<String, Object> additionalParameters) {
        if (serviceAppointmentsIds == null || serviceAppointmentsIds.size() == 0) {return 'Error: no service appointments in list';}

        List<ServiceAppointment> saList = [SELECT FSL__GanttIcon__c, AppointmentNumber, Bookmarked__c
                                            FROM ServiceAppointment
                                            WHERE Id IN :serviceAppointmentsIds];

        List<String> saBookmarked = new List<String>();
        List<String> saBookmarkRemoved = new List<String>();

        // Get static resource needed for gantt icon
        StaticResource static_resource;
        try {
            static_resource = [SELECT Id, SystemModStamp, Name
                                FROM StaticResource 
                                WHERE Name = 'bookmarkIcon'
                                LIMIT 1];
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Static resource "bookmarkIcon" not found.');
            return 'Static resource "bookmarkIcon" not found.  Please contact an administrator for assistance';
        }
        
        // Calculate the static resource relative URL dynamically so that this code can be deployed across orgs
        String url_file_ref = String.format('/resource/{0}/{1}?',
                              new List<String>{
                                String.valueOf(((DateTime)static_resource.get('SystemModStamp')).getTime()),
                                String.valueOf(static_resource.get('Name'))
                              });
        
        for (ServiceAppointment sa : saList) {
            if (sa.Bookmarked__c == false) {
                sa.FSL__GanttIcon__c = url_file_ref;
                sa.Bookmarked__c = true;
                saBookmarked.add(sa.AppointmentNumber);
            } else {
                sa.FSL__GanttIcon__c = null;
                sa.Bookmarked__c = false;
                saBookmarkRemoved.add(sa.AppointmentNumber);
            }
        }

        try {
            UtilityMethods.disableAllFslTriggers(); // Disable all FSL object triggers to avoid unnecessary code execution
            update saList;
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Error while attempting to update service appointments: ' + ex.getMessage());
            return 'Error occurred while attempting to toggle bookmarks.  Please contact an administrator for assistance.';
        }
        
        // Build result string indicating which bookmarks were added, and which bookmarks were removed
        String result;
        if (saBookmarked.size() > 0 && saBookmarkRemoved.size() > 0) {
            result =  String.format('Bookmark(s) added for {0}. Bookmark(s) removed for {1}', new string[]{String.join(saBookmarked, ', '), String.join(saBookmarkRemoved, ',')});
        } else if (saBookmarked.size() > 0) {
            result = String.format('Bookmark(s) added for {0}', new string[]{String.join(saBookmarked, ', ')});
        } else {
            result = String.format('Bookmark(s) removed for {0}', new string[]{String.join(saBookmarkRemoved, ',')});
        }

        return result;
    }
}