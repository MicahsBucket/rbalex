/**
*   {Purpose} - Batch routine to send schedule availability reminders to store employees
*				1 or 7 days prior to their due date.  The due date is set on the
*				store configuration record
*
*   {Contact} - support@demandchainsystems.com
*				www.demandchainsystems.com
*				612-424-0032
*
*	CHANGE HISTORY
*	=============================================================================
*	Date    	Name             		Description
*	20190210	Eric Gronholz DCS		Created
*	=============================================================================
**/
global class SalesSchedAvailabilityReminderBatch implements Database.Batchable<sObject> {
    private Date processDate;
    
    //constructor without parameter
    public SalesSchedAvailabilityReminderBatch() {
        processDate = date.today();
    }
    //constructor with date parameter
    public SalesSchedAvailabilityReminderBatch(Date dtProcess) {
        if (dtProcess == null) {
            dtProcess = date.today();
        }
        processDate = dtProcess;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //find the store configuration records where the rep schedules are due in 1 or 7 days from now
        Set<String> setReminderDays = new Set<String>();
        setReminderDays.add(String.valueOf(processDate.addDays(1).day()));
        setReminderDays.add(String.valueOf(processDate.addDays(7).day()));
        String soqlStmt = 'SELECT Id, Store__c, Rep_Schedules_Due__c, X1_Day_Schedule_Alert_Day__c, X7_Day_Schedule_Alert_Day__c '
            			+ 'FROM Store_Configuration__c '
            			+ 'WHERE Rep_Schedules_Due__c IN :setReminderDays';
        system.debug('soqlStmt: ' + soqlStmt + '\nsetReminderDays: ' + setReminderDays);
        return Database.getQueryLocator(soqlStmt);
    }
    
    global void execute(Database.BatchableContext BC, List<Store_Configuration__c> scope) {
        //for each store configuration record, locate the contacts (employees) associated to the stores and
        //send them a reminder email
        Set<Id> setStoreConfigIds = new Set<Id>();
        for (Store_Configuration__c oSC : scope) {
            setStoreConfigIds.add(oSC.Id);
        }

		//get the org-wide email address from where the emails will be sent
		OrgWideEmailAddress owa = [Select Id, Address, DisplayName from OrgWideEmailAddress Where DisplayName = 'Renewal Retail Support' LIMIT 1];
        
        List<Messaging.SingleEmailMessage> lstMessages = new List<Messaging.SingleEmailMessage>();
        for (Account oStore : [Select Id
                              		, Name
                               		, Active_Store_Configuration__c
                              		, (Select Id
                                      		, Rep_Schedules_Due__c
                                      		, X1_Day_Schedule_Alert_Day__c
                                       		, X7_Day_Schedule_Alert_Day__c
                                      From Store_Configurations__r)
            						, (Select Id
                                  			, FirstName
                                  			, LastName
                                  			, Email
                                       		, Secondary_Email__c
										From Contacts
                                 		Where Email != null
                                      		And RecordType.Name = 'Employee Contacts')
            					From Account
                                Where Active_Store_Configuration__c in :setStoreConfigIds]) {
			//for each configuration, send the appropriate notice to each contact
			for (Store_Configuration__c oStoreConfig : oStore.Store_Configurations__r) {
                for (Contact oEmployee : oStore.Contacts) {
                    system.debug('oStore: ' + oStore 
                                 + '\noStoreConfig: ' + oStoreConfig 
                                 + '\noEmployee: ' + oEmployee 
                                 + '\nreminderDay: ' + processDate.day());
                    if (oStoreConfig.X1_Day_Schedule_Alert_Day__c == processDate.day()) {
                        Messaging.SingleEmailMessage oMailDay1 = new Messaging.SingleEmailMessage();
                        oMailDay1.setTargetObjectId(oEmployee.Id);
                        if(!String.isEmpty(oEmployee.Secondary_Email__c)){
                        	oMailDay1.setCcAddresses(new List<String>{oEmployee.Secondary_Email__c});
                        }
                        oMailDay1.setSubject('Availability Schedule Reminder - Tomorrow');
                        oMailDay1.setPlainTextBody('Your availability for next month is due tomorrow.');
                        oMailDay1.setOrgWideEmailAddressId(owa.Id);
                        lstMessages.add(oMailDay1);
                    }
                    if (oStoreConfig.X7_Day_Schedule_Alert_Day__c == processDate.day()) {
                        Messaging.SingleEmailMessage oMailDay7 = new Messaging.SingleEmailMessage();
                        oMailDay7.setTargetObjectId(oEmployee.Id);
                        if(!String.isEmpty(oEmployee.Secondary_Email__c)){
                        	oMailDay7.setCcAddresses(new List<String>{oEmployee.Secondary_Email__c});
                        }
                        oMailDay7.setSubject('Availability Schedule Reminder - 7 Days');
                        oMailDay7.setPlainTextBody('Your availability for next month is due in 7 days.');
                        oMailDay7.setOrgWideEmailAddressId(owa.Id);
                        lstMessages.add(oMailDay7);
                    }
                }
			}			
		}
        
        system.debug('lstMessages: ' + lstMessages);
        if (lstMessages.size() > 0) {
            Messaging.sendEmail(lstMessages);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}