/**
 * @File Name          : MakabilityCheckGrillePatternConfigTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/1/2019, 2:55:42 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 2:55:42 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public class MakabilityCheckGrillePatternConfigTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even','Colonial' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGrillePatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Grille Config  - passed');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even','Colonial' ,'1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGrillePatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Grille Pattern configurations found for this productId.');
    }
    @isTest
    public static void doesNotMatchTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,10,10,'Even','Even','Colonial' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGrillePatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        string assertMsg2 = results[0].errorMessages[1];
        system.assertEquals(assertMsg, 'Grille Config - Width Less than Allowed for Selected Pattern. Min width = 25 Even inches.');        
        system.assertEquals(assertMsg2, 'Grille Config -  Height Less than Allowed for Selected Pattern. Mininum Height = 25 Even inches.');        
       
    }  

    @isTest
    public static void doesNotMatchPatternTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even','Farmhouse' ,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGrillePatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results doesNotMatchTest ' + results);
        string assertMsg = results[0].errorMessages[0];
         system.assertEquals(assertMsg, 'Grille Config - Pattern Not Found available patterns include:(Colonial)');             
    }   

    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = new Map<string,MakabilityRestResource.OrderItem>();
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckGrillePatternConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingAnythingTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Something bad happened with the Grille Pattern config');        
    }    
     
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
    private static void createTestGrilleConfig(Product_Configuration__c pc){
        Grille_Pattern_Configuration__c gc = new Grille_Pattern_Configuration__c();
        gc.Grille_Pattern__c = 'Colonial';
        gc.Min_Height_Fraction__c = 'Even';
        gc.Min_Height_Inches__c = 25;
        gc.Min_Width_Inches__c = 25;
        gc.Min_Width_Fraction__c = 'Even';
        gc.Product_Configuration__c = pc.id;
        insert gc;
    }   

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, String wFrac, String Hfrac,String gPat, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = wFrac;
        pc.heightInches = hInch;
        pc.heightFractions = hFrac;        
        pc.grillePattern = gPat;
        mc.checkGrille = true;
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}