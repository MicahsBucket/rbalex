/**
 * @File Name          : MakabilityCheckLockSizeTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/1/2019, 5:22:08 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/1/2019, 5:22:08 PM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public with sharing class MakabilityCheckLockSizeTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even',1 ,'1');
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        map<string,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfc);
        // set<id> productIds = new set<id>();
        // productIds.add(pc.Product__c);
        test.startTest();
 	    MakabilityCheckLockSize.lockSizeResult result = MakabilityCheckLockSize.checkLockSize(configsToTest.get('1'),sdc,pfcMap);
        test.stopTest();
        system.debug('results passing makabilty ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, true);
    }

    @isTest
    public static void notEnoughLocksTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,'Even','Even',0 ,'1');
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        map<string,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfc);
        test.startTest();
 	    MakabilityCheckLockSize.lockSizeResult result = MakabilityCheckLockSize.checkLockSize(configsToTest.get('1'),sdc,pfcMap);
        test.stopTest();
        system.debug('results notEnoughLocksTest ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, false);        
       
    }  

    @isTest
    public static void tooManyLocksTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,5,5,'Even','Even', 3 ,'1');
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        map<string,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfc);
        test.startTest();
 	    MakabilityCheckLockSize.lockSizeResult result = MakabilityCheckLockSize.checkLockSize(configsToTest.get('1'),sdc,pfcMap);
        test.stopTest();
        system.debug('results tooManyLocksTest ' + result);
        Boolean assertMsg = result.productMakable;
         system.assertEquals(assertMsg, false);             
    }   

    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        Size_Detail_Configuration__c sdc = createTestSizeDetailConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,20,20,'Even','Even', 0 ,'1');
        Field_Control_List__c fcl = createFieldControlLists('Locks/Sash');
        Product_Field_Control__c pfc = createProductFieldControl(pc,fcl);
        map<string,Product_Field_Control__c> pfcMap = createPfcMap(pc.Product__c,pfc);
        test.startTest();
 	    MakabilityCheckLockSize.lockSizeResult result = MakabilityCheckLockSize.checkLockSize(configsToTest.get('1'),sdc,pfcMap);
        test.stopTest();
        system.debug('results noMatchingAnythingTest ' + result);
        Boolean assertMsg = result.productMakable;
        system.assertEquals(assertMsg, true);        
    }    
     
    

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }

    private static Field_Control_List__c createFieldControlLists(String fieldName){
        Field_Control_List__c fcl = new Field_Control_List__c();
        fcl.Name = fieldName;
        fcl.rForce__c = true;
        fcl.Sales__c = true;
        fcl.Tech__c = true;
        return fcl;
    }    

    private static Product_Field_Control__c createProductFieldControl(Product_Configuration__c pc, Field_Control_List__c fcl ){
        Product_Field_Control__c pfc = new Product_Field_Control__c();
        pfc.Name = fcl.Name;
        pfc.Field_Control_ID__c = fcl.id;
        pfc.Product_Configuration_ID__c = pc.id;
        pfc.Required__c = true;
        return pfc;
    }

    private static map<String,Product_Field_Control__c> createPfcMap(Id prodId,Product_Field_Control__c pfc){
        map<String,Product_Field_Control__c> result = new map<String,Product_Field_Control__c>();
        String key = prodId + '-' + pfc.Name;
        result.put(key,pfc);
        return result;
    }
    
    private static Size_Detail_Configuration__c createTestSizeDetailConfig(Product_Configuration__c pc){
        Size_Detail_Configuration__c sdc = new Size_Detail_Configuration__c();  
            sdc.Product_Configuration__c = pc.id;
            sdc.Performance_Category__c = 'DP Upgrade';
            sdc.Extended_Max_Height_Inches__c = 80;
            sdc.Extended_Max_Height_Fraction__c = 'Even';
            sdc.Extended_Max_Width_Inches__c = 80;
            sdc.Extended_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Width_Inches__c = 15;
            sdc.Lock_Max_Height_Inches__c = 15;
            sdc.Lock_Min_Width_Inches__c = 25;
            sdc.Lock_Min_Height_Inches__c = 25;
            sdc.Lock_Max_Width_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Fraction__c = 'Even';
            sdc.Lock_Max_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Height_Fraction__c = 'Even';
            sdc.Lock_Min_Width_Locks__c = '1';
            sdc.Lock_Max_Width_Locks__c = '1';
            sdc.Lock_Max_Height_Locks__c = '1';
            sdc.Lock_Min_Height_Locks__c = '1';
            sdc.Positive_Force__c = 50;
            sdc.Frame_Type__c = 'Flat Sill';
            sdc.Hardware_Options__c = 'Normal Hinge';
            sdc.Negative_Force__c = 50;
            sdc.Max_Height_Inches__c = 40;
            sdc.Max_Height_Fraction__c = 'Even';
            sdc.Max_Width_Inches__c = 40;
            sdc.Max_Width_Fraction__c = 'Even';
            sdc.Min_Height_Inches__c = 40;
            sdc.Min_Height_Fraction__c = 'Even';        
            sdc.Min_Width_Inches__c = 40;
            sdc.Min_Width_Fraction__c = 'Even';
            sdc.Sash_Operation__c = 'Right';
            sdc.Sash_Ratio__c = '1:1';
            sdc.Specialty_Shape__c = null;
            sdc.United_Inch_Maximum__c = 120;
            sdc.United_Inch_Minimum__c = 30;
            insert sdc;
            return sdc;
    }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, String wFrac, String Hfrac,Decimal locks, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = wFrac;
        pc.heightInches = hInch;
        pc.heightFractions = hFrac;        
        pc.locks = locks; 
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}