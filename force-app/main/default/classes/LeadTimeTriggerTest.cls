@istest
public class LeadTimeTriggerTest {
    
    static testmethod void makeLeadTimeInactive()
    {
        List<Lead_Time__c> leadTimes = new List<Lead_Time__c>();
        Lead_Time__c leadTime =new Lead_Time__c();
        leadtime.CS2_Closed_Through__c=system.today();
        insert leadtime;
        Lead_Time__c leadTime1 =new Lead_Time__c();
        leadtime1.Lead_Time_Active__c=true;      
        leadtimes.add(leadTime1);
        insert leadtimes;
                                              
        
    }
    
    static testmethod void getLeadTimes()
    {
        Lead_Time__c leadTime1 =new Lead_Time__c();
        leadtime1.Lead_Time_Active__c=true;  
        insert leadtime1;
        WindowLeadTimeController.getRecordId();
    }
                                     
                                             

}