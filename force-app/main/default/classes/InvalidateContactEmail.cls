global class InvalidateContactEmail implements Database.Batchable<SObject>
{
	global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query='select id,Email,Secondary_Email__c from Contact where (Email!=null and (NOT Email like \'%example.com%\')) or (Secondary_Email__c!=null and (NOT Secondary_Email__c like \'%example.com%\'))';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        for(Contact con:(List<Contact>)scope)
        {
            String email=con.Email;
            if(email!=null&&!email.contains('@example.com'))
            {
                email=email.replace('@','=');
                email+='@example.com';
                con.Email=email;
            }
            email=con.Secondary_Email__c;
            if(email!=null&&!email.contains('@example.com'))
            {
                email=email.replace('@','=');
                email+='@example.com';
                con.Secondary_Email__c=email;  
            }
        }
        update scope;
    }
    global void finish(Database.BatchableContext BC){
        // Final Email to Send Completion of Batch
    }
}