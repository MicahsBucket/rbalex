/**
 * @File Name          : SalesSchedException.cls
 * @Description        : Exception that is thrown from Sales Scheduling related code.
 * @Author             : James Loghry
 * @Group              : Demand Chain
 * @Last Modified By   : Demand Chain (James Loghry)
 * @Last Modified On   : 1/23/2019, 9:47:23 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    1/23/2019, 9:46:30 AM   Demand Chain (James Loghry)     Initial Version
**/
public class SalesSchedException extends Exception{}