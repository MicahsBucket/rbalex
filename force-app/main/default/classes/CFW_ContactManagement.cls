public with sharing class CFW_ContactManagement {

	public static final String SOFT_PULL = 'Soft Pull';
	public static final String DEFAULT_SSN = '000-00-0000';
	
	public static void checkForDefaultSSN(Contact c){

		// Try to do a soft pull, set default SSN if needed
		if(c.Has_Customer_Authorization_Picklist__c == 'Yes' && c.Pull_Credit_Report__c == SOFT_PULL
			&& String.isBlank(c.LASERCA__Social_Security_Number__c)){
			c.LASERCA__Social_Security_Number__c = DEFAULT_SSN;
		}

	}

	public static void checkForCoApplicant(Contact mainContact){
		if(mainContact.Co_Applicant__c != null){
				List<Contact> cList = [Select Id, FirstName, LastName, BirthDate, LASERCA__Social_Security_Number__c From Contact Where Id = :mainContact.Co_Applicant__c];
				if(cList.isEmpty())
					return;
				mainContact.LASERCA__Co_Applicant_Name__c = cList[0].FirstName;
				mainContact.LASERCA__Co_Applicant_Last_Name__c = cList[0].LastName;
				mainContact.LASERCA__Co_Applicant_DOB__c = cList[0].BirthDate;
				mainContact.LASERCA__Co_Applicant_Social_Security_Number__c = cList[0].LASERCA__Social_Security_Number__c;
		}
	}

}