/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Customer Portal
* @date 1/18
* @description finds and returns the status of the current Order
**/

public with sharing class OrderTrackerController  {

    //returns the current order from the Order Id passed from the Collapsable Content Parent Component
    @AuraEnabled
    public static Order getOrder(Id orderId){
        Order ord = [SELECT Id, Status FROM Order WHERE Id = :orderId LIMIT 1];
        return ord;
    }
}