@isTest
public class ScheduledEmailNoticeTest {
    
    
    @isTest
    public static void testSchedule() {
        String CRON_EXP = '0 0 0 15 3 ? *';

        TestUtilityMethods tum = new TestUtilityMethods();
        tum.setUpConfigs();
        
        String orgId = UserInfo.getOrganizationId();
    	String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
    	Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
    	String uniqueName = orgId + dateString + randomInt;
        List<Profile> profId = [select Id from Profile where Name = 'Super Administrator'];

        User testUser = new User(
			FirstName = 'TestFirst',
			LastName = 'TestLast',
			email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',			
            Alias = 'test', 
			CommunityNickname = 'test' + String.valueOf(System.currentTimeMillis()),
			TimeZoneSidKey = 'America/Mexico_City', 
			LocaleSidKey = 'en_US', 
			EmailEncodingKey= 'UTF-8', 
			ProfileId = profId[0].Id, 
			LanguageLocaleKey = 'en_US'
			
		);

        insert testUser;

        List<Store_Configuration__c> store = [select Id, Contractor_License_1__c, Contractor_License_1_Expiration__c, Contractor_License_2__c, Contractor_License_2_Expiration__c, Compliance_Coordinator__c from Store_Configuration__c];
        Store_Configuration__c sc = store[0];
        sc.Contractor_License_1__c = '12345';
        sc.Contractor_License_2__c = '23456';
        sc.Contractor_License_1_Expiration__c = Date.Today();
        sc.Contractor_License_2_Expiration__c = Date.Today();
        sc.Compliance_Coordinator__c = testUser.Id;
        update sc;
        


        Test.startTest();

        String jobId = System.schedule('ScheduledEmailNotice', CRON_EXP, new ScheduledEmailnotice());
        CronTrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger where Id = :jobid];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}