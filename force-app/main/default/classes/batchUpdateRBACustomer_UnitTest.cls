/**
  * @author Ramya Bangalore
  * @Description This SchedulerBatch class is setting time for the "batchUpdateRBACustomer" -GroundForce App
  * 
  * 
  */
@isTest
public class batchUpdateRBACustomer_UnitTest {
    @TestSetup
    static void testSetup() {
        
		RMS_Settings__c customSetting1 = new RMS_Settings__c(Value__c = '1234567890', Name='Data Loading Profile ID');
		insert customSetting1;
        
		CNVSS_Canvass_Unit__c cu = new CNVSS_Canvass_Unit__c (CNVSS_Previous_RbA_Customer__c=false,CNVSS_City__c='Plymouth', CNVSS_House_No__c='111', CNVSS_Street__c='Main Street',CNVSS_Zip_Code__c='55446' );
		insert cu;
        
        Account acc = new Account(name = 'TestAcc345', 
                                  RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dwelling').getRecordTypeId(),
                                  Canvass_Unit_Id__c=cu.id);
		insert acc;
        
        
       Account ac = [select id, Account.Canvass_Unit_Id__r.CNVSS_Previous_RbA_Customer__c from Account limit 1];
        system.debug('Inside test factory accid,  canvas unit id, prev cust id : ' + ac.id + '--' + ac.Canvass_Unit_Id__c + '--' + ac.Canvass_Unit_Id__r.CNVSS_Previous_RbA_Customer__c);
         

        String pricebookId = Test.getStandardPricebookId();


        Order testOrder =  new Order();
        testOrder.Name ='Sold Order 1';
        testOrder.AccountId = acc.Id;
        testOrder.EffectiveDate = Date.Today();
        testOrder.Status = 'Install Complete';
        testOrder.Pricebook2Id = pricebookId;
        testOrder.Customer_Pickup_All__c = FALSE;
        testOrder.Installation_Date__c = system.today()-1;
        insert testOrder;

    }
    static testMethod void testMethod1()
    {
        Test.startTest();
        batchUpdateRBACustomer b1 = new batchUpdateRBACustomer();
        ID batchprocessid = Database.executeBatch(b1,20);           
        Test.stopTest();
        
        // check the value of previous rba customer on the canvas unit 
        Account ac = [select id, Account.Canvass_Unit_Id__r.CNVSS_Previous_RbA_Customer__c from Account limit 1];
        
        System.assertEquals(TRUE, ac.Canvass_Unit_Id__r.CNVSS_Previous_RbA_Customer__c);
        
    }
}