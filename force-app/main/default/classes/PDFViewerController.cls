/*
* @author Jason Flippen
* @date 11/17/2020
* @description Class to provide support for the homeComponentHandTportal LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - PDFViewerControllerTest
*/ 
public with sharing class PDFViewerController {

    @AuraEnabled(cacheable=true)
    public static Map<Id,String> getRelatedFilesByRecordId() {

/*
        // Get record file IDs       

        Set<Id> fileIdSet = new Set<Id>();
        for (ContentDocumentLink doc : [SELECT ContentDocumentId
                                        FROM   ContentDocumentLink
                                        WHERE  ContentDocumentId = '06955000000vz5a']) {
            fileIdSet.add(doc.ContentDocumentId);
        }
    
        // Filter PDF files

/*
        Map<Id,String> mapIdTitle = new Map<Id,String>();
        for (ContentVersion doc : [SELECT ContentDocumentId,
                                          FileExtension,
                                          Title
                                   FROM   ContentVersion
                                   WHERE  (ContentDocumentId IN :fileIDs OR Title LIKE '%rforce release %')
                                   AND    FileExtension='pdf'
                                   ORDER BY CreatedDate DESC]) {
            mapIdTitle.put(doc.ContentDocumentId, doc.Title);
        }
*/
        Map<Id,String> mapIdTitle = new Map<Id,String>();
        for (ContentVersion doc : [SELECT ContentDocumentId,
                                          FileExtension,
                                          Title
                                   FROM   ContentVersion
                                   WHERE  Title LIKE '%rforce release %'
                                   AND    FileExtension='pdf'
                                   ORDER BY CreatedDate DESC]) {
            mapIdTitle.put(doc.ContentDocumentId, doc.Title);
        }

        System.debug('mapIdTitle'+mapIdTitle);

        return mapIdTitle;

    }
    
    @AuraEnabled(cacheable=true)
    public static List<ReleaseDocuments> getReleaseDocNames() {

        Map<Id,String> mapIdTitle = getRelatedFilesByRecordId();
        
        List<ReleaseDocuments> docnames = new List<ReleaseDocuments>();
        for (Id val: mapIdTitle.keyset()) {
            docnames.add(new ReleaseDocuments(val, mapIdTitle.get(val)));
        }
        
        return docnames;

    }
    
    public class ReleaseDocuments{
        
        @AuraEnabled
        public String ReleaseDocName{get;set;}
        
        @AuraEnabled
        public String ReleaseDocURL{get;set;}
        
        @AuraEnabled
        public String ReleaseDocID{get;set;}
        
        public ReleaseDocuments(id Versionid, string doctitle){
        
            ReleaseDocName = doctitle;
            ReleaseDocID   = Versionid;
            ReleaseDocURL = '/sfc/servlet.shepherd/document/download/' + Versionid;
        }
    
    }

}