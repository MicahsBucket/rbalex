public class CanvassMyRouteController {
    public List<sma__mawaypoint__c> listWpToBeUpdated {get;set;}
    //public String canvassLeadSheetLink {
    //    //HOTFIX: ZT 1.13.2016, using ApexPages.currentPage() method to set URL
    //        get {
    //            //String headersURL = ApexPages.currentPage().getHeaders().get('Host');
    //            //if(UserInfo.getUserType() == 'Standard' && headersURL != NULL && headersURL.contains('.visual.force.com'))
    //            //                return '/apex/CanvassedLeadInfo';
    //            //else
    //            //                return '/canvassing/apex/CanvassedLeadInfo';
    //            return '../apex/CanvassedLeadInfo';
    //        }
    //        Set;
    //}
    // This value is used for the CNVSS_CanvassHome page
    public integer AllHomesToVisitExcludingDoNotKnockHouses {get; set;}
    public boolean waypointsUpdated {get; set;}
    public String userThumbPhoto; // load into a <img src='[here]'> in component
    private static String theUserId = UserInfo.getUserId();
    public string Link1 {get; set;}
    public string Link2 {get; set;}
    public string Link3 {get; set;}
    public string Link4 {get; set;}
    public string HostVal {
        get {
            return ApexPages.currentPage().getHeaders().get('Host');
        }
        set;
    }

    public List<canvassWaypointWrapper> canvassWaypointWrapperList {get; set;}
    public List<canvassWaypointWrapper> canvassWaypointWrapperResultedList {get;set;}
    public List<canvassWaypointWrapper> canvassWaypointWrapperNotResultedList {get;set;}

    public class canvassWaypointWrapper implements Comparable {
        public SMA__MAWaypoint__c wp {get; set;}
        public CNVSS_Canvass_Unit__c cu {get; set;}
        public string lastResultDt {get; set;}
        public string formattedFullAddress {get; set;}

        public canvassWaypointWrapper(SMA__MAWaypoint__c waypoint, CNVSS_Canvass_Unit__c canvass) {
            this.wp = waypoint;
            this.cu = canvass;
            formattedFullAddress = CanvassUnitUtils.formatCanvassUnitAddress(canvass.CNVSS_House_No__c, canvass.CNVSS_Street__c, canvass.Street_Type__c, canvass.Unit_Prefix__c, 
                canvass.Unit_Number__c, canvass.CNVSS_City__c, canvass.CNVSS_State__c, canvass.CNVSS_Zip_Code__c);
            if(canvass.CNVSS_Last_Result_Datetime__c != null) {
                lastResultDt = canvass.CNVSS_Last_Result_Datetime__c.format('MMMMM dd, yyyy \'at\' hh:mm:ss a');
            }
        }

        public Integer compareTo(Object compareTo) {
            canvassWaypointWrapper cw = (canvassWaypointWrapper) compareTo;
            if(wp.sma__SortOrder__c == cw.wp.sma__SortOrder__c) return 0;
            if(wp.sma__SortOrder__c > cw.wp.sma__SortOrder__c) return 1;
            return -1;
        }
    }
    
    public CanvassMyRouteController(){
            Link1 = getDashboardLinkByTitle('My Results: Canvassing Stats');
            Link2 = getDashboardLinkByTitle('My Ranking: Canvassing Leaderboards');
            Link3 = getDashboardLinkByTitle('Team Results');
            Link4 = getDashboardLinkByTitle('Cross Market Dashboard');
            
    
        waypointsUpdated = false;
        AllHomesToVisitExcludingDoNotKnockHouses = 0;
        canvassWaypointWrapperList = new List<canvassWaypointWrapper>();
        canvassWaypointWrapperResultedList = new List<canvassWaypointWrapper>();
        canvassWaypointWrapperNotResultedList = new List<canvassWaypointWrapper>();
        List<SMA__MAWaypoint__c> allWaypointsToday = getAllWaypointsToday();
        /* Begin prepping the filter for the "Do Not Knock" waypoints in today's route */
        if(listWpToBeUpdated == null) {
            listWpToBeUpdated = new List<SMA__MAWaypoint__c>();
            // List to store the canvass unit ids from the waypoints
            List<string> listLinkId = new List<string>();
            // List to store the instantiated canvass units from the listLinkIds
            List<CNVSS_Canvass_Unit__c> listCU = new List<CNVSS_Canvass_Unit__c>();
            // Map to store the key (canvassunit id) and value (waypoint object) in order to retrieve the wp object from the linkid(canvass unit record id)
            Map<string, sma__mawaypoint__c> waypointCanvassMap = new Map<string, sma__mawaypoint__c>();
            for(sma__mawaypoint__c wp : allWaypointsToday) {
                waypointCanvassMap.put(wp.sma__LinkId__c, wp);
                listLinkId.add(wp.sma__LinkId__c);
            }
            // below will find all canvass units from the listLinkId generated from the waypoints
            if((!waypointCanvassMap.isEmpty() && !listLinkId.isEmpty()) && (waypointCanvassMap.size() == listLinkId.size())) {
                listCu = [SELECT Id, CNVSS_DO_NOT_CONTACT__c, CNVSS_Last_Result__c, CNVSS_Last_Result_Datetime__c, CNVSS_Last_Resulted_By__c, 
                            CNVSS_House_No__c, CNVSS_Street__c, Street_Type__c, Unit_Prefix__c, 
                            Unit_Number__c, CNVSS_City__c, CNVSS_State__c, CNVSS_Zip_Code__c
                            FROM CNVSS_Canvass_Unit__c 
                            WHERE Id IN :listLinkId];
                if(!listCu.isEmpty()) {
                    for(CNVSS_Canvass_Unit__c cu : listCu) {
                        // Add to list wrapper object
                        canvassWaypointWrapperList.add(new canvassWaypointWrapper(waypointCanvassMap.get(cu.Id), cu));
                        if(cu.CNVSS_DO_NOT_CONTACT__c) {
                            AllHomesToVisitExcludingDoNotKnockHouses++;
                        }
                    }
                }
            }
        }
        AllHomesToVisitExcludingDoNotKnockHouses = allWaypointsToday.Size() - AllHomesToVisitExcludingDoNotKnockHouses;
        /* End prepping */
        /* Fetching the counts and the waypoints that have been resulted and the ones that have yet to be resulted */
        if(!canvassWaypointWrapperList.isEmpty()) {
            for(canvassWaypointWrapper cw : canvassWaypointWrapperList) {
                // get the waypoints that have not been resulted yet by checking the notes field that was updated through CanvassLeadSheetController.markResultedWaypoint(string result)
                if(!string.isNotBlank(cw.wp.sma__notes__c) || cw.wp.sma__notes__c == 'Do Not Knock') {
                    canvassWaypointWrapperNotResultedList.add(cw);
                }
                else {
                    canvassWaypointWrapperResultedList.add(cw);
                }
            }
        }
        canvassWaypointWrapperNotResultedList.sort();
        canvassWaypointWrapperResultedList.sort();
    }
    
    public List<SMA__MAWaypoint__c> getAllWaypointsToday() {
        return [select id, name, sma__route__c, sma__route__r.name, sma__address__c, sma__linkid__c, sma__notes__c, sma__SortOrder__c 
                             from sma__mawaypoint__c 
                             where sma__route__c in
                             (select id from sma__maroute__c where Owner.Id = :UserInfo.getUserId() AND sma__Date__c = TODAY)
                             order by sma__SortOrder__c];
    }
    
    public PageReference updateWaypointDoNotKnocks() {
        if(!waypointsUpdated) {
            if(!listWpToBeUpdated.isEmpty()) {
                // update each of the waypoints's note field in listWp to 'Do Not Knock' since we've filtered them already in the canvass unit functionality at the top
                for(sma__mawaypoint__c wp : listWpToBeUpdated) {
                    wp.sma__Notes__c = 'Do Not Knock';
                }
            }
            update listWpToBeUpdated;
            waypointsUpdated = true;
        }
        return null;
    }
    
    public integer getTotalHomesExcludingDoNotKnock(){
        integer countHomesExcludingDoNotKnock = 0;
        if(!canvassWaypointWrapperList.isEmpty()) {
            for(canvassWaypointWrapper cw : canvassWaypointWrapperList) {
                if(cw.wp.sma__Notes__c != 'Do Not Knock')
                countHomesExcludingDoNotKnock++;
            }
        }
        return countHomesExcludingDoNotKnock;
    }
    
    public integer getTotalHomes(){
        integer countHomesToVisit = 0;
        if(!canvassWaypointWrapperList.isEmpty()) {
            countHomesToVisit = canvassWaypointWrapperList.size();
        }
        return countHomesToVisit;
    }
    
    public integer getResultedHomes(){
        integer countResultedHomes = 0;
        if(!canvassWaypointWrapperList.isEmpty()) {
            countResultedHomes = canvassWaypointWrapperResultedList.size();
        }
        return countResultedHomes;
    }
    
    private string getDashboardLinkByTitle(string title) {
        string link = '/';
        string id = '';
        Dashboard db = null;
        List<Dashboard> dashboards = [SELECT ID, Title FROM Dashboard Where Title = :title LIMIT 1];
        if(!dashboards.isEmpty()) {
            db = dashboards[0];
            id = db.Id;
            link += id;
        } else {
            //HOTFIX: ZT 1.13.2016, using ApexPages.currentPage() method to set URL
            //String headersURL = ApexPages.currentPage().getHeaders().get('Host');
            //if(USerInfo.getUserType() == 'Standard' && headersURL != null && headersURL.contains('.visual.force.com'))
            //	link += 'apex/CNVSS_CanvassHome';
            //else
            //    link += '/canvassing/apex/CNVSS_CanvassHome';
            link += 'apex/CNVSS_CanvassHome';
        }
        return link;
    }

    //Victor Edit Begin
    //getter - the acutal thumbnail photo url from the db
    public String getUserThumbPhoto() {
        //query with the user id
        String thePhoto = [SELECT SmallPhotoUrl FROM User WHERE User.ID = :theUserId LIMIT 1].SmallPhotoUrl;
        //System.debug('The photo url: '+thePhoto);
        return thePhoto;
    } 
    //Victor Edit End   
        
        // Paul's New Code
        public PageReference MyResultsLink(){
                string dashboardLink = getDashboardLinkByTitle('My Results: Canvassing Stats');
                PageReference Link = new PageReference(dashboardLink);
                return Link;
        }
        public PageReference MyRankingLink(){
                string dashboardLink = getDashboardLinkByTitle('My Ranking: Canvassing Leaderboards');
                PageReference Link = new PageReference(dashboardLink);
                return Link;
        }
        public PageReference MyTeamResultsLink(){
            string dashboardLink = getDashboardLinkByTitle('Team Results');
            PageReference Link = new PageReference(dashboardLink);
            return Link;
        }
        public PageReference CrossMarketLink(){
            string dashboardLink = getDashboardLinkByTitle('Cross Market Dashboard');
            PageReference Link = new PageReference(dashboardLink);
            return Link;
        }
    
}