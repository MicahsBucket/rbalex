@isTest
private class EditManualPAS_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}
	@isTest static void canGetSurvey() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		List<Survey__c> surveyList = TestDataFactory.createSurveys('Post Install', 2, accountId, 'Pending');
		test.startTest();
				Survey__c testSurvey = surveyList[0];
				Survey__c databaseSurvey = EditManualPostAppointmentApexCont.getSurvey(testSurvey.Id);
				system.assertEquals(testSurvey.Id, databaseSurvey.Id);
			
		test.stopTest();
	}
	
	@isTest static void canGetWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		TestDataFactory.createWorkers('Sales Rep', 5, accountId, surveyId);
		TestDataFactory.createWorkers('Tech Measurer', 5, accountId, surveyId);
		test.startTest();
				List<Worker__c> testWorkers = EditManualPostAppointmentApexCont.getWorkers('Sales Rep');
				system.debug('testWorkerSize: ' + testWorkers.size());
				Set<String> testWorkersRoles = new Set<String>();
				for(Worker__c w : testWorkers){
					testWorkersRoles.add(w.Role__c);
				}
				system.assertEquals(testWorkersRoles.contains('Sales Rep'), true);
				system.assertNotEquals(testWorkersRoles.contains('Tech Measurer'), true);
			
		test.stopTest();
	}


	@isTest static void canGetOriginalWorker(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		List<Worker__c> techWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId, surveyId);
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		Set<Worker__c> installerSet = new Set<Worker__c>(TestDataFactory.createWorkers('Installer', 1, accountId, surveyId));
		test.startTest();
				Worker__c originalSalesRep = EditManualPostAppointmentApexCont.getOriginalWorker(surveyId, 'Sales Rep');
				Worker__c originalTechMeasurer = EditManualPostAppointmentApexCont.getOriginalWorker(surveyId, 'Tech Measurer');
				system.assertEquals(originalSalesRep.Id, salesWorker[0].Id);
				system.assertEquals(originalTechMeasurer.Id, techWorker[0].Id);
				system.assertNotEquals(installerSet.contains(originalSalesRep), true);
				system.assertNotEquals(installerSet.contains(originalTechMeasurer), true);
			
		test.stopTest();
	}

	@isTest static void canSavePAS(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c survey = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Id surveyId = survey.Id;
		List<Worker__c> techWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId, surveyId);
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		List<Worker__c> newTechWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId);
		List<Worker__c> newSalesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId);
		List<Worker__c> installers = TestDataFactory.createWorkers('Installer', 5, accountId, surveyId);
		List<Worker__c> newInstallers = TestDataFactory.createWorkers('Installer', 2, accountId);
		List<Id> newInstallerIds = new List<Id>();
		Set<Worker__c> installersSet = new Set<Worker__c>(installers);
		for(Worker__c w : newInstallers){
			newInstallerIds.add(w.Id);
		}
		Set<Id> newInstallerIdsSet = new Set<Id>(newInstallerIds);
		test.startTest();
				String originalFirstName = survey.Primary_Contact_First_Name__c;
				String originalLastName = survey.Primary_Contact_Last_Name__c;
				String originalEmail = survey.Primary_Contact_Email__c;
				String originalPhone = survey.Primary_Contact_Mobile_Phone__c;
				String firstName = 'Calvin12';
				String lastName = 'OKeefe';
				String email = 'calvin.okeefe@gmail.com';
				String phone = '6517657777';
				String appointmentDate = '2017-4-04';
				String result = 'Demo No Sale';
				Id salesRep = newSalesWorker[0].Id;
				Worker__c originalSalesRep = salesWorker[0];

				EditManualPostAppointmentApexCont.savePAS(
					surveyId, firstName, lastName, email, phone, 
					salesRep, originalSalesRep, appointmentDate, result);
				Survey__c savedSurvey = [SELECT Primary_Contact_First_Name__c, Primary_Contact_Email__c, 
												Primary_Contact_Mobile_Phone__c, Primary_Contact_Last_Name__c 
												FROM Survey__c WHERE Primary_Contact_First_Name__c = :firstName LIMIT 1];

				system.assertNotEquals(savedSurvey.Primary_Contact_First_Name__c, originalFirstName);
				system.assertNotEquals(savedSurvey.Primary_Contact_First_Name__c, originalLastName);
				system.assertNotEquals(savedSurvey.Primary_Contact_Email__c, originalEmail);
				system.assertNotEquals(savedSurvey.Primary_Contact_Mobile_Phone__c, originalPhone);
				system.assertEquals(savedSurvey.Primary_Contact_First_Name__c, firstName);
				system.assertEquals(savedSurvey.Primary_Contact_Last_Name__c, lastName);
				system.assertEquals('(651)7657777', savedSurvey.Primary_Contact_Mobile_Phone__c);
		test.stopTest();
	}

	@isTest static void canEditAndUpdateRecord(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c survey = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Id surveyId = survey.Id;
		test.startTest();
			EditManualPostAppointmentApexCont.editRecord('okeef135@test.com', surveyId);
			Survey__c testSurvey = [SELECT Id, Primary_Contact_Email__c FROM Survey__c WHERE Id = :surveyId];
			system.assertEquals('okeef135@test.com', testSurvey.Primary_Contact_Email__c);
			String send = EditManualPostAppointmentApexCont.updateStatus('Send', surveyId);
			system.assertEquals('send', send);
			String alreadySent = EditManualPostAppointmentApexCont.updateStatus('Send', surveyId);
		test.stopTest();		
	}

}