public class ChangeOrderListCtlr {

    @AuraEnabled(cacheable=true)
    public static List<ChangeOrderWrapper> getChangeOrderList(Id orderId) {
 
        List<ChangeOrderWrapper> coWrapperList = new List<ChangeOrderWrapper>();

        Id changeOrderId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId();
        System.debug('Change Order Id: ' + changeOrderId);
        System.debug('Order Id: ' + orderId);
 
        
        for (Order co : [SELECT     Id,
                                    OrderNumber,
                                    Name,
                                    Retail_Total__c,
                                    CreatedDate,
                                    Status,
                                    Sold_Order__c,
                                    Sold_Order__r.Revenue_Recognized_Date__c
                             FROM   Order
                             WHERE  Sold_Order__c = :orderId
                             AND    RecordTypeId =: changeOrderId]) 
                             
                             {
            ChangeOrderWrapper coWrapper = new ChangeOrderWrapper();
            coWrapper.id = co.Id;
            coWrapper.orderNumber = co.OrderNumber;
            coWrapper.name = co.Name;
            coWrapper.retailTotal = co.Retail_Total__c;
            coWrapper.createdDate = co.CreatedDate;
            coWrapper.status = co.Status;
            coWrapper.revRecDate = co.Sold_Order__r.Revenue_Recognized_Date__c;
            coWrapper.soldOrder = co.Sold_Order__c;

            if (coWrapper.id == null) {
                coWrapper.orderNumberUrl = '';
            } else {
                coWrapper.orderNumberUrl = '/' + coWrapper.id;
            }

            coWrapperList.add(coWrapper);
            System.debug('CO Wrapper :' + coWrapper);
           

        }

        System.debug('Wrapper List: ' + coWrapperList);
        return coWrapperList;

    } // End Method

    @TestVisible
    public class ChangeOrderWrapper {

        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String orderNumber {get;set;}
        @AuraEnabled public String name {get;set;}
        @AuraEnabled public Decimal retailTotal {get;set;}
        @AuraEnabled public Datetime createdDate {get;set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public Datetime revRecDate {get;set;}
        @AuraEnabled public String orderNumberUrl {get;set;}
        @AuraEnabled public String soldOrder {get;set;}


    } // End (Wrapper) Class

    @AuraEnabled(cacheable=true)
    public static List<ChangeOrderRevWrapper> getOrderRevRec(Id orderId) {
 
        List<ChangeOrderRevWrapper> corWrapperList = new List<ChangeOrderRevWrapper>();

        Id changeOrderId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId();
        System.debug('Change Order Id: ' + changeOrderId);
        System.debug('Order Id: ' + orderId);
 
        
        for (Order cor : [SELECT    Id,
                                    Revenue_Recognized_Date__c
                             FROM   Order
                             WHERE  Id = :orderId]) 
                             
                             {
            ChangeOrderRevWrapper corWrapper = new ChangeOrderRevWrapper();
            corWrapper.id = cor.Id;
            corWrapper.revRecDate = cor.Revenue_Recognized_Date__c;

            corWrapperList.add(corWrapper);
            System.debug('COR Wrapper :' + corWrapper);
           

        }

        System.debug('Rev Wrapper List: ' + corWrapperList);
        return corWrapperList;

    } // End Method

    @TestVisible
    public class ChangeOrderRevWrapper {

        @AuraEnabled public String id {get;set;}
        @AuraEnabled public Datetime revRecDate {get;set;}


    } // End (Wrapper) Class



    @AuraEnabled
    public static Map<String, String> createChangeOrder(Id orderId) {

    Map<String,String> resultMap = new Map<String,String>();    

    Id changeOrderId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Change_Order').getRecordTypeId();

    List<Order> orderFields = [SELECT       Id,
                                            OpportunityId,
                                            Account.Id,
                                            BillToContactId,
                                            BillingStreet,
                                            Unique_Identifier__c,
                                            Store_Location__c
                                     FROM   Order
                                     WHERE  Id = :orderId]; 

    Order newChangeOrder = new Order(Sold_Order__c = orderFields[0].Id,
                                    RecordTypeId = changeOrderId,
                                    Status = 'In Progress',
                                    OpportunityId = orderFields[0].OpportunityId,
                                    AccountId = orderFields[0].Account.Id,
                                    BillToContactId = orderFields[0].BillToContactId,
                                    BillingStreet = orderFields[0].BillingStreet,
                                    Unique_Identifier__c = orderFields[0].Unique_Identifier__c,
                                    Store_Location__c = orderFields[0].Store_Location__c,
                                    EffectiveDate = Date.today(),
                                    Name = orderFields[0].BillToContactId + ' - ' + orderFields[0].BillingStreet + ' - ' + Date.today() + ' -CO');

                                try {
                                insert newChangeOrder;

                                resultMap.put('New Change Order Success', newChangeOrder.Id);
                                }
                                catch (Exception ex) {
                    
                                    System.debug('***** The following exception occurred in the ChangeOrderListCtlr in the CreateChangeOrder method inserting records:' + ex);
                    
                                    resultMap.put(ex.getMessage(), '');
                                }
                    
                    
                                return resultMap;            
    }

}