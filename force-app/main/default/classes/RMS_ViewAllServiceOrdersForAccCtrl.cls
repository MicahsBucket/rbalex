public class RMS_ViewAllServiceOrdersForAccCtrl{
    public List<Order> listServiceOrders{
        get{
            List<Id> listRecTypeIds;
            
            listRecTypeIds = (List<Id>)JSON.deserialize(ApexPages.currentPage().getParameters().get('recTypeIds'),List<Id>.class);
        	return [SELECT Id,Name,OrderNumber,Status,EffectiveDate,Contract.Name,ContractId,TotalAmount,Amount_Due__c,
                    Retail_Total__c,RecordType.Name,Sold_Order__c,Description,Service_Type__c,Sold_Order__r.OrderNumber 
                    FROM Order 
                    WHERE AccountId = :ApexPages.currentPage().getParameters().get('id')
                    AND RecordTypeId IN :listRecTypeIds];
    	}
		set;
	}
    public RMS_ViewAllServiceOrdersForAccCtrl(){
        
    }
}