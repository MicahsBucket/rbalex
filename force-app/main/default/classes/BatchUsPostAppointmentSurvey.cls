global class BatchUsPostAppointmentSurvey implements Database.Batchable<sObject> {
	
	String query;
	
	global BatchUsPostAppointmentSurvey() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		Set<Date> holidayDateSet = new Set<Date>();
		Date today = date.today();
		Date yesterday = date.today().addDays(-1);
		Date twoDaysAgo = date.today().addDays(-2);
		Date fourDaysAgo = date.today().addDays(-4);
		Date fiveDaysAgo = date.today().addDays(-5);
		Date sixDaysAgo = date.today().addDays(-6);
		Date nullDate = date.today().addDays(-100000);
		Boolean todayBool = false;
		Boolean yesterdayBool = false;
		Boolean twoDaysAgoBool = false;
		List<CXMT_Holiday__c> holidayList = [SELECT Date__c FROM CXMT_Holiday__c 
											 WHERE (Date__c = :today OR Date__c = :yesterday OR Date__c = :twoDaysAgo) AND Country__c = 'US'];
		for(CXMT_Holiday__c ch : holidayList){
			holidayDateSet.add(ch.Date__c);
		}
		//checks if one of these days is a holiday
		system.debug('holidayDateSet:' + holidayDateSet);
		if(holidayDateSet.contains(today)) todayBool = true;
		if(holidayDateSet.contains(yesterday)) yesterdayBool = true;
		if(holidayDateSet.contains(twoDaysAgo)) twoDaysAgoBool = true;
		
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Appointment').getRecordTypeId();
		String sale = 'Sale';
		String enabled = 'EnabledPlus';
		List<String> canadaSpelling = new List<String>{'cANADA','CANADA', 'Canada', 'canada','CAN', 'Can', 'can', 'CA', 'ca', 'Ca'};

		query = 'SELECT Send_to_Medallia__c, Primary_Contact_First_Name__c, Primary_Contact_Last_Name__c, Primary_Contact_Email__c, Secondary_Contact_Email__c FROM Survey__c WHERE RecordTypeId = :recordTypeId AND Appointment_Result__c = :sale AND Source_System__c = :enabled AND Country__c NOT IN :canadaSpelling';
		if(!todayBool && yesterdayBool && twoDaysAgoBool){
			//batch for all three days and combine them
			query += ' AND (Appointment_Date__c = :fourDaysAgo OR Appointment_Date__c = :fiveDaysAgo OR Appointment_Date__c = :sixDaysAgo)'; 
		} else if(!todayBool && yesterdayBool && !twoDaysAgoBool){
			//batch today and yesterday
			query += ' AND (Appointment_Date__c = :fourDaysAgo OR Appointment_Date__c = :fiveDaysAgo)'; 
		} else if(!todayBool){
			//batch today
			query += ' AND Appointment_Date__c = :fourDaysAgo';
		} else {
			query += ' AND Appointment_Date__c = :nullDate';
		}
		system.debug('query: ' + query);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Survey__c> scope) {
		for(Survey__c apptSurvey : scope){
       		if(apptSurvey.Primary_Contact_First_Name__c == null && apptSurvey.Primary_Contact_Last_Name__c == null){
       			apptSurvey.Survey_Status__c = 'Incomplete Data';
   			} else if (apptSurvey.Primary_Contact_Email__c == null && apptSurvey.Secondary_Contact_Email__c == null){
       			apptSurvey.Survey_Status__c = 'Incomplete Data';
   			} else {
   				apptSurvey.Survey_Auto_Sent__c = TRUE;
   				apptSurvey.Send_to_Medallia__c = TRUE;
   				apptSurvey.On_Hold__c = TRUE;
   			}
       	}
       	Database.update(scope, false);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}