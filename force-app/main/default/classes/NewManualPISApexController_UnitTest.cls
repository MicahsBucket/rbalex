@isTest
private class NewManualPISApexController_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
		Account testStoreAccount = TestDataFactory.createStoreAccount('Test Store');
		Database.insert(testStoreAccount, true);
		Contact testContact = TestDataFactory.createCustomerContact(testStoreAccount.Id);
		Database.insert(testContact, true);
		User partnerUser = TestDataFactory.createPartnerUser('Test', 'Partner RMS-RSR', testContact.Id);
		Database.insert(partnerUser, true);
	}

	@isTest static void canGetSurvey() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		List<Survey__c> surveyList = TestDataFactory.createSurveys('Post Install', 2, accountId, 'Pending');
		test.startTest();
				Survey__c testSurvey = surveyList[0];
				Survey__c databaseSurvey = NewManualPISApexController.getSurvey(testSurvey.Id);
				system.assertEquals(testSurvey.Id, databaseSurvey.Id);
		test.stopTest();
	}
	
	@isTest static void canGetWorkers() {
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
		TestDataFactory.createWorkers('Sales Rep', 5, accountId, surveyId);
		TestDataFactory.createWorkers('Tech Measurer', 5, accountId, surveyId);
		test.startTest();
				List<Worker__c> testWorkers = NewManualPISApexController.getWorkers('Sales Rep');
				system.debug('testWorkerSize: ' + testWorkers.size());
				Set<String> testWorkersRoles = new Set<String>();
				for(Worker__c w : testWorkers){
					testWorkersRoles.add(w.Role__c);
				}
				system.assertEquals(testWorkersRoles.contains('Sales Rep'), true);
				system.assertNotEquals(testWorkersRoles.contains('Tech Measurer'), true);
		test.stopTest();
	}


	@isTest static void canGetOriginalWorker(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Id surveyId = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0].Id;
        List<Survey__c> sur = surveyRecord('Post Appointment', 1, accountId, 'Pending');
		List<Worker__c> techWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId, surveyId);
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		Set<Worker__c> installerSet = new Set<Worker__c>(TestDataFactory.createWorkers('Installer', 1, accountId, surveyId));
		test.startTest();
				Worker__c originalSalesRep = NewManualPISApexController.getOriginalWorker(surveyId, 'Sales Rep');
        		Worker__c originalSalesRepPA = NewManualPISApexController.getOriginalWorker(sur[0].Id, 'Sales Rep');
				Worker__c originalTechMeasurer = NewManualPISApexController.getOriginalWorker(surveyId, 'Tech Measurer');
				system.assertEquals(originalSalesRep.Id, salesWorker[0].Id);
				system.assertEquals(originalTechMeasurer.Id, techWorker[0].Id);
				system.assertNotEquals(installerSet.contains(originalSalesRep), true);
				system.assertNotEquals(installerSet.contains(originalTechMeasurer), true);
		test.stopTest();
	}

	@isTest static void canSavePIS(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c survey = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Id surveyId = survey.Id;
		List<Worker__c> techWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId, surveyId);
		List<Worker__c> salesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId, surveyId);
		List<Worker__c> newTechWorker = TestDataFactory.createWorkers('Tech Measurer', 1, accountId);
		List<Worker__c> newSalesWorker = TestDataFactory.createWorkers('Sales Rep', 1, accountId);
		List<Worker__c> installers = TestDataFactory.createWorkers('Installer', 5, accountId, surveyId);
		List<Worker__c> newInstallers = TestDataFactory.createWorkers('Installer', 2, accountId);
		List<Id> newInstallerIds = new List<Id>();
		Set<Worker__c> installersSet = new Set<Worker__c>(installers);
		for(Worker__c w : newInstallers){
			newInstallerIds.add(w.Id);
		}
		Set<Id> newInstallerIdsSet = new Set<Id>(newInstallerIds);
		test.startTest();
				String originalFirstName = survey.Primary_Contact_First_Name__c;
				String originalLastName = survey.Primary_Contact_Last_Name__c;
				String originalEmail = survey.Primary_Contact_Email__c;
				String originalPhone = survey.Primary_Contact_Mobile_Phone__c;
				String firstName = 'Calvin';
				String lastName = 'OKeefe';
				String email = 'calvin.okeefe@gmail.com';
				String phone = '6517657777';
				String street = '123 Fake Street';
				String city = 'St. Paul';
				String state = 'MN';
				String zip = '12345';
				String country = 'United States';
				String comment = 'Test Comment';
				Id techMeasurer = newTechWorker[0].Id;
				Id salesRep = newSalesWorker[0].Id;
				Worker__c originalSalesRep = salesWorker[0];
				Worker__c originalTechMeasurer = techWorker[0];

				NewManualPISApexController.savePIS(
					surveyId, firstName, lastName, email, phone, street, city, state, zip, country, techMeasurer, 
					salesRep, newInstallerIds, originalSalesRep, originalTechMeasurer, comment);
				Survey__c savedSurvey = [SELECT Id, Primary_Contact_First_Name__c, Primary_Contact_Last_Name__c, 
												Primary_Contact_Email__c, Primary_Contact_Mobile_Phone__c 
												FROM Survey__c WHERE Id = :surveyId];

				system.assertNotEquals(savedSurvey.Primary_Contact_First_Name__c, originalFirstName);
				system.assertNotEquals(savedSurvey.Primary_Contact_First_Name__c, originalLastName);
				system.assertNotEquals(savedSurvey.Primary_Contact_Email__c, originalEmail);
				system.assertNotEquals(savedSurvey.Primary_Contact_Mobile_Phone__c, originalPhone);
				system.assertEquals(savedSurvey.Primary_Contact_First_Name__c, firstName);
				system.assertEquals(savedSurvey.Primary_Contact_Last_Name__c, lastName);
				system.assertEquals('(651)7657777', savedSurvey.Primary_Contact_Mobile_Phone__c);

				List<Survey_Worker__c> newInstallerJuncList = [SELECT Role__c, Survey_Id__c, Worker_Id__c FROM Survey_Worker__c WHERE Survey_Id__c = :surveyId AND Role__c = 'Installer' LIMIT 5];
				List<Id> soqlInstallerIds = new List<Id>();
				for(Survey_Worker__c sw : newInstallerJuncList){
					soqlInstallerIds.add(sw.Worker_Id__c);
				}
				system.assertNotEquals(soqlInstallerIds.size(), 5);
				system.assertEquals(soqlInstallerIds.size(), 2);
				system.assertEquals(newInstallerIdsSet.contains(soqlInstallerIds[0]), true);
		test.stopTest();

	}

	@isTest static void canUpdateStatus(){
		Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
		Survey__c survey = TestDataFactory.createSurveys('Post Install', 1, accountId, 'Pending')[0];
		Id surveyId = survey.Id;

		test.startTest();
			String hold = NewManualPISApexController.updateStatus('Hold', surveyId);
			system.assertEquals('hold', hold);
			String alreadyHeld = NewManualPISApexController.updateStatus('Hold', surveyId);
			system.assertEquals('already held', alreadyHeld);
			String send = NewManualPISApexController.updateStatus('Send', surveyId);
			system.assertEquals('send', send);
			String alreadySent = NewManualPISApexController.updateStatus('Send', surveyId);
			system.assertEquals('already sent', alreadySent);
		test.stopTest();
	}
	
      public static List<Survey__c> surveyRecord(String recordType, Integer surveyNum, Id accountId, String status){
		List<Survey__c> surveyList = new List<Survey__c>();
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
		
		for(Integer i = 0; i < surveyNum; i++){
			Survey__c ns = new Survey__c(
				RecordTypeId = recordTypeId,
                Unique_Job_Id__c = '001' + surveyNum,
                Survey_Status__c = status,
				Primary_Contact_Email__c = 'test' + i + '@user.com',
				Primary_Contact_First_Name__c = 'first' + i,
				Primary_Contact_Last_Name__c = 'last' + i,
				Primary_Contact_Mobile_Phone__c = '1234567890',
				State__c = 'Minnesota',
				City__c = 'St. Paul',
				Street__c = '1111 Fairmount Ave.',
				Zip__c = '55105',
				Store_Account__c = accountId
			);
			surveyList.add(ns);
		}
		Database.insert(surveyList, true);
		return surveyList;
	}  
            



	
}