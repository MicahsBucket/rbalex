/*
* @author Jason Flippen
* @date 10/09/2020 
* @description Class to provide functionality for the HomeComponentLightningCardsSection Aura Component.
*
*              Test code coverage provided by the following Test Class:
*			   - HomeComponentDashboardFinderTest
*/
public with sharing class HomeComponentDashboardFinder {

    /*
    * @author Jason Flippen
    * @date 10/09/2020
    * @description Method to retrive the Id of the last viewed Dashboard.
    */
    @AuraEnabled
    public static Id getDashboardId() {
        
        Id dashboardId = null;

        List<Dashboard> dashboardList = [SELECT Id
                                         FROM   Dashboard
                                         WHERE  LastViewedDate != null
                                         ORDER BY LastViewedDate DESC
                                         LIMIT 1];

        if (!dashboardList.isEmpty()) {
            dashboardId = dashboardList[0].Id;
        }
        
        return dashboardId;

    }

}