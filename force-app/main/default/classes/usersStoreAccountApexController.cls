public with sharing class usersStoreAccountApexController {

@AuraEnabled
	public static String getStoreAccount(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();
		String storeAccount = [SELECT Name, RecordTypeId FROM Account WHERE RecordTypeId = :recordTypeId LIMIT 1].Name;
		return storeAccount;
	}
}