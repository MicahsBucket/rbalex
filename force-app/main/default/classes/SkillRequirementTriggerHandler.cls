public with sharing class SkillRequirementTriggerHandler {
    public static final Set<String> scheduledWorkOrderTypes = new Set<String> { 'Tech_Measure', 'Install', 'Service', 'Job_Site_Visit' };
    public static final Set<String> nonScheduledWOStatuses = new Set<String> { 'To be Scheduled', 'Canceled' };

    public static void validateScheduledWorkOrder(List<SkillRequirement> skillReqs) {
        Map<Id, List<SkillRequirement>> woIdToSkillReqs = new Map<Id, List<SkillRequirement>>();
        List<SkillRequirement> invalidSkillReqs = new List<SkillRequirement>();
        for(SkillRequirement skillReq : skillReqs) {
            if(!woIdToSkillReqs.containsKey(skillReq.RelatedRecordId)) {
                woIdToSkillReqs.put(skillReq.RelatedRecordId, new List<SkillRequirement>{ skillReq });
            } else {
                List<SkillRequirement> tempSkillReqs = woIdToSkillReqs.get(skillReq.RelatedRecordId);
                tempSkillReqs.add(skillReq);
                woIdToSkillReqs.put(skillReq.RelatedRecordId, tempSkillReqs);
            }
        }
        for(WorkOrder wo : [SELECT Id,
                                   RecordType.DeveloperName,
                                   Status
                            FROM WorkOrder
                            WHERE Id IN: woIdToSkillReqs.keySet()]) {
            system.debug(wo.status);
            if(scheduledWorkOrderTypes.contains(wo.RecordType.DeveloperName)
                    && !nonScheduledWOStatuses.contains(wo.Status)) {
                for(SkillRequirement skillReq : woIdToSkillReqs.get(wo.Id)) {
                    invalidSkillReqs.add(skillReq);    
                }
            }
        }
        for(SkillRequirement skillReq : invalidSkillReqs) {
            skillReq.addError('This Work Order has already been scheduled, you may not add additional skills to this Work Order. If you need to adjust the skills, cancel the appointment first.');
        }
    }
}