/**
* @author: Jason Flippen
* @description: Test class for Pricing Configurations export
* @class coverage: ExportPricingConfigsController
*/
@isTest
public class ExportPricingConfigsControllerTest {

    /**
    * @author: Jason Flippen
    * @description: Method to test the functionality in the Controller.
    * @params: N/A
    * @returns: N/A
    */ 
    private static testMethod void testController() {

        // Create Original Wholesale Pricebook and related Pricing Configuration records.
        Wholesale_Pricebook__c testWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook',
                                                                                   Status__c = 'In Process',
                                                                                   Effective_Date__c = Date.today());
        insert testWholesalePricebook;

        Id pcRecordTypeId = Schema.SObjectType.Pricing_Configuration__c.getRecordTypeInfosByDeveloperName().get('Window_Pricing_Configuration').getRecordTypeId();
        Pricing_Configuration__c testPricingConfiguration = new Pricing_Configuration__c(Name = 'Test Pricing Configuration',
                                                                                         RecordTypeId = pcRecordTypeId,
                                                                                         Wholesale_Pricebook__c = testWholesalePricebook.Id);
        insert testPricingConfiguration;

        // Create Cloned Wholesale Pricebook and related Pricing Configuration records.
        Wholesale_Pricebook__c clonedTestWholesalePricebook = new Wholesale_Pricebook__c(Name = 'Test Wholesale Pricebook Clone',
                                                                                         Status__c = 'In Process',
                                                                                         Effective_Date__c = Date.today(),
                                                                                         Cloned_Wholesale_Pricebook__c = testWholesalePricebook.Id);
        insert clonedTestWholesalePricebook;

        Pricing_Configuration__c clonedTestPricingConfiguration = new Pricing_Configuration__c(Name = 'Test Pricing Configuration Clone',
                                                                                               RecordTypeId = pcRecordTypeId,
                                                                                               Wholesale_Pricebook__c = clonedTestWholesalePricebook.Id,
                                                                                               Cloned_Pricing_Configuration__c = testPricingConfiguration.Id);
        insert clonedTestPricingConfiguration;

        Test.startTest();

            // Retrieve the Pricing Configuration Wrapper, containing the Pricing
            // Configurations related to the Test Wholesale Pricebook.
            ExportPricingConfigsController.PricingConfigurationWrapper pricingConfigurationWrapper = ExportPricingConfigsController.getPricingConfigurations(clonedTestWholesalePricebook.Id);
            System.assertEquals(1,pricingConfigurationWrapper.pricingConfigurationList.size());

        Test.stopTest();

    }

}