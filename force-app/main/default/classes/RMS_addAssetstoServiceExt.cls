public with sharing class RMS_addAssetstoServiceExt extends RMS_addAssetstoService
{
    
    
    public Account myWO {get; private set;}
    
    public RMS_addAssetstoServiceExt(ApexPages.StandardController stdController) 
    {
        super(stdController);
        
        
        this.myWO = [SELECT Id, Store_Location__c                             
                     FROM Account
                     WHERE Id =: stdController.getRecord().Id];
        
        this.childList = [SELECT Id,
                          Product2Id,
                          ContactId,
                          AccountId,
                          Store_Location__c,
                          Status,
                          Legacy_Asset__c,
                          RecordTypeId,
                          Name,
                          Quantity,
                          Description
                          FROM Asset
                          WHERE AccountId =: mysObject.Id
                          AND Legacy_Asset__c = TRUE];
    }
    
    /*
* This method is necessary for reference on the Visualforce page, 
* in order to reference non-standard fields.
*/
    public List<Asset> getChildren()
    {
        return (List<Asset>)childList;
    }
    
    public override sObject initChildRecord()
    {
        Asset child = new Asset();
        child.AccountId = myWO.Id; 
        child.Status = 'Installed'; 
        child.Name = 'New Asset';
        child.Quantity = 1;
        child.RecordTypeId = UtilityMethods.retrieveRecordTypeId('Installed_Products', 'Asset');
        child.Legacy_Asset__c = TRUE;
        return child;
    }

    public PageReference cancel()
    {
      return null;
    }
    
}