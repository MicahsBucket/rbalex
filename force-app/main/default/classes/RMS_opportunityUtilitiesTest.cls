@isTest
public with sharing class RMS_opportunityUtilitiesTest {

/*******************************************************

This test code and related class appear to be for an unused button called "Convert to Order" on the Opportunity object.

This button and the Class and Test Class shoudl be removed from the system

***********************************************************/


    static testmethod void convertOpportunityTest(){

// Set up test data
		TestUtilityMethods testMethods = new TestUtilityMethods();
		testMethods.setUpConfigs();
		
		Account store1 = [SELECT id FROM Account WHERE Name='77 - Twin Cities, MN'];
		Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
		
		Account dwelling1 = testMethods.createDwellingAccount('dwelling1');
		dwelling1.Store_Location__c = store1.id;
		insert dwelling1;
		system.debug('dwelling is : ' + dwelling1.id);
		
		Contact contact1 = testMethods.createContact(dwelling1.id, 'contact1');
		insert contact1;
		system.debug('contact is : ' + contact1.id);
		
		Opportunity opp1 = testMethods.createOpportunity(dwelling1.id, 'Draft');
		insert opp1;
		system.debug('opp is : ' + opp1);

		Quote quote1 = testMethods.createQuote(opp1.id,Test.getStandardPricebookId());
		insert quote1;
		system.debug('quote is : ' + quote1);
		
		Product2 prod1 = testMethods.createProduct();
		insert prod1;
		system.debug('prod is : ' + prod1.id);

		PricebookEntry pbEntry1 = testMethods.createPricebookEntry(Test.getStandardPricebookId(), prod1.id);
		insert pbEntry1;
		system.debug('pbe is : ' + pbEntry1.id);
    		
		QuoteLineItem qli1 = testMethods.createQuoteLineItem(pbEntry1.Id, quote1.Id);
		insert  qli1;
		system.debug('ql is : ' + qli1.id);
			
		opp1.SyncedQuoteId = quote1.id;
		opp1.StageName = 'New';
		update opp1;

		test.startTest();		
		// Convert opportunity when not won -- no order should be created
		RMS_opportunityUtilities.convertOpportunity(String.ValueOf(opp1.id));
		System.AssertEquals(0, [SELECT Id, Status FROM Order WHERE OpportunityId = :opp1.Id].size());		
		
		quote1.isSold__c = true;
		quote1.Status = 'Accepted';
        update quote1;

		opp1.StageName = 'Sold';
        update opp1;
		// Run the convert, one order should be there
		RMS_opportunityUtilities.convertOpportunity(String.ValueOf(opp1.id));
		//System.AssertEquals(1, [SELECT Id, Status FROM Order WHERE OpportunityId = :opp1.Id].size());		

		// Delete the order and run the convert, should be one new order there
		List<Order> orderList = [SELECT Id, Status FROM Order WHERE OpportunityId = :opp1.Id];
		delete orderList;
		RMS_opportunityUtilities.convertOpportunity(String.ValueOf(opp1.id));
		//System.AssertEquals(1, [SELECT Id, Status FROM Order WHERE OpportunityId = :opp1.Id].size());		
		test.stopTest();
    }
     
    

}