global class BatchScheduleInstallAutoSend implements Schedulable {
	global void execute(SchedulableContext sc) {
		Date today = date.today();
		Id recordTypeId = Schema.SObjectType.Survey__c.getRecordTypeInfosByName().get('Post Install').getRecordTypeId();
		List<Survey__c> surveysToAutoSend = [SELECT Id, Survey_Auto_Sent__c, Survey_Status__c,
													Send_to_Medallia__c, Primary_Contact_First_Name__c, 
													Primary_Contact_Last_Name__c, City__c, State__c, Zip__c, 
													Country__c, Street__c 
													FROM Survey__c WHERE Send_Date__c = :today AND RecordTypeId = :recordTypeId AND Sent_To_Medallia__c = false];
		for(Survey__c s : surveysToAutoSend){
			if(s.Primary_Contact_First_Name__c != null && s.Primary_Contact_Last_Name__c != null
			&& s.City__c != null && s.State__c != null 
			&& s.Zip__c != null && s.Country__c != null && s.Street__c != null){
				s.Send_to_Medallia__c = true;
				s.Survey_Auto_Sent__c = true;
			} else {
				s.Survey_Status__c = 'Incomplete Data';
			}
		}
		Database.update(surveysToAutoSend, true);
	}
}