/*******************************************************//**

@class  RMS_cancelOrderExtension

@brief  Controller extension for VF page RMS_cancelOrder.page

  Cancels the order and other related records

@author  Creston Kuenzi (Slalom.CDK)

@version  2015-10-25  Slalom.CDK
  Created.

@see    RMS_cancelOrderExtensionTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
      Unauthorized use is prohibited. 

***********************************************************/
public with sharing class RMS_cancelOrderExtension {


  /******* Set up Standard Controller for Order  *****************/
  private Apexpages.StandardController standardController;
  private final Order theOrder;
  
  /******* Constructor  *****************/
  public RMS_cancelOrderExtension(ApexPages.StandardController stdController) {
    this.theOrder = (Order)stdController.getRecord();
  }
  
  /*******************************************************
          cancelOrder method
  *******************************************************/
  public PageReference cancelOrder() {
    system.debug('*****Cancelling an order');

    String theId = ApexPages.currentPage().getParameters().get('id');

    if (theId == null) {
      // Display the Visualforce page's content if no Id is found
      return null;
    }

    // Find the order and cancel it
    Order theOrder = [Select Id, Name, Status,OpportunityId, Apex_Context__c, Cancellation_Reason__c from Order where id =:theId];
    // Allows the order status to be changed
    theOrder.Apex_Context__c = true;
    theOrder.Status = 'Cancelled';
    theOrder.Date_Cancelled__c = System.Today();


    /*List<WorkOrder> workOrdersWithOpenEvents = [Select Id from WorkOrder WHERE Sold_Order__c =:theOrder.Id 
                                                            AND Number_Open_Events_Formula__c > 0];
        if(!workOrdersWithOpenEvents.isEmpty()){
            System.debug('Error encountered in RMS_CancelOrderExtensions: ' + RMS_errorMessages.WORK_ORDERS_WITH_OPEN_EVENTS);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, RMS_errorMessages.WORK_ORDERS_WITH_OPEN_EVENTS)); 
            return null;
    }*/  
    List<Purchase_Order__c> purchaseOrdersToCancel = new List<Purchase_Order__c>();
    List<OrderItem> orderItemsToCancel = new List<OrderItem>();
    List<Order_Discount__c> discountsToCancel = new List<Order_Discount__c>();
    List<WorkOrder> workOrdersToCancel = new List<WorkOrder>();
    List<Opportunity> OpportunitiesToCancel = new List<Opportunity>();

    // Find any purchase orders linked to the order that are In Process and cancel them
    for (Purchase_Order__c poToCancel : [SELECT Id, Status__c FROM Purchase_Order__c 
                      WHERE Order__c =: theOrder.Id AND  Status__c = 'In Process']) // only cancel PO's that are In Process
    {
      poToCancel.Status__c = 'Cancelled';
      purchaseOrdersToCancel.add(poToCancel);
    }

    // Find any order items linked to the order that haven't been cancelled and cancel them
    for (OrderItem orderItemToCancel : [SELECT Id, Status__c FROM OrderItem 
                      WHERE OrderId =: theOrder.Id AND Status__c != 'Cancelled']) 
    {
      orderItemToCancel.Status__c = 'Cancelled';
      orderItemToCancel.Cancellation_Reason__c = theOrder.Cancellation_Reason__c;
      orderItemsToCancel.add(orderItemToCancel);
    }

    // Find any discounts linked to the order that haven't been cancelled and cancel them
    for (Order_Discount__c discToCancel : [SELECT Id, Status__c FROM Order_Discount__c 
                      WHERE Order__c =: theOrder.Id AND Status__c != 'Cancelled']) 
    {
      discToCancel.Status__c = 'Cancelled';
      discountsToCancel.add(discToCancel);
    }

    // Find any non-FSL WO's linked to the order that haven't been cancelled and aren't Closed
    for(WorkOrder woTocancel : [Select Id,  Status, Work_Order_Type__c, Cancel_Reason__c FROM WorkOrder
                                        WHERE Work_Order_Type__c in ('LSWP','Building Permit','HOA','Historical','Paint/Stain', 'Collections')
                                        AND Sold_Order__c =:theOrder.Id 
                                        AND (Status != 'Cancelled' AND Status != 'Closed' AND Status != 'Canceled' AND Status != 'Appt Complete / Closed' 
                                                AND Status != 'Appt Complete/Closed' AND Status != 'Complete')]){
        woToCancel.Status = 'Canceled';
        if (woToCancel.Cancel_Reason__c == null) woToCancel.Cancel_Reason__c = 'Order Cancelled';
        workOrdersToCancel.add(woTocancel);
    }
    // Find Opportunity associated to the Order and Update the Canceld Order Checkbox.
            for(Opportunity OppToCancel : [Select id, Canceled_Order__c from Opportunity where id = :theOrder.OpportunityId])
            {
               if(OppToCancel.Canceled_Order__c != TRUE)
              OppToCancel.Canceled_Order__c  = TRUE;
              OpportunitiesToCancel.add(OppToCancel); 
            }
           

    try{
      // Prevent rollups/rolldowns on the FSL objects from running prior the last DML
      FSLRollupRolldownController.disableFSLRollupRolldownController = true;

      // upsert all of the cancelled order items, pos, and the order
      upsert purchaseOrdersToCancel;  
      upsert discountsToCancel; 
      upsert orderItemsToCancel;
      Upsert OpportunitiesToCancel;
      update workOrdersToCancel; 

      // Allow rollups/rolldowns to run at this point
      FSLRollupRolldownController.disableFSLRollupRolldownController = false;
      upsert theOrder;
    } catch (Exception e){
      // TODO: Add comment here
      System.debug('************The following exception occurred in the RMS_cancelOrderExtension in the cancelOrder method:' + e);
      return null;
    }

    // Redirect the user back to the order page
    PageReference pageRef = new PageReference('/' + theId);
    pageRef.setRedirect(true);
    return pageRef;

  }
}