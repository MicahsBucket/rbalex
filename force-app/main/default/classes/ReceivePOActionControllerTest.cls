/*
* @author Jason Flippen
* @date 02/28/2020 
* @description Test Class provides code coverage for the following classes:
*               - ReceivePOActionController
*/
@isTest
public class ReceivePOActionControllerTest {

    /*
    * @author Jason Flippen
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @returns N/A
    */ 
    @testSetup static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();

        List<Account> testAccountList = new List<Account>();

        Account testStoreAccount = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];

        Account testAccount01 = testUtility.createVendorAccount('Test Vendor Account');
        testAccount01.Name = 'Test Vendor Account';
        testAccount01.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testAccount01);

/*
        Account testAccount02 = testUtility.createVendorAccount( 'Renewal by Andersen');
        testAccount02.Name = 'Renewal by Andersen';
        testAccountList.add(testAccount02);

        Account testAccount03 = testUtility.createVendorAccount('0077 - Twin Cities');
        testAccount03.name = '0077 - Twin Cities';
        testAccountList.add(testAccount03);

        Account testAccount04 = testUtility.createVendorAccount('Andersen');
        testAccount04.name = 'Andersen Logistics - Minneapolis';
        testAccountList.add(testAccount04);
*/

        Account testDwellingAccount = testUtility.createDwellingAccount('Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        testAccountList.add(testDwellingAccount);
       
        insert testAccountList;

        Contact testContact = testUtility.createContact(testAccount01.Id, 'BillToTest');
        insert testContact;

/*
        Account testStoreAcct = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;
*/
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
    
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO');
        insert testFAN;

        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Service_Product').getRecordTypeId(),
                                            Vendor__c = testAccount01.Id,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;

        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;

        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Service').getRecordTypeId(),
                                     AccountId = testDwellingAccount.Id,
                                     EffectiveDate = Date.Today(),
                                     Store_Location__c = testStoreAccount.Id,
                                     OpportunityId = testOpportunity.Id,                                 
                                     Status = 'Draft',
                                     Tech_Measure_Status__c = 'New',
                                     Pricebook2Id = Test.getStandardPricebookId());
        insert testOrder;
        
        Purchase_Order__c testPurchaseOrder = new Purchase_Order__c(Name = 'ABCD12345',
                                                                    Estimated_Ship_Date__c = Date.today(),
                                                                    Order__c = testOrder.Id,
                                                                    RecordTypeId = Schema.SObjectType.Purchase_Order__c.getRecordTypeInfosByDeveloperName().get('Service_Purchase_Order').getRecordTypeId(),
                                                                    Status__c = 'Confirmed',
                                                                    Tax__c = 2.00,
                                                                    Vendor__c = testAccount01.Id,
                                                                    Store_Location__c = testStoreAccount.Id);
        insert testPurchaseOrder;

        OrderItem testOI = new OrderItem(OrderId = testOrder.Id,
                                         Has_PO__c = true,
                                         Purchase_Order__c = testPurchaseOrder.Id,
                                         PricebookentryId = testPBEStandard.Id,
                                         Quantity = 2,
                                         Quanity_Ordered__c = 2,
                                         Unit_Wholesale_Cost__c = 50.00,
                                         UnitPrice = 100,
                                         Variant_Number__c = 'ABCD1234');
        insert testOI;

        Charge__c testCharge = new Charge__c(Service_Request__c = testOrder.Id,
                                             Service_Product__c = testOI.Id);
        insert testCharge;

    }

    /*
    * @author Jason Flippen
    * @description Method to test the functionality in the Controller.
    * @param N/A
    * @returns N/A
    */ 
    private static testMethod void testController() {

        Order testOrder = [SELECT Id, Status FROM Order WHERE Name = 'Test Order'];
        Purchase_Order__c testPurchaseOrder = [SELECT Id FROM Purchase_Order__c WHERE Order__c = :testOrder.Id];

        Test.startTest();

            // Retrieve Purchase Order record. It should match the one we created for testing.
            ReceivePOActionController.PurchaseOrderWrapper purchaseOrder = ReceivePOActionController.getPurchaseOrderData(testPurchaseOrder.Id);
            System.assertEquals(testPurchaseOrder.Id, purchaseOrder.id);

            // The Status of the Purchase Order should now be set to "Partially Received".
            purchaseOrder.estimatedShipDate = Date.today();
            purchaseOrder.productList[0].quantityToReceive = 1;
            ReceivePOActionController.receivePurchaseOrder(purchaseOrder);
            Purchase_Order__c updatedPurchaseOrder = [SELECT Id, Status__c FROM Purchase_Order__c WHERE Id = :testPurchaseOrder.Id];
            System.assertEquals('Partially Received', updatedPurchaseOrder.Status__c);

        Test.stopTest();

    }

}