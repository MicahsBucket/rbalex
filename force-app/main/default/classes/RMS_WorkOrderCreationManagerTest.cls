@isTest
public without sharing class RMS_WorkOrderCreationManagerTest{
    private static final String DWELLING_ZIP = '53202';
    private static final String OPPORTUNITY1_NAME = 'Opp1';
    private static final String OPPORTUNITY2_NAME = 'Opp2';
    private static final String OPPORTUNITY3_NAME = 'Opp3';

    @testSetup
    static void testSetup(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
    
        Id municipalityPermitRT = UtilityMethods.retrieveRecordTypeId('Permit', 'Municipality__c');
        Id municipalityHoaRT = UtilityMethods.retrieveRecordTypeId('HOA', 'Municipality__c');
        Id municipalityHistoricalRT = UtilityMethods.retrieveRecordTypeId('Historical', 'Municipality__c');
        Municipality__c permitMunicipality = utility.createMunicipality(null, municipalityPermitRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c hoaMunicipality = utility.createMunicipality(null, municipalityHoaRT, '55555', 'Blaine', 'Anoka', 'MN');
        Municipality__c historicalMunicipality = utility.createMunicipality(null, municipalityHistoricalRT, '55555', 'Blaine', 'Anoka', 'MN');
        insert new List<Municipality__c> { permitMunicipality, hoaMunicipality, historicalMunicipality };

        Account store = [SELECT id FROM Account WHERE Name ='77 - Twin Cities, MN'];

        Id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account newDwelling = new Account(
            Name=DWELLING_ZIP,
            Store_Location__c = store.id,
            RecordTypeId = dwellingRT,
            ShippingStreet = '200 E Wells St 1',
            ShippingCity = 'Milwaukee',
            ShippingStateCode = 'WI',
            ShippingState = 'Wisconsin',
            ShippingCountryCode = 'US',
            ShippingCountry = 'United States',
            ShippingPostalCode =  '53202',
            HOA__c = hoaMunicipality.Id,
            Historical__c = historicalMunicipality.Id,
            Building_Permit__c = permitMunicipality.Id,
            Year_Built__c = '1972' // before 1978 requires LSWP work order
        );
        insert newDwelling;

        Opportunity opp1 = new Opportunity(Name=OPPORTUNITY1_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addYears(1) );
        Opportunity opp2 = new Opportunity(Name=OPPORTUNITY2_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addMonths(6) );
        Opportunity opp3 = new Opportunity(Name=OPPORTUNITY3_NAME, AccountId=newDwelling.id, StageName = 'New', CloseDate = Date.today().addMonths(9) );
        insert new List<Opportunity> { opp1, opp2, opp3 };
    }
    public static testMethod void testSO(){
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Id orderCoroServiceRT = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id workOrderTechMeasureRT = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        Opportunity opp2 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY2_NAME];
        Opportunity opp3 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY3_NAME];
    
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
           Service_Type__c = 'Service',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            OpportunityId = opp1.Id
        );
        insert order1;
        
        Order order3 = new Order(
            RecordTypeId = orderCoroServiceRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp3.Id
        );
        insert order3;
        
        Test.startTest();
          order3.Sold_Order__c = order1.Id;
          order1.Status = 'Tech Measure Scheduled';
          order1.Apex_Context__c = True;
          update order3;
          update order1;
        Test.stopTest();
    }
    @isTest
    static void createWorkOrderOnOrderCreationTest() {
        Test.startTest();
        // Given
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Id orderCoroServiceRT = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id workOrderTechMeasureRT = UtilityMethods.retrieveRecordTypeId('Tech_Measure', 'WorkOrder');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        Opportunity opp2 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY2_NAME];
        Opportunity opp3 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY3_NAME];
    
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        insert order1;
        
        Order order2 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp2.Id
        );
        insert order2;
        Order order3 = new Order(
            RecordTypeId = orderCoroServiceRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Sold_Order__c = order1.id,
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp3.Id
        );
        insert order3;
        Test.stopTest();
        // Then
        List<WorkOrder> order1WorkOrders = [SELECT Id, RecordTypeId FROM WorkOrder WHERE Sold_Order__c = :order1.Id];
        List<WorkOrder> order2WorkOrders = [SELECT Id, RecordTypeId FROM WorkOrder WHERE Sold_Order__c = :order2.Id];
        List<WorkOrder> order3WorkOrders = [SELECT Id, RecordTypeId FROM WorkOrder WHERE Sold_Order__c = :order3.Id];

        System.assertEquals(1, order1WorkOrders.size());
        System.assertEquals(workOrderTechMeasureRT, order1WorkOrders[0].RecordTypeId);
        System.assertEquals(0, order3WorkOrders.size());
    }
    public static testMethod void test1(){
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        insert order1;
    }
    public static testMethod void test2(){
        Id orderCoroServiceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        insert order1;
        
        Order order2 = new Order(
            RecordTypeId = orderCoroServiceRecordTypeId,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        insert order2;
        Order order3 = new Order(
            RecordTypeId = orderCoroServiceRecordTypeId,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id,
            Sold_Order__c = order1.Id
        );
        insert order3;
        Map<Id,Order> mapOld = new Map<Id,Order>{order2.Id=>order2};
        Map<Id,Order> mapNew = new Map<Id,Order>{order2.Id=>order3};
    }
    public static testMethod void test4(){
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = dwelling.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        insert order1;
    }
    public static testMethod void test5(){
        Id orderAroRT = UtilityMethods.retrieveRecordTypeId('ARO_Record_Type', 'Order');
        Id installRT = UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder');
        Account dwelling = [SELECT Id, Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Name LIKE :(DWELLING_ZIP + '%')];
        Opportunity opp1 = [SELECT Id, AccountId FROM Opportunity WHERE Name = :OPPORTUNITY1_NAME];
        
        TestUtilityMethods utility = new TestUtilityMethods();
        Account myStore = utility.createStoreAccount('mystore');
    insert myStore;
        
        Store_Configuration__c myStoreConfig = new Store_Configuration__c(Store__c = myStore.Id, Order_Number__c = 1, Contractor_License_1__c = '12345', Contractor_License_2__c = '23456', Contractor_License_1_Expiration__c = Date.newInstance(2018,1,2), Contractor_License_2_Expiration__c = Date.newInstance(2018,3,4));
        insert myStoreConfig;
        
        Account objClone = dwelling.clone(false,true,false,false);
        objClone.Active_Store_Configuration__c = myStoreConfig.Id;
        objClone.RecordTypeId = UtilityMethods.retrieveRecordTypeId('Store', 'Account');
        insert objClone;
        
        Account acc = dwelling.clone(false,true,false,false);
        acc.Store_Location__c = objClone.Id;
        insert acc;
        
        Order order1 = new Order(
            RecordTypeId = orderAroRT,
            AccountId = acc.id,
            Status = 'Draft',
            EffectiveDate = Date.Today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            Service_Type__c = 'Service',
            OpportunityId = opp1.Id
        );
        try{
            insert order1;
        }
        catch(Exception obj){
            
        }
        
        ServiceResource objServRes = new ServiceResource(
                                        Name = UserInfo.getLastName(),
                                        ResourceType = 'T',
                                        RelatedRecordId = UserInfo.getUserId(),
                                        Retail_Location__c = myStoreConfig.Id,
                                        IsActive = true,
                                        User_Type__c = 'External'
                                    );
        insert objServRes;
        
        WorkOrder wo = new WorkOrder();
        wo.Sold_Order__c = order1.Id;
        wo.AccountId = order1.AccountId;
        wo.RecordTypeId = installRT;
        wo.Primary_Installer_FSL__c = objServRes.Id;
        wo.Work_Order_Type__c = 'Tech Measure';
        insert wo;
        
        WorkOrder wo1 = new WorkOrder();
        wo1.Sold_Order__c = order1.Id;
        wo1.AccountId = order1.AccountId;
        wo1.RecordTypeId = installRT;
        wo1.Status = 'Appt Complete / Closed';
        wo1.Primary_Installer_FSL__c = objServRes.Id;
        insert wo1;
        
        UtilityMethods.unsetWOTriggerRan();
        try{
          RMS_WorkOrderCreationManager.createInstallWorkOrderOnTechMeasureComplete(new Map<Id,WorkOrder>{wo.Id=>wo},new Map<Id,WorkOrder>{wo.Id=>wo1});
        }
        catch(Exception obj){
            
        }
    }
}