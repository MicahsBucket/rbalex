public class CanvassUnitBatchPage 
{
    public CanvassUnitBatchPage()
    {
        
    }
    
    @RemoteAction
    public static List<Canvass_Market_Zip_Code__c> getPickListValues()
    {
        list<SObject> marketPickList = new list<SObject>();
        for(SObject market :[SELECT Id, Name FROM CNVSS_Canvass_Market__c ORDER BY Name])
        {
            marketPickList.add(market);
        }
        return marketPickList;
    }
    
    @RemoteAction
    public static string runBatch(string marketId)
    {
        if(marketId != 'Select Market')
        {
            if(marketId != null)
            {
                if (!Test.isRunningTest())
                {
                    database.executebatch(new CanvassMarketBatch(marketId),1);
                }
                return 'Batch Started';
            }
            else
            {
                return 'Error: Market Not Found';
            }
        }
        else
        {
            return 'Select Market';
        }
    }
}