public class RMS_cancelWOManager {
    
    public static void closeEvent(List<WorkOrder> listOld, List<WorkOrder> listNew, Map<Id, WorkOrder> mapOld, Map<Id, WorkOrder> mapNew){
        
        List<Id> relatedWO = new list<Id>();
        Id woRecordType = UtilityMethods.RecordTypeFor('WorkOrder','Install');
        
        for(WorkOrder wo : listNew){
            if((mapNew.get(wo.Id).Status != mapOld.get(wo.Id).Status) && mapNew.get(wo.Id).Status == 'Canceled'){
                if(wo.RecordTypeId == woRecordType){
                    relatedWO.add(wo.Id);
                }
            }
        }
        
        List<Event> childRecords = [Select Id, Open_Event__c, WhatId                                    
                                    FROM Event 
                                    WHERE WhatId 
                                    IN :relatedWO 
                                    AND Open_Event__c = true];
        
        if(childRecords.size() > 0){
        for (Event ev : childRecords ) {
                ev.WhatId = null;
                ev.OwnerId = UserInfo.getUserId();
            }
            
            update childRecords;
            delete childRecords;
        }    
    }
}