/*//	created by Mark Rothermal Aim - 8/21/18
 * 		purpose - a generic wrapper for better error handling/consistent responses in lightning components
 * 		original author - James Loghry aka dancinllama 
//
*///
public class LightningResponse {

	@AuraEnabled public String jsonResponse {get; set;}
    @AuraEnabled public String error {get; set;}
    @AuraEnabled public String state {get; set;}

    public LightningResponse() {
        this.state = 'SUCCESS';
    }

    public LightningResponse(Exception e){
    	this();
    	if(e != null){
    		this.state = 'ERROR';
            this.error = e.getMessage();
        }
    }
}