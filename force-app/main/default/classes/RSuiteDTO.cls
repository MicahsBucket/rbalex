/*
 *@class  RSuiteDTO
 *@brief  DTO class container for transfering data to/from rSuite
 *@author  Mark Wochnick (Slalom.MAW)
 *@version  2017-04-12  Slalom.MAW
 *@copyright  (c)2017 Slalom/Renewal by Andersen.  All Rights Reserved.  Unauthorized use is prohibited.
 */

public with sharing class RSuiteDTO {

  public static final String genericDiscountName = '$ off the Project';
  public static final String FULL_FRAME = 'Full Frame';
  public static final String INSERT_FRAME = 'Insert Frame';
  public static final String L_TRIM = 'L Trim';
  public static Map<String, String> hardwareColorMap = getHardwareColorMap();

  public static Map<String, String> getHardwareColorMap(){
    Product_Config_Settings__c pcs = Product_Config_Settings__c.getInstance();
    if(pcs == null || pcs.Hardware_Color_Mappings__c == null)
      return null;
    Map<String, String> hardwareColorMap = new Map<String, String>();
    for(String pair : pcs.Hardware_Color_Mappings__c.split(';')){
      List<String> colorPair = pair.split(',');
      hardwareColorMap.put(colorPair[0], colorPair[1]);
    }
    return hardwareColorMap;
  }

  public class Appointments {
    public List<SalesAppointment> salesAppts;
    public List<TechMeasureAppointment> tmAppts;
    public Datetime getApptsCompletedTimeStamp;
    public String[] errors;
    public String[] getErrors() { 
      if (errors == null) { 
        errors = new String[]{};
      } 
      return errors;
    }
  }

  public abstract class Appointment{
    public transient XMLFactory.DTO accountDTO = null;
    public transient List<XMLFactory.DTO> contactDTOs = new List<XMLFactory.DTO>();
    public Event event {get; set;}
    public OpportunityContainer opptyContainer {get; set;}
    public Account account {get; set;}
    public List<Contact> contactList {get; set;}
    public List<OpportunityContainer> historicOpptyContainerList {get; set;}
    public String[] errors;
  public Boolean isSaveSuccessful = true;
    public String[] getErrors() { 
      if (errors == null) { 
        errors = new String[]{};
      } 
      return errors;
    }
  public void clear() {
    event = null;
    account = null;
    historicOpptyContainerList = null;
    contactList = null;
    if (opptyContainer != null) {
      opptyContainer.anOrderContainer = null;
      opptyContainer.quoteContainerList = null;
    }

  }
  public void printNode(DOM.XMLNode node) {
    Dom.XMLNode parentNode = node.getParent();
    String parentName = null;
    if (parentNode != null) {
      parentName = parentNode.getName();
    }
    System.debug('######: node: ' + node);
    System.debug('prn######: name: ' + node.getName() + ',::Parent:' + parentName + ',::nodeType:' + node.getNodeType() 
      + ',::text:' + node.getText() + ',::attributeCount:' + node.getAttributeCount());
    if (node.getNodeType() == Dom.XMLNodeType.ELEMENT) {
      if (node.getAttributeCount() > 0) {
        System.debug('$$$$$$$$ Attributes');
        for (Integer i = 0; i < node.getAttributeCount(); i++) {
          String key = node.getAttributeKeyAt(i);
          String keyNameSpace = node.getAttributeKeyNsAt(i);
          String value = node.getAttribute(key, keyNameSpace);
          System.debug('######: key:' + key + ' value:' + value);
        }
        System.debug('$$$$$$$$ Attributes End');
      }
      for (Dom.XmlNode child : node.getChildren()) {
        printNode(child);
      }
    }
  }
  }

  public class SalesAppointment extends Appointment{
    public SalesAppointment() { super(); }
    public SalesAppointment(String xml) {
      super();
    Dom.Document doc = new Dom.Document();
    doc.load(xml);
    Dom.XMLNode xn = doc.getRootElement();
//    printNode(xn);
    // lets attempt to process this dang thing
    Dom.XMLNode dictNode = xn.getChildElement('dict', null);
    Dom.XMLNode[] cNodes = dictNode.getChildren();
    //System.debug('$$$$$$$$$$$$$$$: node count::' + cNodes.size());
    cNodes = XMLFactory.removeNullNodes(cNodes);
    for(Integer i = 0; i < cNodes.size(); i++) {
      System.debug('#######: sales appt processing Node: ' + cNodes[i].getName() + '::' + cNodes[i].getText());
      if (cNodes[i].getName() == Constants.NODE_NAME_KEY) {
        String text = cNodes[i].getText();
        if (text == 'account') {
          // do account processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            accountDTO = XMLFactory.populateSObject(cNodes[i+1], 'Account');
            if(accountDTO != null && accountDTO.sObj != null) {
              account = (Account)accountDTO.sObj;
              //System.debug('##### account-ro: ' + accountDTO.readOnlyFieldMap);
              //System.debug('##### account-err: ' + accountDTO.errorFieldMap);
              //System.debug('##### account-sObj: ' + accountDTO.sObj);
            }
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'contactList') {
          // do contact processing
          System.debug('#######: contact processing');
          // do orderDiscountList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            contactList = new List<Contact>();
            Dom.XmlNode[] contactNodes = cNodes[i+1].getChildren();
            contactNodes = XMLFactory.removeNullNodes(contactNodes);
            //System.debug('#######: contact processing: ' + contactNodes);
            for (integer j = 0; j < contactNodes.size(); j++) {
              XMLFactory.DTO contactDTO = XMLFactory.populateSObject(contactNodes[j], 'Contact');
              if (contactDTO != null && contactDTO.sObj != null) {
                contactList.add((Contact)contactDTO.sObj);
                contactDTOs.add(contactDTO);
                //System.debug('##### contact-ro: ' + contactDTO.readOnlyFieldMap);
                //System.debug('##### contact-err: ' + contactDTO.errorFieldMap);
                //System.debug('##### contact-sObj: ' + contactDTO.sObj);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'opptyContainer') {
          // do oppty processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            opptyContainer = new OpportunityContainer(cNodes[i+1]);
            // skip the next node in the loop since we already processed it
            i++;
          }
        }
      }
    }
    }

  public void saClear() {
    super.clear();
  }

  public void linkToSaleforceRecords(Id opptyId) {
    // go get salesforce information for the oppotunity
    Opportunity opp = [select id, Pricebook2Id, Store_Location__c, Store_Location__r.Active_Store_Configuration__c from Opportunity where Id = :opptyId limit 1];
    if (opp != null) {
      // get Pricebook Entries
      List<PricebookEntry> childPBEList = [select Id, Product2Id, Pricebook2Id, Cross_Reference__c,
        Product2.Name, Product2.Master_Product__c, Product2.Master_Product__r.Master_Product__c, 
        Product2.rSuite_Product_Type__c,
        Product2.Master_Product__r.rSuite_Product_Type__c,
        Product2.Master_Product__r.Master_Product__r.rSuite_Product_Type__c
        from PricebookEntry
        where (Product2.RecordType.DeveloperName = :Constants.CHILD_PRODUCT_RECORD_TYPE_NAME or Product2.RecordType.DeveloperName = :Constants.MULLION_PRODUCT_RECORD_TYPE_NAME) 
        and Pricebook2Id = :opp.Pricebook2Id
        and IsActive = true];
      
      List<PricebookEntry> miscChargePBEList = [select Id, Product2Id, Pricebook2Id, Product2.Name, Product2.rSuite_Product_Type__c, Product2.Category__c, 
        Product2.Sub_Category__c, Cross_Reference__c, Product2.Master_Product__c
        from PricebookEntry
        where (Product2.RecordType.DeveloperName = :Constants.MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME or 
        Product2.RecordType.DeveloperName = :Constants.CONSTRUCTION_MATERIALS_PRODUCT_RECORD_TYPE_NAME)
        and Pricebook2Id = :opp.Pricebook2Id
        and IsActive = true];

      // Get master products assoicated with misc products
      List<Product2> miscProductsWithMaster = [select Id, Name, Master_Product__c from Product2
        where (RecordType.DeveloperName = :Constants.MISC_JOB_AND_UNIT_CHARGES_PRODUCT_RECORD_TYPE_NAME or 
        RecordType.DeveloperName = :Constants.CONSTRUCTION_MATERIALS_PRODUCT_RECORD_TYPE_NAME) 
        and Master_Product__c != null];

      Set<Id> miscMasterProdList = new Set<Id>();
      for(Product2 prod : miscProductsWithMaster){
        miscMasterProdList.add(prod.Master_Product__c);
      }

      Map<Id, PricebookEntry> masterProdIdToPBE = new Map<Id, PricebookEntry>();
      // Get PBEs for Master products associated with misc
      for( PricebookEntry pbe : [select Id, Product2Id, Pricebook2Id, Product2.Name, Product2.rSuite_Product_Type__c, Product2.Category__c, 
        Product2.Sub_Category__c, Cross_Reference__c
        from PricebookEntry
        where Product2.RecordType.DeveloperName = :Constants.MASTER_PRODUCT_RECORD_TYPE_NAME 
        and Pricebook2Id = :opp.Pricebook2Id
        and IsActive = true
        and Product2Id in :miscMasterProdList]){

        masterProdIdToPBE.put(pbe.Product2Id, pbe);

      }

      Set<Id> productHasHardwareStyle = new Set<Id>();
      for(Product_Configuration__c pc : [select id, Product__c from Product_Configuration__c 
        where Hardware_Style_Control__c != 'Disabled']){
        productHasHardwareStyle.add(pc.Product__c);
      }

      opptyContainer.oppty.Id = opptyId;
      opptyContainer.oppty.Pricebook2Id = opp.Pricebook2Id;

      // get list of discounts for this store
      List<Store_Discount__c> sdList = [select Id, Name from Store_Discount__c where Active__c = true 
        and Store_Configuration__c = :opp.Store_Location__r.Active_Store_Configuration__c];

      Store_Discount__c genericDiscount = null;
      // Also determine the generic discount
      for(Store_Discount__c sd : sdList){
        if(sd.Name == genericDiscountName){
          genericDiscount = sd;
          break;
        }
      }
      
      // update the quotes
      if (opptyContainer.QuoteContainerList != null) {
        for (QuoteContainer qc : opptyContainer.QuoteContainerList) {
          if (qc.aQuote != null) {
            qc.aQuote.OpportunityId = opptyId;
            qc.aQuote.Pricebook2Id = opp.Pricebook2Id;
            if (qc.quoteLineItems != null) {
              for (Integer i = 0; i < qc.QuoteLineItems.size(); i++) {
                Set<Id> productIdSet = new Set<Id>();
                QuoteLineItem qli = qc.QuoteLineItems[i];
                String productType = '';
                Boolean matchFound = false;
                // need to set the pricebook entry id to null it should not be set at this point
                // the xml has it incorrectly in the data
                qli.PricebookEntryId = null;
                // match the quote line item to the pricebook entry
                if (qli.Cross_Reference__c != null) {
                  // process the master/child product (windows/doors/mullions)
                  for(PricebookEntry pbe : childPBEList) {
                    if (qli.Cross_Reference__c == pbe.Cross_Reference__c) {
                      qli.Child_Product_Pricebook_Entry_Id__c = pbe.Id;
                      if(pbe.Product2Id != null){
                        productIdSet.add(pbe.Product2Id);
                        productType = pbe.Product2.rSuite_Product_Type__c;
                      }
                      else if(pbe.Product2.Master_Product__c != null){
                        productIdSet.add(pbe.Product2.Master_Product__c);
                        productType = pbe.Product2.Master_Product__r.rSuite_Product_Type__c;
                      }
                      else if(pbe.Product2.Master_Product__r.Master_Product__c != null){
                        productIdSet.add(pbe.Product2.Master_Product__r.Master_Product__c);
                        productType = pbe.Product2.Master_Product__r.Master_Product__r.rSuite_Product_Type__c;
                      }
                      matchFound = true;
                      break;
                    }
                  }

                } else if (qli.Description != null) {

                  for(Product2 prod : miscProductsWithMaster){
                    if(prod.Name == qli.Description && masterProdIdToPBE.containsKey(prod.Master_Product__c)){
                      PricebookEntry masterPBE = masterProdIdToPBE.get(prod.Master_Product__c);
                      qli.Child_Product_Pricebook_Entry_Id__c = null;
                      qli.PricebookEntryId = masterPBE.Id;
                      productIdSet.add(masterPBE.Product2Id);
                      productType = masterPBE.Product2.rSuite_Product_Type__c;
                      matchFound = true;
                      break;
                    }
                  }

                  if(!matchFound){
                    // process other types of records by using the description field
                    for (PricebookEntry pbe: miscChargePBEList) {
                      if (pbe.Product2.Name == qli.Description) {
                        qli.Child_Product_Pricebook_Entry_Id__c = null;
                        qli.PricebookEntryId = pbe.Id;
                        productIdSet.add(pbe.Product2Id);
                        productType = pbe.Product2.rSuite_Product_Type__c;
                        matchFound = true;
                        break;
                      }
                    }
                  }

                }

                if (!matchFound) {

                  for(Product2 prod : miscProductsWithMaster){
                    if(prod.Name == qli.Child_Product_Pricebook_Entry_Id__c 
                      && masterProdIdToPBE.containsKey(prod.Master_Product__c)){
                      PricebookEntry masterPBE = masterProdIdToPBE.get(prod.Master_Product__c);
                      qli.Child_Product_Pricebook_Entry_Id__c = null;
                      qli.PricebookEntryId = masterPBE.Id;
                      productIdSet.add(masterPBE.Product2Id);
                      productType = masterPBE.Product2.rSuite_Product_Type__c;
                      matchFound = true;
                      break;
                    }
                  }

                  if(!matchFound){
                    // try some other methods - only works for misc items
                    for (PricebookEntry pbe: miscChargePBEList) {
                      if (pbe.Product2.Name == qli.Child_Product_Pricebook_Entry_Id__c) {
                        qli.Child_Product_Pricebook_Entry_Id__c = null;
                        qli.PricebookEntryId = pbe.Id;
                        productIdSet.add(pbe.Product2Id);
                        productType = pbe.Product2.rSuite_Product_Type__c;
                        matchFound = true;
                        break;
                      }
                    }
                  }

                }
                if (!matchFound) {
                  // no luck - no matching product found
                  // add the qli as a message and remove it from the list to insert
                  
                  for (XMLFactory.DTO dto : qc.qliDTOs) {
                    QuoteLineItem dtoqli = (QuoteLineItem)dto.sObj;
                    if (dtoqli != null) {
                      if (qli.rSuite_Id__c == dtoqli.rSuite_Id__c) {
                          String errorMessage = 'Product with Id: '+qli.Cross_Reference__c  +' and Description: ' +qli.Description+
                          ' is present within in the .rpaProj file but no such product exists in rForce.';
                         dto.errors.add(errorMessage);
                        // remove the qli from the list
                        qc.QuoteLineItems.remove(i);
                        i--;
                        break;
                      }
                    }
                  }
                }

                if(!productIdSet.isEmpty()){
                  for(Id productId : productIdSet){
                    if(productHasHardwareStyle.contains(productId)){
                      qli.Hardware_Style__c = 'Standard';
                      break;
                    }
                  }
                }

                // Check for blank fields on Window products, update as needed
                if(productType != null && productType == 'Window'){
                  // Check Frame Type, if blank, use EJ Frame value to determine
                  if(qli.Frame_Type__c == null){
                    if(qli.EJ_Frame__c == true)
                      qli.Frame_Type__c = FULL_FRAME;
                    else
                      qli.Frame_Type__c = INSERT_FRAME;
                  }
                
									 /*
                Populate values for Lift Pulls
                1R -> Recessed hand lift
                2R -> Extra recessed hand lift
                1 -> Estate finish hand Lift
                2 ->Extra estate finish hand lift
                0 -> no selection
                */
                
                  if (qli.Estate_Finish_Hand_Lift__c || qli.Standard_Color_Hand_Lift__c){
                    qli.Lifts_Pulls__c = '1';
                  } if (qli.Estate_Finish_Extra_Hand_Lift__c || qli.Standard_Color_Extra_Hand_Lift__c){
                    qli.Lifts_Pulls__c = '2';
                  } if (qli.Estate_Finish_Recessed_Hand_Lift__c || qli.Standard_Color_Recessed_Hand_Lift__c){
                    qli.Lifts_Pulls__c = '1R';
                  } if (qli.Estate_Finish_Extra_Recessed_Hand_Lift__c || qli.Standard_Color_Extra_Recessed_Hand_Lift__c){
                    qli.Lifts_Pulls__c = '2R';
                  } if(qli.Lifts_Pulls__c == null){
                      qli.Lifts_Pulls__c = '0';
                  }
                }

                if(productType != null && productType == 'Specialty'){
                  if(qli.Fibrex_L_Trim__c == true){
                    qli.Exterior_Trim__c = L_TRIM;
                  }
                }

              }
            } // end quoteLineItems
            if (qc.quoteDiscountList != null) {

              for (Integer i = 0; i < qc.quoteDiscountList.size(); i++) {
                Quote_Discount__c qd = qc.quoteDiscountList[i];
                Boolean matchFound = false;

                for(Store_Discount__c sd : sdList){
                  if(qd.Pivotal_Id__c == sd.Name){
                    qd.Store_Discount__c = sd.Id;
                    matchFound = true;
                    break;
                  }
                }
                
                if(!matchFound){
                  // Associate with a generic discount
                  if(genericDiscount != null)
                    qd.Store_Discount__c = genericDiscount.Id;
                  else{
                    qc.quoteDiscountList.remove(i);
                    i--;
                  }
                }

              }

            }
            if (qc.quoteFinancingList != null) {
              for (Quote_Financing__c qf: qc.quoteFinancingList) {
                // TODO: Match on financing
                // match the quote financing to the store finance programdiscount
                // make sure to implement logic for records not fount
              }
            }
          }
        }
      }
      // update the orders
      if (opptyContainer.anOrderContainer != null) {
        if (opptyContainer.anOrderContainer.anOrder != null) {
          opptyContainer.anOrderContainer.anOrder.OpportunityId = opp.Id;
          opptyContainer.anOrderContainer.anOrder.Pricebook2Id = opp.Pricebook2Id;
          opptyContainer.anOrderContainer.anOrder.Store_Location__c = opp.Store_Location__c;
          if (opptyContainer.anOrderContainer.oiContainerList != null) {
            for (OrderItemContainer oic : opptyContainer.anOrderContainer.oiContainerList) {
              // TODO: match the order item to the pricebook entry
              // this is not being used at the moment generating order from quote instead
            }
          }
        }
      }
    } else {
      // blow up with an error
    }
  }
  }
    
  public class TechMeasureAppointment extends Appointment{
    public WorkOrder workOrder {get; set;}
    public TechMeasureAppointment() { super(); }
  public void tmClear() {
    super.clear();
    workOrder = null;
  }
  }

  public class OpportunityContainer {
    public OpportunityContainer(){}
    public OpportunityContainer(Dom.XmlNode ocNode) {
      // process the opportunity dict node and sub nodes
    //System.debug('%%%%%%%: oppty processing');
    Dom.XMLNode[] cNodes = ocNode.getChildren();
    cNodes = XMLFactory.removeNullNodes(cNodes);
    for(Integer i = 0; i < cNodes.size(); i++) {
      System.debug('#######: oppty processing Node: ' + cNodes[i].getName() + '::' + cNodes[i].getText());
      if (cNodes[i].getName() == Constants.NODE_NAME_KEY) {
        String text = cNodes[i].getText();
        if (text == 'anOrderContainer') {
          // do order container processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            anOrderContainer = new OrderContainer(cNodes[i+1]);
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'oppty') {
          // do oppty processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            opptyDTO = XMLFactory.populateSObject(cNodes[i+1], 'Opportunity');
            if(opptyDTO != null && opptyDTO.sObj != null) {
              oppty = (Opportunity)opptyDTO.sObj;
              //System.debug('##### oppty-ro: ' + opptyDTO.readOnlyFieldMap);
              //System.debug('##### oppty-err: ' + opptyDTO.errorFieldMap);
              //System.debug('##### oppty-sObj: ' + opptyDTO.sObj);
            }
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'quoteContainerList') {
          // do quote container list processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            quoteContainerList = new List<QuoteContainer>();
            Dom.XmlNode[] qcNodes = cNodes[i+1].getChildren();
            qcNodes = XMLFactory.removeNullNodes(qcNodes);
            for (integer j = 0; j < qcNodes.size(); j++) {
              QuoteContainer qc = new QuoteContainer(qcNodes[j]);
              quoteContainerList.add(qc);
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        }
      }      
    }
    }
    public Opportunity oppty {get; set;}
    public OrderContainer anOrderContainer {get; set;}
    public List<QuoteContainer> quoteContainerList {get; set;}
    public transient XMLFactory.DTO opptyDTO = null;
  }

  public class OrderContainer {
    public OrderContainer(){}
    public OrderContainer(Dom.XmlNode ocNode) {
      // process the ordercontainer node and sub nodes
    Dom.XMLNode[] cNodes = ocNode.getChildren();
    cNodes = XMLFactory.removeNullNodes(cNodes);
    for(Integer i = 0; i < cNodes.size(); i++) {
      System.debug('#######: order processing Node: ' + cNodes[i].getName() + '::' + cNodes[i].getText());
      if (cNodes[i].getName() == Constants.NODE_NAME_KEY) {
        String text = cNodes[i].getText();
        if (text == 'anOrder') {
          // do order container processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            orderDTO = XMLFactory.populateSObject(cNodes[i+1], 'Order');
            if(orderDTO != null && orderDTO.sObj != null) {
              anOrder = (Order)orderDTO.sObj;
              //System.debug('##### order-ro: ' + orderDTO.readOnlyFieldMap);
              //System.debug('##### order-err: ' + orderDTO.errorFieldMap);
              //System.debug('##### order-sObj: ' + orderDTO.sObj);
            }
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'oiContainerList') {
          // do oiContainerList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            oiContainerList = new List<OrderItemContainer>();
            Dom.XmlNode[] oicNodes = cNodes[i+1].getChildren();
            oicNodes = XMLFactory.removeNullNodes(oicNodes);
            for (integer j = 0; j < oicNodes.size(); j++) {
              OrderItemContainer oic = new OrderItemContainer(oicNodes[j]);
              oiContainerList.add(oic);
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'orderDiscountList') {
          // do orderDiscountList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            orderDiscountList = new List<Order_Discount__c>();
            Dom.XmlNode[] odNodes = cNodes[i+1].getChildren();
            odNodes = XMLFactory.removeNullNodes(odNodes);
            for (integer j = 0; j < odNodes.size(); j++) {
              XMLFactory.DTO odDTO = XMLFactory.populateSObject(odNodes[j], 'Order_Discount__c');
              if (odDTO != null && odDTO.sObj != null) {

                orderDiscountList.add((Order_Discount__c)odDTO.sObj);
                odDTOs.add(odDTO);

                //System.debug('##### od-ro: ' + odDTO.readOnlyFieldMap);
                //System.debug('##### od-err: ' + odDTO.errorFieldMap);
                //System.debug('##### od-sObj: ' + odDTO.sObj);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'orderFinancingList') {
          // do orderFinancingList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            OrderFinancingList = new List<Order_Financing__c>();
            Dom.XmlNode[] ofNodes = cNodes[i+1].getChildren();
            ofNodes = XMLFactory.removeNullNodes(ofNodes);
            for (integer j = 0; j < ofNodes.size(); j++) {
              XMLFactory.DTO ofDTO = XMLFactory.populateSObject(ofNodes[j], 'Order_Financing__c');
              if (ofDTO != null && ofDTO.sObj != null) {
                orderFinancingList.add((Order_Financing__c)ofDTO.sObj);
                ofDTOs.add(ofDTO);
                //System.debug('##### of-ro: ' + ofDTO.readOnlyFieldMap);
                //System.debug('##### of-err: ' + ofDTO.errorFieldMap);
                //System.debug('##### of-sObj: ' + ofDTO.sObj);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        }
      }      
    }
    }
    public Order anOrder {get; set;}
    public List<Order_Discount__c> orderDiscountList {get; set;}
    public List<Order_Financing__c> orderFinancingList {get; set;}
    public List<OrderItemContainer> oiContainerList{get;set;}
    public transient XMLFactory.DTO orderDTO = null;
    public transient List<XMLFactory.DTO> odDTOs = new List<XMLFactory.DTO>();
    public transient List<XMLFactory.DTO> ofDTOs = new List<XMLFactory.DTO>();
  }

  public class OrderItemContainer{
    public OrderItemContainer(){}
    public OrderItemContainer(Dom.XmlNode oicNode) {
      // process the ordercontainer node and sub nodes
    Dom.XMLNode[] cNodes = oicNode.getChildren();
    cNodes = XMLFactory.removeNullNodes(cNodes);
    for(Integer i = 0; i < cNodes.size(); i++) {
      if (cNodes[i].getName() == Constants.NODE_NAME_KEY) {
        String text = cNodes[i].getText();
        if (text == 'oi') {
          // do order itemr processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            oiDTO = XMLFactory.populateSObject(cNodes[i+1], 'OrderItem');
            if(oiDTO != null && oiDTO.sObj != null) {
              oi = (OrderItem)oiDTO.sObj;
              //System.debug('##### orderitem-ro: ' + oiDTO.readOnlyFieldMap);
              //System.debug('##### orderitem-err: ' + oiDTO.errorFieldMap);
              //System.debug('##### orderitem-sObj: ' + oiDTO.sObj);
            }
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'changeHistoryList') {
          // do changeHistoryList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'tmOffSetList') {
          // do tmOffSetList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            // skip the next node in the loop since we already processed it
            i++;
          }
        }
      }      
    }
    }
    public OrderItem oi {get;set;}
    public List<TM_Offset__c> tmOffSetList {get;set;}
    public List<Change_History__c> changeHistoryList {get;set;}
    public transient XMLFactory.DTO oiDTO = null;
  }

  public class QuoteContainer {
    public QuoteContainer() {}
    public QuoteContainer(Dom.XmlNode qcNode) {
      // process the ordercontainer node and sub nodes
    Dom.XMLNode[] cNodes = qcNode.getChildren();
    cNodes = XMLFactory.removeNullNodes(cNodes);
    for(Integer i = 0; i < cNodes.size(); i++) {
      if (cNodes[i].getName() == Constants.NODE_NAME_KEY) {
        String text = cNodes[i].getText();
        if (text == 'aQuote') {
          // do quote processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_DICT) {
            quoteDTO = XMLFactory.populateSObject(cNodes[i+1], 'Quote');
            if(quoteDTO != null && quoteDTO.sObj != null) {

              // If required fields are missing, set defaults
              if(quoteDTO.sObj.get('Name') == null || quoteDTO.sObj.get('Name') == ''){
                quoteDTO.sObj.put('Name', 'Quote [Name was Missing]');
              }  

              aQuote = (Quote)quoteDTO.sObj;
              //System.debug('##### qoute-ro: ' + quoteDTO.readOnlyFieldMap);
              //System.debug('##### quote-err: ' + quoteDTO.errorFieldMap);
              //System.debug('##### quote-sObj: ' + quoteDTO.sObj);
            }
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'quoteDiscountList') {
          // do quoteDiscountList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            quoteDiscountList = new List<Quote_Discount__c>();
            Dom.XmlNode[] qdNodes = cNodes[i+1].getChildren();
            qdNodes = XMLFactory.removeNullNodes(qdNodes);

            for (integer j = 0; j < qdNodes.size(); j++) {
              XMLFactory.DTO qdDTO = XMLFactory.populateSObject(qdNodes[j], 'Quote_Discount__c');
              if (qdDTO != null && qdDTO.sObj != null) {
                Quote_Discount__c qd = (Quote_Discount__c)qdDTO.sObj;
                quoteDiscountList.add(qd);
                qdDTOs.add(qdDTO);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'quoteFinancingList') {
          // do quoteFinancingList processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            quoteFinancingList = new List<Quote_Financing__c>();
            Dom.XmlNode[] qfNodes = cNodes[i+1].getChildren();
            qfNodes = XMLFactory.removeNullNodes(qfNodes);
            for (integer j = 0; j < qfNodes.size(); j++) {
              XMLFactory.DTO qfDTO = XMLFactory.populateSObject(qfNodes[j], 'Quote_Financing__c');
              if (qfDTO != null && qfDTO.sObj != null) {
                //quoteFinancingList.add((Quote_Financing__c)qfDTO.sObj);
                //qfDTOs.add(qfDTO);
                //System.debug('##### qf-ro: ' + qfDTO.readOnlyFieldMap);
                //System.debug('##### qf-err: ' + qfDTO.errorFieldMap);
                //System.debug('##### qf-sObj: ' + qfDTO.sObj);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        } else if (text == 'quoteLineItems') {
          Set<String> hardwareColorSet = hardwareColorMap != null ? 
            new Set<String>(hardwareColorMap.values()) : new Set<String>();
          // do quoteLineItems processing
          if (cNodes[i+1].getName() == Constants.NODE_NAME_ARRAY) {
            quoteLineItems = new List<QuoteLineItem>();
            Dom.XmlNode[] qliNodes = cNodes[i+1].getChildren();
            qliNodes = XMLFactory.removeNullNodes(qliNodes);
            for (integer j = 0; j < qliNodes.size(); j++) {
              XMLFactory.DTO qliDTO = XMLFactory.populateSObject(qliNodes[j], 'QuoteLineItem');
              if (qliDTO != null && qliDTO.sObj != null) {
                // do a field update to support some of the product mapping special cases
                QuoteLineItem qli = (QuoteLineItem)qliDTO.sObj;
                if ((qli.Cross_Reference__c == null || qli.Cross_Reference__c.trim().equals('')) && 
                  (qli.Child_Product_Pricebook_Entry_Id__c == null || qli.Child_Product_Pricebook_Entry_Id__c.trim().equals('')) &&
                  qli.Product_Sub_Category__c != null) {
                  // set the Child_Product_Pricebook_Entry_Id__c field (temporarily)
                  qli.Child_Product_Pricebook_Entry_Id__c = qli.Product_Sub_Category__c;
                }

                // If the description is too long, truncate it
                if(qliDTO.sObj.get('Description') != null){
                  String description = qli.Description;
                  if(description.length() > 254)
                    description = description.substring(0, 254);
                    qliDTO.sObj.put('Description', description);
                }

                // If required fields are missing, set defaults
                if(qliDTO.sObj.get('Quantity') == null){
                  XMLFactory.addNote(qliDTO.sObj, '[Quantity was missing from XML]');
                  qliDTO.sObj.put('Quantity', 1);
                }
                if(qliDTO.sObj.get('UnitPrice') == null){
                  XMLFactory.addNote(qliDTO.sObj, '[UnitPrice was missing from XML]');
                  qliDTO.sObj.put('UnitPrice', 0);
                }

                String hardwareColor = qli.Hardware_Finish__c; //qliDTO.sObj.get('Hardware_Finish__c');
                String interiorColor = qli.Interior_Color__c; //qliDTO.sObj.get('Interior_Color__c');
                
                // Check hardware color mappings
                // Only make a change if hardware color is non-estate (exists in custom setting)
                Boolean hardwareColorInMap = hardwareColorSet.contains(hardwareColor);
                if(hardwareColorMap != null && interiorColor != null && hardwareColor != null
                    && hardwareColorInMap){
                  String standardHardwareColor = hardwareColorMap.get(interiorColor);
                  if(hardwareColor != standardHardwareColor){
                    // The colors are not a standard pair
                    qliDTO.sObj.put('Hardware_Color__c', hardwareColor);
                    qliDTO.sObj.put('Special_Options__c', true);
                  }
                  qliDTO.sObj.put('Hardware_Finish__c', 'Standard');
                }

                String qliCR = (String)qliDTO.sObj.get('Cross_Reference__c');
                String qliSubCat = (String)qliDTO.sObj.get('Product_Sub_Category__c');
                if(qliDTO.sObj.get('Hardware_Option__c') != 'Wide Opening Hinge' && 
                  qliCR != null && qliCR.contains('W') && 
                  qliSubCat != null && qliSubCat.contains('Casement') ){
                    // This casement window does not have a wide opening hinge set, 
                    // make sure the cross reference points to a normal window
                    // Note: this could use a map instead, but just removing 'W' works for now
                    qliCR = qliCR.replace('W', '');
                    qliDTO.sObj.put('Child_Product_Pricebook_Entry_Id__c', qliCR);
                    qliDTO.sObj.put('Cross_Reference__c', qliCR);
                }

                quoteLineItems.add((QuoteLineItem)qliDTO.sObj);
                qliDTOs.add(qliDTO);
                //System.debug('##### qli-ro: ' + qliDTO.readOnlyFieldMap);
                //System.debug('##### qil-err: ' + qliDTO.errorFieldMap);
                //System.debug('##### qli-sObj: ' + qliDTO.sObj);
              }
            } 
            // skip the next node in the loop since we already processed it
            i++;
          }
        }
      }      
    }
    }
    public Quote aQuote {get; set;}
    public List<QuoteLineItem>  quoteLineItems {get; set;}
    public List<Quote_Discount__c> quoteDiscountList {get; set;}
    public List<Quote_Financing__c> quoteFinancingList {get; set;}
    public transient XMLFactory.DTO quoteDTO = null;
    public transient List<XMLFactory.DTO> qliDTOs = new List<XMLFactory.DTO>();
    public transient List<XMLFactory.DTO> qdDTOs = new List<XMLFactory.DTO>();
    public transient List<XMLFactory.DTO> qfDTOs = new List<XMLFactory.DTO>();
  }

  public class DTOCustomerSearchParameters{
    public String firstName {get;set;}
    public String lastName {get;set;}
    public String address {get;set;}
    public String city {get;set;}
    public String state {get;set;}
    public String phone {get;set;}
    public String email {get;set;}
    public Date startDate {get;set;}
    public Date endDate {get;set;}
  }

  public class DTOCustomerSearchResultsContainer{
    public List<DTOCustomerSearchResults> searchResults{get;set;}
    public String error{get;set;}
  }
  public class DTOCustomerSearchResults{
    public String firstName {get;set;}
    public String lastName {get;set;}
    public String address {get;set;}
    public String city {get;set;}
    public String phone {get;set;}
    public String email {get;set;}
    public String opportunityID {get;set;}
  }
    
}