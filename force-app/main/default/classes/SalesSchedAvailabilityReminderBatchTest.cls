/**
*   {Purpose}  –  This class performs unit tests against the batchAvailabilityReminder class
*                
*   {Function}  – 
*                 
*   {Support}   - For assistance with this code, please contact support@demandchainsystems.com             
*                 www.demandchainsystems.com
*                 (952) 345-4533
*/
/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date     Name             Description
*   20190211  EBG DCS          Created
*   =============================================================================
*/
@isTest
private class SalesSchedAvailabilityReminderBatchTest{

    @testSetup static void setup() {
        TestDataFactoryStatic.setUpConfigs();
        Account oStore = [Select Id, Name, Active_Store_Configuration__c From Account Where Active_Store_Configuration__c != NULL LIMIT 1];

        Id rtEmployeeContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee Contacts').getRecordTypeId();
        Contact oEmployee = TestDataFactoryStatic.createContact(oStore.Id, 'Employee');
        oEmployee.RecordTypeId = rtEmployeeContact;
        update oEmployee;        
    }
    
    @isTest static void testReminderNextDay() {
        Account oStore = [Select Id, Name, Active_Store_Configuration__c From Account Where Active_Store_Configuration__c != NULL LIMIT 1];
        Date dtProcessDate = Date.newInstance(Date.today().year(), Date.today().month(), 24);
        Store_Configuration__c oStoreConfig = new Store_Configuration__c(Id = oStore.Active_Store_Configuration__c
                                                                        , Rep_Schedules_Due__c = '25');
        update oStoreConfig;
        oStoreConfig = [Select Id
                                , Rep_Schedules_Due__c
                                , X1_Day_Schedule_Alert_Day__c
                                , X7_Day_Schedule_Alert_Day__c
						From Store_Configuration__c
						Where Id = :oStore.Active_Store_Configuration__c
                        LIMIT 1];
        System.debug('Inside testReminderNextDay.  Processing oStoreConfig: ' + oStoreConfig);
        System.assertEquals(oStoreConfig.X1_Day_Schedule_Alert_Day__c, 24, '1 Day Alert incorrect');
        System.assertEquals(oStoreConfig.X7_Day_Schedule_Alert_Day__c, 18, '7 Day Alert incorrect');
       	SalesSchedAvailabilityReminderBatch b = new SalesSchedAvailabilityReminderBatch(dtProcessDate);
        Database.executebatch(b,200); 		        
    }

    @isTest static void testReminderNextWeek() {
        Account oStore = [Select Id, Name, Active_Store_Configuration__c From Account Where Active_Store_Configuration__c != NULL LIMIT 1];
        Date dtProcessDate = Date.newInstance(Date.today().year(), Date.today().month(), 18);
        Store_Configuration__c oStoreConfig = new Store_Configuration__c(Id = oStore.Active_Store_Configuration__c
                                                                        , Rep_Schedules_Due__c = '25');
        update oStoreConfig;
        oStoreConfig = [Select Id
                                , Rep_Schedules_Due__c
                                , X1_Day_Schedule_Alert_Day__c
                                , X7_Day_Schedule_Alert_Day__c
						From Store_Configuration__c
						Where Id = :oStore.Active_Store_Configuration__c
                        LIMIT 1];
        system.debug('Inside testReminderNextWeek.  Processing oStoreConfig: ' + oStoreConfig);
        system.assertEquals(oStoreConfig.X1_Day_Schedule_Alert_Day__c, 24, '1 Day Alert incorrect');
        system.assertEquals(oStoreConfig.X7_Day_Schedule_Alert_Day__c, 18, '7 Day Alert incorrect');
        SalesSchedAvailabilityReminderBatch b = new SalesSchedAvailabilityReminderBatch(dtProcessDate);
        Database.executebatch(b,200); 		        
    }    
    
    @isTest static void testNoDate() {
        SalesSchedAvailabilityReminderBatch b1 = new SalesSchedAvailabilityReminderBatch();
        SalesSchedAvailabilityReminderBatch b2 = new SalesSchedAvailabilityReminderBatch(null);
    }
    
    @isTest static void testSchedule() {
        Test.startTest();
        SalesSchedAvailabilityReminderSchedule sh1 = new SalesSchedAvailabilityReminderSchedule();
		String sch = '0 0 2 * * ?';
        system.schedule('job', sch, sh1);
        Test.stopTest();
    }
}