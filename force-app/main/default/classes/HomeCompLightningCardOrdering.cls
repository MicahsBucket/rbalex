/*
* @author Wallace Wylie
* @description Class to provide support for the homeComponentLightningCardOrdering LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - HomeCompLightningCardOrderingTest
*/ 
public with sharing class HomeCompLightningCardOrdering {

    @AuraEnabled(cacheable=true)
    public static List<PurchaseOrderWrapper> getPurchaseOrderList(String searchKey) {

		List<PurchaseOrderWrapper> wrapperList = new List<PurchaseOrderWrapper>();
		
		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'In Process', 'Released'};
		String queryString = 'SELECT Id,' +
							 '		 Name,' +
							 '		 Status__c,' +
							 '		 Vendor_Name_Text__c,' +
							 '		 Reference__c ' +
							 'FROM   Purchase_Order__c ' +
							 'WHERE  Status__c IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (Name LIKE :tempSearchKey OR Status__c LIKE :tempSearchKey OR Reference__c LIKE :tempSearchKey OR Vendor_Name_Text__c LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY Name ' +
					   'LIMIT 20';

		for (Purchase_Order__c po : Database.query(queryString)) {
			PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper(po.Id, po.Name, po.Status__c, po.Vendor_Name_Text__c, po.Reference__c);
			wrapperList.add(wrapper);
		}

        return wrapperList;

    }

    @AuraEnabled(cacheable=true)
    public static List<ReadyToOrderWrapper> getReadyToOrderList(String searchKey) {

		List<ReadyToOrderWrapper> wrapperList = new List<ReadyToOrderWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Ready to Order'};
		String queryString = 'SELECT Id,' +
							 '		 OrderNumber,' +
							 '		 AccountId,' +
							 '		 Account.Name,' +
							 '		 BillToContactId,' +
							 '		 BillToContact.Name,' +
							 '		 Tech_Measure_Complete_Date__c ' +
							 'FROM   Order ' +
							 'WHERE  Status IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

		for (Order o : Database.query(queryString)) {
			ReadyToOrderWrapper wrapper = new ReadyToOrderWrapper(o.Id,
																  o.OrderNumber,
																  o.Account.Name,
																  o.AccountId,
																  o.BillToContact.Name,
																  o.BillToContactId,
																  o.Tech_Measure_Complete_Date__c);
			wrapperList.add(wrapper);
		}
		
        return wrapperList;

    }

    @AuraEnabled(cacheable=true)
    public static List<OrderReleasedWrapper> getOrderReleasedList(String searchKey) {

		List<OrderReleasedWrapper> wrapperList = new List<OrderReleasedwrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Order Released'};
		String queryString = 'SELECT Id,' +
							 '		 OrderNumber,' +
							 '		 AccountId,' +
							 '		 Account.Name,' +
							 '		 BillToContactId,' +
							 '		 BillToContact.Name,' +
							 '		 Time_Order_Released__c ' +
							 'FROM   Order ' +
							 'WHERE  Status IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

		for (Order o : Database.query(queryString)) {
			OrderReleasedWrapper wrapper = new OrderReleasedWrapper(o.Id,
																	o.OrderNumber,
																	o.Account.Name,
																	o.AccountId,
																	o.BillToContact.Name,
																	o.BillToContactId,
																	o.Time_Order_Released__c);
			wrapperList.add(wrapper);
		}

		return wrapperList;

	}


/** Wrapper Classes */


	  public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String PurchaseOrderNumber{get;set;}
        
        @AuraEnabled
        public String PurchaseOrderNumberURL{get;set;}
        
        @AuraEnabled
        public String Vendor{get;set;}
              
        @AuraEnabled
        public String POStatus{get;set;}

        @AuraEnabled
        public String Reference{get;set;}
        
		public PurchaseOrderWrapper(Id purchaseOrderId,
									String purchaseOrderName,
									String status,
									String vendorName,
									String reference) {
        
            PurchaseOrderNumber = purchaseOrderName;
            PurchaseOrderNumberURL = '/' + purchaseOrderId;
            Vendor = vendorName;
            POStatus = status;
            Reference = reference;
            
		}
		
	}

	public class ReadyToOrderWrapper {
        
        @AuraEnabled
        public String ReadyToONumber{get;set;}
        
        @AuraEnabled
        public String ReadyToONumberURL{get;set;}

        @AuraEnabled
        public String ReadyToOAccountName{get;set;}
              
        @AuraEnabled
        public String ReadyToOAccountURL{get;set;}

        @AuraEnabled
        public String ReadyToOBillToContactName{get;set;}

        @AuraEnabled
        public String ReadyToOBillToContactURL{get;set;}

        @AuraEnabled
        public Date ReadyToOCompleteDate{get;set;}
        
		public ReadyToOrderWrapper(Id readyToOrderId,
								   String readyToOrderNumber,
								   String readyToOrderAccountName,
								   Id readyToOrderAccountId,
								   String readyToOrderBillToContactName,
								   Id readyToOrderBillToContactId,
								   Date readyToOrderCompleteDate) {
			
            ReadyToONumber = readyToOrderNumber;
            ReadyToONumberURL = '/' + readyToOrderId;
            ReadyToOAccountName = readyToOrderAccountName;
            ReadyToOAccountURL = '/' + readyToOrderAccountId;
            ReadyToOBillToContactName = readyToOrderBillToContactName;
            If(readyToORderBillToContactId == null) {
                ReadyToOBillToContactURL = '';
            } else {
            	ReadyToOBillToContactURL = '/' + readyToOrderBillToContactId;
            }
            ReadyToOCompleteDate = readyToOrderCompleteDate;
            
		}
		
	}

	public class OrderReleasedWrapper {
        
        @AuraEnabled
        public String OrderRNumber{get;set;}
        
        @AuraEnabled
        public String OrderRNumberURL{get;set;}

        @AuraEnabled
        public String OrderRAccountName{get;set;}
              
        @AuraEnabled
        public String OrderRAccountURL{get;set;}

        @AuraEnabled
        public String OrderRBillToContactName{get;set;}

        @AuraEnabled
        public String OrderRBillToContactURL{get;set;}

        @AuraEnabled
        public DateTime OrderRTime{get;set;}
        
		public OrderReleasedWrapper(Id orderReleasedId,
									String orderReleasedNumber,
									String orderReleasedAccountName,
									Id orderReleasedAccountId,
									String orderReleasedBillToContactName,
									Id orderReleasedBillToContactId,
									DateTime orderReleasedTime) {
        
            OrderRNumber = orderReleasednumber;
            OrderRNumberURL = '/' + orderReleasedId;
            OrderRAccountName = orderReleasedAccountName;
            OrderRAccountURL = '/' + orderReleasedAccountId;
            OrderRBillToContactName = orderReleasedBillToContactName;
            OrderRBillToContactURL = '/' + orderReleasedBillToContactId;
            OrderRTime = orderReleasedTime;
            
		}
		
    }

}