public with sharing class RMS_customProductLookupController {

  public Product2 product2 {get;set;} // new account to create
  public List<Product2> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  
  public RMS_customProductLookupController() {
    product2 = new Product2();

    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
  
  // run the search and return the records found. 
 private List<Product2> performSearch(string searchString) {

    String soql = 'SELECT ID, Name, RecordType.Name, ProductCode, Product_Type__c, Description FROM Product2';
    String miscPriceBookId = System.currentPageReference().getParameters().get('miscPriceBookId');
    if(!String.isBlank(miscPriceBookId) ){
      soql = soql + ' WHERE Product_Type__c = \'Miscellaneous Charge\' AND Id IN (SELECT Product2Id FROM PriceBookEntry WHERE PriceBook2Id = :miscPriceBookId )';
    } else {
      soql += ' WHERE RecordType.Name = \'Master Product\'';
    }
    if(!String.isBlank(searchString)){
      soql = soql +  ' AND Name LIKE \'%' + searchString +'%\'';
    }
    soql = soql + '  ORDER BY Name ASC NULLS LAST LIMIT 100';
    System.debug(soql);
    return database.query(soql); 

  }
    
    
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 

}