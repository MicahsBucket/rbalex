@isTest
private class BatchUSPostAppointmentSurvey_UnitTest {
	@testSetup
	static void setup(){
		TestDataFactory.setUpConfigs();
	}
	@isTest static void holidayPastTwoDaysTest() {
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		List<Date> dateList = new List<Date>();
		Date today = date.today();
		Date yesterday = date.today().addDays(-1);
		Date twoDaysAgo = date.today().addDays(-2);
		Date fourDaysAgo = date.today().addDays(-4);
		Date fiveDaysAgo = date.today().addDays(-5);
		Date sixDaysAgo = date.today().addDays(-6);

		dateList.add(yesterday);
		dateList.add(twoDaysAgo);
		List<CXMT_Holiday__c> holidayList = new List<CXMT_Holiday__c>();
		Integer i = 0;
		for(Date d : dateList){
			CXMT_Holiday__c holiday = new CXMT_Holiday__c(
				Name = 'Holiday' + i,
				Country__c = 'US',
				Date__c = d
			);
			i++;
			holidayList.add(holiday);
		}
		Database.insert(holidayList, true);
		TestDataFactory.createSurveys('Post Appointment', 1, accountId, 'Pending', fourDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 2, accountId, 'Pending', fiveDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 3, accountId, 'Pending', sixDaysAgo);


		Test.startTest();
			BatchUsPostAppointmentSurvey obj = new BatchUsPostAppointmentSurvey();
			Database.executeBatch(obj);
		Test.stopTest();
		List<Survey__c> batchedSurveys = [SELECT Send_to_Medallia__c FROM Survey__c WHERE Send_to_Medallia__c = true];
		System.assertEquals(6, batchedSurveys.size());
	}

	@isTest static void holidayYesterdayTest() {
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		List<Date> dateList = new List<Date>();
		Date yesterday = date.today().addDays(-1);
		Date twoDaysAgo = date.today().addDays(-2);
		Date fourDaysAgo = date.today().addDays(-4);
		Date fiveDaysAgo = date.today().addDays(-5);
		Date sixDaysAgo = date.today().addDays(-6);
		dateList.add(yesterday);
		List<CXMT_Holiday__c> holidayList = new List<CXMT_Holiday__c>();
		Integer i = 0;
		for(Date d : dateList){
			CXMT_Holiday__c holiday = new CXMT_Holiday__c(
				Name = 'Holiday' + i,
				Country__c = 'US',
				Date__c = d
			);
			i++;
			holidayList.add(holiday);
		}
		Database.insert(holidayList, true);
		TestDataFactory.createSurveys('Post Appointment', 1, accountId, 'Pending', fourDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 2, accountId, 'Pending', fiveDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 3, accountId, 'Pending', sixDaysAgo);

		Test.startTest();
			BatchUsPostAppointmentSurvey obj = new BatchUsPostAppointmentSurvey();
			Database.executeBatch(obj);
		Test.stopTest();

		List<Survey__c> batchedSurveys = [SELECT Send_to_Medallia__c FROM Survey__c WHERE Send_to_Medallia__c = true];
		System.assertEquals(3, batchedSurveys.size());
	}
	
	@isTest static void holidayTodayTest() {
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		List<Date> dateList = new List<Date>();
		Date today = date.today();
		Date yesterday = date.today().addDays(-1);
		Date twoDaysAgo = date.today().addDays(-2);
		Date fourDaysAgo = date.today().addDays(-4);
		Date fiveDaysAgo = date.today().addDays(-5);
		Date sixDaysAgo = date.today().addDays(-6);

		//dateList.add(today);
		dateList.add(today);
		List<CXMT_Holiday__c> holidayList = new List<CXMT_Holiday__c>();
		Integer i = 0;
		for(Date d : dateList){
			CXMT_Holiday__c holiday = new CXMT_Holiday__c(
				Name = 'Holiday' + i,
				Country__c = 'US',
				Date__c = d
			);
			i++;
			holidayList.add(holiday);
		}
		Database.insert(holidayList, true);
		TestDataFactory.createSurveys('Post Appointment', 1, accountId, 'Pending', fourDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 2, accountId, 'Pending', fiveDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 3, accountId, 'Pending', sixDaysAgo);


		Test.startTest();
			BatchUsPostAppointmentSurvey obj = new BatchUsPostAppointmentSurvey();
			Database.executeBatch(obj);
		Test.stopTest();
		List<Survey__c> batchedSurveys = [SELECT Send_to_Medallia__c FROM Survey__c WHERE Send_to_Medallia__c = true];
		System.assertNotEquals(2, batchedSurveys.size());
	}

	@isTest static void noHolidayTest() {
		Account account1 = TestDataFactory.createStoreAccount('Calvins Store');
		Database.insert(account1, true);
		Id accountId = account1.Id;
		system.debug('accountId' + accountId);
		List<Date> dateList = new List<Date>();
		Date fourDaysAgo = date.today().addDays(-4);
		Date fiveDaysAgo = date.today().addDays(-5);
		Date sixDaysAgo = date.today().addDays(-6);

		TestDataFactory.createSurveys('Post Appointment', 2, accountId, 'Pending', fourDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 2, accountId, 'Pending', fiveDaysAgo);
		TestDataFactory.createSurveys('Post Appointment', 3, accountId, 'Pending', sixDaysAgo);


		Test.startTest();
			BatchUsPostAppointmentSurvey obj = new BatchUsPostAppointmentSurvey();
			Database.executeBatch(obj);
		Test.stopTest();
		List<Survey__c> batchedSurveys = [SELECT Send_to_Medallia__c FROM Survey__c WHERE Send_to_Medallia__c = true];
		System.assertEquals(2, batchedSurveys.size());
	}
	
}