//Unit Test for the MileStone controller for Customer Portal 
@isTest
public class MileStone_UnitTest {
    @testSetup
    static void setup(){
        TestDataFactoryStatic.setUpConfigs();
        Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
        Id storeConfigId = storeAccount.Active_Store_Configuration__c;
        Id accountId = TestDataFactoryStatic.createAccount('Ramya', storeAccountId).Id;
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'RamyaPrime');
        Opportunity opportunity = TestDataFactoryStatic.createNewOpportunity('Ramyas Opportunity', accountId, storeAccountId, 'Sold', date.today());
        System.debug('opportunity.Store_Location__c: ' + opportunity.Store_Location__c);
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order ord = TestDataFactoryStatic.createOrderTestRecords(opportunity);
        
        // MilestoneDates mdates = new MilestoneDates();
        ord.Order_Processed_Date__c = Date.today().addDays(-1);
        ord.Tech_Measure_Date__c = Date.today().addDays(-3);
        ord.Time_Ready_to_Order__c = Date.today().addDays(-5);
        ord.Time_Install_Scheduled__c = Date.today().addDays(-6);
        ord.Job_Close_Date__c = Date.today();
        ord.CustomerPortalUser__c = UserInfo.getUserId();
        
        Opportunity.CloseDate = Date.today();
        ord.EffectiveDate = Date.today().addDays(1);
        ord.Date_Sat_Survey_Run__c = Date.today();
        ord.Survey_Sent__c = true;
        
        Database.update(ord); 
        
        Id InstallRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Install').getRecordTypeId();
        Id TechMeasureRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Tech Measure').getRecordTypeId();
        
        WorkOrder wo1 = new WorkOrder(Sold_Order__c = ord.id,  RecordTypeId=InstallRecordTypeId, Scheduled_Start_Time__c=Date.today() );
		WorkOrder wo2 = new WorkOrder(Sold_Order__c = ord.id,  RecordTypeId=InstallRecordTypeId, Scheduled_Start_Time__c=Date.today(), scheduled_End_Time__c= Date.today().addDays(1));                
		WorkOrder wo3 = new WorkOrder(Sold_Order__c = ord.id,  RecordTypeId=TechMeasureRecordTypeId, Scheduled_Start_Time__c=Date.today() );        
		WorkOrder wo4 = new WorkOrder(Sold_Order__c = ord.id,  RecordTypeId=TechMeasureRecordTypeId, Scheduled_Start_Time__c=Date.today(), scheduled_End_Time__c= Date.today().addDays(1));        
        
		List <WorkOrder> woList = new List <WorkOrder>();
        woList.add(wo1);
        woList.add(wo2);
        woList.add(wo3);
        woList.add(wo4);
        
        insert woList;
        
        Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('On Hold').getRecordTypeId();
		Task tsk1 = new Task(WhatId = ord.Id, ActivityDate = Date.today().addDays(1), Priority = 'High', Subject = 'Other', Primary_Reason__c = 'Project Approval', Secondary_Reason__c = 'Other - See Comments',  Status = 'Open', RecordTypeId = taskRecordTypeId); 
       // insert tsk1;/* Commented because of dependencies*/
    }
    
    @isTest
    static void getOrderDates(){
        try{
        Test.startTest();
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Order testOrder = MileStoneController.getOrderDates(ord.Id);
        System.assertEquals(Date.today().addDays(-1), testOrder.Order_Processed_Date__c);
        //System.assertEquals(Date.today().addDays(-3), testOrder.Tech_Measure_Date__c);
        System.assertEquals(Date.today().addDays(-5), testOrder.Time_Ready_to_Order__c);
        System.assertEquals(Date.today().addDays(-6), testOrder.Time_Install_Scheduled__c);
        System.assertEquals(Date.today(), testOrder.Job_Close_Date__c);
        Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception: ' + ex);       
        }
    }
    
    @isTest
    static void getMilestoneOrder(){
         try{
        Test.startTest();
        Order ord = [SELECT Id FROM Order LIMIT 1];
        MilestoneDates md = MileStoneController.getAllMileStonesForOrder(ord.Id);
        System.assertEquals(Date.today(), md.techMeasureDate.get(0));
        Test.stopTest();
             }catch(Exception ex){
            System.debug('Exception: ' + ex);       
        }
    }
    
    @isTest
    static void getOrderId(){
        try{
        Test.startTest();
        id ordid  =  MileStoneController.getOrderId();
        Order ord = [SELECT Id FROM Order LIMIT 1];
        System.assertEquals(ord.Id,ordid);
        Test.stopTest();
        }catch(Exception ex){
            System.debug('Exception: ' + ex);       
        }
    }
    
    
}