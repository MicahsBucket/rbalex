/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description Searches for workers based on a string and creates a list of 
*              workers based on role/active status and allows users to update worker records
**/

public with sharing class EditWorkersApexController {

@AuraEnabled
	public static List<Worker__c> findWorker(String searchString){
		if(searchString == null || searchString == ''){
			return null;
		}
        if(searchString.containsWhitespace()){
            Set<Worker__c> workerSet = new Set<Worker__c>();
            List<Worker__c> workerList = new List<Worker__c>();
            String firstString = '%' + searchString.substring(0,searchString.indexOf(' ')) + '%';
            String secondString = '%' + searchString.substring(searchString.indexOf(' ')+1) + '%';
            List<Worker__c> firstWorkers = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c  
												FROM Worker__c 
												WHERE (First_Name__c LIKE :firstString OR Last_Name__c LIKE :firstString OR Email__c LIKE :firstString)
												ORDER BY Worker__c.Last_Name__c ASC];
            List<Worker__c> secondWorkers = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c  
												FROM Worker__c 
												WHERE (First_Name__c LIKE :secondString OR Last_Name__c LIKE :secondString OR Email__c LIKE :secondString) 
												ORDER BY Worker__c.Last_Name__c ASC];
            workerSet.addAll(firstWorkers);
            workerSet.addAll(secondWorkers);
            workerList.addAll(workerSet);
            return workerList;
        } else {  
			String soqlSearch = '%' + searchString + '%';
			List<Worker__c> allWorkers = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c  
													FROM Worker__c 
													WHERE (First_Name__c LIKE :soqlSearch OR Last_Name__c LIKE :soqlSearch OR Email__c LIKE :soqlSearch) 
													ORDER BY Worker__c.Last_Name__c ASC];
													system.debug('allworkers: ' + allWorkers);
	        return allWorkers;
        }
	}

@AuraEnabled
	public static List<Worker__c> getWorkers(String role){
		List<Worker__c> workersWithRole = new List<Worker__c>();
		if(role.equals('Inactive')){ 
			workersWithRole = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c  
												FROM Worker__c 
												WHERE Worker__c.Role__c 
												INCLUDES (:role) ORDER BY Worker__c.Last_Name__c ASC];
		} else {
			workersWithRole = [SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Mobile__c, Role__c  
												FROM Worker__c 
												WHERE Worker__c.Role__c 
												INCLUDES (:role) AND Worker__c.Role__c EXCLUDES ('Inactive') ORDER BY Worker__c.Last_Name__c ASC];
		}
		return workersWithRole;
	}

@AuraEnabled
	public static Worker__c saveWorker(Worker__c worker, String firstName, String lastName, String phone, String email, String workersSelected){
		worker.First_Name__c = firstName;
		worker.Last_Name__c = lastName;
		worker.Email__c = email;
		worker.Mobile__c = phone;
		worker.Role__c = workersSelected;
		Database.update(worker);
		return worker;
	}

}