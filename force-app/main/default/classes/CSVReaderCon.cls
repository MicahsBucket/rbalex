/******************************************************************
 * Author : Sundeep
 * Class Name :CSVReaderCon
 * Createdate :
 * Description : To Read the data from CSV and creating survey records for Orders
 * Change Log
 * ---------------------------------------------------------------------------
 * Date: 3/19/2019       Name: Sundeep         Description
 * ---------------------------------------------------------------------------
 * 
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/
public class CSVReaderCon {
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Boolean Flag{get;set;}
    String[] filelines = new String[]{};
        List<order> orderupload;
    public set<string> ordIds;
    public CSVReaderCon(){
        orderupload = new List<order>();
        System.debug('OrderUpload' + orderupload);
        Flag=false;
    }
    
    /* Method : ReadFile
     * Description: To read data  from CSV
     * Return Type : Pagereference 
     * Paramter  : None
     */
    public Pagereference ReadFile()
    {
        orderupload.clear();
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        Flag=true;
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
            
            order a = new order();
            a.name = inputvalues[0];
            
            string st = '"';
            if(a.name.contains(st)){
                a.name = a.name.replace('"', '');
            }
                       
            orderupload.add(a);
        }
        ordIds = new set<string>();
        for(order ac:orderupload){
            ordIds.add(ac.name);
            
        }
        
        system.debug('OrderUploade12:' + orderupload);
        return null;
    }
    
    /* Method : getuploadedOrders
     * Description: To display the Order list from CSV
     * Return Type : list of orders 
     * Paramter  : None
     */
     
    public List<Order> getuploadedOrders()
    {
        if(orderupload!=null){
            return orderupload;
            
        }else {return null;}
        
    }
    
    /* Method : getInvokeBatch
     * Description: TO create survey records for Order from CSV
     * Return Type : Void 
     * Paramter  : None
     */
    
    public void getInvokeBatch(){
        try{
        if(!ordIds.isEmpty()){
            list<order> ordlist = new list<order>(); 
            for(order od:[SELECT Id, Installation_Date__c,Opportunity.owner.Id, OpportunityId, EffectiveDate, Store_Location__c FROM Order Where Id IN :ordIds and Survey_Created__c=false]){
                ordlist.add(od);
            }
            if(!ordlist.isEmpty()){
                PostInstallationSurveyHandler.surveyProcessCreation(ordlist);
            }
            //orderupload.clear();
            //getuploadedOrders();
            Flag=false;
        }
        }catch(exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getCause()+' Error: '+ex.getMessage() ));}
        
    }
}