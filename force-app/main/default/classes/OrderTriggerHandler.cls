/*******************************************************//**

@class  OrderTriggerHandler

@brief  Controller to hold logic for Order2 Trigger   
 
@author Pavan Gunna  

@see    OrderTriggerHandlerTest

***********************************************************/ 

public without sharing class OrderTriggerHandler{

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public static boolean IsFromBachJob ;
    public static boolean isFromUploadAPI=false;
    
    //Boolean to avoid recursion 
    public static Boolean isBeforeInsert = true;
    public static Boolean isBeforeUpdate = true;
    public static Boolean isAfterInsert = true;
    public static Boolean isAfterUpdate = true;
    public static Boolean isWorkOrdersCreated = false;
    public static Boolean beforeUpdateCreatedWO = false;
    
    // Collections for Before Insert
    public static List<Order> newOrderBI = new List<Order>();
    public static List<Order> newSerReqBI = new List<Order>();
    // Collections for After Insert
    public static Map<ID, Order> newOrderMapAI = new Map<ID, Order>();    
    public static Map<ID, Order> newSerReqMapAI = new Map<ID, Order>();
    // Collection for Before Update
    public static  Map<ID, Order> newOrderMapBU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqMapBU = new Map<ID, Order>();

    // Collection for After Update
    public static  Map<ID, Order> newOrderMapAU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqMapAU = new Map<ID, Order>();
    public static  Map<ID, Order> newSerReqChangeOrderMapAU = new Map<ID, Order>();

    
    RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();

    
    public OrderTriggerHandler(boolean isExecuting, integer size)
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    

  public void OnBeforeInsert(List<Order> newOrder)
    {
        
        for(Order OrderRec: newOrder)
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqBI.add(OrderRec);
            } else 
            {
                newOrderBI.add(OrderRec);
            }
        }
        if(isBeforeInsert){
            isBeforeInsert = false;
            system.debug('Start Order Trigger On Before Insert');
             OrderActions.setNewTimeStamps(newOrder);
             OrderActions.setPriceBookId(newOrder);  
             OrderActions.updateSalesRepContact(newOrder);
            system.debug('End Order Trigger On Before Insert');
        }        
    }    
    public void OnBeforeUpdate( Map<ID, Order> newOrderMap , Map<ID, Order> oldOrderMap )
    {
        for(Order OrderRec: newOrderMap.values())
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqMapBU.put(OrderRec.id, OrderRec);
            } else 
            {
                newOrderMapBU.put(OrderRec.id, OrderRec);
            }
        }
        if(isBeforeUpdate){
            isBeforeUpdate = false ;
            system.debug('Start Order Trigger On Before Update');
            OrderActions.checkLockedByStatus(newOrderMap,oldOrderMap, 'Order');
            RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivation(oldOrderMap.values(),newOrderMapBU.values(),oldOrderMap,newOrderMapBU);
            OrderActions.updateOrderStatus(oldOrderMap,newOrderMap);
            OrderActions.updateOrderTimeStamps(oldOrderMap,newOrderMap);
            OrderActions.updateRevenueRecDate(oldOrderMap,newSerReqMapBU );            
            OrderActions.updateChangeHistories(oldOrderMap, newOrderMapBU);
            OrderActions.updateSalesRepContact(newOrderMap,oldOrderMap );              
            CommissionsManagement.checkCancelOrderAmounts(oldOrderMap,newOrderMap);
            OrderActions.checkRouteMileage(oldOrderMap, newSerReqMapBU);
            //OrderActions.callDLRS(newOrderMap.values());
            system.debug('End Order Trigger On Before Update');
        } 
         else if(!beforeUpdateCreatedWO &&!newOrderMapBU.isEmpty()){
           RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivation(oldOrderMap.values(),newOrderMapBU.values(),oldOrderMap,newOrderMapBU);
             
        }          
    }    
    public void OnAfterInsert( Map<ID, Order> newOrderMap)
    {
        Map<Id, List<Opportunity>> OrderOppIdToOppMap = new Map<Id, List<Opportunity>>();
        for(Order OrderRec: newOrderMap.values()) {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqMapAI.put(OrderRec.id, OrderRec);
            } 
            else {
                newOrderMapAI.put(OrderRec.id, OrderRec);
            }
        }
        if(isAfterInsert){
            isAfterInsert = false;
            system.debug('Start Order Trigger On After Insert');
             RMS_WorkOrderCreationManager.createWorkOrderOnOrderCreation(newOrderMapAI.values(), newOrderMapAI);
             OrderActions.createBackOfficeChecksOnOrderCreation(newOrderMapAI.values(), newOrderMapAI);
             OrderActions.checkLockedByStatus(newOrderMapAI,null, 'Order');
             OrderActions.checkRouteMileage(null, newSerReqMapAI);
             
             system.debug('End Order Trigger On After Insert');
        }        
    }
    public void OnAfterUpdate( Map<ID, Order> newOrderMap , Map<ID, Order> oldOrderMap )
    {
        Map<Id, List<Opportunity>> OrderOppIdToOppMap = new Map<Id, List<Opportunity>>();
         for(Order OrderRec: newOrderMap.values())
        {
            if(OrderRec.RecordTypeName__c == 'CORO_Service')
            {
                newSerReqMapAU.put(OrderRec.id, OrderRec);
            } else 
            {
                newOrderMapAU.put(OrderRec.id, OrderRec);
                
                OrderOppIdToOppMap.put(OrderRec.OpportunityId, new List<Opportunity>());
            }
            If (OrderRec.RecordTypeName__c == 'CORO_Service' || OrderRec.RecordTypeName__c == 'Change_Order'){
                newSerReqChangeOrderMapAU.put(OrderRec.id, OrderRec);
            }
        }
        if(isAfterUpdate){
            isAfterUpdate = false;
            system.debug('Start Order Trigger On After Update ');
            if(!isWorkOrdersCreated){
               RMS_WorkOrderCreationManager.createWorkOrderOnOrderSoldOrderBeingAssigned(oldOrderMap.values(), newSerReqMapAU.values(), oldOrderMap, newSerReqMapAU);
               isWorkOrdersCreated = true;
         }
            financialTransactionManager.onAfterUpdateOrder(oldOrderMap.values(), newOrderMap.values(), oldOrderMap, newOrderMap);
            OrderActions.customerPickup(oldOrderMap,newOrderMap);
            OrderActions.createOnHoldTasks(oldOrderMap, newOrderMap);  
            OrderActions.updateUniqueIdentifier(oldOrderMap, newOrderMapAU, false);
            OrderActions.updateRelatedWOOfSerReq(newSerReqMapAU, oldOrderMap);
            OrderActions.callDLRS(newSerReqChangeOrderMapAU.values());
            FSLRollupRolldownController.triggerFromOrder(oldOrderMap,newOrderMap,FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
            
            //// Code to rollup the Order to the Opportunity.Related_Order__c field////////////
            List<Opportunity> oppList = [SELECT Id, Related_Order__c FROM Opportunity WHERE Id IN: OrderOppIdToOppMap.keyset()];
            for(Opportunity opp: oppList){
                OrderOppIdToOppMap.get(opp.Id).add(opp);
            }
            list<Opportunity> oppsToUpdate = new list<Opportunity>();
            for(Order OrderRec: newOrderMap.values()){
                System.debug('OrderRec.RecordTypeName__c: '+ OrderRec.RecordTypeName__c);
                if(OrderRec.RecordTypeName__c != 'CORO_Service' && OrderRec.RecordTypeName__c != 'Change_Order'){
                    Opportunity opp = new Opportunity();
                    opp.Id = OrderOppIdToOppMap.get(OrderRec.OpportunityId)[0].Id;
                    opp.Related_Order__c = OrderRec.Id;
                    oppsToUpdate.add(opp);
                }
            }
            System.debug('oppsToUpdate: '+oppsToUpdate);
            if(oppsToUpdate.size() > 0){
                System.enqueueJob(new OpportunityUpdateQue(oppsToUpdate));
            }
            ////////////////////
            system.debug('End Order Trigger On After Update '); 
        }
         else if(!newSerReqMapAU.isEmpty() && !isWorkOrdersCreated){
           RMS_WorkOrderCreationManager.createWorkOrderOnOrderSoldOrderBeingAssigned(oldOrderMap.values(), newSerReqMapAU.values(), oldOrderMap, newSerReqMapAU); 
           isWorkOrdersCreated = true;
             
        }
        //Below method is written by Ratna for handling community user activation/deactivation based on order status-Cancelled
        OrderActions.handleCancelOrders(oldOrderMap,newOrderMap);
    }
    
    public void OnAfterDelete(Map<ID, Order> oldOrderMap){
        
    }
     public void OnAfterUnDelete(Map<ID, Order> newOrderMap){
        
    }

    @future 
    public static void OnAfterUpdateAsync(Set<ID> newOrderIDs)
    {
    
    }      
    public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext
    {
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext
    {
        get{ return !IsTriggerContext;}
    }
}