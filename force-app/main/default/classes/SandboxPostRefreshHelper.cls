public class SandboxPostRefreshHelper {
    
    public static void resetAdminEmailAddresses(){
        List<User> userEmailList = new List<User>(); 
        for(User u : [SELECT Email FROM User 
                      WHERE (profile.name = 'System Administrator' OR profile.name = 'Super Administrator') ]) { 
                          u.Email = u.Email.replace('=','@'); 
                          String addedPhrase = '@example.com'; 
                          u.Email = u.Email.remove(addedPhrase); 
                          userEmailList.add(u); 
                      } 
        if(userEmailList.size() > 0){ 
            Update userEmailList;
        } 
    }
    
    public static void populateStoreAccountRecords(){        
        Account twinCities = new Account(
            Name = '0077 - Twin Cities',
            Type = 'CORO',
            RecordTypeId = UtilityMethods.retrieveRecordTypeId('Store', 'Account'),
            BillingStreet  = '1920 County Road C West,',
            BillingCity = 'Roseville',
            BillingState  =  'Minnesota',
            BillingPostalCode  = '55113',
            BillingCountry = 'United States',
            BillingStateCode  =  'MN',
            BillingCountryCode = 'US',
            BillingLatitude = 45.020743,
            BillingLongitude =   -93.182536,
            ShippingStreet  = '1920 County Road C West,',
            ShippingCity  =  'Roseville',
            ShippingState  = 'Minnesota',
            ShippingPostalCode = '55113',
            ShippingCountry  = 'United States',
            ShippingStateCode  = 'MN',
            ShippingCountryCode = 'US',
            ShippingLatitude  =  45.02083747,
            ShippingLongitude  = -93.18272125,
            Phone  = '+1 (651) 264-4777',
            Fax = '+1 (651) 264-4079',
            Active__c  = TRUE,
            Pivotal_Id__c =  'AAAAAAAAAAg=Store',
            Enabled_StoreId__c = '77',
            Over_Receiving_Allowed__c =  FALSE,
            Account_Balance__c = 0,
            Store_Number__c = '77',
            Contractor_License_1__c = 'BC130983 - MN',
            DBA__c = 'Renewal by Andersen of the Twin Cities',
            Survey_Holds__c = 119,
            Legal_Name__c  = 'Renewal by Andersen, LLC',
            Electronic_Orders__c  =  FALSE,
            Contractor_License_2__c = '266951 - WI',
            Applications_Used__c  = 'rForce;rSuite w/o Tech Mapp;Enabled Plus;Ground Force;CFW;Signature Service OCEM Auto'
        );
        
        insert twinCities;
        
        Map<String,String> queueNameToID = new Map<String,String>();
        for( Group g : [SELECT Id, Name FROM Group WHERE  Type = 'Queue']){
            queueNameToID.put(g.Name, g.Id);
        }
        
        Store_Configuration__c config = new Store_Configuration__c(
            Accounts_Receivable_Phone__c = '(651) 264-4111',
            AR_Coordinator__c = [SELECT Id FROM User WHERE FirstName = 'Krystal' AND LastName = 'Harvey'].id,
            Contractor_License_1__c = 'BC130983 - MN',
            Contractor_License_2__c =  '266951 - WI',
            DBA__c = 'Renewal by Andersen of the Twin Cities',
            Fax__c = '+1 (651) 264-4079',
            Install_Work_Order_Queue_Id__c = queueNameToID.get('Install WO - Twin Cities'),
            Invoice_Location_Address__c = '1920 County Road C West,',
            Invoice_Location_City__c = 'Roseville',
            Invoice_Location_State__c  = 'Minnesota',
            Invoice_Location_Zip__c = '55113',
            Invoice_Notes_or_Comments__c = 'We accept Visa, MasterCard, and Discover',
            Lien_County__c = 'Ramsey',
            Lien_State__c = 'Minnesota',
            Location_Number__c = '50',
            Material_Contact__c = [SELECT Id FROM User WHERE FirstName = 'Corey' AND LastName = 'Petherbridge'].id,
            Material_Multiplier__c = 0.04,
            Order_Number__c = 6223,
            Order_Queue_Id__c = queueNameToID.get('Draft Orders - Twin Cities'),
            Paint_Stain_Work_Order_Queue_ID__c =queueNameToID.get('Paint/Stain WO - Twin Cities'),
            Phone__c = '+1 (651) 264-4777',
            Remit_to_Address__c = '1920 County Road C West,',
            Remit_to_City__c = 'Roseville',
            Remit_to_Company_Name__c = 'Renewal by Andersen - Twin Cities',
            Remit_to_State__c = 'MN',
            Remit_to_Zip__c = '55113',
            Sales_Tax__c = 7.125,
            Service_Phone__c = '+1 (651) 264-4301',
            Service_Work_Order_Queue_ID__c = queueNameToID.get('Service WO - Twin Cities'),
            Standard_Hourly_Rate__c = 50,
            Store__c = twinCities.id,
            Store_Abbreviation__c = 'A',
            Tax_Multiplier__c = 60,
            Tech_Measure_Work_Order_Queue_Id__c = queueNameToID.get('Tech Measure WO - Twin Cities')
        );
        
        insert config;
        
        Back_Office_Checklist_Configuration__c checklist = new Back_Office_Checklist_Configuration__c(
            Store_Configuration__c = config.id,
            Contract_Signed__c = true,
            Deposit_Received__c = true,
            Lien_Rights_Signed__c =true,
            Project_Preparation_Expectations__c = true,
            Renovate_Right_Booklet__c = true,
            Rights_of_Cancellation__c = true,
            State_Supplement__c = true
        );
        
        insert checklist;
        
        Financial_Account_Number__c accountNumber = new Financial_Account_Number__c( 
            Name = '11300120500001',
            Account_Label__c = '11300120500001',
            Account_Type__c =  'Check Acct',
            Description__c = 'My Account',
            Store_Configuration__c = config.id
        );
        
        insert accountNumber;
        
        List<Financial_Transaction__c> myTransactions = new List<Financial_Transaction__c>();
        Set<String> transactionTypes = new Set<String>{'Deposit','Final Payment',
            'Inventory Received', 'Inventory Installed', 'Rev Recognized - Deposit', 'Rev Recognized - Labor',
            'Rev Recognized - Refund Deposit', 'Rev Recognized - Internal Labor', 'Rev Recognized - Gross Sales',
            'Rev Recognized - Discount', 'Rev Recognized - AR', 'Rev Recognized - Finance Fees',
            'Rev Recognized - Install Adjustment' 
            };
                
                for(String s : transactionTypes){
                    myTransactions.add( new Financial_Transaction__c(
                        Credit_Account_Number__c = accountNumber.id,
                        Description__c = 'my Description',
                        Store_Configuration__c = config.id,
                        Transaction_Type__c = s
                    ));
                }
        
        insert myTransactions;
        
        twinCities.Active_Store_Configuration__c = config.id;
        update twinCities;
        
        Account renewalByAndersenVendor = new Account(
            Name = 'Renewal by Andersen',
            RecordTypeId = UtilityMethods.retrieveRecordTypeId('Vendor', 'Account'),
            Baan_Business_Partner_Number__c = '5AND00082',
            Vendor_Account_Type__c = 'Intercompany',
            Electronic_Orders__c = true
        );
        
        insert renewalByAndersenVendor;
        
        insert new Store_Vendor__c(
            Store__c = twinCities.id,
            Vendor__c = renewalByAndersenVendor.id
        );
        
        List<Vendor_Product__c> vendorProductsToInsert = new List<Vendor_Product__c>();
        for(PricebookEntry pbe : [SELECT Product2Id FROM PricebookEntry WHERE Pricebook2.Name = 'Twin Cities - 2017 v1.0' LIMIT 1000]){
            vendorProductsToInsert.add( new Vendor_Product__c(
                Vendor__c = renewalByAndersenVendor.id,
                Product__c = pbe.Product2Id
            ));
        }
        insert vendorProductsToInsert; 
    }
    
    
    public static void populateWorkTypes(){
        /*
         * This is done in TestDataFactoryStatic
         * 
        List<WorkType> myTypes = new List<WorkType>();
        myTypes.add(new WorkType(Name = 'Install', EstimatedDuration = 8));
        myTypes.add(new WorkType(Name = 'Service',  EstimatedDuration = 2));
        myTypes.add(new WorkType(Name = 'Measure',  EstimatedDuration = 1));
        Insert myTypes;        
		*/
    }

    
    
    public static void populateServiceTerritories(){
        
        OperatingHours twinCitiesHours = new OperatingHours(
            Name = 'Twin Cities',
            TimeZone = 'America/Chicago'
        );
        Insert twinCitiesHours;
        
        List<TimeSlot> myTimeslots =new List<TimeSlot>();
        Set<String> weekdays = new Set<String>{ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'};
            List<Integer> weekdayTimeSlotHours =new List<Integer>{7,9,11,13,15,17};
                for(String s : weekdays){
                    for(Integer I =0; i<weekdayTimeSlotHours.size()-1; i++){
                        myTimeslots.add(new TimeSlot(
                            DayOfWeek = s,
                            StartTime =  Time.newInstance(weekdayTimeSlotHours[i], 0,0,0),
                            EndTime = Time.newInstance(weekdayTimeSlotHours[i+1], 0,0,0),                        
                            OperatingHoursId = twinCitiesHours.id,
                            type = 'Normal'
                        ));
                    }
                }
        
        myTimeslots.add(new TimeSlot(
            DayOfWeek = 'Saturday',
            StartTime =  Time.newInstance(8, 0,0,0),
            EndTime = Time.newInstance(12, 0,0,0),             			
            OperatingHoursId = twinCitiesHours.id,
            type = 'Normal'
        ));
        
        myTimeslots.add(new TimeSlot(
            DayOfWeek = 'Saturday',
            StartTime =  Time.newInstance(12, 0,0,0),
            EndTime = Time.newInstance(16, 0,0,0),
            OperatingHoursId = twinCitiesHours.id,
            type = 'Normal'
        ));        
        insert myTimeslots; 
        
        ServiceTerritory st = new ServiceTerritory(
            Name = 'RbA of the Twin Cities, MN',
            OperatingHoursId = twinCitiesHours.id,
            Street  = '1920 County Road C West,',
            City = 'Roseville',
            State  =  'Minnesota',
            PostalCode  = '55113',
            Country = 'United States',
            StateCode  =  'MN',
            CountryCode = 'US',
            Latitude = 45.020743,
            Longitude =   -93.182536,
            IsActive = true,
            Store__c =  [SELECT Id from Account WHERE Name = '0077 - Twin Cities'].id
        );
        
        insert st;
        
        List<String> mnZipCodes = new List<String>{
            '44093','54016','54027','54723','55029','55083','55133',
                '55144','55145','55146','55150','55155','55164','55165','55166','55168',
                '55170','55171','55172','55175','55187','55188','55361','55365','55369',
                '55383','55393','55394','55399','55440','55458','55459','55460','55467',
                '55470','55472','55473','55474','55478','55479','55480','55483','55484',
                '55485','55486','55487','55488','55550','55551','55552','55553','55554',
                '55555','55556','55557','55558','55559','55560','55561','55562','55563',
                '55564','55565','55566','55567','55568','55580','55581','55582','55583',
                '55584','55585','55586','55587','55588','55589','55590','55591','55592',
                '55594' };
        List<Service_Territory_Zip_Code__c> zipsToInsert = new List<Service_Territory_Zip_Code__c>();
        for(String s : mnZipCodes){
            zipsToInsert.add( new Service_Territory_Zip_Code__c(Name = s, Service_Territory__c = st.id) );
        }
        insert zipsToInsert;
        
    }
    
    
}