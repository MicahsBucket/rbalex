Public Interface MakabilityService {

	List<MakabilityRestResource.MakabilityResult> checkCompatibility(map <string,MakabilityRestResource.OrderItem> requests, set<id> productIds);

}