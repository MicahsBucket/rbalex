@isTest
public class LightningResponseTest {
    
    @isTest
    private static void responseTest(){
        
        test.startTest();
        LightningResponse lr = new LightningResponse();
        lr.jsonResponse ='test';
        test.stopTest();    
        system.assertEquals('test', lr.jsonResponse);
        
    }
    
    @isTest
    private static void errrorTest(){
        LightningResponse lr = new LightningResponse();        
        test.startTest();
        try{
            if(true){
                throw new testException('This is a test');
            }
        } catch (testException e){
            lr = new LightningResponse(e);
        }        
        test.stopTest();   
        system.assertEquals('This is a test', lr.error);
    }
    private class testException extends Exception {}   
    
}