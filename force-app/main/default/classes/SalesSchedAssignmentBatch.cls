public class SalesSchedAssignmentBatch implements Database.Batchable<sObject>{

    public List<String> rules {get; set;}
    public Date day {get; set;}
    public Time storeCloseTime {get; set;}
    
    public SalesSchedAssignmentBatch(Date day, Time storeCloseTime){
        this.day = day;
        this.rules = new List<String>{'RankRepsByCloseRate'};
        this.storeCloseTime = storeCloseTime;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
    	String query = 
            'Select ' +
            	'Id ' +
            'From ' +
            	'Account ' +
            'Where ' +
            	'RecordType.DeveloperName = \'Store\' ' +
            	'And Active_Store_Configuration__r.Call_Center_Close_Time__c = :storeCloseTime';
        
        if(Test.isRunningTest()){
            query += ' Limit 1';
        }
      	return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        for(Account store : (List<Account>)scope){
            SalesSchedAssignmentRulesFactory.processAllWork(store.Id, this.day, this.rules,'batch');
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}