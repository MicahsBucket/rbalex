public with sharing class RMS_StoreComplianceUserQueueable implements Queueable{
    
    private PermissionSetAssignment permissionSet;
    private Id userId;

    public RMS_StoreComplianceUserQueueable(Id addUserId) {
        this.userId = addUserId;
        System.debug(this.userid);
    }
    
    public void execute(QueueableContext context) {
        PermissionSet pm;
        try {
            pm = [select Id, Name from PermissionSet where Name = 'Store_Configuration_Contract_Editing' limit 1];
        } catch (Exception e){
            System.debug('Missing the Store_Configuration_Contract_Editing PermissionSet');
        }
        if(pm == null)
            return;

        permissionSet = new PermissionSetAssignment(PermissionSetId = pm.Id, AssigneeId = userId);

        System.debug([select COUNT() from PermissionSetAssignment where PermissionSetId = :pm.Id and AssigneeId = :userId]);
        System.debug(permissionSet);        
        if(userId != null && [select COUNT() from PermissionSetAssignment where PermissionSetId = :pm.Id and AssigneeId = :userId] == 0) {
            insert permissionSet;
        }
    }

}