/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Customer Portal
* @date 1/18
* @description Finds the Contact associated to the logged in User
**/
public without sharing class CustomerInformationController {
    @AuraEnabled
    public static Contact getContactInfo(Boolean primaryBool) {
        try{
            Id userId = UserInfo.getUserId();
            User userRecord=[select id,ContactId from User where id=:userId];
            //finds associated Order to User using lookup field CustomerPortalUser__c
            system.debug('userId ------->'+userId );
            order ord = [SELECT Id, EffectiveDate,OpportunityId, CustomerPortalUser__c,Primary_Contact__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC LIMIT 1];
            System.debug('Order Debuging:' + ord);
            System.debug('Boolean value:' + primaryBool);
            
            //finds associated OppCon to order to find the Contact
            OpportunityContactRole oppCon = [SELECT Id, OpportunityId, ContactId 
                                             FROM OpportunityContactRole 
                                             WHERE OpportunityId = :ord.OpportunityId AND isPrimary = :primaryBool];
            System.debug('Opportunity Debug:' + oppCon);
            Contact cont = [SELECT FirstName, LastName, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingStreet, MobilePhone, HomePhone, Email, Preferred_Method_of_Contact__c FROM Contact WHERE Id = :oppCon.ContactId];
            System.debug('Contact Debug:'+ cont);
            return cont;
        } catch(Exception npe){
            system.debug('Excpetion------->'+npe);
            return null;
        }
    }

    @AuraEnabled
    public static void updateContact(Contact contactToUpdate){
        //updates primary or secondary contact
        system.debug('update contact'+ contactToUpdate);
        Database.update(contactToUpdate);
    }

    @AuraEnabled
    public static void createNewContact(String first, String last, String contEmail, String phone, String phone1, String street, String city, String state, String country, String zip){
        //creates new contact if a secondary contact doesnt exist and the user enters in new contact details
        Id userId = UserInfo.getUserId();
        User userRecord=[select id,ContactId from User where id=:userId];
        Order ord = [SELECT Id, EffectiveDate,OpportunityId, CustomerPortalUser__c,Primary_Contact__c FROM Order WHERE (CustomerPortalUser__c=:userid or (RecordType.Name='Coro Record Type' and ((Primary_Contact__c!=null and Primary_Contact__c=:userRecord.ContactId) or (Secondary_Contact__c!=null and Secondary_Contact__c=:userRecord.ContactId))))  ORDER BY createdDate DESC LIMIT 1];
        
        Contact newContact = new Contact(
            FirstName = first,
            LastName = last,
            Email = contEmail,
            MobilePhone = phone,
            HomePhone = phone1,
            MailingCity = city,
            MailingState = state,
            MailingCountry = country,
            MailingStreet = street,
            MailingPostalCode = zip
        );
        Database.insert(newContact);

        //Creates OppCon junction object to associate the new Contact to Opporutnity
        OpportunityContactRole oppCon = new OpportunityContactRole(
            OpportunityId = ord.OpportunityId,
            ContactId = newContact.Id,
            isPrimary = false
        );
        Database.insert(oppCon);
    }
     @AuraEnabled
    public static String getCurrentUserId(){
        return [SELECT Id, FullPhotoUrl  FROM User WHERE ID =: UserInfo.getUserId()].FullPhotoUrl ;
    }
    
    @AuraEnabled
    public static Contact getContactPrimaryInfo(Boolean primaryBool, String orderId) {
        Id userId = UserInfo.getUserId();
        Contact cont = new Contact();
        //finds associated Order to User using lookup field CustomerPortalUser__c
        Order ord = [SELECT Id, EffectiveDate,OpportunityId, CustomerPortalUser__c FROM Order WHERE id=:orderId  LIMIT 1];
        try{
        //finds associated OppCon to order to find the Contact
           list<OpportunityContactRole> oppCon = [SELECT Id, OpportunityId, ContactId 
                                    FROM OpportunityContactRole 
                                    WHERE OpportunityId = :ord.OpportunityId AND isPrimary = :primaryBool];
            if(!oppCon.isEmpty()){
            cont = [SELECT FirstName, LastName, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingStreet, MobilePhone, HomePhone, Email, Preferred_Method_of_Contact__c FROM Contact WHERE Id = :oppCon[0].ContactId];
            }
			return cont;

        } catch(NullPointerException npe){
            system.debug('nullPoint:' +npe);
            return null;
        }
    }
    
    @AuraEnabled
    public static Contact getSecondaryContactInfo(Boolean primaryBool, String orderId) {
        Id userId = UserInfo.getUserId();
        //finds associated Order to User using lookup field CustomerPortalUser__c
        Contact cont;
        System.debug('secondary contact details:'+ primaryBool);
        Order ord = [SELECT Id, EffectiveDate,OpportunityId, CustomerPortalUser__c FROM Order WHERE id=:orderId  LIMIT 1];
        try{
        //finds associated OppCon to order to find the Contact
            OpportunityContactRole oppCon = [SELECT Id, OpportunityId, ContactId 
                                    FROM OpportunityContactRole 
                                    WHERE OpportunityId = :ord.OpportunityId AND isPrimary = :primaryBool];
             cont = [SELECT id,FirstName, LastName, MailingCity, MailingState, MailingPostalCode, MailingCountry, MailingStreet, MobilePhone, HomePhone, Email, Preferred_Method_of_Contact__c FROM Contact WHERE Id = :oppCon.ContactId];
            System.debug('secondary contact details for cont:'+ cont);		
            System.debug('secondary contact details for oppCon:'+ oppCon);
            if(cont.id!=Null){
                return cont;
            }else{
                return null;
            }
        } catch(Exception npe){
            System.debug('exception handler'+ npe);
            return null;
        }
    }
}