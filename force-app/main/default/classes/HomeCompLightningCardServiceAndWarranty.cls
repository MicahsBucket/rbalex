/*
* @author Jason Flippen
* @date 11/17/2020
* @description Class to provide support for the homeComponentLightningCardServiceWarranty LWC.
*
*			   Code Coverage provided by the following Test Class:
*              - HomeCompLightningCardServiceAndWarrantyTest
*/ 
public with sharing class HomeCompLightningCardServiceAndWarranty {

    @AuraEnabled(cacheable=true)
    public static List<ServiceCompletedWrapper> getServiceCompleted(String searchKey) {

        List<ServiceCompletedWrapper> wrapperList = new List<ServiceCompletedWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Install Needed'};
        String queryString = 'SELECT Id,' +
                             '       Name,' +
                             '       OrderNumber,' +
                             '       Status,' +
                             '       BillToContactId,' +
                             '       BillToContact.Name,' +
                             '       Bill_to_Contact_Text__c,' +
                             '       Service_Installation_Complete_Date__c,' +
                             '       Service_Type__c,' +
                             '       AccountId,' +
                             '       Account.Name,' +
                             '       Amount_Due__c,' +
                             '       Primary_Service_FSL__c,' +
                             '       Primary_Service_FSL__r.Name ' +
							 'FROM   Order ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'WHERE (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey OR Account.Name LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

        for (Order o : Database.query(queryString)) {
            ServiceCompletedWrapper wrapper = new ServiceCompletedWrapper(o.Id,
                                                                          o.OrderNumber,
                                                                          o.Status,
                                                                          o.BillToContactId,
                                                                          o.BillToContact.Name,
                                                                          o.Service_Installation_Complete_Date__c,
                                                                          o.Service_Type__c,
                                                                          o.AccountId,
                                                                          o.Account.Name,
                                                                          o.Amount_Due__c,
                                                                          o.Primary_Service_FSL__c,
                                                                          o.Primary_Service_FSL__r.Name);
            wrapperList.add(wrapper);
        }

        return wrapperList;
        
    }

    @AuraEnabled(cacheable=true)
    public static List<WarrantyRejectedWrapper> getWarrantyRejected(String searchKey) {

        List<WarrantyRejectedWrapper> wrapperList = new List<WarrantyRejectedWrapper>();

		String tempSearchKey = '';
		if (String.isNotBlank(searchKey)) {
			tempSearchKey = '%' + searchKey + '%';
		}
		
		Set<String> eligibleStatusSet = new Set<String>{'Warranty Rejected'};
        String queryString = 'SELECT Id,' +
                             '       Name,' +
                             '       OrderNumber,' +
                             '       Status,' +
                             '       BillToContactId,' +
                             '       BillToContact.Full_Name__c,' +
                             '       Bill_to_Contact_Text__c,' +
                             '       Service_Type__c,' +
                             '       AccountId,' +
                             '       Account.Name,' +
                             '       Amount_Due__c,' +
                             '       Warranty_Date_Submitted__c,' +
                             '       Warranty_Rejected_Date__c,' +
                             '       Rejection_Reason__c ' +
                             'FROM   Order ' +
                             'WHERE  Status IN :eligibleStatusSet ';
		
		if (String.isNotBlank(tempSearchKey)) {
			queryString += 'AND (OrderNumber LIKE :tempSearchKey OR BillToContact.Name LIKE :tempSearchKey OR Rejection_Reason__c LIKE :tempSearchKey) ';
		}

		queryString += 'ORDER BY OrderNumber ' +
					   'LIMIT 20';

        for (Order o : Database.query(queryString)) {
            WarrantyRejectedWrapper wrapper = new WarrantyRejectedWrapper(o.Id,
                                                                          o.OrderNumber,
                                                                          o.Status,
                                                                          o.BillToContactId,
                                                                          o.BillToContact.Full_Name__c,
                                                                          o.Service_Type__c,
                                                                          o.AccountId,
                                                                          o.Account.Name,
                                                                          o.Amount_Due__c,
                                                                          o.Warranty_Date_Submitted__c,
                                                                          o.Warranty_Rejected_Date__c,
                                                                          o.Rejection_Reason__c);
            wrapperList.add(wrapper);
        }

        return wrapperList;

    }


/** Wrapper Classes **/


    public class ServiceCompletedWrapper {
        
        @AuraEnabled
        public String ServiceReqNumber {get;set;}
        
        @AuraEnabled
        public String ServiceReqURL {get;set;}
        
        @AuraEnabled
        public String CustomerName {get;set;}
        
        @AuraEnabled
        public String CustomerURL {get;set;}
        
        @AuraEnabled
        public String SerReqStatus {get;set;}
        
        @AuraEnabled
        public datetime SerInstCompDate {get;set;}
        
        @AuraEnabled
        public String SerType {get;set;}
        
        @AuraEnabled
        public String AccountName {get;set;}
        
        @AuraEnabled
        public String AccountURL {get;set;}
        
        @AuraEnabled
        public Decimal AmountDue {get;set;}
        
        @AuraEnabled
        public String PrimaryServiceName {get;set;}
        
        @AuraEnabled
        public String PrimaryServiceURL {get;set;}
        
        public ServiceCompletedWrapper(Id recId,
                                       String OrdNum,
                                       String status,
                                       Id customerId,
                                       String customername1,
                                       Datetime SerInCompDate,
                                       String servicetype,
                                       Id acctId,
                                       String acctName,
                                       Decimal amountdue1,
                                       Id primaryserid,
                                       String primaryser) {
        
            ServiceReqNumber = OrdNum;
            ServiceReqURL = '/'+ recId;
            CustomerName = customername1;
            if (customerId == null) {
                CustomerURL = null;
            }
            else {
                CustomerURL= '/'+ customerId;
            }
            SerReqStatus = status;
            SerInstCompDate = SerInCompDate ;
            SerType = servicetype;
            AccountName = acctName;
            AccountURL= '/' + acctId;
            AmountDue = amountdue1; 
            PrimaryServiceName = primaryser;
            PrimaryServiceURL = '/'+ primaryserid;
            
        }
    
    }
    
    public class WarrantyRejectedWrapper {
        
        @AuraEnabled
        public String ServiceReqNumber {get;set;}
        
        @AuraEnabled
        public String ServiceReqURL {get;set;}
        
        @AuraEnabled
        public String CustomerName {get;set;}
        
        @AuraEnabled
        public String CustomerURL {get;set;}
        
        @AuraEnabled
        public String SerReqStatus {get;set;}
        
        @AuraEnabled
        public String SerType {get;set;}
        
        @AuraEnabled
        public String AccountName {get;set;}
        
        @AuraEnabled
        public String AccountURL {get;set;}
        
        @AuraEnabled
        public Decimal AmountDue {get;set;}
        
        @AuraEnabled
        public date WarrantyDateSubmitted {get;set;}
        
        @AuraEnabled
        public date WarrantyRejectionDate {get;set;}
        
        @AuraEnabled
        public String RejectionReason {get;set;}
        
        public WarrantyRejectedWrapper(Id recId,
                                       String OrdNum,
                                       String status,
                                       Id customerId,
                                       String customername1,
                                       String servicetype,
                                       Id acctId,
                                       String acctName,
                                       Decimal amountdue,
                                       Date warantySubDate,
                                       Date warrantyRejDate,
                                       String rejReason) {
            
            ServiceReqNumber = OrdNum;
            ServiceReqURL = '/'+ recId;
            CustomerName = customername1;
            if (customerId == null) {  
                CustomerURL = null;
            }
            else {
                CustomerURL= '/'+ customerId;
            }    
            SerReqStatus = status;
            SerType = servicetype;
            AccountName = acctName;
            AccountURL = '/'+ acctId;
            AmountDue = amountdue; 
            WarrantyDateSubmitted = warantySubDate ;
            WarrantyRejectionDate = warrantyRejDate;
            RejectionReason = rejReason;
            
        }
    
    }
    
}