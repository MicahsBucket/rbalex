global class BatchScheduleEnabledPAS implements Schedulable {
	global void execute(SchedulableContext sc) {
		BatchEnabledPostAppointmentSurveys b = new BatchEnabledPostAppointmentSurveys();
		database.executebatch(b);
	}
}