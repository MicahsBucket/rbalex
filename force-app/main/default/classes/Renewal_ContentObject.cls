public with sharing class Renewal_ContentObject extends Peak_ContentObject{
    @AuraEnabled public Boolean rforceAdmin{get;set;}
    @AuraEnabled public Boolean dashboardAdmin{get;set;}
}