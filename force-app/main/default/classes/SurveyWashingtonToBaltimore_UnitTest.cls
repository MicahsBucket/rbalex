@isTest
private class SurveyWashingtonToBaltimore_UnitTest {
	
	@isTest static void insertCapitalWashingtonRecordTest(){
		TestDataFactoryStatic.setUpConfigs();
		Account baltimore = TestDataFactoryStatic.createStoreAccountLarge('0085 - Baltimore');
		Account capitalWashington = TestDataFactoryStatic.createStoreAccountLarge('0097 - Capital Washington, DC - Virginia');
		Database.insert(baltimore);
		Database.insert(capitalWashington);
		
		Id baltimoreId = [SELECT Id FROM Account WHERE Name = '0085 - Baltimore' LIMIT 1].Id;
    	Id capitalWashingtonId = [SELECT Id FROM Account WHERE Name = '0097 - Capital Washington, DC - Virginia' LIMIT 1].Id;
    	
		List<Survey__c> washintonSurveys = TestDataFactory.createSurveys('Post Install', 5, capitalWashingtonId, 'Pending');
		test.startTest();
			SchedulableContext sc = null;
			SurveyWashingtonToBaltimore tsc = new SurveyWashingtonToBaltimore();
			tsc.execute(sc);
		test.stopTest();
		List<Survey__c> baltimoreSurveys = [SELECT Store_Account__c FROM Survey__c WHERE Store_Account__c = :baltimoreId];
		System.assertEquals(baltimoreId, baltimoreSurveys[0].Store_Account__c);
	}
	
}