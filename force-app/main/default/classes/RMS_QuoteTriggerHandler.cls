public with sharing class RMS_QuoteTriggerHandler {

    public static void OnBeforeDelete(Map<ID, Quote>  oldMap, Map<ID, Quote> newMap){
         checkForNoOrder(oldMap, newMap);
  }


public static void checkForNoOrder(Map<ID, Quote>  oldMap, Map<ID, Quote> newMap){
    Id profileId = userinfo.getProfileId(); 
    String profileName = [Select Id, Name, UserLicense.Name from Profile where Id=:profileId].Name;

    //if user is any one of these proflies, it will skp logic and just delete the Quote.
    if( profileName == 'Super Administrator' || profileName == 'System Administrator' || profileName == 'Data Migration' || profileName == 'RMS-Data Migration'){
        return;
    }

    List<Id> listRelatedOrderIds = new List<Id>();
    List<Order> listRelatedOrders = [Select Id, OpportunityID from Order where OpportunityID IN (Select OpportunityID from Quote where Id IN :oldMap.keySet() ) ];
    for(Order ord: listRelatedOrders ){
        listRelatedOrderIds.add(ord.Id);
    }

    List<Quote> qt1 = [Select ID, OpportunityID from Quote where OpportunityID IN (Select OpportunityID from Order where Id IN :listRelatedOrderIds ) and id IN :oldMap.keySet()];

    for(Quote qt2: qt1 ){
        Quote qt3 = oldMap.get(qt2.Id);
        if(qt3 != null){
            qt3.addError('You cannot delete a Quote which is associated to an Opportunity that has a related Order.');
                } 
            }

    }

}