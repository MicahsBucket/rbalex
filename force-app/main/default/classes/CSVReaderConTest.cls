@IsTest
private class CSVReaderConTest {
    
    
     
    public static String[] csvFileLines;
    public static Blob csvFileBody;
     static testmethod void  readFile(){
          TestDataFactoryStatic.setUpConfigs();
          Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
        Database.insert(storeAccount, true);
        Id storeAccountId = storeAccount.Id;
         Account testAccount = TestDataFactoryStatic.createAccount('Calvins', storeAccount.Id);
        Id accountId = testAccount.Id;

        // Create a contact for the store account
        Contact primaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');
        Contact SecondaryContact = TestDataFactoryStatic.createContact(accountId, 'CalvinPrime');

        // Create a test opportunity, opportunity primary contact and an order
        Opportunity opportunity = TestDataFactoryStatic.createFSLOpportunity('Calvins Opportunity', accountId, storeAccountId, 'Sold', date.today());
        OpportunityContactRole oppContJunc = TestDataFactoryStatic.createOppCon('Decision Maker', true, primaryContact, opportunity);
        Order order = TestDataFactoryStatic.createOrderTestRecords(opportunity);
         String str = 'Order Id,\n '+order.Id+'\n '+order.Id;
                 
        csvFileBody = Blob.valueOf(str);
        String csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n'); 

        CSVReaderCon importData = new CSVReaderCon();
        importData.contentFile = csvFileBody;
        importData.ReadFile();
         list<Order> ordsList =  importData.getuploadedOrders();
         importData.getInvokeBatch();
         system.assertEquals(2, ordsList.size());
         string str1 = 'Order Id,\n "dhfkjasdfh0932890" \n "9r0qew9r0qee9q0erwkj"';
        CSVReaderCon importData1 = new CSVReaderCon();
        blob csvFileBody1 = Blob.valueOf(str1);
        importData1.contentFile = csvFileBody1;
        importData1.ReadFile();
       list<Order>  ords=  importData1.getuploadedOrders();
       system.assertEquals(2, ords.size());
        
    }
    
}