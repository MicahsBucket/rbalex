@isTest
private class GoogleMapsTest {

	private static List<Id> accountIds {get;set;}

	public class GoogleMapsCalloutMock implements HttpCalloutMock{
			public HTTPResponse respond(HTTPRequest req) {
				 HttpResponse res2 = new HttpResponse();
				 res2.setHeader('Content-Type', 'application/json');
				 res2.setBody('{"destination_addresses":["San Francisco, CA, USA"],"origin_addresses":["Seattle, WA, USA"],"rows":[{"elements":[{"distance":{"text":"808 mi", "value":1299875},"duration":{"text":"12 hours 27 mins", "value":44813}, "status":"OK"}]}],"status":"OK"}');				 
				 res2.setStatusCode(200);
				 return res2;
		}
	}

	@testSetup
	static void setup(){
		
		//RMS_Settings__c customSetting2 = new RMS_Settings__c(
		//	Value__c = 'AIzaSyBSDMscoA2tQp2DhHjk5RFoCoTfzD0kllY', Name='Google API Server Key');
		//insert customSetting2;

		TestDataFactoryStatic.setUpConfigs();

		Account storeAccount = TestDataFactoryStatic.createStoreAccount('Twin Cities');
		setAccountAddress(storeAccount, '1920 County Road C West', 'Roseville', 'Minnesota', '55113');

		Account dwellingAccount = TestDataFactoryStatic.createDwellingAccount('Test Dwelling');
		setAccountAddress(dwellingAccount, '9900 Jamaica Ave S', 'Cottage Grove', 'Minnesota', '55016');

		insert new List<Account>{storeAccount, dwellingAccount};

		Opportunity opp1 = TestDataFactoryStatic.createOpportunity(dwellingAccount.id, 'Closed - Won');
    insert opp1;

		TestDataFactoryStatic.createOrderTestRecords(opp1);

		accountIds = new List<Id>{storeAccount.Id, dwellingAccount.Id};

	}
	
	@isTest static void testSetDistance() {

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new GoogleMapsCalloutMock());
		Order o = [Select Id, Route_Mileage__c from Order limit 1];
		Id oId = o.Id;
		GoogleMaps.setRouteDistance(o.Id);

		Test.stopTest();

		o = [Select Id, Route_Mileage__c from Order where Id = :oId];

		System.assertEquals(o.Route_Mileage__c, 808);

	}

	static void setAccountAddress(Account a, String street, String city, String state, String zip){
		a.ShippingStreet = street;
		a.ShippingCity = city;
		a.ShippingState = state;
		a.ShippingPostalCode = zip;
		a.ShippingCountry = 'United States';
	}
	
}