/*************************************************************
@brief  When an insurance exp date is updated on a vendor account,
any inactive resources for that vendor are reactivated

@author  Brianne Wilson (Slalom.BLW)

@version    2016-5-20  Slalom.BLW
Created.


@copyright  (c)2016 Slalom.  All Rights Reserved. 
Unauthorized use is prohibited.

***********************************************************/

public class RMS_reactivateResource {
    
    public void reactivateResource(List<Account> listOld, List<Account> listNew, Map<Id, Account> mapOld, Map<Id, Account> mapNew){

		// First check if any account's inactive date has been filled in
		Set<Id> acctIdsInactiveEntered = new Set<Id>();
		for(Account a: listNew){
            if(mapNew.get(a.Id).Inactive_Date__c != null && mapNew.get(a.Id).Inactive_Date__c != mapOld.get(a.Id).Inactive_Date__c){
            	if(mapNew.get(a.Id).Inactive_Date__c >= system.TODAY()){
					acctIdsInactiveEntered.add(a.Id);
            	}
			}
		}
		
		// If no account's inactive date has been filled in, just return
		if (acctIdsInactiveEntered.size() == 0) return;

        List<ServiceResource> childRecords = [SELECT Id, Vendor_Account_Id__c, Inactive_Reason__c
                                                FROM ServiceResource WHERE IsActive = false AND Vendor_Account_Id__c IN :Trigger.newMap.keySet()];
        
        system.debug(childRecords);
        
        Map<Id,ServiceResource> resourceMap = new Map<Id,ServiceResource>([SELECT Id FROM ServiceResource WHERE Id IN :childRecords]);
        
        for(ServiceResource child :childRecords){
            if(!resourceMap.IsEmpty()){   
                if(mapNew.get(child.Vendor_Account_Id__c).Inactive_Date__c != null && mapNew.get(child.Vendor_Account_Id__c).Inactive_Date__c != mapOld.get(child.Vendor_Account_Id__c).Inactive_Date__c){
                   if(mapNew.get(child.Vendor_Account_Id__c).Inactive_Date__c >= system.TODAY()){
                    child.IsActive = TRUE;                    
                    child.Inactive_Reason__c = null;                    
                    }
                }                              
            }
        }
        
        if(childRecords.size() > 0){
            update childRecords;
        }                    
    }
}