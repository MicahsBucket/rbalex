@isTest
private class RMS_customPOLookupControllerTest{

    public static final String dwellingName = 'Dwelling Account';
    public static final String postInsertDwellingName = '11111, ' + dwellingName;
    
    @testSetup static void setupData() {
        TestUtilityMethods utility = new TestUtilityMethods();
        
        utility.setUpConfigs();
        
        Account account1 = utility.createVendorAccount('Vendor Account 1');
        insert account1;

        Account account2 = new Account( Name = 'RbA',
                                        AccountNumber = '1234567890',
                                        Phone = '(763) 555-2000'
                                    );
        insert account2;

        Account dwelling = utility.createDwellingAccount(dwellingName);

        Account store = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        dwelling.Store_Location__c = store.Id;
        insert dwelling;

        
        Opportunity opp1 = utility.createOpportunity(dwelling.id, 'Closed - Won');
        insert opp1;
           
        Financial_Account_Number__c fan = new Financial_Account_Number__c(Name ='Finan Acc', Account_Type__c = 'Cost PO');
        insert fan;
        Product2 product1 = new Product2(
            Name='Test Product',
            Vendor__c = account1.id,
            Cost_PO__c = true,
            isActive = true,
            Account_Number__c =  fan.Id
        );
        
        insert product1;
        
        PricebookEntry pricebookEntry1 = utility.createPricebookEntry(Test.getStandardPricebookId(), product1.id);
        insert pricebookEntry1;

        Vendor_Product__c vendProd = new Vendor_Product__c(
                                            Vendor__c = account1.id,
                                            Product__c = product1.id
                                            );
        insert vendProd;
        
        List<Order> orderList =  new List<Order> { new Order (Name='Order1', 
                                    AccountId = dwelling.id, 
                                    EffectiveDate= Date.Today(), 
                                    Store_Location__c = store.Id,
                                    Opportunity = opp1,                                 
                                    Status ='Ready to Order', 
                                    Tech_Measure_Status__c = 'New',
                                    Pricebook2Id = Test.getStandardPricebookId()
                                )};     
        insert orderList;


        List<OrderItem> oiList = new List<OrderItem>{new OrderItem(OrderId = OrderList[0].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 1, UnitPrice = 100, Product2Id = product1.Id  ),
                new OrderItem(OrderId = OrderList[0].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 2, UnitPrice = 100, Product2Id = product1.Id ),
                new OrderItem(OrderId = OrderList[0].Id, PricebookentryId = pricebookEntry1.Id, Quantity = 3, UnitPrice = 100, Product2Id = product1.Id )};
        insert oiList;

  

        // Turn off the financial trigger to avoid SOQL limits in test class
        RMS_Settings__c turnOffFinancialTrigger = new RMS_Settings__c(Name='Turn Financial Transactions Off', Value__c = 'Yes');
        insert turnOffFinancialTrigger;

  
        List<Purchase_Order__c> poList = new List<Purchase_Order__c>();

        for(Order o: orderList){
            if(o.Name == 'Order1'){
                poList.add(new Purchase_Order__c(Order__c = o.Id, Status__c = 'In Process', Vendor__c = account1.Id));
                poList.add(new Purchase_Order__c(Order__c = o.Id, Status__c = 'New', Vendor__c = account1.Id));
            }
        }
        insert poList;

  }
        /*******************************************************
                    SearchTest
    *******************************************************/
    static testmethod void SearchTest(){

        Test.startTest();

        Order soldOrder = [SELECT Id, Name FROM Order WHERE Name = 'Order1' LIMIT 1];
        OrderItem oitest = [Select Id, Product2Id from OrderItem WHERE Order.Name = 'Order1'  LIMIT 1];
        List<Purchase_Order__c> po = [Select Id  from Purchase_Order__c WHERE Order__r.Name = 'Order1'];
        
        PageReference pageRef = Page.RMS_customPOLookup;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('prodid', oitest.Product2Id);
        ApexPages.currentPage().getParameters().put('orderid', soldOrder.Id);
        RMS_customPOLookupController customController  = new RMS_customPOLookupController();

        Test.stopTest();
    }

            /*******************************************************
                    SearchTest with No PO or Vendor Product
    *******************************************************/
    static testmethod void SearchTestNoPO(){

        Test.startTest();

        Order soldOrder = [SELECT Id, Name FROM Order WHERE Name = 'Order1' LIMIT 1];
        OrderItem oitest = [Select Id, Order.Name, Product2Id from OrderItem WHERE Order.Name = 'Order1'  LIMIT 1];
        Product2 prod = [Select Id, Vendor__c from Product2 WHERE Name = 'Test Product' LIMIT 1];
        prod.Vendor__c = null;
        update prod;

        List<Purchase_Order__c> po = [Select Id  from Purchase_Order__c WHERE Order__r.Name = 'Order1'];
        delete po;

        
        PageReference pageRef = Page.RMS_customPOLookup;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('prodid', oitest.Product2Id);
        ApexPages.currentPage().getParameters().put('orderid', soldOrder.Id);
        RMS_customPOLookupController customController  = new RMS_customPOLookupController();

        Test.stopTest();
    }

}