global class BatchSchedulePostAppointmentSurvey implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        BatchUsPostAppointmentSurvey usBatch = new BatchUsPostAppointmentSurvey();
        BatchCanandaPostAppointmentSurvey canadaBatch = new BatchCanandaPostAppointmentSurvey();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(usBatch);
        database.executebatch(canadaBatch);
    }
}