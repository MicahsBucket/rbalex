/******************************************************************
 * Author : Micah Johnson - Demand Chain 1/2021
 * Class Name :closeJobButtonController
 * Createdate : 1/26/2021
 * Description : This class is used to Get order information and Close a job in relation to the LEX "Job Closed" action button on Order Layout
 * Test Class : closeJobButtonControllerTEST
 * 
 * ----------------------------------------------------------------------------
 * *****************************************************************/

public without sharing class closeJobButtonController {
    /// getOrder method: when "Job Closed" button is clicked on Order Record Page, Order Id is passed in 
    /// and returns Id, Status, and Revenue_Recognized_Date__c 
    @AuraEnabled
    public static Order getOrder(Id recordId) {
        System.debug(recordId);
        Order o = [SELECT Id, Status, Revenue_Recognized_Date__c FROM Order WHERE Id =: recordId];
        return o;
    }

    /// closeJob method: When "Close Job" button is clicked, the Id of the Order to close is passed into the method
    /// Order Status and Job_Close_Date__c are updated (if no errors) and the returnText is sent back to the Javascript controller
    @AuraEnabled
    public static string closeJob(Id OrderId){
        System.debug('OrderId in closeJobButtonController '+ OrderId);
        Order o = [SELECT Id, Status, Apex_Context__c, Job_Close_Date__c  FROM Order WHERE Id =: OrderId];
        o.Status = 'Job Closed';
        o.Apex_Context__c = true;
        o.Job_Close_Date__c = Date.today();

        string returnText = '';
        try{
            update o;
            returnText = 'Job Closed';
        }
        catch (Exception ex) {
            returnText = 'ERROR CLOSING JOB';
        }
        return returnText;
    }
}