global class MA_SchedulerAssignedResourceCleanUpBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global MA_SchedulerAssignedResourceCleanUpBatch() {
		query = 'SELECT Id FROM RbA_Work_Order__c';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<RbA_Work_Order__c> scope) {
		Set<Id> workOrderIds = new Set<Id>();
		Set<Id> workOrdersAssigned = new Set<Id>();
		Map<Id, Set<Id>> workOrderToResourcesMap = new Map<Id, Set<Id>>();
		Map<Id, Set<Id>> workOrderToResourcesNeededMap = new Map<Id, Set<Id>>();
		List<Assigned_Resources__c> resourcesToUpsert = new List<Assigned_Resources__c>();

		for (RbA_Work_Order__c workOrder : scope) {
			workOrderIds.add(workOrder.Id);
		}

		workOrdersAssigned = workOrderIds.clone();

		for (MA_Scheduler_Event__c maEvent : [SELECT Id, RbA_Work_Order__c, Resource__c FROM MA_Scheduler_Event__c
												WHERE RbA_Work_Order__c IN :workOrderIds]) {
			
			if (maEvent.Resource__c == null) {
				workOrdersAssigned.remove(maEvent.RbA_Work_Order__c);
				workOrderToResourcesNeededMap.remove(maEvent.RbA_Work_Order__c);
			}
			else if (workOrdersAssigned.contains(maEvent.RbA_Work_Order__c)) {
				if (!workOrderToResourcesNeededMap.containsKey(maEvent.RbA_Work_Order__c)) {
					workOrderToResourcesNeededMap.put(maEvent.RbA_Work_Order__c, new Set<Id>());
				}
				workOrderToResourcesNeededMap.get(maEvent.RbA_Work_Order__c).add(maEvent.Resource__c);
			}
		}

		for (Assigned_Resources__c assignedResource : [SELECT Id, Work_Order__c, Work_Order__r.All_Days_Assigned__c, Scheduled_Resource__c FROM Assigned_Resources__c
														WHERE Work_Order__c IN :workOrderIds]) {

			if (!workOrderToResourcesMap.containsKey(assignedResource.Work_Order__c)) {
				workOrderToResourcesMap.put(assignedResource.Work_Order__c, new Set<Id>());
			}
			workOrderToResourcesMap.get(assignedResource.Work_Order__c).add(assignedResource.Scheduled_Resource__c);

			if (!assignedResource.Work_Order__r.All_Days_Assigned__c) {
				assignedResource.Scheduled_For_Entire_Work_Order__c = true;
				resourcesToUpsert.add(assignedResource);
			}
		}

		for (RbA_Work_Order__c workOrder : scope) {
			if (workOrdersAssigned.contains(workOrder.Id)) {
				workOrder.All_Days_Assigned__c = true;

				if (workOrderToResourcesNeededMap.containsKey(workOrder.Id)) {
					for (Id resourceId : workOrderToResourcesNeededMap.get(workOrder.Id)) {
						if (workOrderToResourcesMap.containsKey(workOrder.Id) && !workOrderToResourcesMap.get(workOrder.Id).contains(resourceId)) {
							resourcesToUpsert.add(
								new Assigned_Resources__c(
									IsPrimary__c = false,
		                            Work_Order__c = workOrder.Id,
		                            Scheduled_Resource__c = resourceId,
		                            Scheduled_For_Entire_Work_Order__c = false
								)
							);
						}
					}
				}
			}
			else {
				workOrder.All_Days_Assigned__c = false;
			}
		}

		upsert resourcesToUpsert;
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}