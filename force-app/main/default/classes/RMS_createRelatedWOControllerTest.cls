@isTest
public with sharing class RMS_createRelatedWOControllerTest {


    static testMethod void testingConstructor(){
        
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        id dwellingRT = UtilityMethods.retrieveRecordTypeId('Dwelling', 'Account');
        Account store = [SELECT id FROM Account WHERE Name ='77 - Twin Cities, MN'];
        Account newDwelling = new Account(Name='test1', Store_Location__c = store.id, RecordTypeId = dwellingRT, BillingPostalCode = '55429' , BillingCity = 'Blaine' );
        insert newDwelling;
        Id orderServiceRecordTypeId = UtilityMethods.retrieveRecordTypeId('CORO_Service', 'Order');
        //CREATE AN ORDER RELATED TO OPPORTUNITY
        Order ord1 = new Order( AccountId = newDwelling.id ,
                                Status = 'Draft', 
                                EffectiveDate = Date.Today(),
                                Pricebook2Id = Test.getStandardPricebookId()
                                );
        insert ord1;
        
         Order ord2 = new Order( AccountId = newDwelling.id ,
								Status = 'New', 
								EffectiveDate = Date.Today(),
                                RecordTypeId = orderServiceRecordTypeId,
                               Service_Type__c= 'Legacy',
								Pricebook2Id = Test.getStandardPricebookId()
								);
		insert ord2;
        
        PageReference pageRef = new PageReference('/apex/RMS_createRelatedWorkOrders'); 
        Test.setCurrentPage(pageRef);
            
        ApexPages.StandardController stdController = new ApexPages.StandardController(ord1);
        RMS_createRelatedWorkOrdersController customController  = new RMS_createRelatedWorkOrdersController(stdController);
        
        ApexPages.StandardController stdController2 = new ApexPages.StandardController(ord2);
        RMS_createRelatedWorkOrdersController customController2  = new RMS_createRelatedWorkOrdersController(stdController2);
        customController2.createWorkOrder();
        customController2.removeLineBreaks('Renewal by Anderson');
        customController2.serviceOrderVisitRecordTypeURL = '/apex/RMS_newWorkOrderRedirect?type=jobSiteVisit';

    }


}