/**
 * This class is a utility class for creating test data
 *
 */
@isTest
public class testUtilitySSOcode {
   
    public static Contact createCommunityContact(String testName, Account a, String emailAddress, Boolean saveToDB) {
	System.debug('in testUtility createCommunityContact');
    // create the contact
    Id rtId = '01261000000SG0j';
    Contact c = new Contact();
    c.RecordTypeId = rtId;
    c.Email = emailAddress;
    c.FirstName = 'Test';
    c.LastName = testName + 'SSO Contact' + System.currentTimeMillis();
    //c.PersonId__c = testName.hashCode();
    c.AccountId = a.Id;
   
    if(saveToDB) {
      insert c;
    }
    return c;
    }

	 
	 public static Account createStoreAccount(String acctName, String enabledStore){ 
        Account theAccount = new Account(
            Name = acctName,
            AccountNumber = acctName+'1234567890',
            Phone = '(763) 555-2000',
		    Enabled_StoreId__c = enabledStore,
            recordTypeId = UtilityMethods.retrieveRecordTypeId('Store', 'Account'));
        insert theAccount;
		return theAccount;
    }

    public static Map<String,String> populateAttributeMap(String testName) {
	System.debug('in populateAttributeMap in testUtility');
      Map<String,String> attributes = new Map<String,String>();
    String uid = UserInfo.getUserId();
    User currentUser =[SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
    attributes.put('User.Username', 'user.name@testsso.com.' + testName);
	attributes.put('User.FirstName', 'Firstname' + testName);
	attributes.put('User.Lastname', 'Lastname' + testName);
    attributes.put('User.FederationID', 'fedId' + testName);
    attributes.put('User.Phone', '123-456-7890');
    attributes.put('User.email', 'user.name@testsso.com.' + testName);
    attributes.put('User.Title', 'Operations Leader');
    attributes.put('User.AboutMe', 'This is not about me');
    attributes.put('User.Street', '123 Some Lane');
    attributes.put('User.State', 'MN');
    attributes.put('User.City', 'Anywhere');
    attributes.put('User.Zip', '44444');
    attributes.put('User.Country', 'USA');
    attributes.put('User.MobilePhone', '987-654-3210');
    attributes.put('User.Department', '12345' + testName);
    attributes.put('User.Extension', '4455');
    attributes.put('User.CommunityNickname', 'atest');
    attributes.put('User.ReceivesInfoEmails', 'false');
    attributes.put('User.LocaleSidKey', currentUser.LocaleSidKey);
    attributes.put('User.LanguageLocaleKey', 'en_us');
    attributes.put('User.Alias', 'atest');
    attributes.put('User.TimeZoneSidKey', currentUser.TimeZoneSidKey);
    attributes.put('User.EmailEncodingKey', 'ISO-8859-1');
    attributes.put('User.IsActive', 'true');
    attributes.put('User.email', 'user.name@testsso.com.' + testName);
	attributes.put('User.DefaultStore', '800');
	attributes.put('User.AllStores', '535,800');
	attributes.put('User.RenewUIsAllowed', 'true');
    //attributes.put('Contact.PersonId', String.valueOf(testName.hashCode()));
	System.debug('populateAttributeMap for User: ' + currentUser);
	System.debug('attribute string: ' + attributes);
    return attributes;
    }
}