global class BatchSchedulePostServiceSurvey implements Schedulable {
    
        global void execute(SchedulableContext sc)
    	{
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        BatchJobPostServiceSurvey obj = new BatchJobPostServiceSurvey();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(obj, 5);
    	}

}