global class ScheduledEmailNotice implements Schedulable{
    //This class will send an email to the Compliance Coordinator listed on a Store Configuration page if the contractor license is within 60 days of expiration.
    
    global void execute(SchedulableContext sc) {
        sendmail();
    }

    public void sendmail() {
        Date expire = Date.today();
        expire = expire.addDays(60);

        //get only the store configurations that have a license expiring within 60 days
        List<Store_Configuration__c> storeConfig = [select Id, Contractor_License_1__c, Contractor_License_1_Expiration__c, Contractor_License_2__c, Contractor_License_2_Expiration__c, Compliance_Coordinator__c from Store_Configuration__c where Contractor_License_1_Expiration__c < :expire or Contractor_License_2_Expiration__c < :expire];

        if(storeConfig.isEmpty()) return;

        //have user id, store config.  want User, store config

        //user Id : User
        List<Id> ccIds = new List<Id>();
        for(Store_Configuration__c sc : storeConfig) {
            ccIds.add(sc.Compliance_Coordinator__c);
        }

        Map<Id, User> complianceCoordinators = new Map<Id, User>();
        for(User u : [select Id, Email from User where Id in :ccIds]) {
            complianceCoordinators.put(u.Id, u);
        }
        
        //User : Config
        Map<User, Store_Configuration__c> scMap = new Map<User, Store_Configuration__c>();
        for(Store_Configuration__c sc : storeConfig) {
            scMap.put(complianceCoordinators.get(sc.Compliance_Coordinator__c), sc);
        }
        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(Store_Configuration__c sc : storeConfig) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            EmailTemplate templateId = [Select id from EmailTemplate where name = 'Contract Expiration Notification Alert'];
            mail.setTemplateId(templateId.Id);
            String[] ccemail = new String[] {complianceCoordinators.get(sc.Compliance_Coordinator__c).Email};
            mail.setToAddresses(ccemail);
            //mail.setReplyTo();
            mail.setSenderDisplayName('Renewal By Anderson Support');
            
            mail.setBccSender(false);

            allmsg.add(mail);            
        }
        Messaging.sendEmail(allmsg, false);



    }
}