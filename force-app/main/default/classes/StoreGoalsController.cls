/**
 *@author Connor Davis
 *@description This class is used to query data for the store goals page, and if needed, create new store goals for locations/years that don't have them yet
 */
public with sharing class StoreGoalsController {

    // This method cannot have '(cacheable=true)' since it involves DML statements
    /**
     *@author Conor Davis
     *@description This method Query's the database for the goals for the passed in year and location, if they don't exist yet, it generates them, then updates the totals
     *@param year This is the year that the user wants the goals for
     *@param loc This is the location the user wants the goals for
     *@return Returns the list of goals for the specified year and location
     */
    @AuraEnabled
    public static List<Store_Goal__c> getStoreGoalsImperative(Integer year, String loc) {

        // Query the database for the goals of the specified year and location
        List<Store_Goal__c> goals = getStoreGoalsWire(String.valueof(year), loc);

        // If the goals don't exist yet, create them
        if (goals.size() < 14) {
            goals = generateGoals(year, loc, goals.size());
        }

        // Update the totals if the year is not a previous year
        if (year != Date.Today().Year() - 1) {
            // The last element in the goals List (aka the totals element)
            Integer totalGoal = goals.size()-1;
            // Reset Totals for recalculation
            goals[totalGoal] = new Store_Goal__c(Id = goals[totalGoal].Id, Month__c = 13, Year__c = year, Store_Configuration__c = getStoreConfig(loc), Installed_Revenue__c = 0, Sold_Units__c = 0, Average_Sales_Price__c = 0, Sold_Dollars__c = 0,
                                                        Issued_Appointments__c = 0, Series_1_Windows__c = 0, Series_2_Windows__c = 0, Entry_Doors__c = 0, Patio_Doors__c = 0, Installed_Units__c = 0);
            // This loop does all the additions we need
            for (Integer i = 0; i < totalGoal; i++) {
                // Calculates the sold units
                goals[i].Sold_Units__c = goals[i].Series_1_Windows__c + goals[i].Series_2_Windows__c + goals[i].Patio_Doors__c + goals[i].Entry_Doors__c;

                // Calculates totals for each field
                goals[totalGoal].Installed_Revenue__c += goals[i].Installed_Revenue__c;
                goals[totalGoal].Sold_Units__c += goals[i].Sold_Units__c;
                goals[totalGoal].Sold_Dollars__c += goals[i].Sold_Dollars__c;
                goals[totalGoal].Issued_Appointments__c += goals[i].Issued_Appointments__c;
                goals[totalGoal].Series_1_Windows__c += goals[i].Series_1_Windows__c;
                goals[totalGoal].Series_2_Windows__c += goals[i].Series_2_Windows__c;
                goals[totalGoal].Entry_Doors__c += goals[i].Entry_Doors__c;
                goals[totalGoal].Patio_Doors__c += goals[i].Patio_Doors__c;
                goals[totalGoal].Installed_Units__c += goals[i].Installed_Units__c;
                goals[totalGoal].Average_Sales_Price__c += goals[i].Average_Sales_Price__c;
            }

            goals[totalGoal].Average_Sales_Price__c = goals[totalGoal].Average_Sales_Price__c / (totalGoal);

            // Update each fields total
            update goals[totalGoal];
        }

        return goals;
    }

    /**
     *@author Connor Davis
     *@description This function just queries the database for the Store Goals of the passed in year and location. This is needed to use @wire in the js file and refreshApex()
     *@param year The year that the user wants the goals for
     *@param loc The location that the user wants the goals for
     *@return Returns a list of the Store Goals for the specified year and location
     */
    @AuraEnabled(cacheable=true)
    public static List<Store_Goal__c> getStoreGoalsWire(String year, String loc) {

        // Query the database for all Store Goals that are for the passed in location and year
        List<Store_Goal__c> goals = [SELECT Id, Month__c, Installed_Revenue__c, Installed_Units__c, Average_Sales_Price__c, Sold_Units__c, Sold_Dollars__c, Issued_Appointments__c, Series_1_Windows__c, Series_2_Windows__c, Entry_Doors__c, Patio_Doors__c FROM Store_Goal__c
                WHERE (Year__c = :integer.valueof(year) AND Store_Configuration__r.Store__r.Name = :loc)
                ORDER BY Month__c];

        return goals;
    }


    /**
     *@author Connor Davis
     *@description This function gets the locations that the user has
     *@return Returns the store locations the user is associated with as a string with the values seperated by semicolons
     */
    @AuraEnabled(cacheable=true)
    public static String getUserLocations() {

        // Query the database for all the user's locations
        String locations = [SELECT Store_Locations__c FROM User WHERE Id = :UserInfo.getUserId()].Store_Locations__c;
        return locations;
    }

    /**
     *@author Connor Davis
     *@description This function gets the user's default store location
     *@return Returns the users default store location
     */
    @AuraEnabled(cacheable=true)
    public static String getUserDefaultLocation() {

        // Query the database for the user's default location
        String location = [SELECT Default_Store_Location__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Default_Store_Location__c;
        return location;
    }

    /**
     *@author Connor Davis
     *@description This function gets the type of the passed in store location
     *@param loc The location that the we want the type of
     *@return Returns the type of store the passed in location is
     */
    @AuraEnabled(cacheable=true)
    public static String getStoreType(String loc) {
        String type = [SELECT Type FROM Account WHERE Name = :loc LIMIT 1].Type;
        System.debug('Type: ' + type);
        return type;
    }

    /**
     *@author Connor Davis
     *@description This function gets the Store Configuration associated with the specified location
     *@param loc the location that we need the Store Configuration of
     *@return Returns the Id of the Store Configuration associated with the specified location
     */
    @AuraEnabled(cacheable=true)
    public static Id getStoreConfig (String loc) {

        try {
            // Query the database for the passed in locations Store Configuration
            Id config = [SELECT Id FROM Store_Configuration__c WHERE Store__r.Name = :loc].Id;
            return config;

        } catch (Exception e) {
            return null;
        }
    }

    /**
     *@author Connor Davis
     *@description This function checks if a store location has a store configuration
     *@param loc is the location that we are checking
     *@return Returns true if the location has a store config, false if it doesn't
     */
    @AuraEnabled(cacheable=true)
    public static Boolean checkStoreConfig (String loc) {
        Id config = getStoreConfig(loc);
        System.Debug('Config: ' + config);
        if (config != null) {
            return true;
        } else {
            return false;
        }
    }
   
    // This method does not have (cacheable=true) because it has a DML statement
    /**
     *@author Connor Davis
     *@description This function generates new goals for the specified year and location, then inserts them in the database
     *@param year This is the year that we are generating goals for
     *@param loc This is the location that we are generating goals for
     *@param numGoals This is how many goals exist for this year and location already, it only has importance when a location has changed from ARO to CORO
     *@return Returns a list of the newly created goals
     */
    @AuraEnabled
    public static List<Store_Goal__c> generateGoals(Integer year, String loc, Integer numGoals) {
        try {
                // Set the value that will determine how many goals to generate to 13 by default
                Integer goalPeriods = 13;
                // Create an empty list of Store Goals
                List<Store_Goal__c> goals = new List<Store_Goal__c>();
                // Get the Store Configuration the goals should be attatched to
                Id storeConfig = getStoreConfig(loc);
                // Get the type of the store
                String type = getSToreType(loc);
                // If the store is a CORO location, generate 14 goals instead of 13
                if (type == 'CORO') {
                    goalPeriods = 14;
                }

                // This should only be different than goalPeriods in the rare case that an ARO becomes a CORO
                Integer goalsToGenerate = goalPeriods-numGoals;
            
                // Create 13 or 14 goals, setting month to i, year to the passed in year, and Store_Configuration__c to the correct Store Configuration. Month number 13 or 14 will be used the totals
                for (Integer i = 1; i <= goalsToGenerate; i++) {

                    goals.add(new Store_Goal__c(Month__c = i, Year__c = year, Store_Configuration__c = storeConfig));
                }

                // Put the newly created goals in the database
                insert goals;

                // Make sure the goals made it in the database
                List<Store_Goal__c> newGoals = getStoreGoalsWire(String.valueof(year), loc);

                return newGoals; 

        } catch (Exception e) {
            
            // Put the error in the debug log
            System.debug(Logginglevel.ERROR, e.getMessage());
            // Throw the error
            throw new AuraHandledException(e.getMessage());
        }
    }
}