global class SizeDetailConfiguration {
    public String  frame {get;set;}
    public String  hardwareOption {get;set;}    
    public String  specialtyShape {get;set;}
    public String  sashOperation {get;set;}
    public String  sashRatio {get;set;}
    public Integer externalForce {get;set;}
    public Integer internalForce {get;set;}
    public Boolean highPerformance {get;set;}    
}