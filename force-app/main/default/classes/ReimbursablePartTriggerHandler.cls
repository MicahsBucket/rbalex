/*
* @author Jason Flippen
* @date 01/13/2021
* @description: Class to handle all Reimbursable_Part__c Trigger actions.
*
*              Code Coverage provided by the following Test Class:
*              - ReimbursablePartTriggerHandlerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
public with sharing class ReimbursablePartTriggerHandler {

    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to handle the "After Insert" functionality.
    * @param newList - List of inserted Reimbursable_Part__c records
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static void afterInsert(List<Reimbursable_Part__c> newList) {

        handleOrderPartsReimbursementTotalRollUps(newList);

    }

    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to handle the "After Delete" functionality.
    * @param oldList - List of deleted Reimbursable_Part__c records.
    * @param oldMap - Map of deleted Reimbursable_Part__c records.
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static void afterDelete(List<Reimbursable_Part__c> oldList, Map<Id,Reimbursable_Part__c> oldMap) {

        handleOrderPartsReimbursementTotalRollUps(oldList);

    }

    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to handle the rolling up of Reimbursable Part costs for related Orders.
    * @param reimbursablePartList - List of Reimbursable_Part__c records that have been inserted/deleted.
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    private static void handleOrderPartsReimbursementTotalRollUps(List<Reimbursable_Part__c> reimbursablePartList) {

        // Grab a Set of Ids of the Orders related to the Reimbursable Part records.
        Set<Id> orderIdSet = new Set<Id>();
        for (Reimbursable_Part__c reimbursablePart : reimbursablePartList) {
            orderIdSet.add(reimbursablePart.Order__c);
        }

        if (!orderIdSet.isEmpty()) {

            // Build an Order (Id)-to-Reimbursable Part (List) Map.
            Map<Id,List<Reimbursable_Part__c>> orderReimbursablePartListMap = new Map<Id,List<Reimbursable_Part__c>>();
            for (Reimbursable_Part__c reimbursablePart : [SELECT Id,
                                                                 Cost__c,
                                                                 Order__c
                                                          FROM   Reimbursable_Part__c
                                                          WHERE  Order__c IN :orderIdSet]) {
                
                if (!orderReimbursablePartListMap.containsKey(reimbursablePart.Order__c)) {
                    orderReimbursablePartListMap.put(reimbursablePart.Order__c, new List<Reimbursable_Part__c>());
                }
                orderReimbursablePartListMap.get(reimbursablePart.Order__c).add(reimbursablePart);

            }

            processOrderPartsReimbursementTotalRollUps(orderIdSet, orderReimbursablePartListMap);

        }

    }

    /*
    * @author Jason Flippen
    * @date 01/13/2021
    * @description: Method to handle the rolling up of Reimbursable Part costs for related Orders.
    * @param orderIdSet - Set of Order Ids.
    * @param orderReimbursablePartListMap - Order (Id)-to-Reimbursable Part (List) Map.
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    private static void processOrderPartsReimbursementTotalRollUps(Set<Id> orderIdSet,
                                                                   Map<Id,List<Reimbursable_Part__c>> orderReimbursablePartListMap) {
        
        // Iterate through the Set of impacted Order Ids and
        // grab a List of Order records to be Updated.
        List<Order> updateOrderList = new List<Order>();
        for (Id orderId : orderIdSet) {

            Decimal totalPartCost = 0;
            if (orderReimbursablePartListMap.containsKey(orderId)) {

                // Iterate through the List of Reimbursable Part records related
                // to this Order and add their Cost to the totalPartCost.
                for (Reimbursable_Part__c reimbursablePart : orderReimbursablePartListMap.get(orderId)) {
                    Decimal partCost = 0;
                    if (reimbursablePart.Cost__c != null) {
                        partCost = reimbursablePart.Cost__c;
                    }
                    totalPartCost += partCost;
                }

            }

            // Update the Order and add it to our List
            // of Order records to be Updated.
            Order updateOrder = new Order(Id = orderId,
                                          Reimbursement_Parts__c = totalPartCost);
            updateOrderList.add(updateOrder);

        }
        System.debug('***** updateOrderList: ' + updateOrderList);

        // If we have Order records to update, update them.
        if (!updateOrderList.isEmpty()) {
            update updateOrderList;
        }

    }

}