@isTest
public without sharing class TechMeasureWOControllerTest {

    @TestSetup
    static void testSetup() {
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account acc = utility.createDwellingAccount('Dwelling Test Account');
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        acc.Store_Location__c = store1.Id;
        insert acc;

        Contact contact = new contact (FirstName = 'Contact', LastName = 'Test ', AccountId = acc.Id, Primary_Contact__c = true);
        insert contact;

        Opportunity opp = new Opportunity();
        opp.Name = 'UnitTest Opportunity';
        opp.StageName = 'New';
        opp.CloseDate = Date.today() + 1;
        opp.AccountId = acc.Id;
        insert opp;

        Id pricebookId = Test.getStandardPricebookId();

        Order soldOrder = new Order();
        soldOrder.Name = 'Test Order';
        soldOrder.Pricebook2Id = pricebookId;
        soldOrder.AccountId = acc.Id;
        soldOrder.OpportunityId = opp.Id;
        soldOrder.EffectiveDate = Date.today();
        soldOrder.Status = 'Draft';
        insert soldOrder;

    }

    @isTest
    static void testConstructorAndRedirect() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'UnitTest Opportunity'];

        Test.startTest();
        PageReference pageRef = Page.NewTechMeasureWORedirect;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', opp.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        TechMeasureWOController controller = new TechMeasureWOController(sc);
        Test.stopTest();

        System.assertNotEquals(null, controller.redirect());
    }
}