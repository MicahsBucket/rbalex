public class AvailableResource implements Comparable {
    @AuraEnabled public Id id;
    @AuraEnabled public String name;
    @AuraEnabled public Decimal grade;
    @AuraEnabled public String photoUrl;
    @AuraEnabled public Datetime start;
    @AuraEnabled public Datetime finish;

    public AvailableResource(ServiceResource sr, Decimal grade) {
        this.id = sr.Id;
        this.name = sr.Name;
        this.photoUrl = sr.RelatedRecord.SmallPhotoUrl;
        this.grade = grade;
    }

    // Compare AvailableResources based on name.
    public Integer compareTo(Object compareTo) {
        // Cast argument to AvailableResource
        AvailableResource ar = (AvailableResource)compareTo;
        return this.name.compareTo(ar.name);
    }
}