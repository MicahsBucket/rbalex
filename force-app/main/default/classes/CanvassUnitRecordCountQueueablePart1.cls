global class CanvassUnitRecordCountQueueablePart1 implements Queueable, Database.AllowsCallouts
{
    private string marketId;
    private string zipCode;
    private string queuedJobId;
    private list<string> recordsAlreadyViewed;
    
    global CanvassUnitRecordCountQueueablePart1(string CUMarketId, string CUZipCode, string queuedJobIdString)
    {
        marketId = CUMarketId;
        zipCode = CUZipCode;
        queuedJobId = queuedJobIdString;
        recordsAlreadyViewed = new list<string>();
    }
    
    global CanvassUnitRecordCountQueueablePart1(string CUMarketId, string CUZipCode, string queuedJobIdString, list<string> CURecordsAlreadyViewed)
    {
        marketId = CUMarketId;
        zipCode = CUZipCode;
        queuedJobId = queuedJobIdString;
        recordsAlreadyViewed = CURecordsAlreadyViewed;
    }
    
    global void execute(QueueableContext context)  
    {
        try
        {
            list<CNVSS_Canvass_Unit__c> listOfOldRecords = [SELECT Id
                                                            FROM CNVSS_Canvass_Unit__c 
                                                            WHERE CNVSS_Canvass_Market__c = :marketId 
                                                            AND Auto_Created__c = false 
                                                            AND CNVSS_Zip_Code__c = :zipCode
                                                            AND Id NOT IN :recordsAlreadyViewed
                                                            LIMIT 49999];
            
            for(CNVSS_Canvass_Unit__c CU :listOfOldRecords)
            {
                recordsAlreadyViewed.add(CU.Id);
            }
            
            if(listOfOldRecords.size() == 49999)
            {
                // make recursive call
                System.enqueueJob(new CanvassUnitRecordCountQueueablePart1(marketId,zipCode,queuedJobId,recordsAlreadyViewed));
            }
            else
            {
                // Object to create / update with 
                list<Custom_Data_Layer_Batch_Queue__c> canvassUnitBatchJobQueue = [SELECT Id, Name, Old_Record_Count__c 
                                                                                   FROM Custom_Data_Layer_Batch_Queue__c
                                                                                   WHERE Id = :queuedJobId];
                                                                                 
                for(Custom_Data_Layer_Batch_Queue__c canvassUnitBatchJob :canvassUnitBatchJobQueue)
                {
                    canvassUnitBatchJob.Old_Record_Count__c = recordsAlreadyViewed.size();
                }
                
                update canvassUnitBatchJobQueue;
                
                // Call new queueable
                System.enqueueJob(new CanvassUnitRecordCountQueueablePart2(marketId,zipCode,queuedJobId));
            }
        }
        catch(Exception e)
        {
            // Object to create / update with 
            list<Custom_Data_Layer_Batch_Queue__c> canvassUnitBatchJobQueue = [SELECT Id, Name, Canvass_Market_Batch_Job__c, Old_Records_Not_Updated_Count__c 
                                                                               FROM Custom_Data_Layer_Batch_Queue__c
                                                                               WHERE Id = :queuedJobId];

            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :canvassUnitBatchJobQueue)
            {
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Failed';
            }
            
            update canvassUnitBatchJobQueue;
        }
    }
}