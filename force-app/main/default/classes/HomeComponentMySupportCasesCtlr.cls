public with sharing class HomeComponentMySupportCasesCtlr {

    @AuraEnabled(cacheable=true)
    public static List<MySupportCases> getMySupportCasesList(string searchKey) {

        List<MySupportCases> mscList = new List<MySupportCases>();
        Id userID = UserInfo.getUserId();
        String sTempSearchKey = '%' + searchKey + '%';
                for (Case msc : [SELECT Id,
                                       CaseNumber,
                                       Subject,
                                       Status,
                                       CreatedDate,
                                       LastModifiedDate,
                                       ClosedDate
                                FROM   Case 
                               WHERE   (OwnerId = : userId) 
                                 AND   (CaseNumber LIKE : sTempSearchKey  OR Subject LIKE : sTempSearchKey OR Subject LIKE : sTempSearchKey)
                            ORDER BY   LastModifiedDate DESC]) {

                                       Date previousWeekDate = Date.today().addDays(-7);
                                       Boolean addToList = true;
                                       if(msc.Status == 'Closed' && msc.ClosedDate < previousWeekDate) {
                                           addToList = false;
                                       }

                                       if(addToList == true) {
                                            mscList.add(new MySupportCases(msc.Id, msc.CaseNumber, msc.Subject, msc.Status, msc.CreatedDate, msc.LastModifiedDate));
                                       }

                             }

                             return mscList;
        }

        public class MySupportCases{
        
        @AuraEnabled
        public String CaseNumber{get;set;}
        
        @AuraEnabled
        public String CaseNumberURL{get;set;}
        
        @AuraEnabled
        public String Subject{get;set;}
              
        @AuraEnabled
        public String Status{get;set;}

        @AuraEnabled
        public DateTime CreatedDate{get;set;}

        @AuraEnabled
        public DateTime LastModifiedDate{get;set;}
        
        public MySupportCases(id Cid, string CNum, string CSubject, string CStatus, datetime CCreatedDate,  datetime CLastModifiedDate){
        
            CaseNumber = CNum;
            CaseNumberURL = '/'+ Cid;
            Subject = CSubject;
            Status = CStatus;
            CreatedDate = CCreatedDate;
            LastModifiedDate = CLastModifiedDate;
            }
        }

}