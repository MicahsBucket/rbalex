public class CanvassHomeController { 
//Victor Edit Begin   
public String userThumbPhoto; // load into a <img src='[here]'> in component
private static String theUserId = UserInfo.getUserId();
//Victor Edit End
    public CanvassMyRouteController myRouteController{
        get
        {
            if(myRouteController == null) {
                myRouteController = new CanvassMyRouteController();
            }
            return myRouteController;
        }
        set;
    }
    
    public CanvassHomeController(){
        
    }
    
    public integer getHomesToVisitToday(){
        return myRouteController.getTotalHomesExcludingDoNotKnock();
    }
    
    public integer getHomesVisitedToday(){
        return myRouteController.getResultedHomes();
    }
    
    private string getDashboardLinkByTitle(string title) {
        string link = '/';
        string id = '';
        Dashboard db = null;
        List<Dashboard> dashboards = [SELECT ID, Title FROM Dashboard Where Title = :title LIMIT 1];
        if(!dashboards.isEmpty()) {
            db = dashboards[0];
            id = db.Id;
            link += id;
        } else {
            link += 'apex/CNVSS_CanvassHome';
        }
        return link;
    }

    //Victor Edit Begin
    //getter - the acutal thumbnail photo url from the db
    public String getUserThumbPhoto() {
        //query with the user id
        String thePhoto = [SELECT SmallPhotoUrl FROM User WHERE User.ID = :theUserId LIMIT 1].SmallPhotoUrl;
        //System.debug('The photo url: '+thePhoto);
        return thePhoto;
    } 
    //Victor Edit End   
        
        // Paul's New Code
        public PageReference MyResultsLink(){
            	string dashboardLink = getDashboardLinkByTitle('My Results: Canvassing Stats');
                PageReference Link = new PageReference(dashboardLink);
                return Link;
        }
        public PageReference MyRankingLink(){
                string dashboardLink = getDashboardLinkByTitle('My Ranking: Canvassing Leaderboards');
                PageReference Link = new PageReference(dashboardLink);
                return Link;
        }
        public PageReference MyTeamResultsLink(){
            string dashboardLink = getDashboardLinkByTitle('Team Results');
            PageReference Link = new PageReference(dashboardLink);
            return Link;
        }
        public PageReference CrossMarketLink(){
            string dashboardLink = getDashboardLinkByTitle('Cross Market Dashboard');
            PageReference Link = new PageReference(dashboardLink);
            return Link;
    	}
        
}