@isTest(SeeAllData=true) 
// need see all data for custom setting validation rules on user creation... please populate the Data Loading Profile ID error.
public class CustomerHeroCtrlTest {
    @isTest
    public static void testGetUserName(){
        User testUser = createNewUser();
        String myName;
        test.startTest();
        system.runAs(testUser){
            myName = CustomerHeroCtrl.getUserName();
        }
        test.stopTest();
        system.assertEquals(testUser.FirstName, myName);
    }
	
    public static User createNewUser(){
        Id pId = [SELECT id from Profile WHERE Name = 'Super Administrator' ].id;        
        User u = new User();
        u.Email = 'test1.email@test.com';
        u.FirstName = 'tests';
        u.LastName = 'testLastNames';
        u.Alias = 'test';
        u.Username= 'test1.email@test.com';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId = pId;
        try{
           insert u; 
            system.debug('successful insert of test user');
        } catch (exception ex){
            system.debug('exception in unit test ' + ex.getMessage());
        }         
        return u;
    }
}