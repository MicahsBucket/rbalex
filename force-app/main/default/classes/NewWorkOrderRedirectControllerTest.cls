/**
* @author Jason Flippen
* @date 01/29/2021
* @description Test Class for the following Classes:
*              - NewWorkOrderRedirectController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/ 
@isTest
public class NewWorkOrderRedirectControllerTest {

    /**
    * @author Jason Flippen
    * @date 01/29/2021 
    * @description Method to create data to be consumed by test methods.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @testSetup
    static void setupData() {

        TestUtilityMethods testUtility = new TestUtilityMethods();
        testUtility.setUpConfigs();
        
        Account testVendorAccount = testUtility.createVendorAccount('Test Vendor Account');
        insert testVendorAccount;
        
        Account testStoreAccount = [SELECT Id FROM Account WHERE Name = '77 - Twin Cities, MN'];
        Account testDwellingAccount = testUtility.createDwellingAccount('Test Dwelling Account');
        testDwellingAccount.Store_Location__c = testStoreAccount.Id;
        insert testDwellingAccount;

        Contact testContact = new Contact(FirstName = 'Test',
                                          LastName = 'Contact',
                                          HomePhone = '1111111111',
                                          MobilePhone = '2222222222',
                                          Primary_Contact__c = true,
                                          Primary_Dwelling_for_Contact__c = true,
                                          AccountId = testDwellingAccount.Id);
        insert testContact;

        Store_Configuration__c testStoreConfiguration = [SELECT Id FROM Store_Configuration__c WHERE Store__c = :testStoreAccount.Id];
        
        Opportunity testOpportunity = testUtility.createOpportunity(testDwellingAccount.Id, 'Closed - Won');
        insert testOpportunity;
        
        Financial_Account_Number__c testFAN = new Financial_Account_Number__c(Name ='Test FAN',
                                                                              Account_Type__c='Cost PO',
                                                                              Store_Configuration__c = testStoreConfiguration.Id);
        insert testFAN;

        List<Financial_Transaction__c> testFinancialTransactionList = new List<Financial_Transaction__c>();
        Financial_Transaction__c testFinancialTransaction01 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Deposit');
        testFinancialTransactionList.add(testFinancialTransaction01);
        Financial_Transaction__c testFinancialTransaction02 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Discount');
        testFinancialTransactionList.add(testFinancialTransaction02);
        Financial_Transaction__c testFinancialTransaction03 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - AR');
        testFinancialTransactionList.add(testFinancialTransaction03);
        Financial_Transaction__c testFinancialTransaction04 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - Refund Deposit');
        testFinancialTransactionList.add(testFinancialTransaction04);
        Financial_Transaction__c testFinancialTransaction05 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - External Labor');
        testFinancialTransactionList.add(testFinancialTransaction05);
        Financial_Transaction__c testFinancialTransaction06 = new Financial_Transaction__c(Credit_Account_Number__c = testFan.Id,
                                                                                           Debit_Account_Number__c = testFAN.Id,
                                                                                           Store_Configuration__c = testStoreConfiguration.Id,
                                                                                           Transaction_Type__c = 'Rev Recognized - COGS');
        testFinancialTransactionList.add(testFinancialTransaction06);
        insert testFinancialTransactionList;
        
        Product2 testProduct = new Product2(Name = 'Test Product',
                                            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Legacy_Service').getRecordTypeId(),
                                            Vendor__c = testVendorAccount.Id,
                                            Product_PO__c = true,
                                            Cost_PO__c = true,
                                            IsActive = true,
                                            Account_Number__c =  testFan.Id);
        insert testProduct;
        
        Pricebook2 testPricebook = testUtility.createPricebook2Name('Standard Price Book');
        insert testPricebook;
        
        // Parent PricebookEntry
        List<PricebookEntry> testPBEList = new List<PricebookEntry>();
        PricebookEntry testPBEStandard = testUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id);
        testPBEList.add(testPBEStandard);
        PricebookEntry testPBE = testUtility.createPricebookEntry(testPricebook.Id, testProduct.Id);
        testPBEList.add(testPBE);
        insert testPBEList;
        
        Order testOrder =  new Order(Name = 'Test Order',
                                     AccountId = testDwellingAccount.Id,
                                     BillingStreet = '123 Somewhere Street',
                                     BillToContactId = testContact.Id,
                                     EffectiveDate = Date.Today(),
                                     OpportunityId = testOpportunity.Id,
                                     Pricebook2Id = Test.getStandardPricebookId(),
                                     RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('CORO_Record_Type').getRecordTypeId(),
                                     Revenue_Recognized_Date__c = Date.today(),
                                     Status = 'Install Scheduled',
                                     Store_Location__c = testStoreAccount.Id,
                                     Tech_Measure_Status__c = 'New');
        insert testOrder;

    }

    /**
    * @author Jason Flippen
    * @date 02/01/2021
    * @description: Method to test the "getOrderData" method in the Controller.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetOrderData() {

        Order testOrder = [SELECT Id FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            Order order = NewWorkOrderRedirectController.getOrderData(testOrder.Id);
            System.assertEquals(testOrder.Id, order.Id, 'Unexpected Order returned');

        Test.stopTest();

    }

    /**
    * @author Jason Flippen
    * @date 02/01/2021
    * @description: Method to test the "getWorkOrderTypeList" method in the Controller.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testGetWorkOrderList() {

        Order testOrder = [SELECT Id, RecordTypeId FROM Order WHERE Name = 'Test Order'];

        Test.startTest();

            List<NewWorkOrderRedirectController.WorkOrderRecordTypeWrapper> orderRecordTypeList = NewWorkOrderRedirectController.getWorkOrderRecordTypeList(testOrder);
            System.assertEquals(true, orderRecordTypeList.size()>1, 'Unexpected Work Order Record Type List returned');

        Test.stopTest();

    }

    /**
    * @author Jason Flippen
    * @date 02/01/2021
    * @description: Method to test the "createWorkOrder" method in the Controller.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    public static testMethod void testCreateWorkOrder() {

        Order testOrder = [SELECT Id,
                                  AccountId,
                                  RecordTypeId,
                                  Sold_Order__c
                           FROM   Order
                           WHERE  Name = 'Test Order'];

        Test.startTest();

            Id installWORTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Install').getRecordTypeId();
            Map<String,String> resultMap = NewWorkOrderRedirectController.createWorkOrder(testOrder, installWORTId);
            String createResult = '';
            for (String result : resultMap.keySet()) {
                createResult = result;
            }
            System.assertEquals('New Work Order Success', createResult, 'Unexpected Create Work Order Result');

        Test.stopTest();

    }

}