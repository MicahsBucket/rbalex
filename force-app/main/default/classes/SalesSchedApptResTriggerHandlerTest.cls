/* 
* @author: Ramya Bangalore
* @Created Date : 08/10/2019 
* @Description: SalesSchedApptResTriggerHandlerTest Test Class for SalesAppointmentResourceTriggerHandler
* Date Modified           Modified By             Description of the update
* *******************************************************************************/
@isTest
public class SalesSchedApptResTriggerHandlerTest {
    @isTest
    public static void SalesAppointmentResourceTriggerHandlerTest(){
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        sstu.createAppointments(1);
        List<Sales_Appointment_Resource__c> resources = new List<Sales_Appointment_Resource__c>();
        resources.add(
            new Sales_Appointment_Resource__c(
                Sales_Appointment__c = sstu.appointments.get(0).Id,
                Status__c = 'Manually Assigned',
                Assignment_Reason__c = 'Manual',
                Primary__c = true
            )
        );
        insert resources;
        
        Test.startTest();
        
        //Update Scenario
        resources[0].Sales_Capacity__c  = sstu.capacities.get(0).Id;
        resources[0].isApptConfirmedOrAccepted__c = true;
        update resources;
        
        //Delete scenario
        delete resources;
        
        Test.stopTest();
    }
}