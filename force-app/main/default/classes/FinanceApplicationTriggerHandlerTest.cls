@isTest
private class FinanceApplicationTriggerHandlerTest{
    static testMethod void testFATrigger(){
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();

        Account acc = new Account(
            Name = 'Test Acc'
        );
        insert acc;
        Contact contact = new Contact(
            AccountId = acc.Id,
            LastName = 'TestContact',
            LASERCA__Social_Security_Number__c = '000-00-0001',
            LASERCA__Co_Applicant_Social_Security_Number__c = '000-00-0002'
        );
        insert contact;
        Finance_Application__c fa = new Finance_Application__c(
            Customer__c = contact.Id,
            Cash_Deposit__c = 123, 
            Contract_Amount_2__c = 123, 
            Status__c = 'New'
        );
        Test.startTest();
        insert fa;
        update fa;
        Test.stopTest();
    }
}