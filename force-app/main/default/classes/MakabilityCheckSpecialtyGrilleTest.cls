/**
 * @File Name          : MakabilityCheckSpecialtyGrilleTest.cls
 * @Description        : 
 * @Author             : mark.rothermal@andersencorp.com
 * @Group              : 
 * @Last Modified By   : mark.rothermal@andersencorp.com
 * @Last Modified On   : 5/13/2019, 9:15:07 AM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    5/13/2019, 9:15:07 AM   mark.rothermal@andersencorp.com     Initial Version
**/
@isTest
public class MakabilityCheckSpecialtyGrilleTest {
    @isTest
    public static void passingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSpecialtyShapeGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,0,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSpecialtyGrilleConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results passing makabilty ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Specialty Grille Config - passed');
    }
    @isTest
    public static void noMatchingProductIdTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSpecialtyShapeGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,50,50,0,'1');
        set<id> productIds = new set<id>();
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSpecialtyGrilleConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingProductIdTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Salesforce - No Specialty Grille configurations found for this productId.');
    }
    @isTest
    public static void failingMakabilityTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSpecialtyShapeGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = createOrderItem(pc.Product__c,70,70,1,'1');
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSpecialtyGrilleConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results failingMakabilityTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Specialty Grille Config - You have selected an invalid grille configuration. Please consult technical documentation for valid pattern, style,lite, hub & spoke configurations for the specified specialty shape');        
       
    }  
    @isTest
    public static void noMatchingAnythingTest() {
        List<Product_Configuration__c> pros = createTestProConfig(1);
        Product_Configuration__c pc = pros.get(0);
        createTestSpecialtyShapeGrilleConfig(pc);
        Map<string,MakabilityRestResource.OrderItem> configsToTest = new Map<string,MakabilityRestResource.OrderItem>();
        set<id> productIds = new set<id>();
        productIds.add(pc.Product__c);
        test.startTest();
 	    List<MakabilityRestResource.MakabilityResult> results = MakabilityCheckSpecialtyGrilleConfig.checkCompatibility(configsToTest,productIds);
        test.stopTest();
        system.debug('results noMatchingAnythingTest ' + results);
        string assertMsg = results[0].errorMessages[0];
        system.assertEquals(assertMsg, 'Something bad happened with the Specialty Grille config');        
    }       

    private static List<Product2> createTestProduct(Integer num){
        List<Product2> pros = new List<Product2>();
        for(Integer i = 0; i < num;i++){
            Product2 p = new Product2();
            p.name = 'test product ' + i;
            pros.add(p);            
        }        
        insert pros;
        return pros;
    }
    
    private static List<Product_Configuration__c> createTestProConfig(Integer num){
        List<Product2> pros = createTestProduct(num);
        List<Product_Configuration__c> pcs = new List<Product_Configuration__c>();
        for(Integer i = 0 ; i < num; i++){
            Product_Configuration__c pc = new Product_Configuration__c();
            pc.name = 'test config ' + i;
            pc.Tempered_Glass_Required_at_UI__c = 100;	
            pc.Product__c = pros[i].id;  
            pcs.add(pc);
        }
        insert pcs;
        return pcs;
    }
    
   private static void createTestSpecialtyShapeGrilleConfig(Product_Configuration__c pc){
       Specialty_Grille_Configuration__c sgc = new Specialty_Grille_Configuration__c(); 
        sgc.Product_Configuration__c = pc.id;
        sgc.Grille_Pattern__c = 'Gothic';
		sgc.Grille_Style__c = 'Full Divided Light (FDL with spacer)';
        sgc.Hubs__c = 0;
        sgc.Lites_High__c = 0;
        sgc.Lites_Wide__c = 0;
        sgc.Specialty_Shape__c ='Circle Top';
        sgc.Spokes__c = 0;
        insert sgc;	
   }

    private static Map<String,MakabilityRestResource.OrderItem>  createOrderItem(Id prodId, Decimal wInch, Decimal hInch, decimal spokes, String oiId){
        Map<string,MakabilityRestResource.OrderItem> orderItems = new Map<String,MakabilityRestResource.OrderItem>();
        MakabilityRestResource.OrderItem oi = new MakabilityRestResource.OrderItem();
        MakabilityCalculator mc = new MakabilityCalculator();
		ProductConfiguration pc = new ProductConfiguration();
        pc.widthInches = wInch;
        pc.widthFractions = 'Even';
        pc.heightInches = hInch;
        pc.heightFractions = 'Even';        
        pc.grillePattern = 'Gothic';
        pc.grilleStyle = 'Full Divided Light (FDL with spacer)';
        pc.specialtyShape= 'Circle Top';
        pc.spokes = spokes;
        pc.hubs = 0;
        pc.litesHigh = 0;
        pc.litesWide = 0;
        mc.checkSpecialGrille = true;        
        /////////////////////       
        pc.productId = prodId; 
        oi.orderItemId = oiId;
        oi.makabilityCalculator = mc;
        oi.productConfiguration = pc;
        orderItems.put(oiId,oi);       
        return orderItems;
    }
}