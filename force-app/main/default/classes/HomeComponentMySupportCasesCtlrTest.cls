@isTest
public with sharing class HomeComponentMySupportCasesCtlrTest {
    
    @testSetup static void setupData() {
 
        TestUtilityMethods testUtility = new TestUtilityMethods();
                testUtility.setUpConfigs();
        
        
                Case testCase2 =  new Case(Subject = 'Test Case2');
                insert testCase2;
                
            } // End Method: setupData()
        
            /**
            * @author Wallace Wylie
            * @description Method to test the functionality in the Controller.
            * @param N/A
            * @returns N/A
            */ 
            private static testMethod void testController() {
        
                Test.startTest();

                    Id userId = UserInfo.getUserId();
        
                    List<Case> testCaseList = [SELECT Id FROM Case WHERE OwnerId = : userId];
            
                    List<HomeComponentMySupportCasesCtlr.MySupportCases> mscList = HomeComponentMySupportCasesCtlr.getMySupportCasesList('0');
                    System.assertEquals(1,mscList.size());
        
        
                Test.stopTest();
        
            } // End Method: testController()
        

}