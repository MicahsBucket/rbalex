public class MakabilityCheckOpenWideHinge {    
    
    public static openWideHingeResult  checkOpenWideHinge( MakabilityRestResource.OrderItem request,  Size_Detail_Configuration__c currentConfig, map<string,Product_Field_Control__c> uniKeyToPfcMap ){
        openWideHingeResult result = new openWideHingeResult();
        boolean makabilityPasses = true;
        result.errMessages = new List<string>();
        ProductConfiguration p = request.ProductConfiguration;
        Boolean configRequiresWideHinge = false;
        
        // determine if lock values are required
        if(uniKeyToPfcMap.containsKey(p.productId + '-' + 'Hardware Option')){
            Product_Field_Control__c pfc = uniKeyToPfcMap.get(p.productId + '-' + 'Hardware Option');
            if(pfc.required__c){
                configRequiresWideHinge = true;
            }
        }
        
        //**********start makability flow*************\\
        
        if(configRequiresWideHinge){
            // Open wide Hinge  Makability specific variables
            if(currentConfig.WOH_Required_Below_Inches__c!=null &&currentConfig.WOH_Allowed_Above_Inches__c!=null) 
            {
                
                Double convertedWidth = p.widthInches + Constants.fractionConversionMap.get(p.widthFractions);
                
                Double convertedRequiredOpenWidth = currentConfig.WOH_Required_Below_Inches__c+ Constants.fractionConversionMap.get(currentConfig.WOH_Required_Below_Fractions__c);
                Double convertedAllowedOpenWidth  = currentConfig.WOH_Allowed_Above_Inches__c + Constants.fractionConversionMap.get(currentConfig.WOH_Allowed_Above_Fractions__c);
                
                if(p.hardwareOption=='wide opening hinge')
                {
                    
                    if(convertedWidth<convertedAllowedOpenWidth)
                    {
                        if(convertedWidth>convertedRequiredOpenWidth )
                        {
                            makabilityPasses = false;
                            result.errMessages.add('A Wide Opening Hinge  is not allowed on a unit of this width ');
                        }
                        
                    }
                    else if(convertedWidth<convertedRequiredOpenWidth)
                    {
                        makabilityPasses = false;
                        result.errMessages.add('You Must Have a Wide Opening Hinge on a unit of this width');
                        
                    }
                    
                    
                }
                else
                {
                
                if(convertedWidth<convertedRequiredOpenWidth)
                    {
                        makabilityPasses = false;
                        result.errMessages.add('You Must Have a Wide Opening Hinge on a unit of this width');
                    }
                }
                
                
                
                
                
                
            }
            //************end makability flow***************\\
        }
        if(makabilityPasses){
            result.productMakable = true;
            //    result.errMessages.add('Check Lock Size - passed');
            
        } else{
            result.productMakable = false;
        }
        return result;
    }
    
    
    public class  openWideHingeResult{
        public  Boolean productMakable {get;set;}
        public  List<String>  errMessages {get;set;}
    }
    
    
    
}