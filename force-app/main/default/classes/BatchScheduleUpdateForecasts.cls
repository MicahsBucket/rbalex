// Code coverage provided by: AerisWeatherForecastHelperTest.cls

global without sharing class BatchScheduleUpdateForecasts implements Schedulable {
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new BatchUpdateServiceAppointmentForecasts());
    }
}