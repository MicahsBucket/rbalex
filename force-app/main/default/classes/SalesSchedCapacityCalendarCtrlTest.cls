@isTest
private class SalesSchedCapacityCalendarCtrlTest {
	static testmethod void capacityCalendarCtrlTestmethod()
    {
        Date d = Date.today().toStartOfMonth();
        
        SalesSchedTestUtils sstu = new SalesSchedTestUtils();
        sstu.createSalesManagersAndReps(1,1);
        Test.startTest();
        SalesSchedCapacityCalendarCtrl.MonthWrapper sw = null;
        SalesSchedCapacityCalendarCtrl.Week sweek = null;
        System.runAs(sstu.reps.get(0)){
        	sw = SalesSchedCapacityCalendarCtrl.getMonth(((Datetime)d).formatGmt('yyyy-MM-dd'),sstu.store.Id);
            sweek = SalesSchedCapacityCalendarCtrl.getWeek(((Datetime)d).formatGmt('yyyy-MM-dd'),sstu.store.Id);
            SalesSchedCapacityCalendarCtrl.addCapacity(sstu.reps.get(0).Id,String.valueOf(System.today()),sstu.slot.Id,sstu.store.Id);
        }
        Test.stopTest();
    }
}