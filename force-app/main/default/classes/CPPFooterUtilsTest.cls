@isTest
public class CPPFooterUtilsTest {
    static String fName='RBAIntegration';
    static Contact contact1=new Contact();
    static User newUser=new User(); 
    @testSetup
    static void testSetup(){
        List<Account> dwellingsToInsert = new List<Account>();     
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        //insert dwelling1;     
        
        Opportunity testOpp=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp;
        Opportunity testOpp2=utility.createOpportunity('Testing Opportunity',dwelling1.id,'New',UserInfo.getUserId(),System.today());
        insert testOpp2;
        
        Account store1 = [SELECT Id,Active_Store_Configuration__c from Account Where Name = '77 - Twin Cities, MN'];
        System.debug('_____store1______'+store1);        
        Store_Configuration__c storeConfig1 = [SELECT id,Invitation_Delay_Days__c,Activate_Community__c FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        storeConfig1.Sales_Tax__c = 5;
        storeConfig1.Activate_Community__c=true;
        storeConfig1.Invitation_Delay_Days__c=0;
        storeConfig1.Portal_Activation_Date__c=System.today()-1;
        storeConfig1.Remit_to_Company_Name__c ='TestLocation';
        update storeConfig1;
        dwelling1.Store_Location__c = store1.Id;
        dwellingsToInsert.add(dwelling1);
        insert dwellingsToInsert;        
        
        ID contactRType=[select id from RecordType where SobjectType='Contact' and name='Customer'].id;
        ID contactCustomerRType=[select id from RecordType where SobjectType='Contact' and name='Customer Contacts'].id;
        List<Contact> contactsToInsert = new List<Contact>();       
         
        contact1 = new contact (Email='RatnaTest@Ratnatest.com', FirstName=fName,LastName='1',AccountId=dwelling1.id, Primary_Contact__c =true,RecordTypeId=contactRType);
        contactsToInsert.add(contact1);
        insert contactsToInsert;
        
        List<OpportunityContactRole> oppRoles=new List<OpportunityContactRole>();
        OpportunityContactRole role1=new OpportunityContactRole(contactId=contact1.id,OpportunityId=testOpp.Id,Role='Decision Maker');
        oppRoles=new List<OpportunityContactRole>{role1};
        insert oppRoles;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'));
        
        Id pricebookId = Test.getStandardPricebookId();
        system.debug(pricebookId);
        
        Pricebook2 pricebook1 =  utility.createPricebook2Name('Standard Price Book');
        insert pricebook1;
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
            
        List<Order> ordersToInsert = new List<Order>();
        ID orderRType=[select id from RecordType where SobjectType='Order' and name='CORO Record Type'].id;
        Order order1 =  new Order(   Name='Sold Order', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 Primary_Contact__c=contact1.id,
                                 OpportunityId=testOpp.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = Test.getStandardPricebookId(),
                                 Customer_Pickup_All__c = FALSE,
                                 Installation_Date__c = system.today()-1,
                                 RecordTypeId=orderRType
                                );
        ordersToInsert.add(order1);
        insert ordersToInsert;
        String profName ='Customer Community Login Primary Profile';
        String profName2 ='Customer Community Login Secondary Profile';
        //String profName2 ='Customer Community Login Profile';
        Profile p = [Select Id,Name from Profile where Name =: profName Limit 1];
        Profile p2 = [Select Id,Name from Profile where Name =: profName2 Limit 1];
        Id portalProfileId = p.Id;
        Id portalProfileId2 = p2.Id;
        newUser = new User(
                            UserName = contact1.Email+'.myproject',
                            LastName = contact1.LastName,
                            FirstName = fName,
                            Email = contact1.Email,
                            phone = contact1.Phone,
                            MobilePhone = contact1.MobilePhone,
                            Street = contact1.MailingStreet,
                            City = contact1.MailingCity,
                            State = contact1.MailingState,
                            Country = contact1.MailingCountry,
                            PostalCode = contact1.MailingPostalCode,
                            CommunityNickName ='.myproject',
                            Alias = 'alias',
                            Store_Location_Text__c='storeName',
                            Selected_Order__c=order1.ID,//mapBillToContactWithOrder.get(conHis.Contact__c).Id,
                            /* link to the contact and to the account through the contact... */
                            ContactId = contact1.Id,
                            
                            //ProfileId = (mapSecondaryContactWithOrder.containsKey(conHis.Contact__c)?portalProfileId2:portalProfileId),
                            ProfileId =portalProfileId,
                            
                            /* various user settings */
                            emailencodingkey = 'UTF-8',
                            languagelocalekey = 'en_US',
                            localesidkey = 'en_US',
                            timezonesidkey = 'America/Los_Angeles'
                        );
        insert newUser;
        System.debug('_____newuser______'+newUser);
    }
    @isTest
    public static void submitRBAFormTest()
    {
        test.startTest();
        contact1=[select id,name,firstname,lastname,email,AccountID from Contact where Firstname=:fName];
        newUser=[select Id,name,ContactID from User where Firstname=:fName];
        Test.setMock(HttpCalloutMock.class, new MockRBAFormRequest());
        CPPFooterUtils.submitRBAFrom(JSON.serialize(contact1));
        
        HttpFormBuilder.WriteFile('testingfile', 'value', 'mimeType', blob.valueOf('contentTypeCrLf'));
        System.runAs(newUser) {
            CPPFooterUtils.loadFooterDetails();
        }
        test.stopTest();
    }
}