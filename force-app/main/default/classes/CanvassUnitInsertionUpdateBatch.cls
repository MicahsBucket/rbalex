global class CanvassUnitInsertionUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable 
{
    //database.executebatch(new CanvassUnitInsertionUpdateBatch(),1);
    
    //CanvassUnitInsertionUpdateBatch rsb = new CanvassUnitInsertionUpdateBatch();
    //String sch = '0 0 * * * ?';
    //system.schedule('CanvassUnit Insertion Update Batch 00', sch, rsb);
    
    //CanvassUnitInsertionUpdateBatch rsb = new CanvassUnitInsertionUpdateBatch();
    //String sch = '0 15 * * * ?';
    //system.schedule('CanvassUnit Insertion Update Batch 15', sch, rsb);
    
    //CanvassUnitInsertionUpdateBatch rsb = new CanvassUnitInsertionUpdateBatch();
    //String sch = '0 30 * * * ?';
    //system.schedule('Canvass Unit Insertion Update Batch 30', sch, rsb);
    
    //CanvassUnitInsertionUpdateBatch rsb = new CanvassUnitInsertionUpdateBatch();
    //String sch = '0 45 * * * ?';
    //system.schedule('Canvass Unit Insertion Update Batch 45', sch, rsb);
    
    global CanvassUnitInsertionUpdateBatch()
    {

    }
    
    global void execute(SchedulableContext SC)
    {
        database.executeBatch(new CanvassUnitInsertionUpdateBatch(),1);
    }
    
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        // query to get holding jobs from queue
        return Database.getQueryLocator ([SELECT Id, Name, Market_Id__c, Status__c, ZipCode__c 
                                          FROM Custom_Data_Layer_Batch_Queue__c 
                                          WHERE Status__c = 'Waiting for Insertion And Update']);
    }
    
    global void execute (Database.BatchableContext BC, List<Custom_Data_Layer_Batch_Queue__c> scope)
    {
        // query to get currently running jobs
        list<AsyncApexJob> listOfRunningJobs = [SELECT Status, NumberOfErrors, ApexClassID, CompletedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, TotalJobItems 
                                                FROM AsyncApexJob 
                                                WHERE Status != 'Completed' AND Status != 'Aborted' AND Status != 'Failed'];
        // if running jobs less than  50
        if(listOfRunningJobs.size() < 50 || Test.isRunningTest() == true)
        {
            // iterate over scope
            for(Custom_Data_Layer_Batch_Queue__c queuedJob :scope)
            {
                if (!Test.isRunningTest())
                {
                    // start queueable job
                    System.enqueueJob(new CanvassUnitInsertionUpdateQueueable(queuedJob.Market_Id__c,queuedJob.ZipCode__c,queuedJob.Id));
                }
                
                // update status so that job won't be queued again
                queuedJob.Status__c = 'Insertion And Update In Progress';
                update queuedJob;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}