@isTest(SeeAllData=false)
global class CanvassUnitInsertionUpdateBatchTest {

    public static testMethod void test1()
    {   
        Custom_Data_Layer_Batch_Queue__c canvassDataLayerQueue = new Custom_Data_Layer_Batch_Queue__c();
        canvassDataLayerQueue.Status__c = 'Waiting for Insertion And Update';
        canvassDataLayerQueue.Field_Ids__c = '123, 456, 789';
        canvassDataLayerQueue.Marker_Ids__c = '123, 456, 789'; 
        insert canvassDataLayerQueue;
        
        Test.startTest();
        //database.executebatch(new CanvassUnitQueueableBatch(),1);
        
        CanvassUnitInsertionUpdateBatch rsb = new CanvassUnitInsertionUpdateBatch();
        String sch = '0 0 * * * ?';
        system.schedule('CanvassUnit Insertion Update Batch 999', sch, rsb);
    
        Test.stopTest();
    }
}