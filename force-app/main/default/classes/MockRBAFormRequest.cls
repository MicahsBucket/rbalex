@isTest
global class MockRBAFormRequest implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"500","data":"Error","message":"Missing required field: Zip or Zipcode."}');
        res.setStatusCode(200);
        return res;
    }
}