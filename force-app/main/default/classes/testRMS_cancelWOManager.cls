@isTest
public without sharing class testRMS_cancelWOManager {

    @TestSetup
    static void createTestRecords() {
        TestUtilityMethods utility = new TestUtilityMethods();
        utility.setUpConfigs();
        
        List<Account> dwellingsToInsert = new List<Account>();  
        Account dwelling1 = utility.createDwellingAccount('Dwelling Account');
        Account dwelling2 = utility.createDwellingAccount('Dwelling Account 2');
        Account store1 = [SELECT Id from Account Where Name = '77 - Twin Cities, MN'];
        Store_Configuration__c storeConfig1 = [SELECT id FROM Store_Configuration__c WHERE Store__c = :store1.id ];
        dwelling1.Store_Location__c = store1.Id;
        dwelling2.Store_Location__c = store1.Id; 
        dwellingsToInsert.add(dwelling1);
        dwellingsToInsert.add(dwelling2);
        insert dwellingsToInsert; 
        
        List<Contact> contactsToInsert = new List<Contact>();       
        Contact contact1 = new contact ( FirstName='Contact',LastName='1',AccountId=dwelling1.id);
        contactsToInsert.add(contact1);
        insert contactsToInsert;
        
        Financial_Account_Number__c finacialAccountNumber1 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '1');
        insert finacialAccountNumber1;
        Financial_Account_Number__c finacialAccountNumber2 = new Financial_Account_Number__c (  Store_Configuration__c = storeConfig1.id, Name = '2');
        insert finacialAccountNumber2;
        
        Financial_Transaction__c finacialTransaction1 = new Financial_Transaction__c(  Store_Configuration__c = storeConfig1.id,
                                                                                     Transaction_Type__c = 'Inventory Received - External Vendor',
                                                                                     Debit_Account_Number__c = finacialAccountNumber1.id,
                                                                                     Credit_Account_Number__c = finacialAccountNumber2.id);
        insert finacialTransaction1;
        
        Product2 masterProduct = new Product2( name = 'master',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Master_Product'));
        Product2 servProduct = new Product2( name = 'service',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        Product2 servProduct2 = new Product2(name = 'service2',IsActive = true, recordTypeId=UtilityMethods.GetRecordTypeIdsMapForSObject(Product2.sObjectType).get('Service_Product'),Master_Product__c = masterProduct.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        List<Product2> products = new List<Product2>{masterProduct,servProduct,servProduct2};
            insert products;
        PricebookEntry pricebookEntryMaster= utility.createPricebookEntry(pricebookId, masterProduct.id);     
        PricebookEntry pricebookEntryServ = utility.createPricebookEntry(pricebookId, servProduct.id);    
        PricebookEntry pricebookEntryServ2 = utility.createPricebookEntry(pricebookId, servProduct2.id);      
        List<PriceBookEntry> pbEntries = new List<PricebookEntry>{pricebookEntryMaster,pricebookEntryServ,pricebookEntryServ2};
            insert pbEntries;
        
        Service_Product__c servProd1 = new Service_Product__c(Service_Product__c = servProduct.Id, Master_Product__c = masterProduct.Id);
        Service_Product__c servProd2 = new Service_Product__c(Service_Product__c = servProduct2.Id, Master_Product__c = masterProduct.Id);
        List<Service_Product__c> sprods = new List<Service_Product__c>{servProd1,servProd2};
            insert sprods;

        List<Opportunity> oppsToInsert = new List<Opportunity>();
        Opportunity opp = new Opportunity(
            Name = 'TESTopp',
            AccountId = dwelling1.Id,
            StageName = 'New',
            Store_Location__c = store1.Id,
            CloseDate = Date.newInstance(2018, 12, 31)
        );
        oppsToInsert.add(opp);
        insert oppsToInsert;

        List<Order> ordersToInsert = new List<Order>();
        Order order =  new Order(   Name='Sold Order 1', 
                                 AccountId = dwelling1.id, 
                                 BilltoContactId = contact1.id,
                                 EffectiveDate= Date.Today(), 
                                 Store_Location__c = store1.Id,                           
                                 Status ='Draft', 
                                 Pricebook2Id = pricebookId
                                );
        ordersToInsert.add(order);
        insert ordersToInsert;
        
        List<WorkOrder> wosToInsert = new List<WorkOrder>();
        WorkOrder rwo = new WorkOrder(
            recordTypeId=UtilityMethods.retrieveRecordTypeId('Install', 'WorkOrder'),
            Sold_Order__c = order.Id,
            AccountId = dwelling1.id,
            ContactId = contact1.id,
            Status = 'To be Scheduled',
            Work_Order_Type__c = 'Install',
            Opportunity__c = opp.Id
        );
        wosToInsert.add(rwo);

        insert wosToInsert;
        
        List<Event> eventToInsert = new List<Event>();
        Event ev = new Event(
            WhatId = rwo.Id,
            ActivityDate = Date.Today()+5
        );                                   
        
        insert eventToInsert;
    }

    @IsTest
    static void deleteEvent(){
        Test.startTest();
        WorkOrder rwo = [SELECT Id, Status FROM WorkOrder WHERE Work_Order_Type__c = 'Install'];
        
        Event ev = new Event(
            WhatId = rwo.Id,
            ActivityDate = Date.Today()+5,
            DurationInMinutes = 5,
         	activitydatetime=system.now().addDays(5),
         	ShowAs = 'Busy',
         	IsReminderSet = false
        );                                   
        insert ev;

        rwo.Status = 'Canceled';
        rwo.Cancel_Reason__c = 'Weather';
        update rwo;
        Test.stopTest();
    }
    
}