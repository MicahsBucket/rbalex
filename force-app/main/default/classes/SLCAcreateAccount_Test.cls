@istest
public class SLCAcreateAccount_Test{
    static testMethod void  testPostRestService(){

        
        SLCAcreateAccountController.AccountWrapper acc=new SLCAcreateAccountController.AccountWrapper();
         acc.accountName = 'Maureen Morne';
         acc.accountCountry = 'United States';
         acc.accountCountryCode = 'US';
         acc.accountStreet = '1529 S George Mason Dr #11';
         acc.accountCity = 'Arlington';
         acc.accountState = 'Virginia';
         acc.accountStateCode = 'VA';
         acc.accountZip = '22204';
         acc.accountStoreLocation = '00161000006RTTMAA4';

        
        List<SLCAcreateAccountController.ContactWrapper > conList=new List<SLCAcreateAccountController.ContactWrapper>();
        
        SLCAcreateAccountController.ContactWrapper contactSLCA=new SLCAcreateAccountController.ContactWrapper();
        contactSLCA.contactCAAppID = '5987';
        contactSLCA.contactLastName  = 'Awal';
        contactSLCA.contactFirstName  = 'Abdul';
        contactSLCA.contactHomePhone  = '9172505686';
        contactSLCA.contactMobilePhone  = '9172505686';
        contactSLCA.contactEmail  = 'toshaoun@gmail.com';
        contactSLCA.contactBirthdate  = '1967-04-23';
        contactSLCA.contactMiddleInitial  = '';
        contactSLCA.contactSsn  = '666-01-2228';
        contactSLCA.contactStreet  = '1529 S George Mason Dr';
        contactSLCA.contactCity  = 'Arlington';
        contactSLCA.contactState  = 'Virginia';
        contactSLCA.contactStateCode  = 'VA';
        contactSLCA.contactPostalCode  = '22204';
        contactSLCA.contactEmployerPhone  = '(478) 475-9343';
        contactSLCA.contactEmployer  = 'Primetech Solutions';
        contactSLCA.contactPosition = 'Manager';
        contactSLCA.contactApplicantID = 'E20394344';
        contactSLCA.contactApplicantIDType = 'Passport';
        contactSLCA.contactIDIssuingState = '480980';
        contactSLCA.contactNameOnID = 'Abdul Awal';
        contactSLCA.contactIDExpDate = '2018-12-22';
        contactSLCA.contactMortAccNumber = '';
        contactSLCA.contactMortLender = '';
        contactSLCA.contactMortMontPayment = '0';
        contactSLCA.ContactDate = '2018-3-7';
        contactSLCA.mainApplicant = '0';
        conList.add(contactSLCA); 
        
        SLCAcreateAccountController.ContactWrapper contactSLCA1 =new SLCAcreateAccountController.ContactWrapper();
        contactSLCA1.contactCAAppID = '5987';
        contactSLCA1.contactLastName  = 'BRIDGEWATER';
        contactSLCA1.contactFirstName  = 'SABRINA';
        contactSLCA1.contactHomePhone  = '9172508986';
        contactSLCA1.contactMobilePhone  = '9172505686';
        contactSLCA1.contactEmail  = 'awalshaoun@gmail.com';
        contactSLCA1.contactBirthdate  = '1969-04-23';
        contactSLCA1.contactMiddleInitial  = '';
        contactSLCA1.contactSsn  = '666-01-2228';
        contactSLCA1.contactStreet  = '1529 S George Mason Dr';
        contactSLCA1.contactCity  = 'Arlington';
        contactSLCA1.contactState  = 'Virginia';
        contactSLCA1.contactStateCode  = 'VA';
        contactSLCA1.contactPostalCode  = '22204';
        contactSLCA1.contactEmployerPhone  = '(478) 475-9343';
        contactSLCA1.contactEmployer  = 'Primetech Solutions';
        contactSLCA1.contactPosition = 'Manager';
        contactSLCA1.contactApplicantID = 'E84208522';
        contactSLCA1.contactApplicantIDType = 'Passport';
        contactSLCA1.contactIDIssuingState = '';
        contactSLCA1.contactNameOnID = 'SABRINA R BRIDGEWATER';
        contactSLCA1.contactIDExpDate = '2018-12-22';
        contactSLCA.contactMortAccNumber = '48928342';
        contactSLCA1.contactMortLender = 'Suntrust Bank';
        contactSLCA1.contactMortMontPayment = '1662';
        contactSLCA1.ContactDate = '2018-3-7';
        contactSLCA1.mainApplicant = '1';
        conList.add(contactSLCA1);
         
        Finance_Application__c financeSlmc = new  Finance_Application__c();
        financeSlmc.Name  = '';
        //financeSlmc.Additional_Financing_Needed__c  = true;
        //financeSlmc.Applicant_Total_Annual_Income__c  = 90000.00;
        //financeSlmc.Applicant_Total_Monthly_Income__c = 6000.00;
        //financeSlmc.Application_Completed__c = true;
        financeSlmc.Cash_Deposit__c = 3000.00;
        //financeSlmc.Final_Contract_Amount__c = 15000.00;
        //financeSlmc.Loan_Amount_Requested__c = 12000.00;
        financeSlmc.Sales_Rep_Notes__c = 'Test Note';
        financeSlmc.Status__c = 'NEW';
        financeSlmc.Store__c = '00161000006RTTMAA4';
        financeSlmc.Time_at_Address__c  = '3 Year 4 Month';
        financeSlmc.Time_on_Job__c  = '2 Year 2 Month';
        //financeSlmc.Customer_Phone_Unformatted__c  = '9172505685';
        //financeSlmc.Total_Cash_Deposit__c = 3000.00;
        //financeSlmc.Customer_Name_Text__c  = 'SABRINA R BRIDGEWATER';
        //financeSlmc.Sales_Rep_Name_Text__c  = 'Malissa Barron';
        //financeSlmc.Finance_App_Number__c = '5987';
        financeSlmc.Applicant_Gross_Annual_Income__c = 110000.00;
        financeSlmc.Source_of_Other_Income__c = 'Uber';
        financeSlmc.Other_Income_Annual_Amount__c  = 10000.00;

        
        SLCAcreateAccountController.RequestWrapper reqst=new  SLCAcreateAccountController.RequestWrapper();
        reqst.acct = acc;
        reqst.contList = conList;
        reqst.finance = financeSlmc;
        
        String JsonMsg=JSON.serialize(reqst);
        
        Test.startTest();
            
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();

            req.requestURI = '/services/apexrest/SLCAcreateAccount';  //Request URL
            req.httpMethod = 'POST';//HTTP Request Type
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;
            RestContext.response= res;
        
        
        
           SLCAcreateAccountController.AccountResponseClass resp = new SLCAcreateAccountController.AccountResponseClass(); 
           resp=SLCAcreateAccountController.createNewAccount(reqst);
           Test.stopTest();
           System.assertEquals('Success', resp.status);//Assert the response has message as expected 
           //System.assert(resp.statusCode.contains('Done'));
           //System.assert(resp.accountId!=null);//Assert that the Account is inserted and has Id
           
           
          
    }

}