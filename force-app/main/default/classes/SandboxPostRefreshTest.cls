@isTest
class SandboxPostRefreshTest {
    
    @testSetup static void setup() {
		TestDataFactoryStatic.setUpConfigs();
	}
    
    @isTest
    static void testMySandboxPrep() {
        Test.startTest();
        exception myException;
        try{
        Test.testSandboxPostCopyScript(
            new SandboxPostRefresh(), UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());
        } catch (exception e){
            myException = e;
        }
        System.assertEquals(null, myException);
        Test.stopTest();
    }
    
}