public with sharing class NewWorkerApexController {
	/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 6/17
* @description gets all survey hold information associated with a store.  Also creates new worker record
**/
	@AuraEnabled
	public static Boolean isManualUser(){
		Id profileId=userinfo.getProfileId();
		String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
		if(profileName.containsIgnoreCase('Hybrid')){
			return false;
		} else {
			return true;
		}
	}
	//saves new worker record
	@AuraEnabled
	public static Worker__c saveWorker(String firstName, String lastName, String phone, String email, String workersSelected){
		Worker__c newWorker = new Worker__c();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Store').getRecordTypeId();

		Account storeAccount = [SELECT Id, RecordTypeId, Name FROM Account WHERE RecordTypeId = :recordTypeId LIMIT 1];
		newWorker.First_Name__c = firstName;
		newWorker.Last_Name__c = lastName;
		newWorker.Email__c = email;
		newWorker.Mobile__c = phone;
		newWorker.Source_System__c = 'Manual Process';
		if(!workersSelected.equals('')){
			newWorker.Role__c = workersSelected;
		}
		newWorker.Store_Account__c = storeAccount.Id;
		Database.insert(newWorker);
		return newWorker;
	}

	//
	@AuraEnabled
	public static String checkForDuplicate(String email){
		String returnNull = null;
		String role;
		Worker__c duplicateWorker;
		try{
			duplicateWorker = [SELECT First_Name__c, Last_Name__c, Role__c, Email__c FROM
									Worker__c WHERE Email__c = :email LIMIT 1];
		} catch(Exception e) {
			return returnNull;
		}
		Set<String> roles = new Set<String>(duplicateWorker.Role__c.split(';'));
		if(roles.contains('Inactive')){
			role = 'Inactive User';
		} else {
			role = duplicateWorker.Role__c.split(';')[0];	
		}
		String returnString = duplicateWorker.First_Name__c + ', ' + duplicateWorker.Last_Name__c + ' already exists as a ' + role + ' with the same email: ' + duplicateWorker.email__c;
		return returnString;
	}
}