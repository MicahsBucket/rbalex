/*
* @author Jason Flippen
* @date 01/08/2020 
* @description Class to provide functionality for the releasePurchaseOrderAction LWC.
*
*              Test code coverage provided by the following Test Class:
*			   - ReleasePOActionControllerTest
*/
public with sharing class ReleasePOActionController {

    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description Method to return (wrapped) data from a Purchase Order.
    * @param purchaseOrderId
    * @returns List of (Wrapped) Purchase Order records
    */
    @AuraEnabled(cacheable=true)
    public static PurchaseOrderWrapper getPurchaseOrderData(Id purchaseOrderId) {

        PurchaseOrderWrapper wrapper = new PurchaseOrderWrapper();

        // Retrieve data from the Purchase Order record.
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Order__c,
                                                  Order__r.Pricebook2Id,
                                                  Order__r.RecordTypeId,
                                                  Order__r.RecordType.DeveloperName,
                                                  Order__r.Revenue_Recognized_Date__c,
                                                  Order__r.Status,
                                                  RecordTypeId,
                                                  RecordType.DeveloperName,
                                                  Status__c,
                                                  Vendor__c,
                                                  Vendor__r.Name
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Set the Wrapper properties.
        wrapper.id = purchaseOrder.Id;
        wrapper.status = purchaseOrder.Status__c;
        String vendorName = '';
        if (purchaseOrder.Vendor__c != null) {
            vendorName = purchaseOrder.Vendor__r.Name;
        }
        wrapper.vendorName = vendorName;

        Boolean costPurchaseOrder = false;
        if (purchaseOrder.RecordType.DeveloperName == 'Cost_Purchase_Order') {
            costPurchaseOrder = true;
        }
        wrapper.costPurchaseOrder = costPurchaseOrder;

        Boolean relatedOrderCORO = false;
        Boolean relatedOrderService = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            relatedOrderCORO = true;
        }
        else if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            relatedOrderService = true;
        }
        wrapper.relatedOrderCORO = relatedOrderCORO;
        wrapper.relatedOrderService = relatedOrderService;

        Boolean relatedOrderIsClosed = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.Status == 'Job Closed') {
            relatedOrderIsClosed = true;
        }
        wrapper.relatedOrderIsClosed = relatedOrderIsClosed;

        // If the related Order is a Purchase Order, make sure the Revenue Recognized
        // Date on the Order is null, and the Order Status is a valid Status.
        Boolean revRecNullAndValidStatus = false;
        Set<String> validStatusSet = new Set<String>{'Job in Progress','Install Complete','Install Needed','Install Scheduled','Order Released','Ready to Order'};
        if (relatedOrderCORO == true && purchaseOrder.Order__c != null &&
            purchaseOrder.Order__r.Revenue_Recognized_Date__c == null &&
            validStatusSet.contains(purchaseOrder.Order__r.Status)) {
            revRecNullAndValidStatus = true;
        }
        wrapper.revRecNullAndValidStatus = revRecNullAndValidStatus;

        // Return the Wrapper Class.
        return wrapper;

    }

    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description Method to update the Purchase Order Status to "Released".
    * @param purchaseOrderId
    * @returns String containing the save result (Success/Error)
    */
    @AuraEnabled
    public static String releasePurchaseOrder(Id purchaseOrderId) {
        
        String returnResult = null;

/*
        Set<String> validStatusSet = new Set<String>{'Job in Progress','Install Complete','Install Needed','Install Scheduled','Order Released','Ready to Order'};
        
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  RecordType.DeveloperName,
                                                  Estimated_Ship_Date__c,
                                                  Order__c,
                                                  Order__r.RecordType.DeveloperName,
                                                  Order__r.Revenue_Recognized_Date__c,
                                                  Order__r.Status,
                                                  Released_Timestamp__c,
                                                  Status__c,
                                                  Vendor__c,
                                                  Vendor__r.Name,
                                                  Vendor__r.Zero_Dollar_Vendor__c,
                                                  (
                                                      SELECT Id,
                                                             Charge_Cost_To__c,
                                                             Pricebookentry.Product2.Name,
                                                             Unit_Id__c,
                                                             Unit_Wholesale_Cost__c,
                                                             Variant_Number__c
                                                      FROM   Order_Products__r
                                                      WHERE  Verify_Item_Configuration__c = false
                                                      AND    NSPR__c = false
                                                      AND    Product_Record_Type__c = 'Master_Product'
                                                      AND    Product_Family__c IN ('Window', 'Specialty')
                                                      ORDER BY Unit_Id__c ASC
                                                  )
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        // Is this a PO a Cost Purchase Order?
        Boolean costPurchaseOrder = false;
        if (purchaseOrder.RecordType.DeveloperName == 'Cost_Purchase_Order') {
            costPurchaseOrder = true;
        }

        // Determine the type of Order related to the PO.
        Boolean relatedOrderCORO = false;
        Boolean relatedOrderService = false;
        if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Record_Type') {
            relatedOrderCORO = true;
        }
        else if (purchaseOrder.Order__c != null && purchaseOrder.Order__r.RecordType.DeveloperName == 'CORO_Service') {
            relatedOrderService = true;
        }

        // Is this an Electronic Order?
        Boolean electronicOrder = false;
        Account vendorAcct = [SELECT Electronic_Orders__c FROM Account WHERE Id = :purchaseOrder.Vendor__c];
        electronicOrder = (vendorAcct.Electronic_Orders__c == true && relatedOrderService == false && costPurchaseOrder == false);

        // If the related Order is a Purchase Order, make sure the Revenue Recognized
        // Date on the Order is null, and the Order Status is a valid Status.
        Boolean revRecNullAndValidStatus = false;
        if (relatedOrderCORO == true &&
            purchaseOrder.Order__c != null &&
            purchaseOrder.Order__r.Revenue_Recognized_Date__c == null &&
            validStatusSet.contains(purchaseOrder.Order__r.Status)) {
            revRecNullAndValidStatus = true;
        }

        // Now let's determine if we're clear to release this PO.
        if (revRecNullAndValidStatus == false && relatedOrderCORO == true) {
            returnResult = RMS_errorMessages.ORDER_STATUS_INVALID_FOR_PO_RELEASE;
        }
        else if (purchaseOrder.Status__c != 'In Process' && purchaseOrder.Status__c != 'Rejected' && relatedOrderCORO == true) {
            returnResult = RMS_errorMessages.PO_STATUS_INVALID_FOR_PO_RELEASE;
        }
        else {

            Boolean orderProductsVerified = true;

            if (electronicOrder == true && !purchaseOrder.Order_Products__r.isEmpty()) {
                orderProductsVerified = false;
            }

            if (orderProductsVerified == true) {

                purchaseOrder.Status__c = 'Released';    
                purchaseOrder.Released_Timestamp__c = System.Now();
//                returnResult = checkConfirmed(purchaseOrder, relatedOrderService);
                
                if (String.isBlank(returnResult)) {

                    try {

                        // Update the po with the new status.
                        update purchaseOrder;
                        
                        returnResult = 'Release PO Success';
                    }
                    catch (Exception ex) {
                        System.debug('************ setPurchaseOrdertoReleased Error: ' + ex);
                        returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
                    }

                }

            }
            else {
                String errorMessage = '';
                for (OrderItem oi : purchaseOrder.Order_Products__r) {
                    errorMessage += 'Unit ' + oi.Unit_Id__c + ' on this Purchase Order is not configured Correctly. Please validate the unit Configuration./n';
                }
                returnResult = errorMessage;
            }

        }
*/
        
        Purchase_Order__c purchaseOrder = [SELECT Id,
                                                  Released_Timestamp__c,
                                                  Status__c
                                           FROM   Purchase_Order__c
                                           WHERE  Id = :purchaseOrderId];

        purchaseOrder.Status__c = 'Released';
        purchaseOrder.Released_Timestamp__c = System.Now();

        try {

            // Update the po with the new status.
            update purchaseOrder;
            
            returnResult = 'Release PO Success';
        }
        catch (Exception ex) {
            System.debug('************ releasePurchaseOrder Error: ' + ex);
            returnResult = RMS_ErrorMessages.UPDATE_PO_EXCEPTION;
        }

        return returnResult;

    }

    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description Method to to determine whether or not the Purchase Order
    *              Status should be set to "Confirmed"
    * @param Purchase Order; 
    * @returns String containing the save result (Success/Error)
    */
/*
    private static String checkConfirmed(Purchase_Order__c purchaseOrder, Boolean servicePurchaseOrder) {

        String returnResult = '';
        
        if (purchaseOrder.Status__c == 'Released' && purchaseOrder.Estimated_Ship_Date__c != null) {

            boolean allOrderItemsHaveWholesaleCost = false;
            boolean completeUnitsHaveVariants = true;

            if (purchaseOrder.Vendor__c != null && purchaseOrder.Vendor__r.Zero_Dollar_Vendor__c == true) {
                allOrderItemsHaveWholesaleCost = true;
            }
            else {

                for (OrderItem oi : purchaseOrder.Order_Products__r) {
                    Boolean isNotServiceManufacturing = (servicePurchaseOrder == false || oi.Charge_Cost_To__c != 'Manufacturing');
                    if (oi.Unit_Wholesale_Cost__c == null ||
                        oi.Unit_Wholesale_Cost__c < 0 ||
                        (oi.Unit_Wholesale_Cost__c == 0 && isNotServiceManufacturing)) {
                        allOrderItemsHaveWholesaleCost = false;
                        break;
                    }
                }

                // Do a check and make sure all complete units have variants for Renewal by Andersen service POs.
                if (servicePurchaseOrder == true && purchaseOrder.Vendor__r.Name == 'Renewal by Andersen') {
                    for (OrderItem oi : purchaseOrder.Order_Products__r) {
                        if (oi.Pricebookentry.Product2.Name == 'Complete Unit' && oi.Variant_Number__c == null) {
                            returnResult += 'Please enter new Variant Number for Unit ' + oi.Unit_Id__c + './n';
                            completeUnitsHaveVariants = false;
                        }
                        else {
                            completeUnitsHaveVariants = true;
                        }
                    }
                }

            } // End If ((oi.Unit_Wholesale_Cost__c == 0 && isNotServiceManufacturing))

            if (allOrderItemsHaveWholesaleCost && completeUnitsHaveVariants) {
                purchaseOrder.Status__c = 'Confirmed';
                purchaseOrder.Confirmed_Timestamp__c = System.Now();
            }

        }

        return returnResult;
        
    }
*/


/** Wrapper Class **/


    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description WRapper Class for Purchase Order.
    */
    @TestVisible
    public class PurchaseOrderWrapper {
        
        @AuraEnabled
        public String id {get;set;}

        @AuraEnabled
        public Boolean costPurchaseOrder {get;set;}

        @AuraEnabled
        public Boolean relatedOrderIsClosed {get;set;}

        @AuraEnabled
        public Boolean relatedOrderCORO {get;set;}

        @AuraEnabled
        public Boolean relatedOrderService {get;set;}

        @AuraEnabled
        public Boolean revRecNullAndValidStatus {get;set;}

        @AuraEnabled
        public String status {get;set;}

        @AuraEnabled
        public String vendorName {get;set;}

    } // End (Wrapper) Class

}