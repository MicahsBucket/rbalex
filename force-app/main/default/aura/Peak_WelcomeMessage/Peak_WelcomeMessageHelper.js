({
    getUserLoginCount: function(component, event, helper) {
        helper.doCallout(component, 'c.getLoginCount', null).then(function(response){
            if (response.success){
                component.set('v.loginAmount', response.peakResults[0].count);
            } else {
                helper.showMessage('error',response.messages[0]);
                console.log('Error: ', response.messages[0]);
            }
        }).catch(function (response) {
            console.log(response[0].message);
        }).finally(function(){
            component.set('v.isInit', true);
        });
    },
})