({
    doInit : function(component, event, helper) 
    {
        var confirm=window.confirm("Are you sure you want to log out?");
        console.log("_______confirm__________"+confirm);    
        if(confirm)
        {
            helper.doInit(component);	
        }
        else
        {
            var url = window.location.href; 
            var value = url.substr(0,url.lastIndexOf('/') + 1);
            window.history.back();
            return false;
        }
    },
})