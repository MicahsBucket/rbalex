({
    init : function(component, event, helper) {
        //finds the most recent order
        var getId = component.get('c.getOrderId');
        getId.setCallback(this, function(returnVal){
           var ordId = returnVal.getReturnValue();
           component.set('v.orderId', ordId); 
        }); 
        $A.enqueueAction(getId);

        //finds all of the orders the user is associated to
        var getOrders = component.get('c.getOrders');
        getOrders.setCallback(this, function(returnVal){
            var orders = returnVal.getReturnValue();
            var orderList = [];
            //creates the list of order dates that is displayed to the user 
            for(var i = 0; i < orders.length; i++){
                orderList.push(orders[i].Order_Processed_Date__c);
            }
            component.set('v.orderList', orderList);
        });
        $A.enqueueAction(getOrders);
        helper.helperFun(component,event,'articleThree');
    },

    //changes the displayed order based on the users selection
    updateOrderId : function(component, event, helper){
        var date = component.find('orderSelect').get('v.value');
        var getNewId = component.get('c.getNewOrderId'); 
        getNewId.setParams({
            "processDate" : date 
        })
        getNewId.setCallback(this, function(returnVal){
            var orderId = returnVal.getReturnValue();
            component.set('v.orderId', orderId);
            //causes all of the child components to refresh based on the new order Id
            helper.reInit(component, event, helper);
        });
        $A.enqueueAction(getNewId);
    },


    /*
    The below methods allow the user to open and close each 
    section of the collapsable content
    */
    sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');
    },
    
   sectionTwo : function(component, event, helper) {
      helper.helperFun(component,event,'articleTwo');
    },
   
   sectionThree : function(component, event, helper) {
      helper.helperFun(component,event,'articleThree');
   },
   
   sectionFour : function(component, event, helper) {
      helper.helperFun(component,event,'articleFour');
   },

   sectionFive : function(component, event, helper) {
      helper.helperFun(component,event,'articleFive');
   },


})