({

    initializeComponent: function(component, event, helper) {

        var recordId = '';
        var objectName = '';

        // Find the Parent Id and Object Name.  This only works for Desktop Users.
        var pageReference = component.get("v.pageReference");
        var state = pageReference.state; 
        var context = state.inContextOfRef;
        if (context.startsWith("1\.")) {

            context = context.substring(2);
            var addressableContext = JSON.parse(window.atob(context));
            console.log('addressableContext',JSON.stringify(addressableContext));

            recordId = addressableContext.attributes.recordId;
            objectName = addressableContext.attributes.objectApiName;

        }

        var getRecordDataAction = component.get("c.getRecordData");
        getRecordDataAction.setParams({
            'recordId': recordId,
            'objectName': objectName
        });

        // Configure the response handler for the action
        getRecordDataAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var record = response.getReturnValue();

                // Navigate to new Payment record.
                var createPaymentEvent = $A.get("e.force:createRecord");
                createPaymentEvent.setParams({
                    "entityApiName": "Payment__c",
                    "recordTypeId": record.recordTypeId,
                    "defaultFieldValues": {
                        'Order__c': record.id,
                        'Contact__c': record.contactId,
                        'Store_Location__c': record.storeLocationId
                    }
                });
                createPaymentEvent.fire();

            }
            else if (state === "ERROR") {
                console.log('Problem retrieving Record, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }

        });

        // Send the request to get Record data.
        $A.enqueueAction(getRecordDataAction);    

    }

})