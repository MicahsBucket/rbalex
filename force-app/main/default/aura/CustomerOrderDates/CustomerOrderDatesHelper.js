({
	showStatus: function (order, component, event, helper) {
    var status = order.Status;
        //highlights the current stage of the order for the user based on the order status
        if(status == 'Draft'){
            helper.updateTarget(1, component, event, helper);
		} else if(status == 'Tech Measure Needed'){
            helper.updateTarget(3, component, event, helper); 
        }										   		 
        else if(status == 'Ready to Order' || status == 'Order Released' || status == 'Tech Measure Scheduled'){
            helper.updateTarget(2, component, event, helper);
        } else if(status == 'Install Needed'){
			
            helper.updateTarget(3, component, event, helper);
        } else if(status == 'Install Scheduled' || status == 'Job in Progress'){
            helper.updateTarget(4, component, event, helper);
        } else if(status == 'Install Complete'){
            helper.updateTarget(5, component, event, helper);
        } else if(status == 'Closed'){
            helper.updateTarget(5, component, event, helper);
        } else if(status == 'Cancelled'){

        } else if(status == 'On Hold'){

        }
    },												   
		showStatusOnMileStones: function (mileStone, component, event, helper) {
           
    	var surveySentDate = mileStone.surveySentDate;
       // var installDate = mileStone.lstInstall;
        var installDate = mileStone.installDate;
        var techMeasureDate = mileStone.techMeasureDate;
       // var techMeasureDate = mileStone.lstTechMeasure;
        var orderPlaceDate = mileStone.orderPlaceDate;
        var consultationDate = mileStone.consultationDate;
        var ReinstallDate = mileStone.ReinstallDate;
            
        //highlights the current stage of the order for the user based on the order status
        if( !$A.util.isUndefined(surveySentDate) && surveySentDate!=null){
            helper.updateTarget(7, component, event, helper);
        } else if(!$A.util.isUndefined(ReinstallDate) && ReinstallDate!=null){
            helper.updateTarget(6, component, event, helper); 
        }   
            else if(!$A.util.isUndefined(installDate) && installDate!=null){
            helper.updateTarget(5, component, event, helper); 
        }        
        else if(!$A.util.isUndefined(techMeasureDate) && techMeasureDate!=null){
            helper.updateTarget(3, component, event, helper);
        }  else if(!$A.util.isUndefined(orderPlaceDate) && orderPlaceDate!=null){
            helper.updateTarget(2, component, event, helper);
        } else if(!$A.util.isUndefined(consultationDate) && consultationDate!=null){
            helper.updateTarget(1, component, event, helper);
        } 
    },
    //updates the display of the current status of an order when a new order is selected
    updateTarget : function(numTargets, component, event, helper){
        debugger;
        var auraIdsList = ['created', 'measureProcess', 'manufacturing', 'installProcess','Re-install', 'orderComplete', 'ordercomplete12'];
        var timeLength = [0, 1000, 2000, 3000, 4000, 4500,5000];
        for(var i = 0; i < numTargets; i++){
            helper.timer(auraIdsList[i], timeLength[i], component, event, helper);
        }
    },

    //resets the display of the current status of an order when a new order is selected by the user
    clearStatus : function(component, event, helper){
		 var cmpList =  ['created', 'measureProcess', 'manufacturing', 'installProcess','Re-install', 'orderComplete','orderComplete12'];
        for(var i = 0; i<cmpList.length; i++){
             var cmpTarget = component.find(cmpList[i]);
            $A.util.removeClass(cmpTarget, 'statusReached');
        }
       
    },

    //delays the css update of the element to statusReached for UX reasons
    timer : function(auraId, time, component, event, helper){
        window.setTimeout(
            $A.getCallback(function() {
                var cmpTarget = component.find(auraId);
                $A.util.addClass(cmpTarget, 'statusReached');
            }), time
        );
    },
})