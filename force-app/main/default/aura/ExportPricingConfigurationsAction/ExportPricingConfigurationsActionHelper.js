({
    createCSVFile: function(component, pricingConfigurationWrapper) {

        var returnCSVString = '';

        var fieldMap = pricingConfigurationWrapper.fieldMap;
        var pricingConfigurationList = pricingConfigurationWrapper.pricingConfigurationList;
        var clonedPricingConfigurationList = pricingConfigurationWrapper.clonedPricingConfigurationList;

        // Constant variables to separate fields and lines.
        const fieldDivider = ',';
        const lineDivider =  '\n';

        var fieldKeyList = [];
        var fieldLabelList = [];
        for (var key in fieldMap) {
            fieldKeyList.push(key);
            fieldLabelList.push(fieldMap[key]);
        }
        returnCSVString += fieldLabelList.join(fieldDivider);
        returnCSVString += lineDivider;

        // Do we have any Pricing Configuration data?
        if (Array.isArray(pricingConfigurationList) && pricingConfigurationList.length) {

            // Was the Wholesale Pricebook cloned from
            // one that had Pricing Configurations?
            var hasClonedPricingConfigurations =  false;
            if (Array.isArray(clonedPricingConfigurationList) && clonedPricingConfigurationList.length) {
                hasClonedPricingConfigurations = true;
            }

            // Add the field values to the CSV file.
            returnCSVString += this.addFieldValues(pricingConfigurationList,
                                                   clonedPricingConfigurationList,
                                                   hasClonedPricingConfigurations,
                                                   fieldKeyList);

        }
        
        return returnCSVString;

    },

    addFieldValues: function(pricingConfigurationList,
                             clonedPricingConfigurationList,
                             hasClonedPricingConfigurations,
                             fieldKeyList) {

        var returnCSVString = '';

        // Constant variables to separate fields and lines.
        const fieldDivider = ',';
        const lineDivider =  '\n';

        // Build an Array of Data Rows from the List of
        // Pricing Configuration and the cloned Pricing
        // Configuration records.
        var dataRows = [];
        for (var index=0;index<pricingConfigurationList.length;index++) {

            var pricingConfiguration = pricingConfigurationList[index];

            var dataDictionary = {};
            for (var field in pricingConfiguration) {
                dataDictionary[field] = pricingConfiguration[field];
            }

            // If we have cloned Pricing Configurations add them to the Array.
            if (hasClonedPricingConfigurations === true) {

                for (var clonedIndex=0;clonedIndex<clonedPricingConfigurationList.length;clonedIndex++) {

                    var clonedPricingConfiguration = clonedPricingConfigurationList[clonedIndex];
                    if (clonedPricingConfiguration.Id === pricingConfiguration.Cloned_Pricing_Configuration__c) {
                        for (var field in clonedPricingConfiguration) {
                            dataDictionary['Original ' + field] = clonedPricingConfiguration[field];
                        }
                        break;
                    }

                }

            }

            dataRows.push(dataDictionary);

        }

        // Iterate through the Array of Data Rows and align each
        // field value to appropriate field name in the CSV file.
        dataRows.forEach(function(drItem, drIndex) {

            var fieldValueList = [];
            fieldKeyList.forEach(function(fieldKey, fieldKeyIndex) {

                // Align the field value to the appropriate
                // field name in the CSV file. If the row
                // doesn't contain a value for a field
                // add an empty string. 
                var valueFound = false;
                for (var drFieldKey in drItem) {

                    if (drFieldKey === fieldKey) {
                        fieldValueList.push(drItem[drFieldKey]);
                        valueFound = true;
                        break;
                    }
                }
                if (valueFound === false) {
                    fieldValueList.push('');
                }


            });

            // Add the field values to the CSV and move to the next row.
            returnCSVString += fieldValueList.join(fieldDivider);
            returnCSVString += lineDivider;

        });

        return returnCSVString;

    }

})