({
    sendUpdateEvt : function(component, order) {   
     //   console.log('order in send update event helper', order);
        var appEvent = $A.get("e.c:SendOrder");
        appEvent.setParams({
            order : order,
            orderDate:order.Order_Processed_Date__c,
            orderId : order.Id
        });
        //alert('Order Id '+order.Id);
        appEvent.fire();
    },
    getSavedOrder : function(component){
        // query user to get last order picked from chooser.  
        this.handleAction(
            component,
            null,
            "c.getSelectedOrder",
            this.getSelectedOrderCallback,
            this.getSelectedOrderCallbackError
        ); 
    },
    getSelectedOrderCallback: function(component, returnValue, ctx)
    {   
        var ordRecord=returnValue.orderRecord;
        var userRecord=returnValue.userRecord;
        if(userRecord.IsSessionChanged__c!=undefined&&userRecord.IsSessionChanged__c==true)
        {
            if(ordRecord.Notification_Message_Pop_Up__c==true&&ordRecord.Message_Pop_up__c!=undefined)
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    type:"warning",
                    mode: 'sticky',
                    message: ordRecord.Message_Pop_up__c,
                });
                toastEvent.fire();   
            }            
            var onHoldTask=returnValue.taskRecord;
            if(onHoldTask!=undefined)
            {
                var onHoldMessage;
                if(onHoldTask.Primary_Reason__c=="Homeowner Decision")
                {
                    onHoldMessage=ordRecord.Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c;
                }
                else if(onHoldTask.Primary_Reason__c=="Project Approval")
                {
                    onHoldMessage=ordRecord.Store_Location__r.Active_Store_Configuration__r.Project_Approval__c;
                }
                
                if(onHoldMessage!=undefined)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        type:"warning", 
                        mode: 'sticky',
                        message: onHoldMessage 
                    });
                    toastEvent.fire();     
                }                  
            }
        }
        if(returnValue.userRecord.Selected_Order__c){
            component.set("v.currentOrderId",returnValue.userRecord.Selected_Order__c);
        } else {
            console.log('no order selected');
        }
        ctx.getAllOrders(component);        
    },
    getSelectedOrderCallbackError : function (component, errorTitle, errorMessage) {
        console.log('In getSelectedOrderCallbackError : ' + errorTitle);      
        console.log('errorMessge: ' + errorMessage);
    },
    getAllOrders : function(component){
        // query all orders and build list for chooser.
  //      console.log('in get all orders');
        this.handleAction(
            component,
            null,
            "c.getAllOrders",
            this.getAllOrdersCallback,
            this.getAllOrdersCallbackError
        );
    },
    getAllOrdersCallback : function(component, returnValue, ctx){
        var orders = returnValue;
        component.set("v.orderList",orders);
        ctx.sendOrderEvent(component);
    },
    getAllOrdersCallbackError : function(component, errorTitle, errorMessage){
        console.log('In getAllORdersCallbackError: ' + errorTitle);          
        console.log('errorMessge: ' + errorMessage);
    },
    
    updateSavedOrder: function(component, orderid){        
        this.handleAction(
            component,
            {orderId:orderid},
            "c.updateSelectedOrder",
            this.updateSelectedOrderCallback,
            this.updateSelectedOrderCallbackError
        );         
    },
    updateSelectedOrderCallback : function(component, returnValue, ctx)
    {
        var ordRecord=returnValue.orderRecord;
        var userRecord=returnValue.userRecord;
        if(userRecord.IsSessionChanged__c!=undefined&&userRecord.IsSessionChanged__c==true)
        {
            if(ordRecord.Notification_Message_Pop_Up__c==true&&ordRecord.Message_Pop_up__c!=undefined)
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    type:"warning",
                    mode: 'sticky',
                    message: ordRecord.Message_Pop_up__c,
                });
                toastEvent.fire();   
            }            
            var onHoldTask=returnValue.taskRecord;
            if(onHoldTask!=undefined)
            {
                var onHoldMessage;
                if(onHoldTask.Primary_Reason__c=="Homeowner Decision")
                {
                    onHoldMessage=ordRecord.Store_Location__r.Active_Store_Configuration__r.Home_Owners_Decision__c;
                }
                else if(onHoldTask.Primary_Reason__c=="Project Approval")
                {
                    onHoldMessage=ordRecord.Store_Location__r.Active_Store_Configuration__r.Project_Approval__c;
                }
                if(onHoldMessage!=undefined)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        type:"warning", 
                        mode: 'sticky',
                        message: onHoldMessage 
                    });
                    toastEvent.fire();     
                }                  
            }
        }
    },
    updateSelectedOrderCallbackError : function(component, errorTitle, errorMessage){
        console.log('In updateSelectedOrderCallbackError: ' + errorTitle);                  
        console.log('errorMessge: ' + errorMessage);
    },
    sendOrderEvent : function(component){
        var orderDateId = component.find("Dates").get("v.value");
        var orderList = component.get('v.orderList');
        var currentOrderId = component.get("v.currentOrderId");
        var currentOrder;
        console.log('current order in loop',orderList);
        if(currentOrderId){
       		if(!orderDateId){
            	orderDateId = currentOrderId;
        	}            
        } else {
            if(!orderDateId){
                orderDateId = orderList[0].Id;
            }
        }

     //   console.log('orderdate id in send order event before loop',orderDateId);        
        for(var i=0; i<orderList.length; i++){
            if (orderList[i].Id == orderDateId){
                currentOrder = orderList[i];
      //          console.log('current order in loop',currentOrder);
            }
        }
        component.set("v.currentOrderId",currentOrder.Id);
        this.updateSavedOrder(component,currentOrder.Id);
        this.sendUpdateEvt(component,currentOrder);
    },
})