({
    init : function(component, event, helper) {
        var salesRole = 'Sales Rep';
        

        //get sales reps to populate dropdown menu
        var setSalesReps = component.get("c.getWorkers");       
        setSalesReps.setParams({
                  "workerRole": salesRole
              });
        setSalesReps.setCallback(this, function(salesReps){
            var salesRepList = salesReps.getReturnValue();
            component.set("v.salesReps", salesRepList);
            component.set("v.selectedSalesRep", null);

        });
        $A.enqueueAction(setSalesReps);

        //get survey to populate existing fields for user
        var recordId = component.get("v.recordId");
        var setSurvey = component.get("c.getSurvey");
        setSurvey.setParams({
            "recordId":recordId
        })
        setSurvey.setCallback(this, function(survey){
            var surveyRecord = survey.getReturnValue();
            var result = surveyRecord.Appointment_Result__c;
            component.find('appointmentResult').set('v.value', result);
            component.set("v.survey", surveyRecord);
            console.log(surveyRecord.Date_Sent__c);
            if(surveyRecord.Date_Sent__c != null){
                component.set("v.isDateSent", true);
            }
        });
        $A.enqueueAction(setSurvey);

        helper.getCurrentSalesRep(component, event, helper);
    },

    setSalesRep : function(component, event, helper){
        var salesRep = event.getSource().get("v.value");
        if(salesRep != undefined){
            component.set("v.selectedSalesRep", salesRep);
            component.set("v.isNullSalesRep", false);
        }
    },

    saveSurvey : function(component, event, helper){
        component.set("v.disable", true);
        //email validation
        var email = component.find('email').get("v.value");
        if(email != null && email != ""){
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf("."); 
            if(atpos < 1 || ( dotpos - atpos < 2 )){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failure!",
                "message": "The email format you entered is invalid",
                "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            } else {
                helper.saveSurvey(component, event, helper);
            }
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failure!",
            "message": "Please enter an email address",
            "type": "error",
            });
            toastEvent.fire();
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
        }
    },

    //checks to see if the user has filled in email information and gives them a chance to add one if none exists
    onSendClick : function(component, event, helper){
        //email validation
        component.set("v.disable", true);
        var email = component.find('email').get("v.value");
        if(email != null && email != ""){
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf(".");
            var apptResult = component.find('appointmentResult').get('v.value');
            if(atpos < 1 || ( dotpos - atpos < 2 )){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failure!",
                "message": "The email format you entered is invalid",
                "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
                return;
            }
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "title": "Failure!",
            "message": "Please enter an email address",
            "type": "error",
            });
            toastEvent.fire();
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.disable", false);
                }), 3000
            );
            return;
        }
        
        //recission validation
        var appointmentDate = component.find('appointmentDate').get("v.value");
        var today = new Date();
        var today1 = new Date();
        today1.setDate(today.getDate());
        var yesterday = new Date();
        var twoDaysAgo = new Date();
        var threeDaysAgo = new Date();
        yesterday.setDate(today.getDate()-1);
        twoDaysAgo.setDate(today.getDate()-2);
        threeDaysAgo.setDate(today.getDate()-3);
        today = helper.getDate(today, component, event, helper);
        yesterday = helper.getDate(yesterday, component, event, helper);
        twoDaysAgo = helper.getDate(twoDaysAgo, component, event, helper);
        threeDaysAgo = helper.getDate(threeDaysAgo, component, event, helper);
        var recissionWarning = component.get('v.recissionWarning');
        if(apptResult == 'Sale'){
            if(appointmentDate == today || appointmentDate == yesterday || appointmentDate == twoDaysAgo || appointmentDate == threeDaysAgo){
                if(recissionWarning){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Warning",
                        "message": "This order is still within the recision period",
                        "type": "info",
                    });
                    toastEvent.fire();
                    component.set('v.recissionWarning', false);
                    window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.disable", false);
                        }), 3000
                    );
                    return false;
                }
            }   
        }
        var dateCompareBool = helper.dateComparer(today1, appointmentDate, component, event, helper);
        if(dateCompareBool){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Failure!",
                    "message": "You can't send a survey for an appointment that hasn't occured yet",
                    "type": "error",
                });
                toastEvent.fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                ); 
                return;
        } 
        helper.saveSurvey(component, event, helper);
        var sendBool = component.get('v.readyToSend');
        if(sendBool = true){
            var firstName = component.find('firstName').get("v.value");
            var lastName = component.find('lastName').get("v.value");
            var email = component.find('email').get("v.value");
            var appointmentDate = component.find('appointmentDate').get("v.value");
            var apptResult = component.find('appointmentResult').get('v.value');
            var salesRep = component.get("v.selectedSalesRep");
            
            
            if(email != "" && email != null && appointmentDate != "" && appointmentDate != null && apptResult != "" && apptResult != null && salesRep != null ){
                helper.sendToMedallia(component, event);
            } else {
                //shows error message if save fails
                var title = "Failed!";
                var message = "The survey failed to send, ensure all required fields are filled";
                var type = "error";
                helper.newToastEvent(component, event, helper, title, message, type);
                window.setTimeout(
                    $A.getCallback(function() {
                        component.set("v.disable", false);
                    }), 3000
                );
            }
        }
    },

    cancel : function(component, event, helper){
        component.set("v.disable", true);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        "url": "/post-appointment-survey"
        });
        urlEvent.fire();
    }

})