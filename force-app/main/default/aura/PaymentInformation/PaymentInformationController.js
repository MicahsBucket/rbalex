({
	 //finds the currently active order based on the Order Id passed from the Collapsable Content Parent Component																											 
    init : function(component, event, helper) {
    
        var getId = component.get('c.getOrderId');
        getId.setCallback(this, function(returnVal){
           var ordId = returnVal.getReturnValue();
           component.set('v.orderId', ordId); 
		var getOrd = component.get("c.getOrder");
  
        getOrd.setParams({
            "orderId" : ordId
        });
        getOrd.setCallback(this, function(returnVal){
            var order = returnVal.getReturnValue();
        //finds all of the payments made on the current order
		 helper.findPayments(order, component, event, helper);
            //finds the total payments and how much is left to pay
            helper.findOrder(order, component, event, helper);
			helper.paymentFacility(order,component,event,helper);
         });
        $A.enqueueAction(getOrd);
       }); 
      $A.enqueueAction(getId);
	},

    handleComponentEvent: function(component,event,helper){
        var message = event.getParam("ChangeOrderId");
        component.set("v.orderId", message);
        helper.reCalculate(component,event,helper);
    },
})