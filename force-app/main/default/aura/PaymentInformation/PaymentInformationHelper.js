({
    //finds all the payments associated to the current order
	findPayments : function(order, component, event, helper){
        var setPayments = component.get("c.getPayments");
        var ordId = order.Id;
        setPayments.setParams({
            "orderId":ordId
        })
        setPayments.setCallback(this, function(returnVal){
            var payments = returnVal.getReturnValue();
            for(var i = 0; i < payments.length; i++){
                //converts the Payment Amount to a currency format
                var amount = payments[i].Payment_Amount__c.toLocaleString();
                payments[i].Payment_Amount__c = amount;
            }
            component.set('v.payments', payments);
        });
        $A.enqueueAction(setPayments);							  
	},

 //finds the total cost and how much has been paid on the current order
    findOrder : function(order, component, event, helper){
        var Sum=parseInt(order.Retail_Subtotal__c)-parseInt(order.Discount_Amount__c);
        var retail = order.Retail_Subtotal__c.toLocaleString();
        var payment = order.Payments_Received__c.toLocaleString();
        var financed = order.Amount_Financed__c.toLocaleString();
        var due = order.Amount_Due__c.toLocaleString();
		var discount = order.Discount_Amount__c.toLocaleString();
        order.Retail_Subtotal__c = retail;
        order.Payments_Received__c = payment;
        order.Amount_Financed__c = financed;
        order.Amount_Due__c = due;
		order.Discount_Amount__c = discount;        
        component.set("v.discountTotal",Sum);
        component.set('v.order', order);							
    }
})