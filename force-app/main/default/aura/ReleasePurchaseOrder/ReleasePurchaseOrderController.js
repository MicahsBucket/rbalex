({
	showToast : function(component, event, helper) {
        var x =event.getParam('mesg');
        var y =event.getParam('type');
      
		var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": y,
        "message": x,
        "type":y
    });
    toastEvent.fire();
	},
    
    
    close : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
	}
})