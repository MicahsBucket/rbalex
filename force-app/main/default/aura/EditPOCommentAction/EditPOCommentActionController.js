({
    closeQuickAction: function(component, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },
    
    handleCancelClick: function(component, event, helper) {
        $A.get('e.force:closeQuickAction').fire();
    },

    handleEnableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', false);
    },

    handleDisableSave: function(component, event, helper) {
        component.set('v.disableSaveButton', true);
    },

    handleSaveClick: function(component, event, helper) {
        var editPOCommentActionComponent = component.find('editPOCommentActionComponent');
        editPOCommentActionComponent.handleSaveClick();
    },

    hideSpinner: function(component, event, helper) {
        component.set('v.showSpinner', false);
        component.set('v.disableCancelButton', false);
        component.set('v.disableSaveButton', false);
	},
    
	showSpinner: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disableCancelButton', true);
        component.set('v.disableSaveButton', true);
	},
    
	showToast: function(component, event, helper) {

        var toastMessage = event.getParam('message');
        var toastType = event.getParam('type');
      
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();

	},

    refresh: function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    }
    
})