({
	init : function(component, event, helper) {
        var recordId = component.get('v.recordId');

        helper.executeAction(component, 'c.getAssignedResources', { workOrderId: recordId })
		.then($A.getCallback(function(result) {
            if (result.assignedResources == null || result.assignedResources == undefined || result.assignedResources.length == 0) {
            	component.set('v.page','unavailable');
            } else {
            	component.set('v.resources', result.assignedResources);
            	component.set('v.primaryServiceResourceId', result.primaryServiceResourceId);
            	component.set('v.page','selection');
            }
            component.set('v.workOrderNumber', result.workOrderNumber);
            component.set('v.showSpinner', false);
        }))
        .catch($A.getCallback(function(error) {
            component.set('v.failure', true);
            console.log(error.message);
            component.set('v.showSpinner', false);
        }));
    },

    onRadioChange: function(component, event, helper) {
        var name = event.currentTarget.id;
        var nameArray = name.split('-');
        var index = nameArray[1];
        var assignedResource = component.get('v.resources')[parseInt(index)];
        component.set('v.selectedResourceId', assignedResource.ServiceResourceId);
        component.set('v.selectedResourceName',assignedResource.ServiceResource.Name);
    },

    updatePrimaryResource: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.failure', false);

        var params = {
            workOrderId: component.get('v.recordId'),
            resourceId: component.get('v.selectedResourceId')
        };

        helper.executeAction(component, 'c.setPrimaryResource', params)
		.then($A.getCallback(function() {
			component.set('v.page','confirmation');
			component.set('v.showSpinner', false);
		}))
		.catch($A.getCallback(function(error) {
            component.set('v.failure', true);
            console.log(error.message);
            component.set('v.showSpinner', false);
        }));
    }
})