({
	doInit : function(component, event, helper) {
		helper.doInit(component);
	},
    validate: function(component,event,helper){
      helper.validateForm(component);
    }
})