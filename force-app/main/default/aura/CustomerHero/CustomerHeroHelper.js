({
    //need to update to check window.location.pathname 
    setHeroImage : function(cmp,event) {
        var url = this.setUrl(cmp);
        console.log(url);        
        var showName = cmp.find("showName");
        var noName = cmp.find("noName");        
        var cmpTarget;
        cmpTarget = cmp.find('myProject');
        if($A.util.hasClass(cmp.find("showName"), "slds-hide")  ){
                this.toggleHide(cmp,showName);                
            } 
            if($A.util.hasClass(cmp.find("noName"), "slds-show")){
                this.toggleHide(cmp,noName);                                
            }   
        /*if(url == 'myProject'){
            cmpTarget = cmp.find('myProject');
            if($A.util.hasClass(cmp.find("showName"), "slds-hide")  ){
                this.toggleHide(cmp,showName);                
            } 
            if($A.util.hasClass(cmp.find("noName"), "slds-show")){
                this.toggleHide(cmp,noName);                                
            }       
            
        } else {
            cmpTarget = cmp.find('hero');
            if($A.util.hasClass(cmp.find("noName"), "slds-hide")  ){            
                this.toggleHide(cmp,noName); 
            } 
            if($A.util.hasClass(cmp.find("showName"), "slds-show")  ){            
                this.toggleHide(cmp,showName);
            }
            
        } */
        var previousPage = cmp.get("v.previousPage");
        if(previousPage != '' || previousPage != null){
            $A.util.removeClass(cmpTarget, previousPage);
        }
        $A.util.addClass(cmpTarget, url); 
        cmp.set("v.previousPage",url);
    },
    toggleHide: function(cmp,element){        
        $A.util.toggleClass(element, 'slds-show');  
        $A.util.toggleClass(element, 'slds-hide');   
    },
    setUrl:function(cmp){
        var currentUrlPathName = window.location.pathname;
        var index = currentUrlPathName.lastIndexOf("/");
        var url = currentUrlPathName.slice(index + 1);
        if(url == '' || url == null){
            url = "myProject";
        } 
        return url;
    }    
    
})