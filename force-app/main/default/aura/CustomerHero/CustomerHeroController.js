({
doInit: function(cmp,event,helper){
    var action = cmp.get("c.getUserName");
    action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            cmp.set("v.firstName", response.getReturnValue());
         }
      });
       $A.enqueueAction(action);
       helper.setHeroImage(cmp);
     },
    handleRouteChange: function(cmp,evt,helper){
       helper.setHeroImage(cmp); 
    }    
})