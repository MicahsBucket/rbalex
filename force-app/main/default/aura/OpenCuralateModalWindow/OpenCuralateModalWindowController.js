({
	loadCuralate : function(component, event, helper) {
        if (
            window.crl8 &&
            window.crl8.ready &&
            typeof window.crl8.ready === "function"
        ) 
        {
            crl8.ready(function() {
                // Use the actual experience name here
                crl8.getExperience('homepage').then(() => {
                    console.log('Experience already created');
                }).catch(() => {
                    crl8.createExperience('homepage');
                });
                    
                });
        }
	},
    closeModal: function(component,event,helper) {
        component.find("overlayLibDemo1").notifyClose();   
        if (
            window.crl8 &&
            window.crl8.ready &&
            typeof window.crl8.ready === "function"
        ) {
            crl8.ready(function() {
                // Use the actual experience name here
                crl8.getExperience('homepage').then(() => {
                    crl8.destroyExperience('homepage');
                }).catch(() => {
                    console.log('No Experience');
                });
                    
                });
                }
  		                   
    },
    afterCuralateScriptLoaded : function(component,event,helper) {
    	console.log("___Executed in Open Curalate Modal Window________");                    
    }
})