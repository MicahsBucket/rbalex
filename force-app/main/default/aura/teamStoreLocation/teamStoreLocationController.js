({
	init : function(component, event, helper) {
        //finds the most recent order
        component.set("v.toggleSpinner", true); 
        var getId = component.get('c.getOrderId');
        getId.setCallback(this, function(returnVal){
           var ordId = returnVal.getReturnValue();
           component.set('v.orderId', ordId);
        });
        $A.enqueueAction(getId);
		/*var orderId = component.get('v.orderId');
        console.log('==>>>',orderId);
        var orderPrimaryContact = component.get("c.getOrderPrimaryContact");
        orderPrimaryContact.setParams({
            "orderId" : ordId
        });
        orderPrimaryContact.setCallback(this, function(returnVal){
           console.log('Response is ===>>>',returnVal.getReturnValue());
           component.set('v.primaryContact', returnVal.getReturnValue());
        });
        $A.enqueueAction(orderPrimaryContact);*/
		  //finds the store location for latest order
        var getStoreInfo = component.get('c.getStoreInfo');
        getStoreInfo.setCallback(this, function(returnVal){
        var getStoreInfo = returnVal.getReturnValue();
        var storeInfo = getStoreInfo.myStore;
        var storeConfigInfo = getStoreInfo.myStoreConfig;
        var billContact = getStoreInfo.billToContact;
        var phone = storeInfo.Phone;
        var LegalName = storeInfo.Legal_Name__c;
        var BillingStreet = storeInfo.BillingStreet;
        var BillingCity = storeInfo.BillingCity;
        var BillingState = storeInfo.BillingState;
        var BillingPostalCode =storeInfo.BillingPostalCode;
        var StartDayOfTheWeek= storeConfigInfo.Start_Day_Of_The_Week__c;
        var EndDayOfTheWeek= storeConfigInfo.End_Day_Of_The_Week__c;
            console.log("end day" , EndDayOfTheWeek);
            console.log("Start day" , StartDayOfTheWeek);
        var OpenTime= storeConfigInfo.Open_Time__c;
        var CloseTime = storeConfigInfo.Close_Time__c;
            console.log("Start time" , OpenTime);
            console.log("End time" , CloseTime);
        component.set('v.primaryContact', billContact);
        component.set('v.order',getStoreInfo.myOrder);
        component.set('v.storeConfig',storeConfigInfo);
        component.set('v.OrderStoreConfigWrap',getStoreInfo);
        component.set('v.startDayOfTheWeek',StartDayOfTheWeek);
        component.set('v.endDayOfTheWeek',EndDayOfTheWeek);
        component.set('v.openTime',OpenTime);
        component.set('v.endTime',CloseTime);
           //var BillingCountry = getStoreInfo.Store_Location__r.BillingCountry;
		   var BillingAddress = '';
           if (BillingStreet)
             BillingAddress = BillingAddress + BillingStreet +',';

           var BillingAddress1 = '';
           if (BillingCity)
             BillingAddress1 =  BillingAddress1 +  BillingCity;
           if (BillingState)
             BillingAddress1 = BillingAddress1 + ', ' + BillingState;
           if (BillingPostalCode)
             BillingAddress1 = BillingAddress1 + '  ' + BillingPostalCode;
           /*if (BillingCountry)
             BillingAddress1 = BillingAddress + ', ' + BillingCountry;*/
            component.set('v.BillingAddress', BillingAddress);
            component.set('v.BillingAddress1', BillingAddress1);
            component.set('v.StorePhone', phone);
            component.set('v.LegalName', LegalName);

        });
        $A.enqueueAction(getStoreInfo);

        //finds all of the orders the user is associated to
        var getOrders = component.get('c.getOrders');
        getOrders.setCallback(this, function(returnVal){
            var orders = returnVal.getReturnValue();
            var orderList = [];
            //creates the list of order dates that is displayed to the user
            for(var i = 0; i < orders.length; i++){
                orderList.push(orders[i].EffectiveDate);
            }
            component.set('v.orderList', orderList);
        });
        $A.enqueueAction(getOrders);
       // helper.helperFun(component,event,'articleFive');
        
        
	},
    
    eventOrder: function(component, event, helper) {
               var order = event.getParam("order");
       var orderDate = event.getParam("orderDate");
       var orderId = event.getParam("orderId");
       var action = component.get('c.getStoreInfoUpdated');
        action.setParams({

            "orderId" : orderId
        });
        action.setCallback(this, function(returnVal){
          var getStoreInfo = returnVal.getReturnValue();
            console.log('the res '+JSON.stringify(returnVal.getReturnValue()));
           var storeInfo = getStoreInfo.myStore;
           var storeConfigInfo = getStoreInfo.myStoreConfig;
           var billContact = getStoreInfo.billToContact;
          var phone = storeInfo.Phone;
          var LegalName = storeInfo.Legal_Name__c;
          var BillingStreet = storeInfo.BillingStreet;
          var BillingCity = storeInfo.BillingCity;
          var BillingState = storeInfo.BillingState;
          var BillingPostalCode =storeInfo.BillingPostalCode;
       	  var StartDayOfTheWeek= storeConfigInfo.Start_Day_Of_The_Week__c;
          var EndDayOfTheWeek= storeConfigInfo.End_Day_Of_The_Week__c;
            console.log("Event end day" , EndDayOfTheWeek);
            console.log("Event Start day" , StartDayOfTheWeek);
          var OpenTime= storeConfigInfo.Open_Time__c;
          var CloseTime = storeConfigInfo.Close_Time__c;
            console.log("eventStart time" , OpenTime);
            console.log("eventEnd time" , CloseTime);
           component.set('v.primaryContact', billContact);
           component.set('v.order',getStoreInfo.myOrder);
           component.set('v.storeConfig',storeConfigInfo);
           component.set('v.OrderStoreConfigWrap',getStoreInfo);
           component.set('v.startDayOfTheWeek',StartDayOfTheWeek);
           component.set('v.endDayOfTheWeek',EndDayOfTheWeek);
           component.set('v.openTime',OpenTime);
           component.set('v.endTime',CloseTime);
      var BillingAddress = '';
          if (BillingStreet)
            BillingAddress = BillingAddress + BillingStreet +',';
          var BillingAddress1 = '';
          if (BillingCity)
            BillingAddress1 =  BillingAddress1 +  BillingCity;
          if (BillingState)
            BillingAddress1 = BillingAddress1 + ', ' + BillingState;
          if (BillingPostalCode)
            BillingAddress1 = BillingAddress1 + '  ' + BillingPostalCode;
           component.set('v.BillingAddress', BillingAddress);
           component.set('v.BillingAddress1', BillingAddress1);
           component.set('v.StorePhone', phone);
           component.set('v.LegalName', LegalName);
           component.set("v.toggleSpinner", false);
       });
       $A.enqueueAction(action);
    },
})