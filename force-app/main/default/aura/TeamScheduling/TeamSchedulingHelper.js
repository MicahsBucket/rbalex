({
    ADDRESS_COMPONENTS: ['Street', 'City', 'State', 'PostalCode', 'CountryCode'],

    SA_STATUS_NEW: 'None',
    SA_STATUS_SCHEDULED: 'Scheduled',

    formatAddress: function(record) {
        var tokens = [];

        this.ADDRESS_COMPONENTS.forEach(function(component) {
            if (record[component]) {
                tokens.push(record[component]);
            }
        });

        return tokens.join(', ');
    },

    saveAppointment: function(component) {
        var params = {
            appointment: component.get('v.serviceAppointment')
        };

        return this.executeAction(component, 'c.saveAppointmentWithoutRollup', params)
            .then($A.getCallback(function(serviceAppointment) {
                component.set('v.serviceAppointment', serviceAppointment);
                return serviceAppointment;
            }));
    },

    extendDates: function(component) {
        var daySlots = component.get('v.daySlots');

        daySlots.forEach(function(slot) {
            console.log('timeSlot ' + slot.timeSlots);
            slot.timeSlots = undefined;
        });

        component.set('v.daySlots', daySlots);

        var params = {
            appointment: component.get('v.serviceAppointment')
        };

        return this.executeAction(component, 'c.extendDate', params)
            .then($A.getCallback(function(serviceAppointment) {
                component.set('v.serviceAppointment', serviceAppointment);
                return serviceAppointment;
            }));
    },

    getAvailableTimeSlots: function(component) {
        var helper = this,
            serviceAppointment = component.get('v.serviceAppointment'),
            resourcesNeeded = component.get('v.resourcesNeeded'),
            workOrderId = component.get('v.recordId'),
            params = {
                serviceAppointmentId: serviceAppointment.Id,
                workOrderId: workOrderId,
                territoryId: component.get('v.selectedTerritoryId'),
                duration: component.get('v.duration')
            };

        component.set('v.daySlots', []);

        return this.executeAction(component, 'c.getTimeSlotsForTerritory', params)
            .then($A.getCallback(function(timeSlots) {
                component.set('v.showSpinner', false);
                component.set('v.showCandidatesSpinner', true);
                component.set('v.step', 1);

                var days = [],
                    previousDate;

                console.log(timeSlots);
                if (timeSlots && timeSlots.length > 0) {
                    component.set('v.appointmentTimeZone', timeSlots[0].appointmentTimeZone);
                }
                
                var promise = timeSlots.reduce(function(p, timeSlot) {
                    console.log(timeSlot);
                    return p.then($A.getCallback(function() {
                        return component.get('v.step') === 1 ?
                            helper.getCandidates(component, timeSlot) : // still on the selection screen, keep loading candidates
                            Promise.resolve(); //  user has already selected a time slot, no need to load more candidates
                    })).then($A.getCallback(function(candidates) {
                        timeSlot.candidates = candidates;

                        if (candidates && candidates.length >= resourcesNeeded) {
                            var slotDate = new Date(timeSlot.start).getDate();

                            if (!previousDate || slotDate !== previousDate) {
                                days.push({
                                    day: timeSlot.start,
                                    timeSlots: [timeSlot]
                                });
                            } else {
                                days[days.length - 1].timeSlots.push(timeSlot);
                            }

                            previousDate = slotDate;
                            component.set('v.daySlots', days);
                        }
                    }));
                }, Promise.resolve());

                promise.then($A.getCallback(function() {
                    component.set('v.showCandidatesSpinner', false);
                }));
            }));
    },

    getCandidates: function(component, timeSlot) {
        return this.executeAction(component, 'c.getCandidatesForTerritory', {
            start: timeSlot.start,
            finish: timeSlot.finish,
            appointmentId: component.get('v.serviceAppointment').Id,
            workOrderId: component.get('v.recordId'),
            territoryId: component.get('v.selectedTerritoryId'),
            duration: component.get('v.duration')
        });
    },

    createAppointments: function(component) {
        var helper = this,
            selectedTimeSlot = component.get('v.selectedTimeSlot'),
            resourceIds = component.get('v.selectedResourceIds'),
            params = {
                serviceAppointmentId: component.get('v.serviceAppointment').Id,
                startTime: selectedTimeSlot.start,
                endTime: selectedTimeSlot.candidates[0].finish,
                resourceIds: resourceIds,
                duration: component.get('v.duration')
            };

        return this.executeAction(component, 'c.createAppointmentsWithDuration', params)
            .then($A.getCallback(function(updatedAppointments) {
                return Promise.all(updatedAppointments.map(function(appointment) {
                    appointment.Scheduled_Via__c = 'Custom - Time Slots';
                    var saveParams = {
                        appointment: appointment
                    };

                    return helper.executeAction(component, 'c.saveAppointment', saveParams);
                }));
            }));
    },

    assignResources: function(component) {
        var helper = this,
            appointments = component.get('v.createdAppointments'),
            resourceIds = component.get('v.selectedResourceIds'),
            assignedResources = [],
            promise = Promise.resolve();

        resourceIds.forEach(function(resourceId, index) {
            var params = {
                appointmentId: appointments[index].Id,
                resourceId: resourceId
            };

            promise = promise.then($A.getCallback(function() {
                return helper.executeAction(component, 'c.assignResource', params)
                    .then($A.getCallback(function(resource) {
                        assignedResources.push(resource);
                    }));
            }));
        });

        return promise.then($A.getCallback(function() {
            return assignedResources;
        }));
    },

    updateDurationOnWorkOrder: function(component) {
        var params = {
                workOrderId: component.get('v.workOrder').Id,
                duration: component.get('v.duration')
        };
        return this.executeAction(component, 'c.updateWorkOrderDuration', params);
    },

    rescheduleAppointments: function(component) {
        var helper = this,
            selectedTimeSlot = component.get('v.selectedTimeSlot'),
            params = {
                workOrderId: component.get('v.workOrder').Id,
                startTime: selectedTimeSlot.start,
                endTime: selectedTimeSlot.candidates[0].finish,
                resourceIds: component.get('v.selectedResourceIds'),
                duration: component.get('v.duration')
            };

        return this.executeAction(component, 'c.rescheduleAppointmentsWithDuration', params)
            .then($A.getCallback(function(updatedAppointments) {
                return Promise.all(updatedAppointments.map(function(appointment) {
                    appointment.Scheduled_Via__c = 'Custom - Time Slots';
                    var saveParams = {
                        appointment: appointment
                    };
                    return helper.executeAction(component, 'c.saveAppointmentWithoutRollup', saveParams);
                }));
            }));
    },

    updateSelectedResources: function(component) {
        var selectedResources = [],
            resourcesNeeded = component.get('v.resourcesNeeded'),
            resourceCmp = component.find('resource'),
            selectedResourceIds = [];

        if (Array.isArray(resourceCmp)) {
            selectedResourceIds = resourceCmp.filter(function(cmp) {
                return cmp.get('v.checked');
            }).map(function(cmp) {
                return cmp.get('v.value');
            });
        } else {
            var isChecked = resourceCmp.get('v.checked');
            if (isChecked) {
                selectedResourceIds.push(resourceCmp.get('v.value'));
            }
        }

        component.set('v.selectedResourceIds', selectedResourceIds);
        var selectedTimeSlot = component.get('v.selectedTimeSlot'),
            candidates = selectedTimeSlot.candidates;
        candidates.forEach(function(resource) {
            if (selectedResourceIds.indexOf(resource.id) > -1) {
                selectedResources.push(resource);
            }
        });
        component.set('v.selectedResources', selectedResources);

        if (selectedResourceIds.length >= resourcesNeeded) {
            component.set('v.resourcesError', false);
        }
    },

    setPrimaryResource: function(component, resourceId) {
        var params = {
            workOrderId: component.get('v.recordId'),
            resourceId: resourceId
        };

        return this.executeAction(component, 'c.setPrimaryResource', params);
    },

    validate: function(component) {
        var helper = this,
            step = component.get('v.step'),
            valid = true;

        if (step === 0) {
            valid = component.find('formInput').reduce(function(validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return inputCmp.get('v.validity').valid && validSoFar;
            }, true);

            valid = component.find('dateInput').reduce(function(validSoFar, dateCmp) {
                return helper.validateDateInput(dateCmp) && validSoFar;
            }, valid);
        } else if (step === 2) {
            var resourcesNeeded = component.get('v.resourcesNeeded'),
                selectedResourceIds = component.get('v.selectedResourceIds');

            valid = selectedResourceIds.length >= resourcesNeeded;
            component.set('v.resourcesError', !valid);
            component.set('v.scheduleErrorMessage', '');
        } else if (step === 3) {
            var primaryResource = component.get('v.primaryResource');
            valid = primaryResource != null;
        }

        return valid;
    },

    validateDateInput: function(dateCmp) {
        var dateString = dateCmp.get('v.value'),
            valid = true;

        if (dateString) {
            var d = new Date(dateString);

            if (window.isNaN(d.getTime())) {
                dateCmp.set('v.errors', [{ message: 'Invalid date' }]);
                valid = false;
            } else {
                dateCmp.set('v.errors', null);
            }
        } else {
            if (dateCmp.get('v.required')) {
                dateCmp.set('v.errors', [{ message: 'Complete this field' }]);
                valid = false;
            } else {
                dateCmp.set('v.errors', null);
            }
        }

        return valid;
    },

    executeAction: function(component, actionName, params) {
        return new Promise(function(resolve, reject) {
            var action = component.get(actionName);

            action.setParams(params);
            action.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    reject(response.getError()[0]);
                }
            });
            $A.enqueueAction(action);
        });
    }
});