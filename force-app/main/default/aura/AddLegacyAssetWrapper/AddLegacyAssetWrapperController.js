({
doInit : function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var accId = myPageRef.state.c__aid;
        $A.createComponent(
            "c:addLegacyAsset",
            {
                "aura:id": "childComponent",
                "accountId": accId,
                
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
        
        
    }
})