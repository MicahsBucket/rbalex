({
	callOrdering : function(component, event, helper) {

		var communityPageName = component.get("v.orderingPageName");

		if (communityPageName == '') {

			var evt = $A.get("e.force:navigateToComponent");
			console.log('Event '+evt);

			evt.setParams({
				componentDef: "c:homeComponentLightningCardOrdering"
			});
			evt.fire();
			
		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callTechSchedule : function(component, event, helper) {
		
		var communityPageName = component.get("v.techSchedulingPageName");
			
		if (communityPageName == '') {

			var evt = $A.get("e.force:navigateToComponent");
			console.log('Event '+evt);

			evt.setParams({
				componentDef: "c:homeComponentLightningCardTechScheduling"
			});
			evt.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callServiceOrdering : function(component, event, helper) {

		var communityPageName = component.get("v.serviceOrderingPageName");
			
		if (communityPageName == '') {

			var evt = $A.get("e.force:navigateToComponent");
			console.log('Event '+evt);

			evt.setParams({
				componentDef: "c:homeComponentLightningCardServiceOrdering"
			});
			evt.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
			
		}


	},

	callInstallSchedule : function(component, event, helper) {
		
		var communityPageName = component.get("v.installSchedulingPageName");
			
		if (communityPageName == '') {

			var evt = $A.get("e.force:navigateToComponent");
			console.log('Event '+evt);

			evt.setParams({
				componentDef: "c:homeComponentLightningCardInstallScheduling"
			});
			evt.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
	
		}

	},

	callDashboards : function(component, event, helper) {


		var getDashboardIdAction = component.get("c.getDashboardId");

		 // Configure the response handler for the action.
		 getDashboardIdAction.setCallback(this, function(response) {
 
			// Make sure everything is find and grab the return value.
			var state = response.getState();
 
			if (component.isValid()) {
 
				var dashboardId = response.getReturnValue();
				if (dashboardId != null && state === "SUCCESS") {
					window.location.href = '/' + dashboardId;
				}
				else {
					window.location.href = '/lightning/o/Dashboard/home?queryScope=mru';
				}
				
			}
			else {
				console.log('Unknown problem, response state: ' + state);
			}

		});
 
		// Execute the action.
		$A.enqueueAction(getDashboardIdAction);

	},

	callServiceWarranty : function(component, event, helper) {

		var communityPageName = component.get("v.serviceWarrantyPageName");
			
		if (communityPageName == '') {

			var evt = $A.get("e.force:navigateToComponent");
			console.log('Event '+evt);

			evt.setParams({
				componentDef: "c:homeComponentLightningCardServiceWarranty"
			});
			evt.fire();

		}
		else {

			component.find("navigationService").navigate({
				type: "comm__namedPage",
				attributes: {
					name: communityPageName
				}
			}, false);
			
		}

	}
	
})