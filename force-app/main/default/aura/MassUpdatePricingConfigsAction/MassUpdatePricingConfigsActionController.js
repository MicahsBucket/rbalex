({
    doInit: function(component, event, helper) {

        // Set some default values.
        component.set("v.disablePreview", true);
        component.set("v.beginningUILabel", "Beginning UI >=");
        component.set("v.endingUILabel", "Ending UI <=");

        // Grab a reference to the "Preview" page/component.
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__MassUpdatePricingConfigsPreviewWrapper'
            }
        };
        component.set("v.pageReference", pageReference);

    },

    handleUnitChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleTrimChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleColorChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleGlazingChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleGrilleChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleHardwareChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleScreenChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleIncreaseDollarChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handleIncreasePercentageChange: function(component, event, helper) {
        helper.enablePreviewButton(component);
    },

    handlePreviewClick: function(component, event, helper) {

        component.set("v.showSpinner", true);

        // Grab the data entry values to be passed to the LWC.
        var wholesalePricebookId = component.get("v.recordId");
        var productValueList = component.get("v.productValueList");
        var beginningUIValue = component.get("v.beginningUIValue");
        var endingUIValue = component.get("v.endingUIValue");
        var unitValue = component.get("v.unitValue");
        var trimValue = component.get("v.trimValue");
        var colorValue = component.get("v.colorValue");
        var glazingValue = component.get("v.glazingValue");
        var grilleValue = component.get("v.grilleValue");
        var hardwareValue = component.get("v.hardwareValue");
        var screenValue = component.get("v.screenValue");
        var increaseByValue = component.get("v.increaseByValue");
        var increaseDollarValue = component.get("v.increaseDollarValue");
        var increasePercentageValue = component.get("v.increasePercentageValue");
        var roundToValue = component.get("v.roundToValue");

        // Uses the pageReference definition from the init handler.
        var pageReference = component.get("v.pageReference");
        pageReference.state = {
            "c__recordId": wholesalePricebookId,
            "c__productList": productValueList,
            "c__beginningUI": beginningUIValue,
            "c__endingUI": endingUIValue,
            "c__unit": unitValue,
            "c__trim": trimValue,
            "c__color": colorValue,
            "c__glazing": glazingValue,
            "c__grille": grilleValue,
            "c__hardware": hardwareValue,
            "c__screen": screenValue,
            "c__increaseBy": increaseByValue,
            "c__increaseDollar": increaseDollarValue,
            "c__increasePercentage": increasePercentageValue,
            "c__roundTo": roundToValue
        };

        var navService = component.find("navService");
        event.preventDefault();

        // Navigate to the LWC.
        navService.navigate(pageReference);
        
        component.set("v.showSpinner", false);
        
    },

	handleCancelClick: function(component, event, helper) {
	    $A.get("e.force:closeQuickAction").fire();
    }

})