({
    getAllOrderItems : function(component) {
        var orderNumber = component.get('v.orderNumber');
        this.handleAction(
            component,
            {orderId:orderNumber},
            "c.getAllOrderItemsApex",
            this.getAllOrderItemsCallback,
            this.getAllOrderItemsCallbackError
        ); 
    },
    getAllOrderItemsCallback : function(component, returnValue, ctx){
        component.set('v.orderItems' , returnValue);
    },
    getAllOrderItemsCallbackError : function (component, errorTitle, errorMessage){
        console.log('In getAllOrderItemsCallbackError : ' + errorTitle);      
        console.log('errorMessge: ' + errorMessage);        
    },
    validateOrderItem : function(component,event){
        var orderItemId = event.getSource().get("v.value");
        var index = event.getSource().get("v.name");
        component.set("v.currentIndex",index);
//        console.log('orderItemId in controller', orderItemId, index);
        this.handleAction(
            component,
            {orderItemId:orderItemId},
            "c.makabilityOrderItem",
            this.validateOrderItemCallback,
            this.validateOrderItemCallbackError
        ); 
        
    },
    validateOrderItemCallback: function(component,returnValue,ctx){
 //       console.log('return value - validate Order Item', returnValue);
        ctx.sendResultEvent(component,returnValue);   
    },
    validateOrderItemCallbackError : function(component,errorTitle, errorMessage){
        console.log('In validateOrderItemCallbackError : ' + errorTitle);      
        console.log('errorMessge: ' + errorMessage);             
    },
    sendResultEvent: function (component, results) {
        //console.log('results in send results event helper', results);
        // all the child components have the same id - mrResults. need to use the current index
        var index = component.get("v.currentIndex");
        var resultsComponentList = component.find('mrResults');
        //console.log('index and child component List[index] in send result', index, resultsComponentList[index]);

        // component.find can return an individual object or an array. need to handle both instances, incase of order with one item only.
        if (Array.isArray(resultsComponentList)) {
            resultsComponentList[index].displayResult(results);
        } else {
            resultsComponentList.displayResult(results);
        }
    },
})