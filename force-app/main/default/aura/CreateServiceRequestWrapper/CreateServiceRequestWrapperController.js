({
    doInit : function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var accId = myPageRef.state.c__aid;
        var storeId = myPageRef.state.c__slid;
        var orderId = myPageRef.state.c__oid;
        var currentStep = myPageRef.state.c__currentStep;
        console.log("Stage: " + myPageRef.state.c__currentStep);
        if(myPageRef.state.c__currentStep==undefined){
            currentStep = "2";
        };
        var showProductListing = myPageRef.state.c__showProductListing;
        console.log("Listing: " + showProductListing);
        if(myPageRef.state.c__showProductListing==undefined){
            showProductListing = false;
        };
        var isServiceRecordCreated = myPageRef.state.c__isServiceRecordCreated;
        console.log('@@@isServiceRecordCreated'+isServiceRecordCreated);
        $A.createComponent(
            "c:createServiceRequest",
            {
                "aura:id": "childComponent",
                "accountId": accId,
                "orderId": orderId,
                "storeId": storeId,
                "currentStep": currentStep,
                "isServiceRecordCreated": isServiceRecordCreated,
                "showProductListing": showProductListing,
            },
            function(newButton, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newButton);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        // Show error message
                    }
            }
        );
        
        
    }
})