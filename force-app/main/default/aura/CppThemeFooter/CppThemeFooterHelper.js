({
    Model:function(component)
    {
        console.log("Model Window for curulate");
        var customId = document.getElementById("curalate").getAttribute("data-crl8-container-id");
        console.log("_____customId________"+customId);
        if(customId!=undefined) {
            try {
                window.crl8.ready(function() {
                    window.crl8.createExperience('homepage');
                });
            } 
            catch(e)
            {
                console.log(e);
            }
        }
    },
    formatPhoneNumber : function(phoneNumber) {
        var cleaned = ('' + phoneNumber).replace(/\D/g, '');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            //var formatphone=  [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
            var formatphone= match[2]+'-'+match[3]+'-'+match[4];
            return formatphone;
        }
        return null;
	},
    patternMatcher:function(componentId,inputValue,inputPattern,isValidPattern)
    {
        if(!inputPattern.test(inputValue))
        {
            $A.util.addClass(componentId, 'slds-has-error');
            componentId.showHelpMessageIfInvalid();
            isValidPattern=false;
        }
        return isValidPattern;
    }
})