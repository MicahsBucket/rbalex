({

    handleCancelClick : function(component, event, helper) {

        var objectName = component.get("v.objectName");
        console.log('Object Name', objectName);

        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": objectName
        });
        homeEvent.fire();
        
    }

})