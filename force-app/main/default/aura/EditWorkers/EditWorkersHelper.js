({
    //clears all input fieldss
	clearFields : function(component, event) {
        	   var firstName = component.find("firstName").set("v.value", "");
                var lastName = component.find("lastName").set("v.value", "");
                var email = component.find("email").set("v.value", "");
                var phone = component.find("phone").set("v.value", "");
                var tech = component.find('techMeasure').set("v.value", false);
                var sales = component.find('salesRep').set("v.value", false);
                var installer = component.find('installer').set("v.value", false);
                var inactive = component.find('inactive').set("v.value", false);
	},

    //creates a list of workers from a server call to the function getWorkers(String role)
    displayWorkerList : function(component, event) {
            var selectedRole = component.find("roleList").get("v.value");
            var rolesAction = component.get("c.getWorkers");
            rolesAction.setParams({
              "role": selectedRole
            });
            rolesAction.setCallback(this, function(workers){
                var workerList = workers.getReturnValue();
                if(workerList.length != 0){
                    component.set("v.workers", workerList);
                } else {
                    component.set("v.editDisplayed", false); 
                    component.set("v.workers", workerList);
                }
            });
            $A.enqueueAction(rolesAction);
    },

    //displays error message based on the message variable passed in
    failMessage : function(component, event, helper, message) {
        var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Failed!",
                "message": message,
                "type": "error",
                });
                toastEvent.fire();
    },

    editDisplay: function (component, event, helper){
        var worker = event.getSource().get("v.value");
        component.set("v.editWorker", worker);
        var roles = worker.Role__c;
        component.set("v.isInstaller", false);
        component.set("v.isSalesRep", false);
        component.set("v.isTechMeasurer", false);
        component.set("v.isInactive", false)
        if(roles.includes("Installer")){
            component.set("v.isInstaller", true);
        }
        if(roles.includes("Sales Rep")){
            component.set("v.isSalesRep", true);
        }
        if(roles.includes("Tech Measurer")){
            component.set("v.isTechMeasurer", true);
        }
        if(roles.includes("Inactive")){
            component.set("v.isInactive", true);
        }
    },
        
})