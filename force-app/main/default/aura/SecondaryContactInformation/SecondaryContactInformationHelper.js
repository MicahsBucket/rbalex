({
    //finds and returns the contact infor for either the primary or secondary contact(if one exists)
	getContactInfo : function(primBool, component, event, helper) {
        
        var getContact = component.get("c.getContactInfo");
        getContact.setParams({
            "primaryBool":primBool
        })
        getContact.setCallback(this, function(returnVal){
        debugger;
            var contact = returnVal.getReturnValue();            
            if(!primBool && contact != null){
                component.set("v.secondContact", contact);
                component.set("v.secondBool", true);
                }

        });
        $A.enqueueAction(getContact);
	},    

    updateSecondaryContact : function(component, event, helper){
        console.log('--------pre  1-------');
      /*  var secfName = component.find('secFirstName').get('v.value');
        alert('2');
        console.log('--------1-------');
        var secLastName1 = component.find('secLastName').get('v.value');
        var secEmail1 = component.find('secEmail').get('v.value');
        var secPhone1 = component.find('secPhone').get('v.value');
        var secStreet = component.find('secStreet').get('v.value');
        var secCity = component.find('secCity').get('v.value');
        var secState = component.find('secState').get('v.value');
        console.log('--------2-------');
      	var secCountry = component.find('secCountry').get('v.value');*/
        var secContact1 = component.get('v.secondContact');
       /* secContact1.FirstName = secfName;
        secContact1.LastName = secLastName1;
        secContact1.Email = secEmail1;
        secContact1.MobilePhone = secPhone1;
        secContact.MailingStreet = secStreet;
        secContact.MailingCity = secCity;
        secContact.MailingCountry = secCountry;
        secContact.MailingPostalCode = secZip;*/
		console.log('--------3-------');
        var updateSec = component.get('c.updateContact');
        updateSec.setParams({
            'contactToUpdate' : secContact1
        });
        updateSec.setCallback(this, function(returnVal){
            var state = returnVal.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Success!",
                "message": "The Secondary Contact has been updated",
                "type": "success",
                });
                toastEvent.fire();
            }else{
            var toastEvent = $A.get("e.force:showToast");  
                toastEvent.setParams({         
                    "title": "Error!",         
                    "message": "The Secondary Contact Could not been updated",
                    "type": "error",
                });             
                toastEvent.fire();
            }
       })
       $A.enqueueAction(updateSec);
    },

    createSecondaryContact : function(component, event, helper){
        console.log('Inside create Contact ');
        var secFirstName = component.find('secFirstName').get('v.value');
        var secLastName = component.find('secLastName').get('v.value');   
        var secEmail = component.find('secEmail').get('v.value');   
        var secPhone = component.find('secPhone').get('v.value');
        var secHomePhone = component.find('secHomePhone').get('v.value');
        var secStreet = component.find('secStreet').get('v.value'); 
        var secCity = component.find('secCity').get('v.value');    
        var secState = component.find('secState').get('v.value');     
        var secCountry = component.find('secCountry').get('v.value');
        var secZip = component.find('secZip').get('v.value'); 
        var contMethod = component.find('contactMethod2').get('v.value');
        console.log('Inside create Contact 2');
        var createContact = component.get('c.createNewContact');
        createContact.setParams({'first' : secFirstName,'last' : secLastName,'contEmail' : secEmail,'phone' : secPhone,'phone1' : secHomePhone,'street' : secStreet,'city' : secCity,'state' : secState,'country' : secCountry,'zip' : secZip, 'prefferedMethod' : contMethod });
        createContact.setCallback(this, function(returnVal){
            var state = returnVal.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");  
                toastEvent.setParams({         
                    "title": "Success!",         
                    "message": "The Secondary Contact has been created",
                    "type": "success",
                });             
                toastEvent.fire();
                console.log('Inside toast Final ');
                helper.getContactInfo(false, component, event, helper);
            }           
            else {
                var toastEvent = $A.get("e.force:showToast");  
                toastEvent.setParams({         
                    "title": "Error!",         
                    "message": "The Secondary Contact Could not been created",
                    "type": "error",
                });             
                toastEvent.fire();
            }
                
        });
        $A.enqueueAction(createContact);
    },
    
   getSecondaryContact: function(component, event, helper){
        var orderId = event.getParam("orderId");
        var action = component.get('c.getSecondaryContactInfo');
       console.log("SecondaryContact:",action);
         console.log("event for the OrderId", orderId);
         action.setParams({
             "primaryBool":false,
             orderId : orderId
         });
        action.setCallback(this, function(returnVal){
            var state = returnVal.getState();
            if (state === "SUCCESS") {
                var con = returnVal.getReturnValue();
                if (con!=null){
               component.set('v.secondContact',returnVal.getReturnValue());
                     console.log("Inside if of the Con:");
                     component.set("v.secondBool", true);
                }else{
                    component.set('v.secondContact',null);
                    component.set("v.secondBool", false);
                }
            }else{
                 var errors = returnVal.getError();
                console.log("error:", errors);
            }
         }) 
        $A.enqueueAction(action);
        
    },
})