//
// Created by 7Summits - Joe Callin on 9/25/17.
//
({
    init: function (component, event, helper) {
        helper.setLink(component);
    },
    linkClick: function(component, event, helper) {
        var link = component.get('v.linkUrl');
        helper.goToLink(component, event, helper, link);
    }
})