/*
* Copyright (c) 2018. 7Summits Inc.
* Created by 7Summits - Joe Callin on 5/29/18.
*/
({
    buildList: function(component, event, helper) {
        var numOfItems = parseInt(component.get('v.numOfItems'));
        var itemList = [];
        for(var i = 1; i <= numOfItems; i++){
            var itemIcon = component.get('v.itemIcon' + i);
            var itemCategory = component.get('v.itemCategory' + i);
            var itemUrl = component.get('v.itemUrl' + i);
            var itemText = component.get('v.itemText' + i);
            if(!$A.util.isUndefinedOrNull(itemUrl) && !$A.util.isEmpty(itemUrl)){
                itemList.push({
                    category: itemCategory,
                    icon: itemIcon.toLowerCase(),
                    text: itemText,
                    url: itemUrl
                })
            }
        }
        console.log(itemList);
        component.set('v.itemList', itemList);
        console.log(component.get('v.itemList'));
        component.set('v.isInit', true);
    },
})