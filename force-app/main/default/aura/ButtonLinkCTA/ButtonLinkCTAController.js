({
	handleClick: function(component, event, helper){
		var address = component.get("v.linkURL");
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": address
		});
		urlEvent.fire();
	}
})