({
    //gets the first and last name of the user and displays it to them
	init: function (component, event, helper) {
        var getPrim = component.get('c.getPrimary');
        getPrim.setCallback(this, function(returnVal){
            component.set('v.primary', returnVal.getReturnValue());
        });
        $A.enqueueAction(getPrim);

        var getSecond = component.get('c.getSecondary');
        getSecond.setCallback(this, function(returnVal){
        var temp = returnVal.getReturnValue(); 
            component.set('v.secondary', returnVal.getReturnValue());
            if(temp != '' && temp != null){
                component.set('v.isSecond', true);
            }
        });
        $A.enqueueAction(getSecond);
	}
})