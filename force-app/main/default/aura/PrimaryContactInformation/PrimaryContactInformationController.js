({
    init : function(component, event, helper) {
        component.set("v.toggleSpinner", true);
        helper.getContactInfo(true, component, event, helper);       
        var gtUserId = component.get("c.getCurrentUserId");
        gtUserId.setCallback(this, function(returnVal){
            //alert('The retVal is '+returnVal.getReturnValue());
            component.set("v.currentUserId",returnVal.getReturnValue());
        });
        $A.enqueueAction(gtUserId);	 
    },
    
    updateContacts : function(component, event, helper) {       
        
        var inputCmp = component.find("primFirstName");
        //var value = inputCmp.get("v.value");
        var validExpense =  $A.util.isEmpty(inputCmp.get("v.value"));
        
        if(validExpense){
            inputCmp.setCustomValidity("Please enter value");
        }else{
            
            helper.updatePrimaryContact(component, event, helper);
            
        }
        
    },
    
    clickPhone : function(component, event, helper){
        document.getElementById('phoneLink').click();
    },
    eventOrder : function(component, event, helper){
        helper.getOrderBookingDates(component, event, helper);  
        
    },
    
})