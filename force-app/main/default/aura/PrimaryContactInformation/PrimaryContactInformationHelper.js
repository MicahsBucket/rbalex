({
    //finds and returns the contact infor for either the primary or secondary contact(if one exists)
	getContactInfo : function(primBool, component, event, helper) {
        
        var getContact = component.get("c.getContactInfo");
        getContact.setParams({
            "primaryBool":primBool
        })
        getContact.setCallback(this, function(returnVal){
            var contact = returnVal.getReturnValue();
            if(primBool){
                component.set("v.primContact", contact);
            }            
        });
        $A.enqueueAction(getContact);
	},

    //updates primary contact (currently only first, last, phone and email are updatable but if more fields are desired just make the fields editable in the cmp and they will update as well)
    updatePrimaryContact : function(component, event, helper){
        var primFirstName = component.find('primFirstName').get('v.value');
        var primLastName = component.find('primLastName').get('v.value');
        var primEmail = component.find('primEmail').get('v.value');
        var primPhone = component.find('primPhone').get('v.value');
        var primHomePhone = component.find('primHomePhone').get('v.value');
        var primCountry = component.find('primCountry').get('v.value');
        var primContact = component.get('v.primContact')
        primContact.FirstName = primFirstName;
        primContact.LastName = primLastName;
        primContact.Email = primEmail;
        primContact.MobilePhone = primPhone;
        primContact.HomePhone= primHomePhone;
        primContact.MailingCountry = primCountry;
        primContact.Preferred_Method_of_Contact__c = component.find('contactMethod').get('v.value');
        var updatePrim = component.get('c.updateContact');
        updatePrim.setParams({
            'contactToUpdate' : primContact
        });
        updatePrim.setCallback(this, function(returnVal){
            var state = returnVal.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Success!",
                "message": "The Primary Contact has been updated",
                "type": "success",
                });
                toastEvent.fire();
				$A.get('e.force:refreshView').fire();
            }
            else if (state === "ERROR") {
                var errors = returnVal.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
       })
       $A.enqueueAction(updatePrim);
    },
    
    getOrderBookingDates: function(component, event, helper){
        var orderId = event.getParam("orderId");
        var action = component.get('c.getContactPrimaryInfo');
         console.log("event for the OrderId", orderId);
         action.setParams({
             "primaryBool":true,
             orderId : orderId
         });
        action.setCallback(this, function(returnVal){
            var state = returnVal.getState();
            if (state === "SUCCESS") {
                
               component.set('v.primContact',returnVal.getReturnValue());
               
            }else{
                 var errors = returnVal.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.toggleSpinner", false);
         }) 
        $A.enqueueAction(action);
 
    },
 })