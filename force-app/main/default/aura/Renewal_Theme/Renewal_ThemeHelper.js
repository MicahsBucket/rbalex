/*
 * Copyright (c) 2019. 7Summits Inc.
 * Created by 7Summits - Joe Callin on 2019-03-27.
*/
({
    checkUserPermission: function(component, event, helper) {
        var params = {};
        helper.doCallout(component, 'c.checkIfSuperAdmin', params).then(function(response){
            if (response.success){
                component.set('v.dashboardAdmin', response.renewalResults[0].dashboardAdmin);
                component.set('v.rforceAdmin', response.renewalResults[0].rforceAdmin);
            } else {
                helper.showMessage('error',response.messages[0]);
                console.log('Error: ', response.messages[0]);
            }
        }).catch(function (response) {
            console.log(response.message);
        }).finally(function(){

        });
    },
})