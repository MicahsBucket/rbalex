/**
 * Created by lee-anneclarke on 12/14/18.
 */
({
    doInit: function(component, event, helper) {
        var inBuilder = helper.isInBuilder(component, event, helper);
        if(!inBuilder){
            helper.checkUserPermission(component, event, helper);
        }
        component.set('v.inBuilder', inBuilder);
    },
    goToSite: function goToSite(component, event, helper) {
        // Using Peak_Base helper method.
        helper.goToUrl(event.currentTarget.dataset.url);
    },

    toggleSearch: function(component, event, helper) {
        var searchWrap = component.find("searchWrap");
        $A.util.toggleClass(searchWrap, "slds-hide");
    },

    hideSearch: function(component) {
        var searchWrap = component.find("searchWrap");
        $A.util.addClass(searchWrap, "slds-hide");
    },
})