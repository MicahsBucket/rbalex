({  
    //finds all the payments associated to the current order
	findPayments : function(order, component, event, helper){
        var setPayments = component.get("c.getPayments");
        var ordId = order.Id;
        setPayments.setParams({
            "orderId":ordId
        })
        setPayments.setCallback(this, function(returnVal){
            var payments = returnVal.getReturnValue();
            for(var i = 0; i < payments.length; i++){
                //converts the Payment Amount to a currency format
                var amount = payments[i].Payment_Amount__c.toLocaleString();
                amount = helper.commafy(amount);
                if (amount.indexOf('.')!=-1){
                    amount= amount.split('.')[0];
                }
                payments[i].Payment_Amount__c = amount;
            }
            component.set('v.payments',payments);
        });
        $A.enqueueAction(setPayments);
	},

    //finds the total cost and how much has been paid on the current order
    findOrder : function(order, component, event, helper){
        var retail ='';
        if (order.Retail_Subtotal__c.toLocaleString().indexOf('.')!=-1){
            retail = order.Retail_Subtotal__c.toLocaleString().split('.')[0];
        }else{
             retail = order.Retail_Subtotal__c.toLocaleString();
        }  
        retail = helper.commafy(retail);
        var payment ='';
        if (order.Payments_Received__c.toLocaleString().indexOf('.')!=-1){
        	 payment = order.Payments_Received__c.toLocaleString().split('.')[0];
        }else{
        	 payment = order.Payments_Received__c.toLocaleString();   
        }
        payment = helper.commafy(payment);
         var financed='';
        if(order.Amount_Financed__c.toLocaleString().indexOf('.')!=-1){
        	 financed = order.Amount_Financed__c.toLocaleString().split('.')[0];
        }else{
             financed = order.Amount_Financed__c.toLocaleString();
        }
        financed = helper.commafy(financed);
        var due ='';
        if(order.Amount_Due__c.toLocaleString().indexOf('.')!=-1){
        	 due = order.Amount_Due__c.toLocaleString().split('.')[0];
        }else{
             due = order.Amount_Due__c.toLocaleString();
        }
        due = helper.commafy(due);
        var discount = '';
         if(order.Discount_Amount__c.toLocaleString().indexOf('.')!=-1){
        	 discount = order.Discount_Amount__c.toLocaleString().split('.')[0];
        }else{
              discount = order.Discount_Amount__c.toLocaleString();
        }
        discount = helper.commafy(discount);
		var retailtotal ='';
         if(order.Retail_Total__c.toLocaleString().indexOf('.')!=-1){
        	 retailtotal = order.Retail_Total__c.toLocaleString().split('.')[0];
        }else{
              retailtotal = order.Retail_Total__c.toLocaleString();
        }
        retailtotal = helper.commafy(retailtotal);
        order.Retail_Total__c = retailtotal;
        order.Retail_Subtotal__c = retail;
        order.Payments_Received__c = payment;
        order.Amount_Financed__c = financed;
        order.Amount_Due__c = due;
		order.Discount_Amount__c = discount;
        component.set('v.order', order);
    },
        commafy : function( num){
      var parts = (''+(num<0?-num:num)).split("."), s=parts[0], L, i=L= s.length, o='';
      while(i--){ o = (i===0?'':((L-i)%3?'':',')) 
                      +s.charAt(i) +o }
      return (num<0?'':'') + o + (parts[1] ? '.' + parts[1] : ''); 
    }
})