({
	init: function(component, event, helper) {
      //call the helper function with pass [component, Controller field and Dependent Field] Api name
      helper.fetchPicklistValues(component, event, helper);
   },

    // function call on change tha controller field
   ChangeTheOption: function(component, event, helper) {
      //alert('Inside one '+event.getSource().get("v.value"));
      // get the selected value
      var scontrollerValueKey = event.getSource().get("v.value");
       var selectOrdered =  component.get('v.selectedOrder');
       var faqWrap = component.get('v.FAQWrap.orderMap');
       var orderId = faqWrap[selectOrdered];
       component.set("v.selOrder",orderId);
      // console.log("@orderId",orderId.Store_Location__r.Active_Store_Configuration__r.Customer_Service_Number__c); 
      // console.log("@sun",scontrollerValueKey); 
      // console.log("!selectedOrder", component.get('v.selectedOrder'));
 		//alert('The selected value is '+scontrollerValueKey);
      //component.set("v.primContact.MailingCountry",controllerValueKey);
      // get the map values
      var Map = component.get("v.depnedentFieldMap");

      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (scontrollerValueKey != '') {

         // get dependent values for controller field by using map[key].
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[scontrollerValueKey];
          console.log("!ListOfDependentFields", ListOfDependentFields);
         helper.fetchDepValues(component, ListOfDependentFields);

      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
        component.set("v.topicList", defaultVal);
         component.set("v.isDependentDisable", true);
      }
   },

   // function call on change tha Dependent field
   onDependentFieldChange: function(component, event, helper) {
      //alert(event.getSource().get("v.value"));
      component.set("v.primContact.MailingState",event.getSource().get("v.value"));
   },
    submitContact21 : function(component,event,helper){
        
         var myLabel = component.find("top").get("v.label");
        console.log('------Print the myLabel value--------'+ myLabel );
        var sTopic = component.get("v.selectedTopic");
        console.log('------Print the sTopic value--------'+ sTopic );
        //alert(1);
        console.log('------Method inside submitContact21 -------------');
        var selectOrdered =  component.get('v.selectedOrder');
        console.log('------Select Order value -------------' + selectOrdered);
        var faqWrap = component.get('v.FAQWrap.orderMap');
        console.log('------Print the faqWrap value--------'+ faqWrap );
       
        var orderId = faqWrap[selectOrdered];
        console.log('------Print the orderId value--------'+ orderId );
        var selTopicLabel = component.get("v.topicList");
        console.log('------Print the selTopicLabel value--------'+ selTopicLabel );
        
        
        var len = component.get("v.topicList")[sTopic];
        
        console.log('------Print the len value--------'+ len );

        var action1 = component.get("c.submitContact");
       
        console.log('------Method inside submitContact21 action1-------------');

        action1.setParams({'notess' : component.get("v.noteAreaField"), 'stopic' : len.strValue, 'selOrder' : orderId.Id, 'stopicLabel':len.strLabel});
        action1.setCallback(this,function(response){
            console.log('------Print the value for the orderId--------'+ orderId );
            console.log('------Print the value for the sTopic--------'+ sTopic );
             if (response.getState() === "SUCCESS") {
                 var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Email Sent Successfully',
            messageTemplate: 'Record {0} created! See it {1}!',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
            
        });
        toastEvent.fire();
					component.set('v.orderList', null);
					component.set('v.noteAreaField', null);
					component.set('v.topicList',null);
					component.set('v.isDependentDisable',true);
                 //alert(response.getReturnValue());
                 //alert(3);
                $A.get('e.force:refreshView').fire(); // refreshing the page for CPP- 520
             }else{
                 //alert(4);
                 var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message:'Unable to create Contact',
            messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
             }
        });
        $A.enqueueAction(action1);
    }
    
})