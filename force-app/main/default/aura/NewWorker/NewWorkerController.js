({
    init: function(component, event, helper) {
        var action = component.get("c.isManualUser");
        action.setCallback(this, function(response){
           var isManual = response.getReturnValue();
           component.set("v.isManualBool", isManual);
        });
        $A.enqueueAction(action);
    },
    
    //creates a new worker based on the users input
    saveNewWorker: function (component, event, helper) {
        component.set("v.disable", true);
        var firstName = component.find("firstName").get("v.value");
        var lastName = component.find("lastName").get("v.value");
        var email = component.find("email").get("v.value");
        var phone = component.find("phone").get("v.value");
        
        //gets which worker roles were selected 
        var workersSelected = "";
        var tech = component.find('techMeasure').get("v.value");
        var sales;
        if(component.get('v.isManualBool')){
            sales = component.find('salesRep').get("v.value");    
        }
        var installer = component.find('installer').get("v.value");
        if(tech) workersSelected += "Tech Measurer;"; 
        if(installer) workersSelected += "Installer;";
        if(sales) workersSelected += "Sales Rep;"; 

        //if email is left blank or no roles are selected no new worker is created
        if(email != null && workersSelected != ""){
            var dupChecker = component.get("c.checkForDuplicate");
            dupChecker.setParams({
                "email": email
            });
            $A.enqueueAction(dupChecker);
            dupChecker.setCallback(this, function(response) {
                var val = response.getReturnValue();
                component.set('v.duplicateWorker', val);

            if(component.get('v.duplicateWorker') == null){
                var workerAction = component.get("c.saveWorker");
                workerAction.setParams({
                    "firstName": firstName,
                    "lastName": lastName,
                    "email": email,
                    "phone": phone,
                    "workersSelected": workersSelected
                });
                $A.enqueueAction(workerAction);
                workerAction.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state === "SUCCESS"){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": "Success!",
                        "message": "The new worker has been saved successfully.",
                        "type": "success",
                        });
                        toastEvent.fire();
                        helper.clearFields(component, event);
                    } else {
                        //shows error message if save fails
                        var message = "New worker failed to save";
                        helper.failMessage(component, event, helper, message);
                    }
                });
            } else {
                var message = component.get('v.duplicateWorker');
                helper.failMessage(component, event, helper, message);
                component.set('v.duplicateWorker', null);
            }
            });
        } else {
            var message = "Add email and/or select worker role";
            helper.failMessage(component, event, helper, message);
        }
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
    },

    clearFields: function (component, event, helper) {
        component.set("v.disable", true);
        helper.clearFields(component, event);
        window.setTimeout(
            $A.getCallback(function() {
                component.set("v.disable", false);
            }), 3000
        );
    },
})