import { LightningElement, api, track, wire } from "lwc";
import getAppointmentAll from "@salesforce/apex/SalesSchedAppointmentCtrl.getAppointmentAll";

export default class SalesSchedAssignModalAll extends LightningElement {
  //ss-167
  @api appointmentList = false; //Passed from parent in case of All
  @api displayRideAlongDetails; //Passed in by a parent component, determines if ride along info is shown (e.g. if the appointment is already assigned.)
  @api readOnly;
  @api prepopulatedRepId;
  @api prepopulatedRepName;
  @api capacityId;
  @api header;
  @track error = false;
  @track displayResultedButton;
  @track accordianStyle;

  @wire(getAppointmentAll, { appointmentIdSet: "$appointmentList" })
  appointments;

  get boxStyle() {
    debugger;
    if (this.appointments.data) {
      let styleAttr = "padding: 5px;";
      styleAttr +=
        this.appointments.data.length > 1
          ? "border-bottom: 5px solid #828f69;"
          : "";
      return styleAttr;
    }
    return null;
  }

  handleErrorModalClose() {
    this.error = false;
  }

  // get AppointnmentLabels() {
  //   let IdToLabelMap = {};
  //   if (this.appointments.data) {
  //     this.appointments.data.forEach(appt => {
  //       IdToLabelMap[appt.appointment.Id] =
  //         appt.formattedTime + "with" + appt.primaryContact.Name;
  //     });
  //   }
  //   return IdToLabelMap;
  // }

  handleAssignmentModalClose() {
    //this.appwrapper = false;
    //this.appointmentId = undefined;
    this.reset();
    this.dispatchEvent(new CustomEvent("assignmodalclose"));
  }

  handlerror(evt) {
    this.error = evt.detail.errorMessage;
  }

  // populateInfo(evt) {
  //   debugger;
  //   this.accordianStyle = "border-bottom: 5px solid #828f69";
  //   this.template.querySelector(`[data-id=${evt.detail.id}]`).label =
  //     evt.detail.heading;
  //   //this.header = evt.detail.repName ? evt.detail.repName : this.header;
  //   this.displayResultedButton = evt.detail.displayResultedButton;
  // }

  handleResultantsData() {
    this.template.querySelector("[data-id=compBody]").handleResultantData();
  }

  //SS-257 Start
  handleRemoveAssignment(evt) {
    debugger;
    let removedAppointmentId = evt.detail.appointmentId;
    if (removedAppointmentId && this.appointmentList) {
      if (this.appointmentList.length > 1) {
        this.appointmentList = this.appointmentList.filter(
          appt => appt !== removedAppointmentId
        );
        this.dispatchEvent(new CustomEvent("refresh"));
      }
    }
  }
  //SS-257 End

  refresh() {
    this.dispatchEvent(new CustomEvent("refresh"));
  }

  get AppointmentListSize() {
    return this.appointmentList.length;
  }

  reset() {
    this.appointmentList = false;
    this.capacityId = undefined;
    this.header = undefined;
    this.error = false;
    this.prepopulatedRepId = undefined;
    this.prepopulatedRepName = undefined;
    this.displayRideAlongDetails = false;
    this.dispatchEvent(new CustomEvent("reset"));
  }
}