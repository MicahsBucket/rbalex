/*
* @author Jason Flippen
* @date 01/23/2020 
* @description Class to provide functionality for the receivePurchaseOrderAction LWC.
*              Supporting database functionality provided by the following Apex Class:
*			   - ReceivePOActionController
*/
import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';
import getPurchaseOrderData from '@salesforce/apex/ReceivePOActionController.getPurchaseOrderData';
import receivePurchaseOrder from '@salesforce/apex/ReceivePOActionController.receivePurchaseOrder';

const productColumns = [
    { label: 'Product', fieldName: 'productName', type: 'string', cellAttributes: { 'alignment': 'left' } },
    { label: 'Quantity', fieldName: 'quantity', type: 'number', cellAttributes: {'alignment': 'left'} },
    { label: 'Quantity to Receive', fieldName: 'quantityToReceive', type: 'number', editable: true, cellAttributes: {'alignment': 'left'} }
];

export default class receivePurchaseOrderAction extends LightningElement {

    @api recordId;

    wiredPOResult;
    recordError;

    purchaseOrder;
    productColumnList = productColumns;
    isEligible = false;
    displayText = '';
    disableButtons = false;
    showSpinner = false;
    allProductsSelected = false;

    @wire(CurrentPageReference) pageRef;

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Event fired when the element is inserted into the document.
    *              It's purpose here is to subscribe the "updatePOEvent". 
    */
    connectedCallback() {
        registerListener('updatePOEvent', this.refreshPOData, this);
    }
    
    /*
    * @author Jason Flippen
    * @date 02/28/2020 
    * @description Retrieves data from Purchase Order.
    */
    @wire(getPurchaseOrderData, { purchaseOrderId: '$recordId' })
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.purchaseOrder = JSON.parse(JSON.stringify(data));

            // Is the Purchase Order eligible to be Received?
/*
            if (this.purchaseOrder.relatedOrderService === true &&
                this.purchaserOrder.vendorName === 'Andersen Logistics - Minneapolis' &&
                this.purchaseOrder.storeLocation.Name === '0077 - Twin Cities' &&
                this.purchaseOrder.chargeCostTo === 'Manufacturing') {
                this.isEligible = false;
            }
*/
            if (this.purchaseOrder.vendorName !== '' && this.purchaseOrder.relatedOrderIsClosed === false &&
                (this.purchaseOrder.status === 'Confirmed' || this.purchaseOrder.status === 'Partially Received')) {
                this.isEligible = true;
                this.displayText = 'All available products are currently selected';
            }
            else {
                this.isEligible = false;
                this.displayText = 'Purchase Order cannot be Received';
            }

            this.setAllProductsSelected();
            this.enableSave();

        }
        else if (error) {
            this.recordError = error;
            console.log('error in PurchaseOrderData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/16/2021 
    * @description Sets the "allProductsSelected" variable.
    */
    setAllProductsSelected() {

        let newValue = true;

        for (let index in this.purchaseOrder.productList) {

            // Make sure the Product has a Quantity to Receive.
            if (this.purchaseOrder.productList[index].quantityToReceive < this.purchaseOrder.productList[index].quantity) {
                newValue = false;
                break;
            }

        }

        this.allProductsSelected = newValue;

    }

    /*
    * @author Jason Flippen
    * @date 02/26/2020
    * @description Method to handle the "Change" event from the Product List datatable component.
    */
    handleProductValueChange(event) {

        let draftValues = JSON.parse(JSON.stringify(event.detail.draftValues));
        let productList = JSON.parse(JSON.stringify(this.purchaseOrder.productList));
        for(let draftIndex in draftValues){

            let draftId = draftValues[draftIndex].id;
            let draftQuantityToReceive = draftValues[draftIndex].quantityToReceive;
            if (draftQuantityToReceive == '') {
                draftQuantityToReceive = 0;
            }

            console.log('Draft Id', draftId);
            console.log('Draft quantityToReceive', draftQuantityToReceive);

            for (let productIndex in productList) {

                let productId = productList[productIndex].id;
                if (productId === draftId) {
                    if (draftQuantityToReceive !== undefined) {
                        productList[productIndex].quantityToReceive = draftQuantityToReceive;
                        this.purchaseOrder.productList[productIndex] = productList[productIndex];
                    }
                    break;
                }
            }

        }
        console.log('Product List (Changed)', JSON.parse(JSON.stringify(this.purchaseOrder.productList)));

        this.setAllProductsSelected();

        this.enableSave();

    }

    /*
    * @author Jason Flippen
    * @date 02/26/2020
    * @description Method to enable/disable the Save button.
    */
    enableSave() {

        let enableSave = false;
        
        if (this.isEligible === true) {

            for (let index in this.purchaseOrder.productList) {

                // Make sure the Product has a Quantity to Receive.
                if (this.purchaseOrder.vendorOverReceivingAllowed === false &&
                    this.purchaseOrder.productList[index].quantityToReceive > this.purchaseOrder.productList[index].quantity) {
                    enableSave = false;
                    break;
                }
                else if (this.purchaseOrder.productList[index].quantityToReceive > 0) {
                    enableSave = true;
                }

            }

        }

        // Raise the event to Enable or Disable the Save button.
        if (enableSave === false) {
            const disableSaveEvent = new CustomEvent('DisableSave');
            this.dispatchEvent(disableSaveEvent);
        }
        else if (enableSave === true) {
            const enableSaveEvent = new CustomEvent('EnableSave');
            this.dispatchEvent(enableSaveEvent);
        }

    }

    /*
    * @author Jason Flippen
    * @date 02/28/2020
    * @description Method to update the Status of the Purchase Order to "Received" or "Partially Received".
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);
        // Call Apex method to receive the Purchase Order.
        receivePurchaseOrder({ purchaseOrder: this.purchaseOrder })
        .then((result) => {

            let toastMessage;
            let toastType;
            let saveSuccess = false;
            if (result === 'Receive PO Success') {
                saveSuccess = true;
                toastMessage = 'Purchase Order has been Received';
                toastType = 'success';
            }
            else if (result === 'Partial Receive PO Success') {
                saveSuccess = true;
                toastMessage = 'Purchase Order has been Partially Received';
                toastType = 'success';
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);
            
            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

//                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
//                return refreshApex(this.wiredPOResult);
            }
            
        })
        .catch((error) => {
            console.log('Receive Purchase Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Method to refresh the cache. 
    */
    refreshPOData() {
        console.log('Refreshing receivePO');
        return refreshApex(this.wiredPOResult);
    }

}