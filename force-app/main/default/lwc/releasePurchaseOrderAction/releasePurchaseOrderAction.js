/*
* @author Jason Flippen
* @date 01/08/2020 
* @description Provides functionality for the releasePurchaseOrderAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - ReleasePOActionController
*/

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';
import getPurchaseOrderData from '@salesforce/apex/ReleasePOActionController.getPurchaseOrderData';
import releasePurchaseOrder from '@salesforce/apex/ReleasePOActionController.releasePurchaseOrder';

export default class releasePurchaseOrderAction extends LightningElement {

    @api recordId;

    wiredPOResult;
    purchaseOrder;
    recordError;
    displayText = '';
    disableButtons = false;
    showSpinner = false;

    @wire(CurrentPageReference) pageRef;

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Event fired when the element is inserted into the document.
    *              Its purpose here is to subscribe the "updatePOEvent". 
    */
    connectedCallback() {
        registerListener('updatePOEvent', this.refreshPOData, this);
    }
    
    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description Method to retrieve data from Purchase Order. 
    */
    @wire(getPurchaseOrderData, { 'purchaseOrderId': '$recordId' })
    wiredGetPurchaseOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredPOResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Record Data', JSON.parse(JSON.stringify(data)));
            this.purchaseOrder = JSON.parse(JSON.stringify(data));

            let disableSave = false;

            // Is the Purchase Order eligible to be Released?
            if (this.purchaseOrder.vendorName !== '' && (this.purchaseOrder.status === 'In Process' || this.purchaseOrder.status === 'Rejected') &&
                (this.purchaseOrder.costPurchaseOrder === true || this.purchaseOrder.relatedOrderService === true ||
                 (this.purchaseOrder.relatedOrderIsClosed === false &&
                  (this.purchaseOrder.relatedOrderCORO === false || (this.purchaseOrder.relatedOrderCORO === true && this.purchaseOrder.revRecNullAndValidStatus === true))))) {
                disableSave = false;
                this.displayText = 'Are you sure you want to release this Purchase Order?';
            }
            else {
                disableSave = true;
                this.displayText = 'Purchase Order cannot be Released';
            }

            if (disableSave === false) {
                const enableSaveEvent = new CustomEvent('EnableSave');
                this.dispatchEvent(enableSaveEvent);
            }
            else {
                const disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

        }
        else if (error) {
            this.recordError = error;
            console.log('error in PurchaseOrderData callback', error);
        }

    }

    /*
    * @author Jason Flippen
    * @date 01/08/2020
    * @description Method to update the Status of the Purchase Order to "Released".
    */
    @api
    handleSaveClick() {

       const showSpinner = new CustomEvent('ShowSpinner');
       const hideSpinner = new CustomEvent('HideSpinner');
       const closeQuickAction = new CustomEvent('CloseQuickAction');

       this.dispatchEvent(showSpinner);

       // Call Apex method to release the Purchase Order.
       releasePurchaseOrder({ purchaseOrderId: this.recordId })
       .then((result) => {

           let toastMessage;
           let toastType;
           let saveSuccess = false;
           if (result === 'Release PO Success') {
               saveSuccess = true;
               toastMessage = 'Purchase Order has been Released';
               toastType = 'success';
           }
           else  {
               toastMessage = result;
               toastType = 'error';
           }

           this.dispatchEvent(hideSpinner);
           
           const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
           this.dispatchEvent(showToastEvent);
           this.dispatchEvent(closeQuickAction);

           if (saveSuccess === true) {
               
                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updatePOEvent', '');

//                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
//                return refreshApex(this.wiredPOResult);
            }
           
       })
       .catch((error) => {
           console.log('Release Purchase Order Error', error);
           this.dispatchEvent(hideSpinner);
           this.dispatchEvent(this.closeQuickAction);
       });

    }

    /*
    * @author Jason Flippen
    * @date 03/16/2020
    * @description Method to refresh the cache. 
    */
    refreshPOData() {
        console.log('Refreshing releasePO');
        return refreshApex(this.wiredPOResult);
    }

}