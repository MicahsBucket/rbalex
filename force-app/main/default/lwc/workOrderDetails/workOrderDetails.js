import { LightningElement, api, wire } from 'lwc';
import fetchWorkOrder from '@salesforce/apex/Work_Order_Details_Controller.getWorkOrder';
import fetchUserTimeZone from '@salesforce/apex/Work_Order_Details_Controller.getCurrentLoggedInUserTimeZone';

export default class WorkOrderDetails extends LightningElement {
    @api recordId;

    @wire(fetchWorkOrder, { workOrderId: '$recordId'  })
    contact;


    @wire(fetchUserTimeZone, {})
    _timezone;
    get timezone() {
        return this._timezone;
    }
    

    get worktypeurl() {
        return '/'+this.contact.data.WorkTypeId;
    }
    
    get serviceTerritoryurl() {
        return '/'+this.contact.data.ServiceTerritoryId;
    }

    get accounturl() {
        return '/'+this.contact.data.AccountId;
    }

    get contacturl() {
        return '/'+this.contact.data.ContactId;
    }

    get soldOrderurl() {
        return '/'+this.contact.data.Sold_Order__c;
    }
    
    get opportunityurl() {
        return '/'+this.contact.data.Opportunity__c;
    }
}