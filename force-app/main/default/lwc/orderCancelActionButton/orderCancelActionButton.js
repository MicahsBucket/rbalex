import { LightningElement, api, wire } from 'lwc';
import getOrder from '@salesforce/apex/cancelOrderButtonController.getOrder';
import cancelOrder from '@salesforce/apex/cancelOrderButtonController.cancelOrder';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import CANCEL_REASON_FIELD from '@salesforce/schema/Order.Cancellation_Reason__c';

export default class OrderCancelActionButton extends LightningElement {
    @api recordId;
    textToDisplay = '';
    showCancellationDropdown = false;
    picklistOptions;
    CancelReason='';
    returnedData;
    showCancelWithWOButton = false;
    connectedCallback(){
        console.log('recordId IN OrderCancelActionButton', this.recordId);
        // this.textToDisplay = `Record to Cancel ${this.recordId}`
        this.getOrderDetails()
    }

    @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: CANCEL_REASON_FIELD })
    wiredPicklist({ error, data }) {
        if (data) {
            console.log('Picklist data', data);
            this.picklistOptions = data.values;
        } else if (error) {
            console.log('Picklist Error', error);
        }
    }

    getOrderDetails(){
        getOrder({recordId: this.recordId})
        .then(data => {
            if(data){
                this.returnedData = data;
                console.log('getOrderDetails data',data);
                console.log('getOrderDetails Work_Orders__r',data.Work_Orders__r);
                if(data.Status == 'Cancelled'){
                    this.textToDisplay = 'This order is already cancelled';
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if(data.Revenue_Recognized_Date__c != null){
                    this.textToDisplay = 'An order with a Revenue Recognized Date cannot be cancelled.';
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if(data.Status == 'Order Released' || data.Status == 'Install Needed' || data.Status == 'Install Scheduled'){
                    this.textToDisplay = `You can't cancel an Order with '${data.Status}' status.`;
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if(data.Retail_Total__c != data.Amount_Due__c){
                    this.textToDisplay = `Please enter a refund before cancelling the Order.`;
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if(data.Work_Orders__r != null || data.Work_Orders__r != undefined){
                    this.textToDisplay = `There are open Visit Work Orders, Are you sure you want to cancel this Order?`;
                    this.showCancelWithWOButton = true;
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else if(data.Cancellation_Reason__c == null){
                    this.textToDisplay = `You must select a Cancellation Reason.`;
                    this.showCancellationDropdown = true;
                    // this.getCancellationReasonPicklist();
                    this.dispatchEvent(new CustomEvent('DisableSave'));
                }
                else{
                    this.textToDisplay = 'Are you sure you want to cancel this order?';
                }
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    @api
    handleSaveClick() {
        console.log('IN CLICK HANDLER ON OrderCancelActionButton ID', this.recordId);
        var showSpinner = new CustomEvent('ShowSpinner');
        var hideSpinner = new CustomEvent('HideSpinner');
        var closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to cancel the Purchase Order.
        cancelOrder({ OrderId: this.recordId, Reason: this.CancelReason })
        .then((result) => {

            var toastMessage;
            var toastType;
            var saveSuccess = false;
            if (result === 'Order Cancelled') {
                saveSuccess = true;
                toastMessage = 'Order Cancelled';
                toastType = 'success';
                this.getOrderDetails();
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);
            
            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

//             if (saveSuccess === true) {

//                 // Workaround way to refresh the record page.
//                 updateRecord({ fields: { Id: this.recordId } });

//                 // Fire event to refresh the cache.
//                 fireEvent(this.pageRef, 'updatePOEvent', '');

// //                // Use the provisioned value to refresh wiredGetPurchaseOrderData.
// //                return refreshApex(this.wiredPOResult);
//             }
            
        })
        .catch((error) => {
            console.log('Cancel Purchase Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    getCancellationReasonPicklist(){
        getCancelReasonPicklist()
        .then((result) => {
            console.log(result);
        })
        .catch((error) => {
            console.log(error);
        })
    }

    handleReasonChange(event){
        console.log('reason', event.target.value);
        this.CancelReason = event.target.value;
        this.dispatchEvent(new CustomEvent('EnableSave'));
    }

    handleCancelWithWOButton(){
        if(this.returnedData.Cancellation_Reason__c == null){
            this.textToDisplay = `You must select a Cancellation Reason.`;
            this.showCancellationDropdown = true;
            this.showCancelWithWOButton = false;
            // this.getCancellationReasonPicklist();
            this.dispatchEvent(new CustomEvent('DisableSave'));
        }else{
            this.showCancelWithWOButton = false;
            this.textToDisplay = `Are you sure you want to cancel this Order?`;
            this.dispatchEvent(new CustomEvent('EnableSave'));
        }

    }
}