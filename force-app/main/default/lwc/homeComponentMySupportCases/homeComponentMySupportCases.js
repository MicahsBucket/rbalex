import { LightningElement, track, wire } from 'lwc';

import getMySupportCasesList from '@salesforce/apex/HomeComponentMySupportCasesCtlr.getMySupportCasesList';

const MySupportCasesColumns = [
    {label: 'Case #', fieldName: 'CaseNumberURL', type: "url", typeAttributes: {'label': { fieldName: 'CaseNumber'}, value:{fieldName: 'CaseNumberURL'}, 'target': '_blank', 'tooltip': {fieldName: 'CaseNumber'}}, sortable: true}, 
    {label: 'Subject', fieldName: 'Subject', type: "text", sortable: true}, 
    {label: 'Status', fieldName: 'Status', type: "text", sortable: true}, 
    {label: 'Date/Time Opened', fieldName: 'CreatedDate', type: 'date', sortable: true, typeAttributes:{
        year: "numeric",
        month: "short",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit"}},
    {label: 'Last Updated Date', fieldName: 'LastModifiedDate', type: "date", sortable: true}
    ]; 


export default class HomeComponentMySupportCases extends LightningElement {

    @track MySupportCasesColumns=MySupportCasesColumns;
    @track listMySupportCases;
    @track listMySupportCasesDup;
    @track mscDataLength;
    @track mscDataLengthWord = "cases";

    sVal = '';

    @wire(getMySupportCasesList, {searchKey: ''})
    wiredMySupportCasesListMethod({ error, data }) {             
        if (data) {           
            this.listMySupportCases =  data;
            this.listMySupportCasesDup =  data;   
            this.mscDataLength = data.length;
            if(this.mscDataLength === 1){
                this.mscDataLengthWord = "case";
             }         
        } else if (error) {
            this.error = error;
           }     
           console.log('List My Support Cases ' +JSON.stringify(data));
           console.log('List My Support Cases 2 ' +JSON.stringify( this.listMySupportCases));
           console.log('MSC Length: ' + this.inDataLength);       
    }

    updateSearchKeyMySupportCases(event) {
        this.sVal = event.target.value;
        this.handleSearchMySupportCases();
    }

    handleSearchMySupportCases() {
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getMySupportCasesList({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.listMySupportCases = result;
                })
                .catch(error => {
                    // display server exception in toast msg 
                    const event = new ShowToastEvent({
                        title: 'Error',
                        variant: 'error',
                        message: error.body.message,
                    });
                    this.dispatchEvent(event);
                    // reset contacts var with null   
                    this.listMySupportCases = null;
                });
        } else {
           
            this.listMySupportCases = this.listMySupportCasesDup;
        }
    }

    mySupportCasesSortData(fieldName, sortDirection){
        var data = JSON.parse(JSON.stringify(this.listMySupportCases));
        
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        
                data.sort((a,b) => {
                    let valueA = key(a) ? key(a).toLowerCase() : ''; 
                    let valueB = key(b) ? key(b).toLowerCase() : '';
                    return reverse * ((valueA > valueB) - (valueB > valueA));
                });

        this.listMySupportCases = data;
    }    
    mySupportCasesUpdateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.mySupportCasesSortData(this.sortedBy,this.sortedDirection);
    }

}