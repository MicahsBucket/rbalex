import {wire, track, LightningElement, api} from 'lwc';
import getMySalesTeam from '@salesforce/apex/SalesSchedStoreManagerCtrl.getMySalesTeam';
import { NavigationMixin } from 'lightning/navigation';

export default class SalesSchedMyTeam extends NavigationMixin(LightningElement){
    @track mySalesTeam;
    @api store;

    @wire(getMySalesTeam,{store:'$store'})
    handle({error,data}){
        if(data){
            this.mySalesTeam = data;
        }

        if(error){
            this.getErrorMessage(error);
        }
    }

    getErrorMessage(error){
        if(!error.message && error.details && error.details.body){
            error.message = error.details.body;
        }
        this.error = error;
    }

    handleAppointments(event){
        //Hack to be replaced later
        var recordId = event.currentTarget.dataset.id;
        window.location.href = ('/rForceARO/apex/SalesSchedRepCalendar?id=' + recordId);
    }

    handleCapacity(event){
        var recordId = event.currentTarget.dataset.id;
        window.location.href = ('/rForceARO/apex/SalesSchedRepCalendar?id=' + recordId);
    }
}