import { LightningElement, api, wire,track } from 'lwc';
import getWorkOrderLineItemsDetails from '@salesforce/apex/WorkOrderRelatedListController.getWorkOrderLineItems';
import getAllAttachments from '@salesforce/apex/WorkOrderRelatedListController.getAttachments';

const columns = [
    { label: 'SUBJECT', fieldName: 'subject', type: 'text' },
    { label: 'OFFICE NOTES', fieldName: 'officeNotes', type:'text'},
    { label: 'JOB NOTES', fieldName: 'jobNotes', type: 'text' },    
    { label: 'CUSTOMER-FACING', fieldName: 'customerFacing', type: 'text' },
    { label: 'LINE TYPE', fieldName: 'LineType', type: 'text' },
    { label: 'STATUS', fieldName: 'status', type: 'text' },
];

const columns1 = [
    { label: 'Title', fieldName: 'titleUrl' , type: 'url', 
    typeAttributes: {label: {fieldName: 'title'}, target: '_blank'}},
    { label: 'LAST MODIFIED', fieldName: 'LastModifiedDate', type: 'date',   
    typeAttributes: {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            hour12: true }},
    { label: 'CREATED BY', fieldName: 'createdBy', type:'text'},
];

export default class WorkOrderRelatedList extends LightningElement {
@api recordId;

@wire(getWorkOrderLineItemsDetails, { workOrderId: '$recordId'  }) 
data ;

@wire(getAllAttachments, { workOrderId: '$recordId'  }) 
data1 ;

@track columns1 = columns1;

@track columns = columns;
}