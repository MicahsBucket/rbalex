import { LightningElement, track, wire } from "lwc";
import getDashboard from "@salesforce/apex/SalesSchedStoreManagerCtrl.getDashboard";
import confirmAppointments from "@salesforce/apex/SalesSchedStoreManagerCtrl.confirmAppointments";
import getAvailableCapacities from "@salesforce/apex/SalesSchedStoreManagerCtrl.getAvailableCapacities";

import getAppointmentsByResult from "@salesforce/apex/SalesSchedStoreManagerCtrl.getAppointmentsByResult";
import getUnassignedAppointments from "@salesforce/apex/SalesSchedStoreManagerCtrl.getTodaysUnassignedAppointments";
import autoAssign from "@salesforce/apex/SalesSchedStoreManagerCtrl.autoAssign";
//import autoAssignSS from "@salesforce/apex/SalesSchedStoreManagerCtrl.autoAssignSS";
import { refreshApex } from "@salesforce/apex";
import { getRecord } from 'lightning/uiRecordApi';
import userId from '@salesforce/user/Id';

//<!-- SS-246 Start -->
import { loadStyle } from "lightning/platformResourceLoader";
import dataTableClass from "@salesforce/resourceUrl/DataTableStyles";
//<!-- SS-246 End -->

//<!-- SS-246 Start -->
const AppointmentCols = [
  {
    label: "Name",
    fieldName: "Name",
    cellAttributes: { class: { fieldName: "workingCSSClassName" } }
  },
  {
    label: "Town",
    fieldName: "Town",
    cellAttributes: { class: { fieldName: "workingCSSClass" } }
  },
  {
    label: "Source",
    fieldName: "Source",
    cellAttributes: { class: { fieldName: "workingCSSClass" } }
  },
  {
    label: "Time",
    fieldName: "Time",
    type: "date",
    typeAttributes: {
      hour: "2-digit",
      minute: "2-digit",
      hour12: "true"
    },
    cellAttributes: { class: { fieldName: "workingCSSClass" } }
  },
  {
    label: "1 Party",
    fieldName: "PartyApproved",
    cellAttributes: { class: { fieldName: "partyapprovedclass" } }
  },
  {
    label: "Resit",
    fieldName: "Resit",
    cellAttributes: { class: { fieldName: "resitclass" } }
  },
  {
    label: "Resale",
    fieldName: "Resale",
    cellAttributes: { class: { fieldName: "resaleclass" } }
  },
 /* {
    label: "Lastdnsrep",
    fieldName: "LastDnsRep",
    cellAttributes: { class: { fieldName: "Lastdnsrepclass" } }
  },
  {
    label: "Lastsalesrep",
    fieldName: "Lastsalesrep",
    cellAttributes: { class: { fieldName: "Lastsalesrepclass" } }
  },*/
  {
    label: "Revisit",
    fieldName: "Revisit",
    cellAttributes: { class: { fieldName: "revisitclass" } }
  },
  {
    label: "Cancel-Save",
    fieldName: "Cancel_Save",
    cellAttributes: { class: { fieldName: "canclesaveclass" } }
  },
  {
    label: "Condo",
    fieldName: "Condo",
    cellAttributes: { class: { fieldName: "condoclass" } }
  },
  {
    label: "Windows",
    fieldName: "Windows",
    cellAttributes: {
      class: { fieldName: "workingCSSClass" }
    }
  },
  {
    label: "Doors",
    fieldName: "Doors",
    cellAttributes: {
      class: { fieldName: "workingCSSClass" }
    }
  },
  {
    label: "",
    fieldName: "Notes",
    type: "button-icon",
    typeAttributes: {
      iconName: "utility:info",
      class: "slds-m-left_xx-small iconCls",
      //Data_Hover Task Start
      title: { fieldName: "NotesSection" }
      //Data_Hover Task Stop
    },
    cellAttributes: {
      alignment: "left",
      class: { fieldName: "workingCSSClass" }
    }
  },
  {
    label: "Action",
    type: "button",
    typeAttributes: {
      label: "Assign",
      name: "ViewAndAssign",
      title: "View and Assign",
      value: "",
      variant: {
        fieldName: "Neutral"
      }
    },
    cellAttributes: { class: { fieldName: "workingCSSClass" } }
    //cellAttributes:  {alignment: "left", class: { fieldName: "workingCSSClass" } }
  }
];
//<!-- SS-246 -->

export default class SalesSchedStoreManager extends LightningElement {
  @track dashboard;
  @track day = "";
  @track error;
  @track days = [];
  @track AppointmentCols = AppointmentCols;
  @track state = "Appointments";
  @track store = 'All';
  @track isStoreChanged = true;
  @track storeoptions = [];
  @track countapptcapp = [];
  @track showStoreOptions = false;
  @track showConfirmationModal = false;
  @track confirmationError;
  @track assignappointmentid = "";
  @track report;
  @track dashboardReturn;
  @track displayAutoAssignModal = false;
  //<!-- SS-246 Start -->
  @track showAMAppointments = true;
  @track showPMAppointments = true;
  @track showMidAppointments = true;
  @track showEARLYAMAppointments = true;
  @track showLATEAMAppointments = true;
  @track showEARLYPMAppointments = true;
  @track showLATEPMAppointments = true;
  @track salesTileAppointmentList = false;
  @track salesTileModalHeading = "";
  @track userId = userId;
  
  //<!-- SS-246 Stop -->
  dayIndex = 0;

  // SS19 - 342 - display default Store on the Sales Manager Page.
  @wire(getRecord, { recordId: '$userId', fields: ['User.Default_Store_Location__c'] })
  getUserRec(userRec) {
    if (userRec.data)
      if (userRec.data.fields.Default_Store_Location__c.value)
        this.store = userRec.data.fields.Default_Store_Location__c.value;
  }

  @wire(getDashboard, { day: "$day", store: "$store" })
  handleGetDashboard(dashboardReturn) {
    this.dashboardReturn = dashboardReturn;
    this.handleDashboardSuccess(dashboardReturn.data);
    this.handleDashboardError(dashboardReturn.error);
  }

  @wire(getAvailableCapacities, { day: "$day", userId: "$assignRepId" })
  handleGetOptions({ error, data }) {
    if (data) {
      this.slotOptions = data;
    }

    if (error) {
      this.error = error;
    }
  }

  handleDayChange(event) {
    let daymoment = event.detail;
    if (daymoment) {
      this.day = daymoment.format("YYYY-MM-DD");
    }
    refreshApex(this.dashboardReturn);
  }

  connectedCallback() {
    loadStyle(this, dataTableClass + "/DataTableStyles.css");
  }

  @wire(getUnassignedAppointments, {
    day: "$day",
    store: "$store",
    resultVal: ""
  })
  appointments;

  handleTeam() {
    this.state = "Team";
  }

  handleAppointments() {
    this.state = "Appointments";
  }

  handleShowStoreOptions() {
    this.showStoreOptions = true;
  }

  handleStoreChange(event) {
    this.showStoreOptions = false;
    this.store = event.detail.value;
  }

  handleCloseConfirmModal() {
    this.showConfirmationModal = false;
  }

  handleShowConfirmModal() {
    this.showConfirmationModal = true;
  }

  handleConfirm() {
    confirmAppointments({
      day: this.day,
      repReports: this.dashboard.repReports
    })
      // eslint-disable-next-line no-unused-vars
      .then(result => {
        // eslint-disable-next-line no-console
        this.showConfirmationModal = false;
        this.refresh();
      })
      .catch(error => {
        this.confirmationError = this.getErrorMessage(error);
      });
  }

  handleShowAssign(event) {
    debugger;
    //SS-246 Start
    if (!event.detail.action.iconName)
      this.assignappointmentid = event.detail.row.Id;
    //SS-246 End
  }

  handleAssignModalClose() {
    this.assignappointmentid = false;
  }

  handleDashboardSuccess(data) {
    if (data) {
      this.day = data.day;
      let options = [{ label: "All Stores", value: "All" }];

      if (data.stores) {
        data.stores.forEach(function (store) {
          options.push({ label: store.Name, value: store.Name });

        });
      }
      console.log('---------slotApptcapacity---------' + data.slotApptcapacity);
      this.storeoptions = options;
      this.dashboard = data;
      if (this.isStoreChanged) {
        this.isStoreChanged = false;
      }
      else {
        this.isStoreChanged = true;
      }
      var aobj = [];
      if (this.dashboard.slotAppCapCount) {
        let slotapp = this.dashboard.slotAppCapCount;
        console.log("-----Slot APP's ------>" + JSON.stringify(slotapp));
        for (let key in slotapp) {
          if (slotapp.hasOwnProperty(key)) {

            //this.countapptcapp.push({key:key, value:slotapp[key]});
            aobj.push({ key: key.toUpperCase(), value: slotapp[key] });
          }

        }
        this.countapptcapp = aobj;
        console.log('slottoapptcap count ---------------->' + JSON.stringify(this.countapptcapp));

      }

      console.log('dashboard data ---------------->' + JSON.stringify(data.slotAppCapCount));
    }
  }

  sortReportData(report) {
    let reportSorted = [report].sort((a, b) => -1 * (a.rpa6 - b.rpa6));
    reportSorted = reportSorted.sort((a, b) => -1 * (a.rpa28 - b.rpa28));
    reportSorted = reportSorted.sort(
      (a, b) => -1 * (a.closerate - b.closerate)
    );
    return reportSorted;
  }

  handleDashboardError(error) {
    if (error) {
      this.error = this.getErrorMessage(error);
    }
  }

  refresh() {
    debugger;
    this.assignappointmentid = false;
    refreshApex(this.dashboardReturn);
    refreshApex(this.appointments);
  }

  get teamButtonCSS() {
    return this.getCSS(this.showTeam);
  }

  get appointmentsButtonCSS() {
    return this.getCSS(this.showAppointments);
  }

  get showTeam() {
    return this.state === "Team" && this.dashboard;
  }

  get showAppointments() {
    return this.state === "Appointments";
  }

  getCSS(b) {
    if (b) {
      return "slds-button slds-button_neutral activeslot";
    }
    return "slds-button slds-button_neutral inactiveslot";
  }

  getErrorMessage(error) {
    if (!error.message && error.body.message) {
      return { message: error.body.message };
    }
    return error;
  }

  //<!-- SS-246 Start -->
  get AMAppointments() {
    let appointments = this.getAppoitments("AM");
    this.showAMAppointments = !(appointments.length > 0);
    if (this.showAMAppointments) {
      if (this.template.querySelector("[data-id = AMAppointments]"))
        this.template.querySelector(
          "[data-id = AMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = AMAppointments]"))
        this.template.querySelector("[data-id = AMAppointments]").style = {};
    }
    return appointments;
  }
  get MIDAppointments() {
    let appointments = this.getAppoitments("Mid");
    this.showMidAppointments = !(appointments.length > 0);
    if (this.showMidAppointments) {
      if (this.template.querySelector("[data-id = MidAppointments]"))
        this.template.querySelector(
          "[data-id = MidAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = MidAppointments]"))
        this.template.querySelector("[data-id = MidAppointments]").style = {};
    }
    return appointments;
  }
  get PMAppointments() {
    let appointments = this.getAppoitments("PM");
    this.showPMAppointments = !(appointments.length > 0);
    if (this.showPMAppointments) {
      if (this.template.querySelector("[data-id = PMAppointments]"))
        this.template.querySelector(
          "[data-id = PMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = PMAppointments]"))
        this.template.querySelector("[data-id = PMAppointments]").style = {};
    }
    return appointments;
  }
  get EARLYAMAppointments() {
    let appointments = this.getAppoitments("Early AM");
    this.showEARLYAMAppointments = !(appointments.length > 0);
    if (this.showEARLYAMAppointments) {
      if (this.template.querySelector("[data-id = EARLYAMAppointments]"))
        this.template.querySelector(
          "[data-id = EARLYAMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = EARLYAMAppointments]"))
        this.template.querySelector("[data-id = EARLYAMAppointments]").style = {};
    }
    return appointments;
  }
  get LATEAMAppointments() {
    let appointments = this.getAppoitments("Late AM");
    this.showLATEAMAppointments = !(appointments.length > 0);
    if (this.showLATEAMAppointments) {
      if (this.template.querySelector("[data-id = LATEAMAppointments]"))
        this.template.querySelector(
          "[data-id = LATEAMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = LATEAMAppointments]"))
        this.template.querySelector("[data-id = LATEAMAppointments]").style = {};
    }
    return appointments;
  }
  get EARLYPMAppointments() {
    let appointments = this.getAppoitments("Early PM");
    this.showEARLYPMAppointments = !(appointments.length > 0);
    if (this.showEARLYPMAppointments) {
      if (this.template.querySelector("[data-id = EARLYPMAppointments]"))
        this.template.querySelector(
          "[data-id = EARLYPMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = EARLYPMAppointments]"))
        this.template.querySelector("[data-id = EARLYPMAppointments]").style = {};
    }
    return appointments;
  }

  get LATEPMAppointments() {
    let appointments = this.getAppoitments("Late PM");
    this.showLATEPMAppointments = !(appointments.length > 0);
    if (this.showLATEPMAppointments) {
      if (this.template.querySelector("[data-id = LATEPMAppointments]"))
        this.template.querySelector(
          "[data-id = LATEPMAppointments]"
        ).style.display = "none";
    } else {
      if (this.template.querySelector("[data-id = LATEPMAppointments]"))
        this.template.querySelector("[data-id = LATEPMAppointments]").style = {};
    }
    return appointments;
  }


  getAppoitments(TimeSlot) {
    debugger;
    let tempList = [];
    if (this.appointments) {
      if (this.appointments.data) {
        console.log("_____appointemnt data_salesmanager___" + JSON.stringify(this.appointments.data));
        this.appointments.data.forEach(rec => {
          if (rec.appntment.Slot__r.Name === TimeSlot)
            tempList.push({
              Id: rec.appntment.Id,
              Name: rec.primarycontact.Name,
              Town:
                rec.appntment.Sales_Order__r.Opportunity__r.Account
                  .ShippingCity +
                " " +
                rec.appntment.Sales_Order__r.Opportunity__r.Account
                  .ShippingStateCode +
                " " +
                rec.appntment.Sales_Order__r.Opportunity__r.Account.ShippingPostalCode.slice(
                  0,
                  5
                ),
              Source:
                rec.appntment.Sales_Order__r.Opportunity__r
                  .Enabled_Marketing_Source__c,
              Time: rec.appntment.Appointment_Date_Time__c,
              PartyApproved: rec.appntment.Sales_Order__r.Opportunity__r
                .Approved_One_Party__c
                ? "Yes"
                : "No",
                partyapprovedclass : rec.appntment.Sales_Order__r.Opportunity__r.Approved_One_Party__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Resit: rec.appntment.Sales_Order__r.Opportunity__r
                .Last_DNS_Rep__c
                ? rec.appntment.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c
                : "No",
                resitclass : rec.appntment.Sales_Order__r.Opportunity__r
                .Last_DNS_Rep__c ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Resale: rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c
                ? rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c
                : "No",
                resaleclass : rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
               /* LastDnsRep: rec.appntment.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c
                ? rec.appntment.Sales_Order__r.Opportunity__r.Last_DNS_Rep__c 
                : "No",
                Lastdnsrepclass : rec.appntment.Sales_Order__r.Opportunity__r
                .Last_DNS_Rep__c ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Lastsalesrep: rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c
                ? "" + 
                rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c + 
                "" 
                : "No",
                Lastsalesrepclass : rec.appntment.Sales_Order__r.Opportunity__r.Last_Sale_Rep__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,*/
              Revisit: rec.appntment.Sales_Order__r.Opportunity__r.Revisit__c
                ? "Yes"
                : "No",
                revisitclass : rec.appntment.Sales_Order__r.Opportunity__r.Revisit__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Cancel_Save: rec.appntment.Sales_Order__r.Opportunity__r
                .Cancel_Save__c
                ? "Yes"
                : "No",
                canclesaveclass : rec.appntment.Sales_Order__r.Opportunity__r.Cancel_Save__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Condo: rec.appntment.Sales_Order__r.Opportunity__r.Account.HOA__c
                ? "Yes"
                : "No",
                condoclass : rec.appntment.Sales_Order__r.Opportunity__r.Account.HOA__c 
                ? ' slds-text-color_error slds-text-title_bold '+TimeSlot+'Class' : TimeSlot + "Class" ,
              Windows: rec.appntment.Sales_Order__r.Opportunity__r.of_Windows__c
                ? "" +
                rec.appntment.Sales_Order__r.Opportunity__r.of_Windows__c +
                ""
                : "0",
              Doors: rec.appntment.Sales_Order__r.Opportunity__r.Num_of_Doors__c
                ? "" +
                rec.appntment.Sales_Order__r.Opportunity__r.Num_of_Doors__c +
                ""
                : "0",
              Notes: "",
              //Data_Hover Task Start SS-265
              NotesSection:
                "Message Confirmation :  " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Message_Confirm__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Message_Confirm__c
                  : "") + "\n" +
                "Referral Date : " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Referral_Date__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Referral_Date__c
                  : "") + "\n" +
                "Referral Type :  " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Referral_Type__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Referral_Type__c
                  : "") + "\n" +
                "Employee :  " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Referral_Employee__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Referral_Employee__c
                  : "") + "\n" +
                "Customer :  " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Referral_Customer__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Referral_Customer__c
                  : "") + "\n" +
                "Problems w/Windows :  " +
                // '\xa0\xa0\xa0\xa0\xa0' +
                (rec.appntment.Sales_Order__r.Opportunity__r.Window_Problems__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Window_Problems__c
                  : "") +
                "\n" +
                "Important Lead Notes :  " +
                (rec.appntment.Sales_Order__r.Opportunity__r.Important_Lead_Notes__c
                  ? rec.appntment.Sales_Order__r.Opportunity__r.Important_Lead_Notes__c
                  : ""),
              //Data_Hover Task Stop
              workingCSSClass: TimeSlot + "Class",
              workingCSSClassName: TimeSlot + "Class NameClass"
            });
        });
      }
    }
    return tempList;
  }

  //<!-- SS-246 Stop -->

  showSalesResults(evt) {
    this.salesTileModalHeading = evt.currentTarget.title;

    let href = location.toString();
    let baseURL = href.split("/apex/SalesSchedStoreManager")[0];
    getUnassignedAppointments({
      day: this.day,
      store: this.store,
      resultVal: evt.currentTarget.title
    }).then(result => {
      debugger;
      let tempArr = [];
      result.forEach(rec => {
        let tempObj = { ...rec };

        if (rec.appntment.Sales_Order__r.Result__c === "Sale") {
          tempObj.URL = baseURL + "\\" + rec.appntment.Sales_Order__r.Opportunity__r.Id;

        } else {
          // baseURL + "\\" + rec.appntment.Sales_Order__r.Opportunity__r.Id;
          tempObj.URL = baseURL + "\\" + rec.appntment.Sales_Order__r.Id;
        }
        if (rec.appntment.Sales_Order__r.Result__c === "Sale") {
          tempObj.BodyString =
            (rec.appntment.Sales_Order__r.Contract_Amount_Quoted__c
              ? " - $" + rec.appntment.Sales_Order__r.Contract_Amount_Quoted__c
              : "") +
            (rec.appntment.Sales_Order__r.Series_One_Windows_Quoted__c
              ? " - " +
              rec.appntment.Sales_Order__r.Series_One_Windows_Quoted__c +
              " Windows"
              : "") +
            (rec.appntment.Sales_Order__r.Patio_Doors_Quoted__c
              ? " - " +
              rec.appntment.Sales_Order__r.Patio_Doors_Quoted__c +
              " Doors"
              : "");

        }
        if (rec.appntment.Sales_Order__r.Result__c === "Demo No Sale") {
          tempObj.BodyString =
            (rec.appntment.Sales_Order__r.Contract_Amount_Quoted__c
              ? " - $" + rec.appntment.Sales_Order__r.Contract_Amount_Quoted__c
              : "") +
            (rec.appntment.Sales_Order__r.Series_One_Windows_Quoted__c
              ? " - " +
              rec.appntment.Sales_Order__r.Series_One_Windows_Quoted__c +
              " Windows"
              : "") +
            (rec.appntment.Sales_Order__r.Patio_Doors_Quoted__c
              ? " - " +
              rec.appntment.Sales_Order__r.Patio_Doors_Quoted__c +
              " Doors"
              : "") +
            (rec.appntment.Sales_Order__r.Future_Purchase__c
              ? " - " + rec.appntment.Sales_Order__r.Future_Purchase__c
              : "");
        }
        if (rec.appntment.Sales_Order__r.Result__c === "No Demo") {
          tempObj.BodyString =
            rec.appntment.Sales_Order__r.Why_No_Demo__c
              ? " - " + rec.appntment.Sales_Order__r.Why_No_Demo__c
              : "";
        }
        if (rec.appntment.Sales_Order__r.Result__c === "Not Home") {
          tempObj.BodyString = rec.appntment.Sales_Order__r.Comments__c
            ? " - " + rec.appntment.Sales_Order__r.Comments__c
            : "";
        }
        if (rec.appntment.Sales_Order__r.Result__c === "Not Covered") {
          tempObj.BodyString = rec.appntment.Sales_Order__r.Why_Not_Covered__c
            ? " - " + rec.appntment.Sales_Order__r.Why_Not_Covered__c
            : "";
        }
        tempArr.push(tempObj);
      });
      if (tempArr)
        if (tempArr.length > 0) this.salesTileAppointmentList = tempArr;
    });
  }

  handlesalesTileModalClose() {
    this.salesTileAppointmentList = false;
    this.salesTileModalHeading = false;
  }

  handleAutoAssign() {
    this.displayAutoAssignModal = true;
    autoAssign({ store: this.store, day: this.day })
      // eslint-disable-next-line no-unused-vars
      .then(result => {
        this.displayAutoAssignModal = false;
        refreshApex(this.dashboardReturn);
        refreshApex(this.appointments);
      })
      .catch(error => {
        this.displayAutoAssignModal = false;
        this.error = this.getErrorMessage(error);
      });
  }


  handleAutoAssign2() {
    this.displayAutoAssignModal = true;
    autoAssignSS({ store: this.store, day: this.day })
      // eslint-disable-next-line no-unused-vars
      .then(result => {
        this.displayAutoAssignModal = false;
        refreshApex(this.dashboardReturn);
        refreshApex(this.appointments);
      })
      .catch(error => {
        this.displayAutoAssignModal = false;
        this.error = this.getErrorMessage(error);
      });
  }




}