import { LightningElement, api, wire,track } from 'lwc';
import getServiceSkills from '@salesforce/apex/ServiceResourceRelatedListCtrl.getServiceResourceSkill';
import getServiceTerritories from '@salesforce/apex/ServiceResourceRelatedListCtrl.getServiceTerritoryLists';
   
const columns = [
    { label: 'NAME', fieldName: 'name', type: 'text' },
    { label: 'SKILL LEVEL', fieldName: 'level', type:'number',
    cellAttributes: { alignment: 'left' }},
    { label: 'START DATE', fieldName: 'startDate', type: 'date', 
    typeAttributes: {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true }},    
    { label: 'END DATE', fieldName: 'endDate', type: 'date',
    typeAttributes: {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true }},
];

const columns1 = [
    { label: 'Member Number', fieldName: 'memberUrl', type: 'url',
    typeAttributes: {label: {fieldName: 'memberNumber'}, target: '_blank'}},
    { label: 'Service Territory', fieldName: 'serviceTerritoryUrl', type:'url',
    typeAttributes: {label: {fieldName: 'serviceTerritoryName'}, target: '_blank'}},
    { label: 'Territory Type', fieldName: 'territoryType', type:'text'},
    { label: 'Start Date', fieldName: 'effectiveStartDate', type: 'date', 
    typeAttributes: {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true }},    
    { label: 'End Date', fieldName: 'effectiveEndDate', type: 'date',
    typeAttributes: {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true }},
];

export default class ServiceResourceRelatedList extends LightningElement {
@api recordId;

@wire(getServiceSkills, { SerResourceId: '$recordId'  }) 
data;

@wire(getServiceTerritories, { SerResourceId: '$recordId' })
service;

@track columns = columns;

@track columns1 = columns1;

}