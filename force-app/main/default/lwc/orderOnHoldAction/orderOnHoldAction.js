/**
* @author Jason Flippen
* @date 01/26/2021 
* @description Provides functionality for the orderOnHoldAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - OrderOnHoldActionController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
import { LightningElement, api, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { updateRecord } from 'lightning/uiRecordApi';
import { registerListener } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';

import { CurrentPageReference } from 'lightning/navigation';
import getOrderData from '@salesforce/apex/OrderOnHoldActionController.getOrderData';
import updateOrder from '@salesforce/apex/OrderOnHoldActionController.updateOrder';

export default class OrderOnHoldAction extends LightningElement {

    @api recordId;

    wiredOrderResult;
    order;
    displayText = '';
    
    @wire(CurrentPageReference) pageRef;

    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description Event fired when the element is inserted into the document.
    *              It's purpose here is to subscribe the "updateOrderEvent".
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    connectedCallback() {
        registerListener('updateOrderEvent', this.refreshOrderData, this);
    }
    
    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description Method to retrieve Order data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @wire(getOrderData, { orderId: '$recordId' })
    wiredGetOrderData(result) {

        // Hold on to the provisioned value so it can be refreshed.
        this.wiredOrderResult = result;
        const { data, error } = result; // destructure the provisioned value
        if (data) {
            console.log('Order Data', JSON.parse(JSON.stringify(data)));
            this.order = JSON.parse(JSON.stringify(data));

            let disableSave = false;

            // Is the Order for revenue recognition?
            if (this.order.Status === 'Draft') {
                
                // Not eligible due to it being in "Draft" Status.
                disableSave = true;
                this.displayText = 'Draft Orders cannot be put On Hold';
    
            }
            else if (this.order.Status === 'Cancelled' ||
                     this.order.Status === 'Install Complete' ||
                     this.order.Status === 'Job Closed' ||
                     this.order.Status === 'Job in Progress' ||
                     this.order.Status === 'Pending Cancellation') {
                
                // Not eligible due it being in an excluded Status.
                disableSave = true;
                this.isNotEligibleExcludedStatus = true;
                this.displayText = 'Cannot put this Order on hold. Order Status cannot be Install Complete, Job Closed, Job In Progress, Pending Cancellation, or Cancelled.';
    
            }
            else {

                // The Order is eligible to be placed in an "On Hold" Status.
                disableSave = false;
                this.displayText = 'Are you sure you want to put this Order on hold?';

            }

            if (disableSave === false) {
                const enableSaveEvent = new CustomEvent('EnableSave');
                this.dispatchEvent(enableSaveEvent);
            }
            else {
                const disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }

       }
       else if (error) {
           console.log('error in getOrderData callback', error);
       }

    }

    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description Method to update the Status and Revenue Recognized Date of the Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to update the Order.
        updateOrder({ order: this.order })
        .then((result) => {

            let toastMessage;
            let toastType;
            let saveSuccess = false;
            if (result === 'Update Order Success') {
                saveSuccess = true;
                toastMessage = 'This Order has been put On Hold';
                toastType = 'success';
            }
            else  {
                toastMessage = result;
                toastType = 'error';
            }

            this.dispatchEvent(hideSpinner);

            const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
            this.dispatchEvent(showToastEvent);
            this.dispatchEvent(closeQuickAction);

            if (saveSuccess === true) {

                // Workaround way to refresh the record page.
                updateRecord({ fields: { Id: this.recordId } });

                // Fire event to refresh the cache.
                fireEvent(this.pageRef, 'updateOrderEvent', '');

            }

        })
        .catch((error) => {
            console.log('Update Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(this.closeQuickAction);
        });

    }

    /**
    * @author Jason Flippen
    * @date 01/26/2021
    * @description Method to refresh the cache. 
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
   refreshOrderData() {
        console.log('Refreshing orderOnHoldAction');
        return refreshApex(this.wiredOrderResult);
    }

}