/*
* @author Jason Flippen
* @date 10/21/2020 
* @description Lightning Web Component to display Reimbursement data from an Order and its related Part Reimbursements.
*
*              Server-Side support provided by the following Class:
*			   - ServiceReimbursementsController
*/
import { LightningElement, api, track, wire} from 'lwc';
import getReimbursements from '@salesforce/apex/ServiceReimbursementsController.getReimbursements';

const reimbursablePartColumns = [
    { label: 'Product', fieldName: 'productName', type: 'string', cellAttributes: { alignment: 'left' }, wrapText: true },
    { label: 'Part', fieldName: 'partCode', type: 'string', cellAttributes: { alignment: 'left' }, wrapText: true },
    { label: 'Description', fieldName: 'partDescription', type: 'string', cellAttributes: { alignment: 'left'}, wrapText: true },
    { label: 'Reason', fieldName: 'reason', type: 'string', cellAttributes: { alignment: 'left'}, wrapText: true },
    { label: 'Quantity', fieldName: 'quantity', type: 'number', initialWidth: 75, cellAttributes: { alignment: 'center' }, hideDefaultActions: true },
    { label: 'Cost', fieldName: 'cost', type: 'currency', cellAttributes: { alignment: 'left'}, hideDefaultActions: true },
];

export default class ServiceReimbursements extends LightningElement {

    @api recordId;

    @track reimbursablePartColumnList = reimbursablePartColumns;
    @track reimbursablePartList;
    @track title = 'Reimbursable Parts (0)';
    @track showData = false;
    @track error;

    @wire(getReimbursements, { orderId: '$recordId' })
    wiredGetReimbursements({data, error}) {
        if (data) {
            console.log('Reimbursement Data', JSON.parse(JSON.stringify(data)));
            var reimbursement = JSON.parse(JSON.stringify(data));
            this.reimbursablePartList = reimbursement.reimbursablePartList;
            this.title = 'Reimbursable Parts (' + this.reimbursablePartList.length + ')';
            if (this.reimbursablePartList.length > 0) {
                this.showData = true;
            }
        }
        else if (error) {
            this.error = error;
            console.log('error in getReimbursements callback', error);
        }

    }

}