import { LightningElement, api } from 'lwc';

export default class PillContainer extends LightningElement {
    @api items;

    handleItemRemove(event){
        debugger;
        let name = event.currentTarget.dataset.name;
        let item;
        this.items.forEach(function(i){
            debugger;
            if(i.name == name && i.showClose){
                item = i;
            }
        })

        if(item){
            this.dispatchEvent(new CustomEvent('itemremove',{detail : name}));
        }
    }
}