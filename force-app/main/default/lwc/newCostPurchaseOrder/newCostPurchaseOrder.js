import { LightningElement, api, track, wire } from 'lwc';
//import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getNewCostPOData from '@salesforce/apex/NewCostPOActionController.getNewCostPOData';
import createCostPO from '@salesforce/apex/NewCostPOActionController.createCostPO';

export default class newCostPurchaseOrder extends LightningElement {

    @api returnURL;

    @track error;
    @track newCostPO = {};
    @track disableCancelButton = false;
    @track disableSaveButton = true;
    @track showSpinner = false;

    /*
    * @author Jason Flippen
    * @date 06/22/2020
    * @description Method to retrieve new (Cost) Purchase Order record. 
    */
    @wire(getNewCostPOData, {})
    wiredGetNewCostPOData({ error, data }) {

        if (error) {
            console.log('Error in wiredGetNewCostPOData callback', error);
            this.error = error;
        }
        else if (data) {
            console.log('New Cost PO Data', JSON.parse(JSON.stringify(data)));
            this.newCostPO = JSON.parse(JSON.stringify(data));
        }

    }


/** Click Events **/


    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to close the Cost Purchase Order modal window.
    */
    handleCancelCostPOClick(event) {
        
/*  2020/06/22 - Jason Flippen - NavigationMixin does NOT work from a Lightning
    Web Component wrapped by a Visualforce Page.

        // Navigate to the Purchase Order object's Cost Purchase Order list view.
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Account',
                actionName: 'list'
            },
            state: {
                // 'filterName' is a property on the page 'state'
                // and identifies the target list view.
                // It may also be an 18 character list view id.
                filterName: 'Recent'
            }
        });
*/
        window.open('/lightning/o/Purchase_Order__c/list?filterName=Cost_Purchase_Orders2', '_self');

    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to fire the "handleSaveClick" event in the child component.
    */
    handleSaveCostPOClick(event) {

        this.disableButtons = true;
        this.showSpinner = true;

        createCostPO({ newPurchaseOrder: this.newCostPO })
        .then(result => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Cost PO Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            if (saveSuccess === true) {
                this.handleSaveCostPOSuccess(returnValue);
            }
            else if (saveSuccess === false) {

                this.handleSaveCostPOFailure(returnValue);

            }

        })
        .catch(error => {
            console.log('Error in createCostPO callback', error);
            this.error = error;
        })

    }


/** Handle Events **/


    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the changed Vendor value.
    */
    handleVendorChange(event) {
        var newVendor = event.target.value;
        if (newVendor === '') {
            newVendor = null;
        }
        this.newCostPO.Vendor__c = newVendor;
        this.enableSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the changed Store Location value.
    */
    handleStoreLocationChange(event) {
        var newStoreLocation = event.target.value;
        if (newStoreLocation === '') {
            newStoreLocation = null;
        }
        this.newCostPO.Store_Location__c = newStoreLocation;
        this.enableSaveButton();
    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the changed Requested Ship Date value.
    */
    handleReqShipDateChange(event) {
        this.newCostPO.Requested_Ship_Date__c = event.target.value;
    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the changed Reference value.
    */
    handleReferenceChange(event) {
        this.newCostPO.Reference__c = event.target.value;
    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the changed Comments value.
    */
    handleCommentsChange(event) {
        this.newCostPO.Comments__c = event.target.value;
    }


/** Miscellaneous Methods **/


    /*
    * @author Jason Flippen
    * @date 08/25/2020
    * @description Method to set the "disableSaveButton" flag.
    */
    enableSaveButton() {

        if (this.newCostPO.Vendor__c !== null && this.newCostPO.Store_Location__c !== null) {
            this.disableSaveButton = false
        }
        else {
            this.disableSaveButton = true;
        }

    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the successful creation of a new Cost PO.
    */
    handleSaveCostPOSuccess(purchaseOrderId) {

        this.disableButtons = false;
        this.showSpinner = false;

        // Notify the User that the Cost PO was created successfully.
        const toastEvent = new ShowToastEvent({
            message: 'New Cost Purchase Order created',
            variant: 'success',
            mode: 'pester'
        });
        this.dispatchEvent(toastEvent);

/*  2020/06/22 - Jason Flippen - NavigationMixin does NOT work from a Lightning
    Web Component wrapped by a Visualforce Page.

        // Navigate to the new Cost Purchase Order.
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: purchaseOrderId,
                objectApiName: 'Purchase_Order__c', // objectApiName is optional
                actionName: 'view'
            }
        });
*/

        window.open('/' + purchaseOrderId, '_self');

    }

    /*
    * @author Jason Flippen
    * @date 06/15/2020
    * @description Method to handle the new Cost PO creation failure.
    */
    handleSaveCostPOFailure(errorMessage) {

        this.disableButtons = false;
        this.showSpinner = false;

        // Notify the user that the Cost PO creation failed.
        const toastEvent = new ShowToastEvent({
            title: 'Unable to create New Cost Purchase Order',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky'
        });
        this.dispatchEvent(toastEvent);

    }

}