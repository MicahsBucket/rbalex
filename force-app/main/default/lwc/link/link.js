import { LightningElement, api} from 'lwc';

export default class Link extends LightningElement {
    @api protocol;
    @api domain;
    @api parameters;

    get url(){
        let protocol = this.getEmptyIfUndefined(this.protocol);
        let domain = this.getEmptyIfUndefined(this.domain);
        let parameters = this.getEmptyIfUndefined(this.parameters);
        return `${protocol}${domain}${parameters}`;
    }

    getEmptyIfUndefined(str){
        return !str ? '' : str;
    }
}