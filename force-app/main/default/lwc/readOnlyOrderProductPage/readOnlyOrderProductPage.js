import { LightningElement, api, track, wire } from 'lwc';
import getProductFieldcontrols from '@salesforce/apex/EditOrderPageLwcController.getProductFieldcontrols';
import getShowOrderButton from '@salesforce/apex/OrderProductListController.showProductButton';
import getRelatedOrderItems from '@salesforce/apex/ReadOnlyOrderPageLwcController.getRelatedOrderItems';	
import getIsSandboxOrg from '@salesforce/apex/UtilityMethods.isSandboxOrg';	
import getIsrForceUser from '@salesforce/apex/UtilityMethods.isrForceUser';	
import { getRecord } from 'lightning/uiRecordApi';
const FIELDS = [
    'OrderItem.Product2Id',
    'OrderItem.OrderId',
    'OrderItem.Purchase_Order__r.Released_Timestamp__c',
    'OrderItem.Retail_Purchase_Order__r.Released_Timestamp__c'

];
const PRODUCTFIELDS = [
    'Product2.Description',
    'Product2.Unit_Short_Description__c'
];
const sfClassicBaseUrl = '/';
const sfClassicEditBaseUrl='/apex/EditOrderProduct';
const sfAROBaseUrl = '/rForceARO/';
const sfAROEditBaseUrl = '/rForceARO/apex/EditOrderProduct';

let partnerResult;
let sandboxResult;

export default class ReadOnlyOrderProductPage extends LightningElement {

    @api orderId;
    @api orderItemId;
    @track activeSections = ['Product','Main', 'Window-Configuration','Order-Information','Sales-Information','Change-History'];
    @track productRecordId;
    @track productFieldControl = {};
    @track orderItemNavigationList = [];
    @track showMakabilitySpinner = false;
    @track currentIndex;
    @track navlistLength;
    @track nextId;
    @track previousId;
	@track isSandboxOrg;
    @track isrForceUser;
    @track hasProductDescription = false;
    @api inRop;
    @api returnBaseUrl;
    @api editBaseUrl;
    @api showEditButton;
    @api showButton;


    connectedCallback(){
        console.log('hit connectedCallback', this.orderId);
        // if(this.orderItemId){
        //     console.log('order Item id in connected callback ', this.orderItemId);
        //     getOrderItemForEdit({orderItemId:this.orderItemId})
        //     .then(result => {
        //     let res = {};
        //     res = JSON.parse(result.jsonResponse);
        //     this.productRecordId = res.Product2Id;
        //     console.log('order item in get order item', this.orderItem);
        //     })
        //     .catch(err => {
        //     this.error = err;
        //     console.log('error in getOrderItem callback ', err);
        //     });
        // }

    getShowOrderButton ({ orderId: this.orderId }).then(result => {
		this.showButton = result;			

		}).catch(error => {
			this.error = error;
			console.log('error in callback for get order revenue recognized date', error);
		});


	getIsSandboxOrg ().then(result => {
		console.log('Sandbox Result', result);
		sandboxResult = result;
		this.isSandboxOrg = sandboxResult;	
			

	getIsrForceUser ().then(res => {
		console.log('Partner Result', res);
		partnerResult = res;
		this.isrForceUser = partnerResult;
		
		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Partner User', error);

		}).then(() => {
		if(this.editBaseUrl === undefined && this.readBaseUrl === undefined){
			if(this.isSandboxOrg === true && this.isrForceUser === true) {
				this.returnBaseUrl = sfAROBaseUrl;
				this.editBaseUrl = sfAROEditBaseUrl;
			} else {
				this.returnBaseUrl = sfClassicBaseUrl;
				this.editBaseUrl = sfClassicEditBaseUrl;
			}
        console.log('hit connectedCallback, close base url',this.returnBaseUrl);
        console.log('hit connectedCallback, edit base url',this.editBaseUrl);

		}
		});


		}).catch(error => {
			this.error = error;
			console.log('error in callback for get Sandbox Org', error);
		});

		}

 
    @wire(getRecord, { recordId: '$orderItemId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {
            console.log('Error in get product2Id wire service ' , error);
        } else if (data) {
            let poReleased;
            let rpoReleased;
            this.productRecordId = data.fields.Product2Id.value;
            this.orderId = data.fields.OrderId.value; 
            console.log(' data rop timestamp ',JSON.parse(JSON.stringify(data)));

            if(data.fields.Purchase_Order__r.value !== null){
                if(data.fields.Purchase_Order__r.value.fields.Released_Timestamp__c.value !== null){
                    poReleased = true;
                }
            }
            if(data.fields.Retail_Purchase_Order__r.value !== null ){
                if(data.fields.Retail_Purchase_Order__r.value.fields.Released_Timestamp__c.value !== null){
                    rpoReleased = true;
                }
            }
            this.showEditButton = true;
            if (poReleased === true || rpoReleased === true) {
                console.log('hide edit button');
                this.showEditButton = false;	
            }
            if (this.showButton === false) {
                this.showEditButton = false;
            }
        }
    }

    @wire(getRecord, { recordId: '$productRecordId', fields: PRODUCTFIELDS })
    wiredProduct({ error, data }) {
        if (error) {
            console.log('Error in get product2 wire service ' , error);
        } else if (data) {
            console.log('data.fields.Description', JSON.parse(JSON.stringify(data.fields.Description)));
            if(data.fields.Description.value !== null && data.fields.Description.value !== ''){
                this.hasProductDescription = true;
            } else{
                this.hasProductDescription = false;
            }
            console.log('hasProductDescription', this.hasProductDescription);
        }
    }

    @wire(getProductFieldcontrols,{productId:'$productRecordId'})
    wiredProductFieldControls({error,data}){
        if (error){
            this.error = error;
            console.log('error in getProductFieldcontrols callback ', error.jsonResponse);
        } else if (data){
            let response = {};
            // console.log('product field control data ', JSON.parse(data.jsonResponse));
            response = JSON.parse(data.jsonResponse);
            if(this.productRecordId !== null){
                this.createProductFieldControlsObject(response);
            }           
        } else {
            console.log(' getProductFieldcontrols something bad happened...');
        }
    }

    @wire(getRelatedOrderItems,{orderId:'$orderId'})
    wiredGetOrderItems({error,data}){
        if (error){
            this.error = error;
            console.log('error in getOrderItems callback ', error.jsonResponse);
        } else if (data){
            let response = [];
            console.log('Related orderItems response ', JSON.parse(data.jsonResponse));
            response = JSON.parse(data.jsonResponse);
            for(let oi of response){
                this.orderItemNavigationList.push(oi.Id);
            }
            let currentId = this.orderItemId
            console.log('nav list ',this.orderItemNavigationList);
            console.log('order item id in nav list set',currentId);
            this.currentIndex = this.orderItemNavigationList.indexOf(currentId);
          //  let next = this.currentIndex + 1;
            this.nextId = this.orderItemNavigationList[this.currentIndex + 1];
        //    let previous = this.currentIndex - 1;
            this.previousId = this.orderItemNavigationList[this.currentIndex - 1];
            this.navlistLength = this.orderItemNavigationList.length;
        } else {
            console.log(' getRelatedOrderItems something bad happened...');
        }
    }

    createProductFieldControlsObject(pfcArray){
        this.productFieldControl = {};
        for(let i = 0; i<pfcArray.length; i++){
            this.productFieldControl[pfcArray[i].Field_Control_ID__r.Target_Api_Name__c] = true;
        }
    }
    handlePrevious(){
        console.log('hit previous');
        this.currentIndex = this.currentIndex -1;
        if(this.currentIndex < 0){
            this.currentIndex = this.orderItemNavigationList.length -1;
        }
        this.orderItemId = this.orderItemNavigationList[this.currentIndex];
        this.nextId = this.orderItemNavigationList[this.currentIndex + 1];
        if(this.currentIndex === this.orderItemNavigationList.length -1  ){
            this.nextId = this.orderItemNavigationList[0];
        }
        this.previousId = this.orderItemNavigationList[this.currentIndex - 1];
        if(this.currentIndex === 0){
            this.previousId = this.orderItemNavigationList[this.navlistLength -1];
        }    
    }
    handleEdit(){
        //TODO - do we want to be able to return to read only mode from Edit page? 
        console.log('hit edit oi and o ids', this.orderItemId , this.orderId);
        window.open(this.editBaseUrl+'?orderItemId='+this.orderItemId+'&orderId='+this.orderId, '_top');
    }
    handleNext(){
        console.log('hit next');
        this.currentIndex = this.currentIndex + 1;
        if(this.currentIndex > this.orderItemNavigationList.length -1 ){
            this.currentIndex = 0;
        } 
        this.orderItemId = this.orderItemNavigationList[this.currentIndex];
        this.nextId = this.orderItemNavigationList[this.currentIndex + 1];
        if(this.currentIndex === this.orderItemNavigationList.length -1  ){
            this.nextId = this.orderItemNavigationList[0];
        }
        this.previousId = this.orderItemNavigationList[this.currentIndex - 1];
        if(this.currentIndex === 0){
            this.previousId = this.orderItemNavigationList[this.navlistLength -1];
        }
    }
    handleClose(){
        console.log('hit close', this.orderId);
        window.open(this.returnBaseUrl + this.orderId ,'_top');  
    }

}