/* eslint-disable no-console */
import { LightningElement, wire, api ,track} from 'lwc';
import getMessage from '@salesforce/apex/NotificationController.getMessage';
import getUpdate from '@salesforce/apex/NotificationController.getUpdate';
import { getSObjectValue } from '@salesforce/apex';
import NAME_FIELD from '@salesforce/schema/ServiceNotes__c.NotificationMessage__c';
import ID_FIELD from '@salesforce/schema/ServiceNotes__c.Id';
import { refreshApex } from '@salesforce/apex';
export default class NotificationNotes extends LightningElement {

    
    @track notificationData;
    @track notificationDataRefresh;
    @track recordToPass;
    @track updateVisibility;
    @track messageNote;
    @track recordfromApex;
    
    @wire(getMessage)
    wiredMessage(result ) {
        this.notificationDataRefresh=result;
        if (result.data) {
            console.log('the res'+JSON.stringify(result.data));
           
            this.messageNote=result.data.NotificationMessage__c;
            this.recordfromApex=result.data.Id;
           
           
        } else if (result.error) {
            this.error = result.error;
          
        }
    }


    @wire(getUpdate)
    wiredUpdate(result ) {
        console.log('the res in boolean'+JSON.stringify(result.data));
        if (result.data) {

           
            this.updateVisibility=result.data;
           
        } else if (result.error) {
            this.error = result.error;
          
        }
    }
    get name() {
        return this.messageNote;
    }
    get recordData()
    {
     return this.recordfromApex;
    }

 
    updateNotificationMessage()
    {
        
    }

    handleSucess(event){
        const payload = event.detail;
        console.log(JSON.stringify(payload));
        
        refreshApex(this.notificationDataRefresh);

    }

}