import { LightningElement,api } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import ServiceConsole from '@salesforce/resourceUrl/ServiceConsole';


export default class NavigationComponent extends LightningElement {
    @api recordName;
    @api recordId;
    @api sforce;
    @api recordUrl;
    
    
    connectedCallback() {
        Promise.all([
            loadScript(this, ServiceConsole),
        ])
            .then(() => {
                this.sforce = window.sforce;
            })
            .catch(error => {
               console.log(error);
            }); 
    }
    
    openUrl(){
        console.log(this.sforce.console.isInConsole());
        if(this.sforce.console.isInConsole()) {
            this.sforce.console.openPrimaryTab(null,this.recordId ,true, 
                this.recordName,this.recordName);
        }
        else{
            console.log(this.recordUrl);
            window.open(this.recordUrl,'_blank');
        }
    }

}