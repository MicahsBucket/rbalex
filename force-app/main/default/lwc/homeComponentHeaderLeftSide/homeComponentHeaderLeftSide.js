import { LightningElement } from 'lwc';
import RFORCE_Images from '@salesforce/resourceUrl/rForce';

export default class HomeComponentHeaderLeftSide extends LightningElement {
    Rforceimage = RFORCE_Images ;
}