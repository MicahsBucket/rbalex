import { LightningElement,api} from 'lwc';

export default class EditLeadTime extends LightningElement {

    @api recordId;
    @api url;
    gotoListView(evt) {
        // eslint-disable-next-line no-console
        console.log('the url'+this.url);
        // eslint-disable-next-line no-alert
        
        window.open(this.url,"_self");
    }


    handleLeadTimeUpdated(){
        this.dispatchEvent(
            // Default values for bubbles and composed are false.
            new CustomEvent('leadtimecreation')
        );
    }
}