import { LightningElement, wire, api ,track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord, getRecord } from 'lightning/uiRecordApi';
import setPurchaseOrderToReleased from '@salesforce/apex/PurchaseOrderButtonController.setPurchaseOrderToReleased';
import getStatus from '@salesforce/apex/PurchaseOrderButtonController.getStatus';




export default class PurchaseOrderButtons extends NavigationMixin(LightningElement) {


@api recordId;
@track visible=false;
@track isLoaded=false;
@track statusVisible=false;
@track type;
@track recid=this.recordId;
toastMessage;
eligible;
toastEvnt;
@wire(getStatus,{purchaseId:'$recordId'})
wiredStatus(result) {
   /* eslint-disable no-console */
  console.log('the '+JSON.stringify(result));
  if (result.data) {
    /* eslint-disable no-console */
  console.log('the '+result.data);
   if(result.data==='Released' ||result.data==='Confirmed') 
   {
this.visible=true;
   }
else
{
  this.statusVisible=true;
}
  }

}

    
openModal()
{
this.visible=true;

}

closeModal()
{
this.visible=false;

}
updatePoToRelease()
{

this.isLoaded=true;
    setPurchaseOrderToReleased({
    purchaseOrderId: this.recordId
})
.then((result) => {
this.eligible=result;

if(this.eligible==='Purchase Order cannot be released until all Purchase Order line items have been verified')
{
    this.toastMessage='Purchase Order cannot be released until all Purchase Order line items have been verified';
    this.type='error';
  
}
else{
    this.toastMessage= 'Purchase Order Has Been Released';
    this.type='Success';
 
} 
const selectedEvent = new CustomEvent('releasePO', { detail:{mesg:this.toastMessage, type:this.type} });
this.isLoaded=false;
  this.dispatchEvent (selectedEvent);
  updateRecord({ fields: { Id: this.recordId } });
  const closeQuick = new CustomEvent('closeQuick');
  this.dispatchEvent (closeQuick);
  this.closeModal();
  if(this.type==='Success')
  {
  this.navigateHome();
  }

})
.catch((error) => {
    this.closeModal();  
    this.isLoaded=false;
    this.dispatchEvent (this.closeQuick);
});
  
}


navigateHome() {
  this[NavigationMixin.Navigate]({
    type: 'comm__namedPage',
    attributes: {
        pageName: 'home'
    }
  });
}

}