import { LightningElement, api, track} from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import momentJS from '@salesforce/resourceUrl/moment';

export default class SalesSchedDaySwitcher extends LightningElement {
    @api limitToCurrentDay;
    @api limitToFutureDays;
    @api day;
    @track currentMoment;
    startMoment;
    futureLimitMoment;

    renderedCallback(){
        if(this.startMoment){
            return;
        }

        Promise.all([
            loadScript(this, momentJS + '/moment.js')
        ]).then(() => {
            // eslint-disable-next-line no-undef
            this.currentMoment = moment();
            if(this.day){
                this.currentMoment = moment(this.day);
            }else{
                this.day =  this.currentMoment.format('YYYY-MM-DD');
            }
            this.startMoment = this.currentMoment.clone();
            let daysToAdd = this.limitToFutureDays ? this.limitToFutureDays : 365;
            this.futureLimitMoment = this.startMoment.clone().add(daysToAdd,'days');
        })
        .catch(error => {});
    }

    handleGetNextDay(){
        this.dispatchEventHelper(this.limitToFutureDays, this.nextDaysCount, 1);
    }

    handleGetPreviousDay(){
        this.dispatchEventHelper(this.limitToCurrentDay, this.previousDaysCount, -1);
    }

    dispatchEventHelper(limit, count, increment){
        if(!limit || count > 0){
            console.log('----log for this.day------' + this.day);
            let m = moment(this.day);
            m.add(increment, 'days');
            console.log('----log for the m------' + m);
            this.dispatchEvent(new CustomEvent("daychange", {detail : m}));
        }
    }

    get previousDaysCount(){
        return this.currentMoment.diff(this.startMoment,'days');
    }

    get nextDaysCount(){
        return (this.futureLimitMoment.diff(this.currentMoment,'days') - 1);
    }

    get previousDayLabel(){
        return 'Previous Day' + (this.limitToCurrentDay ? ' (' + this.previousDaysCount + ')' : '');
    }

    get nextDayLabel(){
        return 'Next Day' + (this.limitToFutureDays ? ' (' + this.nextDaysCount + ')' : '');
    }

    get dayFormatted(){
        return this.currentMoment ? moment(this.day).format('dddd, MMMM Do') : '';
    }

    get inputDate(){
        return this.currentMoment ? this.day : '';
    }

    onClickCalendarEvent(event){
        var d = event.target.value;
        let m = moment(d);
        console.log('---------date event dispatch---------' + m);
        this.dispatchEvent(new CustomEvent("daychange", {detail : m}));
    }
}