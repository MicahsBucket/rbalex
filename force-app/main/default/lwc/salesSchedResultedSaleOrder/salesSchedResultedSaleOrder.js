import { LightningElement, track, wire, api } from 'lwc';


export default class SalesSchedResultedSaleOrder extends LightningElement {
    @api selsalesrecord;
    @api contact;


  
    get displayResultedButton() {
        return this.selsalesrecord.sl.Resulted__c;
      }
      get displaySaleResulted() {
        return this.selsalesrecord.sl.Result__c === 'Sale';
      }
      get displaySaleDemoNoSaleOrNoDemoResulted() {
        return this.displaySaleResulted || this.displayDemoNoSaleResulted || this.displayNoDemoResulted;
      }
      get displaySaleOrDemoNoSaleResulted() {
        let result = this.selsalesrecord.sl.Result__c;
        return ('Sale' === result || 'Demo No Sale' === result);
      }
      get displaySeriesTwo() {
        return this.selsalesrecord.sl.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c;
      }
      get displayEntryDoor() {
        return this.selsalesrecord.sl.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c;
      }
      get displaySeriesA() {
        return this.selsalesrecord.sl.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c;
      }
      get displayDemoNoSaleResulted() {
        return 'Demo No Sale' === this.selsalesrecord.sl.Result__c;
      }
      get displayFinanceResulted() {
        return this.selsalesrecord.sl.Payment_Type__c === 'Finance';
      }
      get displayCashAndFinanceResulted() {
        return this.selsalesrecord.sl.Payment_Type__c === 'Cash & Finance';
      }
      get displayFinanceTurnDownResulted() {
        return this.selsalesrecord.sl.Payment_Type__c === 'Finance Turn-Down';
      }
      get displayFinanceDetailsResulted() {
        return 'No' === this.selsalesrecord.sl.Finance_Issues__c;
      }
      get displayNoDemoResulted() {
        return 'No Demo' === this.selsalesrecord.sl.Result__c;
      }
      get displayFollowup() {
        return 'Yes' === this.selsalesrecord.sl.Rescheduled__c;
      }
      get displayNotHomeResulted() {
        return 'Not Home' === this.selsalesrecord.sl.Result__c;
      }
      get displayNotCoveredResulted() {
        return 'Not Covered' === this.selsalesrecord.sl.Result__c;
      }
     /* get displayRideAlongAdvice() {
        return this.appwrapper && this.appwrapper.hasRideAlongs;
      }*/

      handleCloseResultant(event){
        this.dispatchEvent(new CustomEvent('close'));  
      }
}