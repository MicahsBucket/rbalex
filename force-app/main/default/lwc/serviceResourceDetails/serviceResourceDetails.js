import { LightningElement, api, wire } from 'lwc';
import getServiceResourceList from '@salesforce/apex/ServiceResourceDetailCtrl.getServiceResources';
import fetchUserTimeZone from '@salesforce/apex/Resource_Absence_Controller.getCurrentLoggedInUserTimeZone';

export default class ServiceResourceDetails extends LightningElement {

    @api recordId;

    @wire(getServiceResourceList, { SerResourceId: '$recordId' })
    service;

    @wire(fetchUserTimeZone, {})
    _timezone;
    get timezone() {
        return this._timezone;
    }

}