import { LightningElement, api, track, wire } from 'lwc';
import getVendorId from '@salesforce/apex/RetailPurchaseOrderFormController.getVendorId';
import getRecordTypeId from '@salesforce/apex/RetailPurchaseOrderFormController.getRecordTypeId';
import { getRecord } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';



// TODO - remove hard coding of ids. replace with proper queries/custom settings etc.
const FIELDS = [
    'Order.Store_Location__c',
    'Order.Store_Number__c',
];

export default class RetailPurchaseOrderForm extends NavigationMixin(LightningElement) {
        @api recordTypeId; 
        @api recordId; // order Id
        @api storeId;
        @api storeNumber;
        @api vendor;
        @api objectApiName = 'Retail_Purchase_Order__c';
        @track isButtonDisabled = false;

        connectedCallback(){

            console.log('hit connectedCallback retail purchase order form ', this.recordId);
  
        }	

        @wire(getVendorId)
        getVendor({ error, data }) {
            if (error) {
                console.log('Error in get vendor Id ' , error);
            } else if (data) {
                console.log('response in get vendor',data);
                this.vendor = data;
           }
        }

        @wire(getRecordTypeId)
        getRecordTypeId({ error, data }) {
            if (error) {
                console.log('Error in get record type Id ' , error);
            } else if (data) {
                console.log('response in get record type id',data);
                this.recordTypeId = data;
           }
        }
    
        @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
        wiredRecord({ error, data }) {
            if (error) {
                console.log('Error in get order wire service ' , error);
            } else if (data) {
                this.storeId = data.fields.Store_Location__c.value;
                this.storeNumber = data.fields.Store_Number__c.value;
            }
        }

        handleSubmit(event){
            event.preventDefault();       // stop the form from submitting
            this.isButtonDisabled = true;
            let fields = event.detail.fields;
            console.log('event details in submit ', JSON.parse(JSON.stringify(event.detail)));
            this.template.querySelector('lightning-record-edit-form').submit(fields);
         }
        handleSuccess(event) {
            console.log('event detail in retail purchase order success',JSON.parse(JSON.stringify(event.detail)));
            this.handleRefreshView();
         //   location.reload(true);
            this.showToast('Success','Purchase Order Created Successfully','success');
            this.isButtonDisabled = false;
            // let rpoId  = event.detail.id;
            // this[NavigationMixin.Navigate](
            //     {
            //         type: 'standard__recordPage',
            //         attributes: {
            //             objectApiName: 'Retail_Purchase_Order__c',
            //             actionName: 'view',
            //             recordId: rpoId                   
            //     }}); 
        }

        showToast(title,message,variant) {
            const evt = new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            });
            this.dispatchEvent(evt);
        }

        // fire event to close quick action - event handled by aura wrapper.
        handleCancel() {
            let closeQuickAction = true;
            let cancelRpoEvent = new CustomEvent('closequickaction', {
                detail: { closeQuickAction },
            });
            // Fire the custom event
            this.dispatchEvent(cancelRpoEvent);
        }

        handleRefreshView() {
            let refreshview = true;
            let refreshRopEvent = new CustomEvent('refreshview', {
                detail: { refreshview },
            });
            // Fire the custom event
            this.dispatchEvent(refreshRopEvent);
        }
}