import { LightningElement, api} from 'lwc';

export default class SalesSchedAppointmentTeamMember extends LightningElement {
    @api user;
    @api primary = false;
}