import { LightningElement, api, track } from 'lwc';

export default class ErrorPanel extends LightningElement {
    @api error;
    @track messages;

    connectedCallback(){
        debugger;
        let messages = [];
        if(this.error){
            if(this.error.body){
                if(this.error.body.output){
                    if(this.error.body.output.fieldErrors){
                        messages = Object.assign(messages,Object.keys(this.error.body.output.fieldErrors).map(k => this.error.body.output.fieldErrors[k][0].message));
                        debugger;
                    }else if(this.error.body.output.errors){
                        this.error.body.output.errors.forEach(function(err){
                            messages.push(err.message);
                        });
                    }
                }else if(this.error.body.message){
                    messages.push(this.error.body.message);
                }
            } else if(this.error.output){
                console.log("____this.error.output___"+JSON.stringify(this.error.output));
                let errorArray=this.error.output.errors;
                console.log("____only errors___"+JSON.stringify(errorArray));
                console.log("____errorArray Size________"+errorArray.length);
                if(this.error.output.fieldErrors){
                    messages = Object.assign(messages,Object.keys(this.error.output.fieldErrors).map(k => this.error.output.fieldErrors[k][0].fieldLabel+" : "+this.error.output.fieldErrors[k][0].message));
                    debugger;
                }
                if(errorArray.length>0){
                    console.log("____this.error.output.errors___"+JSON.stringify(this.error.output.errors));
                    let errorArray=this.error.output.errors;

                    for(let errorIndex=0;errorIndex<errorArray.length;errorIndex++)
                    {
                        messages.push(errorArray[errorIndex].message);
                    }
                    //messages = Object.assign(messages,Object.keys(this.error.output.errors).map(k => this.error.output.errors[k][0].message));
                    /*this.error.output.errors.forEach(function(err){
                        messages.push(err.message);
                    });*/
                }
            }else if(this.error.message){
                messages.push('__body not here___'+this.error.message);
            }else{
                messages.push(this.error);
            }
        }
        this.messages = messages;
    }
}