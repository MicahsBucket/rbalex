import {
	LightningElement,
	track,
	api,
	wire
} from 'lwc';
import {
	updateRecord
} from 'lightning/uiRecordApi';
import findFinanceCompany from '@salesforce/apex/SalesSchedAppointmentManagerCtrl.getFinanceCompanies';
import findFinancePlan from '@salesforce/apex/SalesSchedAppointmentManagerCtrl.getFinancePlans';
//import scheduleFollowup from '@salesforce/apex/SalesSchedStoreManagerCtrl.scheduleFollowup';

import TIME_START_FIELD from '@salesforce/schema/Sales_Order__c.Time_Arrived__c';
import TIME_END_FIELD from '@salesforce/schema/Sales_Order__c.Time_Left__c';
import STATUS_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Status__c';
import RESULT_FIELD from '@salesforce/schema/Sales_Order__c.Result__c'
import SALES_ORDER_ID_FIELD from '@salesforce/schema/Sales_Order__c.Id';
import RESOURCE_ID_FIELD from '@salesforce/schema/Sales_Appointment_Resource__c.Id';
import PAYMENT_TYPE_FIELD from '@salesforce/schema/Sales_Order__c.Payment_Type__c';
import SALES_ORDER_LOOKUP_FIELD from '@salesforce/schema/Sales_Appointment__c.Sales_Order__c';
import ORIGINAL_APPOINTMENT_LOOKUP_FIELD from '@salesforce/schema/Sales_Appointment__c.Original_Appointment__c';
import RESCHEDULE_FIELD from '@salesforce/schema/Sales_Order__c.Rescheduled__c';
import FINANCE_ISSUES_FIELD from '@salesforce/schema/Sales_Order__c.Finance_Issues__c';
import STORE_FINANCE_PROGRAM_FIELD from '@salesforce/schema/Sales_Order__c.Store_Finance_Program__c';
import TURNED_DOWN_BY_FIELD from '@salesforce/schema/Sales_Order__c.Turned_Down_By__c';
import CHECK_OUT_TIME from '@salesforce/schema/Sales_Order__c.Check_out_Time__c';
import CHECK_In_TIME from '@salesforce/schema/Sales_Order__c.Check_In_Time__c';

const DELAY = 300;

export default class SalesSchedAppointmentCard extends LightningElement {
	@track error = false;
	@track displayResulting = false;
	/* below variable are created by Ratna for highlighting fields*/
	@api WhyNotCovered;
	@api keyContactPerson;
	@api Comments;
	@api ProjectDescription;
	@api whyNoDemo;
	@api followUpDateTime;
	@api mail;
	@api ownerPresent;
	@api seriesOneWindow;
	@api seriesTwoWindow;
	@api patioDoors;
	@api entryDoors;
	@api amountIfSold;
	@api seriesOneQuote;
	@api seriesTwoQuote;
	@track seriesAQuote
	@api patioQuote;
	@api entryQuote;
	@api caQuote;
	@api cashField;
	@api bayWQuote;
	@api bowWQuote;
	@api keyReason;
	@api futurePurchase;
	@api depositType;
	@api deposit;
	@api construction;
	@api customer;
	@api concerns;
	@api addressConcerns;
	@api partialSale;
	@api historicalArea;
	@api accountNumber;
	@api approved;
	@api renewal;
	/* Till here */
	@api wrapper;
	@api showResultedDetails = false;
	@api paymentTypeOptions;
	@api resultOptions;
	@api yesNoOptions;
	@track resultStatus;
	@track paymentType;
	@track reschedule;
	@track financeIssues;
	@track followups;
	@track financeCompanySearchKey = '';
	@track financePlanSearchKey = '';
	@track selectedFinanceCompany;
	@track selectedFinanceProgram;
	@track selectedFinanceProgramObject;
	@track selectedFinanceCompanyObject;
	@track financePlanFocus;
	@track financeCompanyFocus;
	//
	@track turnedDownBySearchKey = '';
	@track selectedTurnedDownBy;
	@track selectedTurnedDownByObject;
	@track turnedDownByFocus;

	@track showAppointmentDetails;
	@track displayResultedDetails;

	@wire(findFinanceCompany, {
		configId: '$wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		searchText: '$financeCompanySearchKey'
	})
	financeCompanies;

	@wire(findFinanceCompany, {
		configId: '$wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		searchText: '$turnedDownBySearchKey'
	})
	turnedDownByCompanies;

	@wire(findFinancePlan, {
		configId: '$wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__c',
		companyId: '$selectedFinanceCompany',
		searchText: '$financePlanSearchKey'
	})
	financePlans;
	
	handleCheckIn() {
		this.handleUpdateTimefield(TIME_START_FIELD, 'Checked In');
	}

	handleCheckOut() {
		this.handleUpdateTimefield(TIME_END_FIELD, 'Checked Out');
	}

	handleAccept() {
		this.handleUpdateStatus('Accepted');
	}

	handleUpdateTimefield(field, status) {
		this.error = false;
		// eslint-disable-next-line no-undef
		let timestamp = moment().format('HH:mm:ss.SSS');
		const fields = {};
		fields[field.fieldApiName] = timestamp;
		this.handleUpdateRecord(SALES_ORDER_ID_FIELD, this.wrapper.appointment.Sales_Order__r.Id, fields, status);
	}

	handleUpdateStatus(status) {
		this.error = false;
		const fields = {};
		fields[STATUS_FIELD.fieldApiName] = status;
		this.handleUpdateRecord(RESOURCE_ID_FIELD, this.wrapper.resource.Id, fields);
	}

	handleUpdateRecord(idField, id, fields, status) {
		fields[idField.fieldApiName] = id;
		updateRecord({
				fields: fields
			})
			//eslint-disable-next-line no-unused-vars
			.then(result => {
				if (status) {
					this.handleUpdateStatus(status);
				} else {
					this.dispatchEvent(new CustomEvent('refreshappointments'));
				}
			})
			.catch(error => {
				console.log("____error___" + JSON.stringify(error));
				this.error = error;
			});
	}

	handleShowAppointmentDetails() {
		this.showAppointmentDetails = true;
	}

	handleShowResultedDetails() {
		if (this.displayResultedDetails) {
			this.displayResultedDetails = false;
		} else {
			this.displayResultedDetails = true;
		}
	}

	handleHideAppointmentDetails() {
		this.showAppointmentDetails = false;
	}

	handleResultChange(event) {
		this.resultStatus = event.detail.value;
		console.log("____resultStatus______"+this.resultStatus);
	}

	handlePaymentTypeChange(event) {
		this.paymentType = event.detail.value;
	}

	handleRescheduleChange(event) {
		this.reschedule = event.detail.value;
		//this.reschedulePK=event.detail.value;
	}

	handleFinanceIssuesChange(event) {
		this.financeIssues = event.detail.value;
	}

	handleErrorModalClose() {
		this.error = false;
	}

	handleDisplayResulting() {
		this.resultStatus = this.wrapper.appointment.Sales_Order__r.Result__c;
		/* Below code written by Ratna for hightlighting fields*/
		this.WhyNotCovered=this.wrapper.appointment.Sales_Order__r.Why_Not_Covered__c;
		this.keyContactPerson=this.wrapper.appointment.Sales_Order__r.Key_Contact_Person__c;
		this.Comments=this.wrapper.appointment.Sales_Order__r.Comments__c;
		this.ProjectDescription=this.wrapper.appointment.Sales_Order__r.Project_Description__c;
		this.whyNoDemo=this.wrapper.appointment.Sales_Order__r.Why_No_Demo__c;
		this.followUpDateTime=this.wrapper.appointment.Sales_Order__r.Follow_Up_Appt_Date_Time__c;
		this.mail=this.wrapper.appointment.Sales_Order__r.Mail_and_Canvass_Neighborhood__c;
		this.ownerPresent=this.wrapper.appointment.Sales_Order__r.Were_All_Owners_Present__c;
		this.seriesOneWindow=this.wrapper.appointment.Sales_Order__r.Series_One_Windows__c;
		this.seriesTwoWindow=this.wrapper.appointment.Sales_Order__r.Series_Two_Windows__c;
		this.seriesAQuote = this.wrapper.appointment.Sales_Order__r.Includes_Series_A_Window__c;
		this.patioDoors=this.wrapper.appointment.Sales_Order__r.Patio_Doors__c;
		this.entryDoors=this.wrapper.appointment.Sales_Order__r.Entry_Doors__c;
		this.amountIfSold=this.wrapper.appointment.Sales_Order__r.Amount_If_Sold__c;
		this.seriesOneQuote=this.wrapper.appointment.Sales_Order__r.Series_One_Windows_Quoted__c;
		this.seriesTwoQuote=this.wrapper.appointment.Sales_Order__r.Series_Two_Windows_Quoted__c;
		this.patioQuote=this.wrapper.appointment.Sales_Order__r.Patio_Doors_Quoted__c;
		this.entryQuote=this.wrapper.appointment.Sales_Order__r.Entry_Doors_Quoted__c;
		this.caQuote=this.wrapper.appointment.Sales_Order__r.Contract_Amount_Quoted__c;
		this.bayWQuote=this.wrapper.appointment.Sales_Order__r.BAY_Windows_Quoted__c;
		this.bowWQuote=this.wrapper.appointment.Sales_Order__r.BOW_Windows_Quoted__c;
		this.keyReason=this.wrapper.appointment.Sales_Order__r.Key_Reason_Customer_Didnt_Proceed__c;
		this.futurePurchase=this.wrapper.appointment.Sales_Order__r.Future_Purchase__c;
		this.depositType=this.wrapper.appointment.Sales_Order__r.Deposit_Type__c;
		this.deposit=this.wrapper.appointment.Sales_Order__r.Deposit__c;
		this.construction=this.wrapper.appointment.Sales_Order__r.Construction_Department_Notes__c;
		this.customer=this.wrapper.appointment.Sales_Order__r.Why_Customer_Purchased__c;
		this.concerns=this.wrapper.appointment.Sales_Order__r.Concerns__c;
		this.addressConcerns=this.wrapper.appointment.Sales_Order__r.Address_Concerns__c;
		this.partialSale=this.wrapper.appointment.Sales_Order__r.Partial_Sale__c;
		this.historicalArea=this.wrapper.appointment.Sales_Order__r.Historical_Area__c;
		this.accountNumber=this.wrapper.appointment.Sales_Order__r.Account_Number__c;
		this.approved=this.wrapper.appointment.Sales_Order__r.Approved__c;
		this.renewal=this.wrapper.appointment.Sales_Order__r.Next_For_Renewal__c;
		this.cashField = this.wrapper.appointment.Sales_Order__r.Cash__c;
		//this.reschedulePK=this.wrapper.appointment.Sales_Order__r.Rescheduled__c;
		/* Till Here */	
		try
		{
			if (this.wrapper.appointment.Sales_Order__r.Store_Finance_Program__r && this.resultStatus==='Sale') {
				let sfp = this.wrapper.appointment.Sales_Order__r.Store_Finance_Program__r;
				this.selectedFinanceProgramObject = {
					label: sfp.Name,
					value: sfp.Id
				};
				this.selectedFinanceCompanyObject = {
					label: sfp.Finance_Company__r.Name,
					value: sfp.Finance_Company__r.Id
				};
				this.selectedFinanceProgram = sfp.Id;
				this.selectedFinanceCompany = sfp.Finance_Company__r.Id;
			}
			else {
				this.selectedFinanceProgram = undefined;
				this.selectedFinanceCompany = undefined;
				this.selectedFinanceProgramObject = undefined;
				this.selectedFinanceCompanyObject = undefined;
			}

			this.paymentType = this.wrapper.appointment.Sales_Order__r.Payment_Type__c;
			this.reschedule = this.wrapper.appointment.Sales_Order__r.Rescheduled__c;
			this.financeIssues = this.wrapper.appointment.Sales_Order__r.Finance_Issues__c;

			if (this.wrapper.appointment.Sales_Order__r.Turned_Down_By__r) {
				let sfp = this.wrapper.appointment.Sales_Order__r.Turned_Down_By__r;
				this.selectedTurnedDownByObject = {
					label: sfp.Name,
					value: sfp.Id
				};
				this.selectedTurnedDownBy = sfp.Id;
			}

			let followups = [];
			if (this.wrapper.appointment.Follow_Ups__r) {
				this.wrapper.appointment.Follow_Ups__r.forEach(followup => {
					followups.push(followup.Appointment_Date_Time__c);
				});
			}
			this.followups = followups;

			this.displayResulting = true;
		}
		catch(e)
		{
			console.log("_____error_____"+e.message);
		}
	}

	handleResultCancel() {
		this.resultStatus = undefined;
		this.paymentType = undefined;
		this.displayResulting = undefined;
	}

	handleResultingSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		let isSecondary = this.wrapper.isSecondary;
		const fields = event.detail.fields;
		if (!isSecondary) {
            let captureCheckoutTime = moment('July 1, 2014 ' + fields[CHECK_OUT_TIME.fieldApiName]);
            let captureCheckInTime = moment('July 1, 2014 '+fields[CHECK_In_TIME.fieldApiName]);
			let timestamppattern = "((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))";
			let validateTimeStamp = fields[CHECK_OUT_TIME.fieldApiName].match(timestamppattern);
			console.log("______validateTimeStamp________" + validateTimeStamp);
			if (validateTimeStamp == null) {
				//fields[CHECK_OUT_TIME.fieldApiName].
			}
			fields[TIME_END_FIELD.fieldApiName] = captureCheckoutTime.format('HH:mm:ss.SSS'); //.format('HH:mm:ss.SSS');
            fields[TIME_START_FIELD.fieldApiName] =captureCheckInTime.format('HH:mm:ss.SSS');//.format('HH:mm:ss.SSS');
            fields[RESULT_FIELD.fieldApiName] = (this.resultStatus ? this.resultStatus : '');
			fields[PAYMENT_TYPE_FIELD.fieldApiName] = (this.paymentType ? this.paymentType : '');
			fields[RESCHEDULE_FIELD.fieldApiName] = (this.reschedule ? this.reschedule : '');
			fields[FINANCE_ISSUES_FIELD.fieldApiName] = (this.financeIssues ? this.financeIssues : '');
			fields[STORE_FINANCE_PROGRAM_FIELD.fieldApiName] = (this.selectedFinanceProgram ? this.selectedFinanceProgram : '');
			fields[TURNED_DOWN_BY_FIELD.fieldApiName] = (this.selectedTurnedDownBy ? this.selectedTurnedDownBy : '');
		}
		let el = this.template.querySelector('lightning-record-edit-form');
		if (el) {
			el.submit(fields);
			//this.dispatchEvent(new CustomEvent('refreshappointments'));
		}
	}

	handleRescheduleSubmit(event) {
		event.preventDefault(); // stop the form from submitting
		const fields = event.detail.fields;
		fields[SALES_ORDER_LOOKUP_FIELD.fieldApiName] = this.wrapper.appointment.Sales_Order__r.Id;
		fields[ORIGINAL_APPOINTMENT_LOOKUP_FIELD.fieldApiName] = this.wrapper.appointment.Id;
		let el = this.template.querySelector('lightning-record-form').submit(fields);
		if (el) {
			el.submit(fields);
		}
	}
 
	handleRescheduleSuccess(event) {
		let dt = event.detail.fields.Appointment_Date_Time__c;
		if (dt) {
			//let timeLong = new Date(dt).getTime();
			this.followups.push(dt.value);
		}
	}

	handleRescheduleError(event) {
		this.error = event.detail;
	}

	handleResultingSuccess() {
		this.displayResulting = false;
		this.resultStatus = false;
		this.paymentType = false;
		this.dispatchEvent(new CustomEvent('refreshappointments'));
	}

	handleResultingError(event) {
		console.log("____error_event.detail__" + JSON.stringify(event.detail));
		this.error = event.detail;
	}

	handleTurnedDownByChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.turnedDownBySearchKey = searchKey;
		}, DELAY)
	}

	handleFinanceCompanyChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.financeCompanySearchKey = searchKey;
		}, DELAY)
	}
	/* Below methods written by Ratna for highlightning fields */
	checkValue(event)
	{
		this.WhyNotCovered=event.target.value;
	}
	keyContactValue(event)
	{
		this.keyContactPerson=event.target.value;
	}
	commentsValue(event)
	{
		this.Comments=event.target.value;
	}
	projectDescriptionValue(event)
	{
		this.ProjectDescription=event.target.value;
	}
	whyNoDemoValue(event)
	{
		this.whyNoDemo=event.target.value;
	}
	get WhyNotCoveredColor()
	{
		return this.WhyNotCovered===undefined||this.WhyNotCovered.length===0?'changeColorRed':'';
	}
	get keyContactPersonColor()
	{
		return this.keyContactPerson===undefined||this.keyContactPerson.length===0?'changeColorRed':'';
	}
	get commentsColor()
	{
		return this.Comments===undefined||this.Comments.length===0?'changeColorRed':'';
	}
	get projectDescriptionColor()
	{
		return this.ProjectDescription===undefined||this.ProjectDescription.length===0?'changeColorRed':'';
	}	
	get whyNoDemoColor()
	{
		return this.whyNoDemo===undefined||this.whyNoDemo.length===0?'changeColorRed':'';
	}
	get reschedulePKColor()
	{
		return this.reschedule===undefined||this.reschedule.length===0?'changeColorRed':'';
	}

	followUpDateTimeValue(event)
	{
		this.followUpDateTime=event.target.value;
	}
	get followUpDateTimeColor()
	{
		return this.followUpDateTime===undefined||this.followUpDateTime.length===0?'changeColorRed':'';
	}

	mailValue(event)
	{
		this.mail=event.target.value;
	}
	get mailColor()
	{
		return this.mail===undefined||this.mail.length===0?'changeColorRed':'';
	}

	cashFieldValue(event) {
		this.cashField = event.target.value;
	}
	get cashFieldColor() {
		return this.cashField === undefined || this.cashField.length === 0 ? 'changeColorRed' : '';
	}

	ownerPresentValue(event)
	{
		this.ownerPresent=event.target.value;
	}
	get ownerPresentColor()
	{
		return this.ownerPresent===undefined||this.ownerPresent.length===0?'changeColorRed':'';
	}

	seriesOneWindowValue(event)
	{
		this.seriesOneWindow=event.target.value;
	}
	get seriesOneWindowColor()
	{
		return this.seriesOneWindow===undefined||this.seriesOneWindow.length===0?'changeColorRed':'';
	}

	seriesTwoWindowValue(event)
	{
		this.seriesTwoWindow=event.target.value;
	}
	get seriesTwoWindowColor()
	{
		return this.seriesTwoWindow===undefined||this.seriesTwoWindow.length===0?'changeColorRed':'';
	}
	seriesAQuoteValue(event) {
		this.seriesAQuote = event.target.value;
	}
	get seriesAQuoteColor() {
		return this.seriesAQuote === undefined || this.seriesAQuote.length === 0 ? 'changeColorRed' : '';
	}

	patioDoorsValue(event)
	{
		this.patioDoors=event.target.value;
	}
	get patioDoorsColor()
	{
		return this.patioDoors===undefined||this.patioDoors.length===0?'changeColorRed':'';
	}

	entryDoorsValue(event)
	{
		this.entryDoors=event.target.value;
	}
	get entryDoorsColor()
	{
		return this.entryDoors===undefined||this.entryDoors.length===0?'changeColorRed':'';
	}

	amountIfSoldValue(event)
	{
		this.amountIfSold=event.target.value;
	}
	get amountIfSoldColor()
	{
		return this.amountIfSold===undefined||this.amountIfSold.length===0?'changeColorRed':'';
	}

	seriesOneQuoteValue(event)
	{
		this.seriesOneQuote=event.target.value;
	}
	get seriesOneQuoteColor()
	{
		return this.seriesOneQuote===undefined||this.seriesOneQuote.length===0?'changeColorRed':'';
	}

	seriesTwoQuoteValue(event)
	{
		this.seriesTwoQuote=event.target.value;
	}
	get seriesTwoQuoteColor()
	{
		return this.seriesTwoQuote===undefined||this.seriesTwoQuote.length===0?'changeColorRed':'';
	}

	patioQuoteValue(event)
	{
		this.patioQuote=event.target.value;
	}
	get patioQuoteColor()
	{
		return this.patioQuote===undefined||this.patioQuote.length===0?'changeColorRed':'';
	}
	
	entryQuoteValue(event)
	{
		this.entryQuote=event.target.value;
	}
	get entryQuoteColor()
	{
		return this.entryQuote===undefined||this.entryQuote.length===0?'changeColorRed':'';
	}

	caQuoteValue(event)
	{
		this.caQuote=event.target.value;
	}
	get caQuoteColor()
	{
		return this.caQuote===undefined||this.caQuote.length===0?'changeColorRed':'';
	}

	bayWQuoteValue(event)
	{
		this.bayWQuote=event.target.value;
	}
	get bayWQuoteColor()
	{
		return this.bayWQuote===undefined||this.bayWQuote.length===0?'changeColorRed':'';
	}

	bowWQuoteValue(event)
	{
		this.bowWQuote=event.target.value;
	}
	get bowWQuoteColor()
	{
		return this.bowWQuote===undefined||this.bowWQuote.length===0?'changeColorRed':'';
	}

	keyReasonValue(event)
	{
		this.keyReason=event.target.value;
	}
	get keyReasonColor()
	{
		return this.keyReason===undefined||this.keyReason.length===0?'changeColorRed':'';
	}

	futurePurchaseValue(event)
	{
		this.futurePurchase=event.target.value;
	}
	get futurePurchaseColor()
	{
		return this.futurePurchase===undefined||this.futurePurchase.length===0?'changeColorRed':'';
	}

	get paymentTypeColor()
	{
		return this.paymentType===undefined||this.paymentType.length===0?'changeColorRed':'';
	}

	depositTypeValue(event)
	{
		this.depositType=event.target.value;
	}
	get depositTypeColor()
	{
		return this.depositType===undefined||this.depositType.length===0?'changeColorRed':'';
	}

	depositValue(event)
	{
		this.deposit=event.target.value;
	}
	get depositColor()
	{
		return this.deposit===undefined||this.deposit.length===0?'changeColorRed':'';
	}

	constructionValue(event)
	{
		this.construction=event.target.value;
	}
	get contructionColor()
	{
		return this.construction===undefined||this.construction.length===0?'changeColorRed':'';
	}

	customerValue(event)
	{
		this.customer=event.target.value;
	}
	get customerColor()
	{
		return this.customer===undefined||this.customer.length===0?'changeColorRed':'';
	}

	concernsValue(event)
	{
		this.concerns=event.target.value;
	}
	get concernsColor()
	{
		return this.concerns===undefined||this.concerns.length===0?'changeColorRed':'';
	}

	addressConcernsValue(event)
	{
		this.addressConcerns=event.target.value;
	}
	get addressConcernsColor()
	{
		return this.addressConcerns===undefined||this.addressConcerns.length===0?'changeColorRed':'';
	}

	partialSaleValue(event)
	{
		this.partialSale=event.target.value;
	}
	get partialSaleColor()
	{
		return this.partialSale===undefined||this.partialSale.length===0?'changeColorRed':'';
	}

	historicalAreaValue(event)
	{
		this.historicalArea=event.target.value;
	}
	get historicalAreaColor()
	{
		return this.historicalArea===undefined||this.historicalArea.length===0?'changeColorRed':'';
	}

	get financeCompanyColor()
	{
		return this.selectedFinanceCompany===undefined||this.selectedFinanceCompany.length===0?'changeColorRed':'';
	}

	get financePlanColor()
	{
		return this.selectedFinanceProgram===undefined||this.selectedFinanceProgram.length===0?'changeColorRed':'';
	}

	accountNumberValue(event)
	{
		this.accountNumber=event.target.value;
	}
	get accountNumberColor()
	{
		return this.accountNumber===undefined||this.accountNumber.length===0?'changeColorRed':'';
	}

	approvedValue(event)
	{
		this.approved=event.target.value;
	}
	get approvedColor()
	{
		return this.approved===undefined||this.approved.length===0?'changeColorRed':'';
	}

	get financeTurnDownColor()
	{
		return this.selectedTurnedDownBy===undefined||this.selectedTurnedDownBy.length===0?'changeColorRed':''
	}

	renewalValue(event)
	{
		this.renewal=event.target.value;
	}
	get renewalColor()
	{
		return this.renewal===undefined||this.renewal.length===0?'changeColorRed':'';
	}

	get financeIssueColor()
	{
		return this.financeIssues===undefined||this.financeIssues.length===0?'changeColorRed':'';
	}
	/* Till Here */

	handleFinancePlanChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.financePlanSearchKey = searchKey;
		}, DELAY)
	}

	handleTurnedDownByClick(event) {
		this.selectedTurnedDownBy = event.detail.value;
		this.selectedTurnedDownByObject = event.detail;
	}

	handleTurnedDownByRemove() {
		this.selectedTurnedDownBy = undefined;
		this.selectedTurnedDownByObject = undefined;
	}

	handleCompanyClick(event) {
		this.selectedFinanceCompany = event.detail.value;
		this.selectedFinanceCompanyObject = event.detail;
	}

	handleCompanyRemove() {
		this.selectedFinanceCompany = undefined;
		this.selectedFinanceProgram = undefined;
		this.selectedFinanceCompanyObject = undefined;
		this.selectedFinanceProgramObject = undefined;
	}

	handlePlanClick(event) {
		this.selectedFinanceProgram = event.detail.value;
		this.selectedFinanceProgramObject = event.detail;
	}

	handleRemoveTurnedDownByFocus() {
		this.turnedDownByFocus = false;
	}

	handleTurnedDownByFocus() {
		this.turnedDownByFocus = true;
	}

	handleRemovePlanFocus() {
		this.financePlanFocus = false;
	}

	handleRemoveCompanyFocus() {
		this.financeCompanyFocus = false;
	}

	handleFinancePlanFocus() {
		this.financePlanFocus = true;
	}

	handleFinanceCompanyFocus() {
		this.financeCompanyFocus = true;
	}

	get displayResultOptions() {
		return !this.wrapper.isSecondary && this.resultOptions;
	}

	get displayFinanceDetails() {
		return 'No' === this.financeIssues;
	}

	get displayAppointment() {
		return this.wrapper && this.wrapper.appointment;
	}

	get displayAccept() {
		return this.isStatus('Confirmed');
	}

	get displayCheckIn() {
		return this.isStatus('Accepted');
	}

	get displayCheckOut() {
		return this.isStatus('Checked In');
	}

	get displayResult() { 
		return this.isStatus('Checked Out');
	}

	get displayResulted() {
		return this.isStatus('Resulted');
	}

	isStatus(status) {
		return this.wrapper && this.wrapper.resource && this.wrapper.resource.Status__c === status;
	}

	get displayResultingFields() {
		return !this.wrapper.isSecondary && this.resultStatus;
	}

	get displayFieldsAfterResultSelection() {
		return this.resultStatus;
	}

	get displayPayment() {
		return this.resultStatus === 'Sale';
	}

	get displayRecord() {
		return this.wrapper && this.wrapper.appointment && this.wrapper.appointment.Sales_Order__r && this.wrapper.appointment.Sales_Order__r.Result__c
	}

	get displaySale() {
		return this.resultStatus && 'Sale' === this.resultStatus;
	}

	get displaySaleOrDemoNoSale() {
		return this.resultStatus && ('Sale' === this.resultStatus || 'Demo No Sale' === this.resultStatus);
	}

	get displaySeriesTwo() {
		return this.wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c
	}
	get displaySeriesA() {
		return this.wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_A__c;
	}
	get displayEntryDoor() {
		return this.wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Entry_Doors__c
	}

	get displayFinance() {
		return this.paymentType && 'Finance' === this.paymentType;
	}

	get displayCashAndFinance() {
		return this.paymentType && 'Cash & Finance' === this.paymentType;
	}

	get displayFinanceTurnDown() {
		return this.paymentType && 'Finance Turn-Down' === this.paymentType;
	}

	get displayDemoNoSale() {
		return this.resultStatus && 'Demo No Sale' === this.resultStatus;
	}

	get displayNoDemo() {
		return this.resultStatus && 'No Demo' === this.resultStatus;
	}

	get displaySaleDemoNoSaleOrNoDemo() {
		return this.displaySale || this.displayDemoNoSale || this.displayNoDemo;
	}

	get displayNotCovered() {
		return this.resultStatus && 'Not Covered' === this.resultStatus;
	}

	get displayNotHome() {
		return this.resultStatus && 'Not Home' === this.resultStatus;
	}

	get displayFollowup() {
		return 'Yes' === this.reschedule;
	}

	get displayRideAlongAdvice() {
		console.log("______is Primary_____" + this.wrapper.isSecondary)
		console.log("___this.wrapper________"+JSON.stringify(this.wrapper));
		return this.wrapper && this.wrapper.hasRideAlongs;
	}

	get displayPaymentResulted() {
		return this.wrapper.appointment.Sales_Order__r.Result__c === 'Sale';
	}

	get displaySaleResulted() {
		return this.wrapper.appointment.Sales_Order__r.Result__c === 'Sale';
	}

	get displaySaleOrDemoNoSaleResulted() {
		let result = this.wrapper.appointment.Sales_Order__r.Result__c;
		return ('Sale' === result || 'Demo No Sale' === result);
	}

	get displayFinanceResulted() {
		return this.wrapper.appointment.Sales_Order__r.Payment_Type__c === 'Finance';
	}

	get displayCashAndFinanceResulted() {
		return this.wrapper.appointment.Sales_Order__r.Payment_Type__c === 'Cash & Finance';
	}

	get displayFinanceTurnDownResulted() {
		return this.wrapper.appointment.Sales_Order__r.Payment_Type__c === 'Finance Turn-Down';
	}

	get displayDemoNoSaleResulted() {
		return 'Demo No Sale' === this.wrapper.appointment.Sales_Order__r.Result__c;
	}

	get displayNoDemoResulted() {
		return 'No Demo' === this.wrapper.appointment.Sales_Order__r.Result__c;
	}

	//get displaySeriesTwo(){
	//    return this.wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Sells_Series_2__c
	//}


	get displaySaleDemoNoSaleOrNoDemoResulted() {
		return this.displaySaleResulted || this.displayDemoNoSaleResulted || this.displayNoDemoResulted;
	}

	get displayNotCoveredResulted() {
		return 'Not Covered' === this.wrapper.appointment.Sales_Order__r.Result__c;
	}

	get displayNotHomeResulted() {
		return 'Not Home' === this.wrapper.appointment.Sales_Order__r.Result__c;
	}

	// get displayFollowup(){
	//    return 'Yes' === this.wrapper.appointment.Sales_Order__r.Opportunity__r.Store_Location__r.Active_Store_Configuration__r.Reps_can_schedule_follow_ups__c
	//}

	//get displayRideAlongAdvice(){
	//    return this.wrapper && this.wrapper.hasRideAlongs;
	//}

	get displayFinanceDetailsResulted() {
		return 'No' === this.wrapper.appointment.Sales_Order__r.Finance_Issues__c;
	}
}