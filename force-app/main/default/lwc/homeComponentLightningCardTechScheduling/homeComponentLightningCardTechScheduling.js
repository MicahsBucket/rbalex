import { LightningElement, track, wire } from 'lwc';
import getOpportunities from '@salesforce/apex/HomeComponentTechScheduleController.getOpportunities';
import getAssignedWorkOrders from '@salesforce/apex/HomeComponentTechScheduleController.getAssignedWorkOrders';
import getToBeScheduledWorkOrders from '@salesforce/apex/HomeComponentTechScheduleController.getToBeScheduledWorkOrders';

const opportunityColumns = [
    {label: 'Opportunity', fieldName: 'opportunityURL', type: "url", 
        typeAttributes: {label: { fieldName: 'opportunityName'}, value:{fieldName: 'opportunityURL'}, target: '_blank'}},   
    {label: 'Account', fieldName: 'accountURL', type: "url", 
        typeAttributes: {label: { fieldName: 'accountName'}, value:{fieldName: 'accountURL'}, target: '_blank'}},   
    {label: 'Close Date', fieldName: 'closeDate', type:'date-local', 
        typeAttributes: {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric'}},
    {label: 'Stage', fieldName: 'stageName', type: "text"}, 
    ];

const AssignedWorkOrderColumns = [
    {label: 'Work Order', fieldName: 'workOrderURL', type: "url", 
        typeAttributes: {label: { fieldName: 'workOrderName'}, value:{fieldName: 'workOrderURL'}, target: '_blank'}},   
    {label: 'Contact', fieldName: 'contactURL', type: "url", 
        typeAttributes: {label: { fieldName: 'contactName'}, value:{fieldName: 'contactURL'}, target: '_blank'}}, 
    {label: 'Account', fieldName: 'accountURL', type: "url", 
        typeAttributes: {label: { fieldName: 'accountName'}, value:{fieldName: 'accountURL'}, target: '_blank'}}, 
    {label: 'Sold Order', fieldName: 'soldOrderURL', type: "url", 
        typeAttributes: {label: { fieldName: 'soldOrderName'}, value:{fieldName: 'soldOrderURL'}, target: '_blank'}},     
    ];

const ToBeScheduledWorkOrderColumns = [
    {label: 'Work Order', fieldName: 'workOrderURL', type: "url", 
        typeAttributes: {label: { fieldName: 'workOrderName'}, value:{fieldName: 'workOrderURL'}, target: '_blank'}},   
    {label: 'Contact', fieldName: 'contactURL', type: "url", 
        typeAttributes: {label: { fieldName: 'contactName'}, value:{fieldName: 'contactURL'}, target: '_blank'}}, 
    {label: 'Scheduled Start Time', fieldName: 'scheduledStartTime', type:'date',  
        typeAttributes: {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            hour12: true }},
    {label: 'Primary Resource', fieldName: 'primaryResource', type: "text"},     
    ];

export default class HomeComponentLightningCardTechScheduling extends LightningElement {
   
    @track ToBeScheduledWorkOrderColumns = ToBeScheduledWorkOrderColumns;
    @track AssignedWorkOrderColumns = AssignedWorkOrderColumns;
    @track opportunityColumns = opportunityColumns;

    @track opportunityListOriginal = [];
    @track opportunityList = [];
    @track toBeScheduledWOListOriginal = [];
    @track toBeScheduledWOList = [];
    @track assignedWOListOriginal = [];
    @track assignedWOList = [];

    @wire(getOpportunities, {searchKey: ''})
    wiredGetOpportunities({ error, data }) {             
        if (data) {           
            this.opportunityListOriginal = data;
            this.opportunityList = data;
        }
        else if (error) {
            this.error = error;
        }     
        console.log('opportunityList', JSON.stringify(data));
        console.log('opportunityList Length', this.opportunityList.length);       
    }

    @wire(getAssignedWorkOrders, {searchKey: ''})
    wiredGetAssignedWorkOrders({ error, data }) {             
        if (data) {           
            this.assignedWOListOriginal = data;
            this.assignedWOList = data;
        }
        else if (error) {
            this.error = error;
        }     
        console.log('assignedWOList', JSON.stringify(data));
        console.log('assignedWOist Length', this.assignedWOList.length);       
    }

    @wire(getToBeScheduledWorkOrders, {searchKey: ''})
    wiredGetToBeScheduledWorkOrders({ error, data }) {             
        if (data) {           
            this.toBeScheduledWOListOriginal = data;
            this.toBeScheduledWOList = data;
        }
        else if (error) {
            this.error = error;
        }     
        console.log('toBeScheduledWOList', JSON.stringify(data));
        console.log('assignedWOist Length: ' + this.assignedWOList.length);       
    }

    updateSeachKeySerOppComp(event) {
        this.sVal = event.target.value;
        this.handleSearchSerOppComp();
    }

    handleSearchSerOppComp() {
        console.log('Search val   ' +this.sVal);
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getOpportunities({
                    searchKey: this.sVal
                })
                .then(result => {
                    // set @track contacts variable with return contact list from server  
                    this.opportunityList = result;
                })
                .catch(error => {
                    this.opportunityList = null;
                });
        }
        else {
            this.opportunityList = this.opportunityListOriginal;
        }
    }

    updateSeachKeySerWOAssignedComp(event) {
        this.sVal = event.target.value;
        this.handleSearchSerWOAssignedComp();
    }

    handleSearchSerWOAssignedComp() {
        console.log('Search val   ' +this.sVal);
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getAssignedWorkOrders({
                    searchKey: this.sVal
                })
                .then(result => {
                    this.assignedWOList = result;
                })
                .catch(error => {
                    this.assignedWOList = null;
                });
        }
        else {
            this.assignedWOList = this.assignedWOListOriginal;
        }
    }

    updateSeachKeySerWOScheduledComp(event) {
        this.sVal = event.target.value;
        this.handleSearchSerWOscheduledComp();
    }

    handleSearchSerWOscheduledComp() {
        console.log('Search val   ' +this.sVal);
        // if search input value is not blank then call apex method, else display error msg 
        if (this.sVal !== '') {
            getToBeScheduledWorkOrders({
                    searchKey: this.sVal
                })
                .then(result => {
                    this.toBeScheduledWOList = result;
                })
                .catch(error => {
                    this.toBeScheduledWOList = null;
                });
        }
        else {
            this.toBeScheduledWOList = this.toBeScheduledWOListOriginal;
        }
    }

}