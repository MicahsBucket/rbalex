/**
* @author Jason Flippen
* @date 01/25/2021 
* @description Provides functionality for the newChangeOrderAction LWC.
*
*			   Supporting database functionality provided by the following Apex Class:
*			   - NewWorkOrderActionController
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* N/A           N/A                     N/A
* ====================================================================================================
*/
import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getOrderData from '@salesforce/apex/NewWorkOrderActionController.getOrderData';
import getWorkOrderTypeList from '@salesforce/apex/NewWorkOrderActionController.getWorkOrderTypeList';
import createWorkOrder from '@salesforce/apex/NewWorkOrderActionController.createWorkOrder';

export default class NewWorkOrderAction extends NavigationMixin(LightningElement) {
    
    @api recordId;

    order;
    workOrderTypeList = [];
    selectedWorkOrderTypeId = '';
    
    isEligible = true;

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to retrieve Order data.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @wire(getOrderData, { orderId: '$recordId' })
    wiredGetOrderData({ error, data }) {

        if (data) {

            console.log('Order Data', JSON.parse(JSON.stringify(data)));
            this.order = JSON.parse(JSON.stringify(data));

            // Set default Flag values.
            let disableSave = false;
            this.isEligible = true;

            // Are we able to create a Work Order from this Order?
            if (this.order.Status == 'Draft') {
                disableSave = true;
                this.isEligible = false;
            }

            if (disableSave === false) {
                const enableSaveEvent = new CustomEvent('EnableSave');
                this.dispatchEvent(enableSaveEvent);
            }
            else {
                const disableSaveEvent = new CustomEvent('DisableSave');
                this.dispatchEvent(disableSaveEvent);
            }
            
            this.getWorkOrderTypeList();

        }
        else if (error) {
            console.log('error in getOrderData callback', error);
        }

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to List of Work Order Record Types available
    *              to be created for this type of Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    getWorkOrderTypeList() {

        getWorkOrderTypeList({ orderId: this.recordId })
        .then(result => {
            console.log('Work Order Type List', JSON.parse(JSON.stringify(result)));
            this.workOrderTypeList = JSON.parse(JSON.stringify(result));
        })
        .catch(error => {
            console.log('Error in getWorkOrderTypeList callback', error);
        })

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method to create a new Change Order.
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    @api
    handleSaveClick() {

        const showSpinner = new CustomEvent('ShowSpinner');
        const hideSpinner = new CustomEvent('HideSpinner');
        const closeQuickAction = new CustomEvent('CloseQuickAction');

        this.dispatchEvent(showSpinner);

        // Call Apex method to update the Order.
        createWorkOrder({ order: this.order, selectedWorkOrderTypeId: this.selectedWorkOrderTypeId })
        .then((result) => {

            var resultMap = result;

            var returnValue = '';
            var saveSuccess = false;
            for (var key in resultMap) {
                returnValue = key;
                if (key === 'New Work Order Success') {
                    returnValue = resultMap[key];
                    saveSuccess = true;
                }
                break;
            }
            console.log('Return Value', returnValue);

            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);    

            if (saveSuccess === true) {

                // Navigate to the new Cost Purchase Order.
                this[NavigationMixin.Navigate]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: returnValue,
                        objectApiName: 'WorkOrder', // objectApiName is optional
                        actionName: 'view'
                    }
                });

            }
            else  {

                const toastMessage = returnValue;
                const toastType = 'error';
                const showToastEvent = new CustomEvent('ShowToast', { detail: { message: toastMessage, type: toastType } });
                this.dispatchEvent(showToastEvent);

            }

        })
        .catch((error) => {
            console.log('Create Work Order Error', error);
            this.dispatchEvent(hideSpinner);
            this.dispatchEvent(closeQuickAction);
        });

    }

    /**
    * @author Jason Flippen
    * @date 01/29/2021
    * @description Method handle the "Change" event on the Work Order Type combobox. 
    * @param N/A
    * @return N/A
    *
    * CHANGE HISTORY
    * ====================================================================================================
    * DATE          NAME                    DESCRIPTION
    * N/A           N/A                     N/A
    * ====================================================================================================
    */
    handleWorkOrderTypeChange(event) {
        this.selectedWorkOrderTypeId = event.detail.value;
    }

}