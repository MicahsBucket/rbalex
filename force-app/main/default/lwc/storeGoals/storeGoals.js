/* eslint-disable */
import { LightningElement, track, wire } from 'lwc';
import getStoreGoalsImperative from '@salesforce/apex/StoreGoalsController.getStoreGoalsImperative';
import getStoreGoalsWire from '@salesforce/apex/StoreGoalsController.getStoreGoalsWire'
import getUserLocations from '@salesforce/apex/StoreGoalsController.getUserLocations';
import getUserDefaultLocation from '@salesforce/apex/StoreGoalsController.getUserDefaultLocation';
import getStoreType from '@salesforce/apex/StoreGoalsController.getStoreType';
import checkStoreConfig from '@salesforce/apex/StoreGoalsController.checkStoreConfig';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

/**
 * @author Connor Davis
 * @description This class makes all the data from the apex class usable in the html page and adds some functionality
 */
export default class StoreGoals extends LightningElement {
    // These values will store the year and location for the goal
    @track year = String(new Date().getFullYear());

    // The current selected store
    @track loc = {};

    // The type of the current selected store
    @track type = {};

    // These are to track when to show ceratin buttons/modal boxes
    @track distribute = false; // Show the button that opens the Distribute Totals Modal Box
    @track showDistribute = false; // Show the Distribute Totals Modal Box
    @track enterTotals = false; // Have the body of the modal box be a list of fields to enter totals into
    @track enterWeightsMonth = false; // Have the body of the modal box be a list of months to add weights to
    @track enterWeightsPeriod = false; // Have the body of the modal box be a list of periods to add weights to
    @track varDisButtons = false; // Show the Back button and the Distribute Totals button on the footer

    // The weight that is assigned to each "Month" used for distributing totals across the "Months"
    @track monthWeights = [];

    // A list of the propertiies' total's that were edited.
    @track updatedTotals = [];
    
    // The raw goals data for a specific store
    @track rawGoals = [];

    // The monthly goals for a specific store
    @track monthGoals = [];

    // The draft values of the monthly goals table
    @track newGoals = [];

    // The totals for the monthly goals for a specific store
    @track totals = [];

    // The draft values of the totals table
    @track totalsDrafts = [];
    
    // Sets loc to the user's default location
    @wire(getUserDefaultLocation) loc;

    // Gets a string of all the possible locations
    @wire(getUserLocations) locationOptions;

    // Get the type of the current store
    @wire(getStoreType, { loc: '$loc.data' }) type;

    // Calls the getStoreGoalsWire method, which returns all related goals,
    // which are then stored in updatedGoals (which is used for refreshing the data in handleSave)
    //Then getGoals is called, which will create goals if they don't exist, and store the goals in
    // rawGoals. This is needed because in order to use refreshApex(), you need to pass in a variable that is used for an @wire
    @wire(getStoreGoalsWire, { year: '$year', loc: '$loc.data' })
    refreshGoals(goals) {
        this.updatedGoals = goals;
        this.getGoals();
    }

    /**
     * @author Connor Davis
     * @description This method calls the apex method getStoreGoalsImperative, stores the returned data in rawGoals, 
     * then sets the months to the correct names
     */
    getGoals() {
        if (this.loc.data != null) {
            getStoreGoalsImperative({ year: Number(this.year), loc: this.loc.data })
                .then( result => { 
                    this.rawGoals = result;
                    this.totals = [];
                    this.monthGoals  = [];
                    // If store is CORO, set Month__c to periods
                    if (this.type.data === 'CORO') {
                        for (let i = 0; i < this.rawGoals.length-1; i++) {
                            this.rawGoals[i].Month__c = 'Period ' + String(i+1);
                        }
                      // Otherwise use months
                    } else {
                        this.rawGoals[0].Month__c = 'January';
                        this.rawGoals[1].Month__c = 'February';
                        this.rawGoals[2].Month__c = 'March';
                        this.rawGoals[3].Month__c = 'April';
                        this.rawGoals[4].Month__c = 'May';
                        this.rawGoals[5].Month__c = 'June';
                        this.rawGoals[6].Month__c = 'July';
                        this.rawGoals[7].Month__c = 'August';
                        this.rawGoals[8].Month__c = 'September';
                        this.rawGoals[9].Month__c = 'October';
                        this.rawGoals[10].Month__c = 'November';
                        this.rawGoals[11].Month__c = 'December';
                    }
                    // Set the last Month to 'Totals'
                    this.rawGoals[this.rawGoals.length-1].Month__c = 'Totals';
                    this.splitGoals();
                })
                .catch( error => { console.log('An error has occured while getting the goals');
                                   console.log(error); 
            });
        }
    }

    /**
     * @author Connor Davis
     * @description This method takes the locationOptions string, parses it, and returns an array of all possible locations
     * @return Returns an array of all the user's locations
     */
    get locationOptionsFunc() {
        let choices = [];
        let options = this.locationOptions.data;
        let parsedString = [];
        let config = null;

        // If options exist, split them at ';'
        if (options != null) {
            parsedString = options.split(';');
        }

        // Go through the list of locations and check if they have a store configuration
        for (let i = 0; i < parsedString.length; i++) {
            // If the location has a store configuration, push to the list of possible choices
            if (this.checkConfig(parsedString[i])) {
                choices.push({ label: parsedString[i], value: parsedString[i] });
            }
        }

        return choices;
    }

    /**
     * @author Connor Davis
     * @description This method takes the location of a store and checks whether it has a store configuration or not.
     * @param {String} storeName The name of the store we want to check
     * @return Returns true if the store has a store configuration, false if it doesn't
     */
    checkConfig(storeName) {
        let check = false;
        checkStoreConfig({ loc: storeName })
            .then(result => {
                if (result != undefined) {
                    check = result;
                }
            })
            .catch(error => {
                console.log('Failed to get config');
                check = false;
            });
        return check;
    }

    /**
     * @author Connor Davis
     * @description This method calculates all the years the user can select from, starting at the current year - 1 and going to the current year + 1
     * @return Returns an array of years from current year - 1 to current year + 1
     */
    get yearOptions() {

        let choices = [];
        let currentYear = new Date().getFullYear();
        for (let i = currentYear - 1; i <= currentYear + 1; i++) {
            choices.push({ label: String(i), value: String(i) });
        }

        return choices;
    }

    /**
     * @author Connor Davis
     * @description This method creates an array of columns to be used in the monthly goals datatable
     * @return Returns an array of columns
     */
    get monthColumns() {
        let monthLabel = 'Month';
        if (this.type.data === 'CORO') {
            monthLabel = 'Period';
        }
        const columns = [
            { label: monthLabel, fieldName: 'Month__c', type: 'text', cellAttributes: { alignment: 'left' } },
            { label: 'Issued Appt.', fieldName: 'Issued_Appointments__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Avg. Sales Price', fieldName: 'Average_Sales_Price__c', type: 'currency', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Sold Dollars', fieldName: 'Sold_Dollars__c', type: 'currency', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Sold Units', fieldName: 'Sold_Units__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: 'Series 1', fieldName: 'Series_1_Windows__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Series 2', fieldName: 'Series_2_Windows__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Entry Doors', fieldName: 'Entry_Doors__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Patio Doors', fieldName: 'Patio_Doors__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Installed Rev.', fieldName: 'Installed_Revenue__c', type: 'currency', cellAttributes: { alignment: 'left' }, editable: true },
            { label: 'Installed Units', fieldName: 'Installed_Units__c', type: 'number', cellAttributes: { alignment: 'left' }, editable: true }
        ];

        return columns;
    }

    /**
     * @author Connor Davis
     * @description This method creates an array of columns to be used in the total goals datatable
     * @return Returns an array of columns
     */
    get totalColumns() {
        const columns = [
            { label: '', fieldName: 'Month__c', type: 'text', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Issued_Appointments__c', type: 'number', cellAttributes: { alignment: 'left' } },  
            { label: '', fieldName: 'Average_Sales_Price__c', type: 'currency', cellAttributes: { alignment: 'left' } },          
            { label: '', fieldName: 'Sold_Dollars__c', type: 'currency', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Sold_Units__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Series_1_Windows__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Series_2_Windows__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Entry_Doors__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Patio_Doors__c', type: 'number', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Installed_Revenue__c', type: 'currency', cellAttributes: { alignment: 'left' } },
            { label: '', fieldName: 'Installed_Units__c', type: 'number', cellAttributes: { alignment: 'left' } }
        ];

        return columns;
    }


    /**
     * @author Connor Davis
     * @description This method will change loc to the location that the user selected
     * @param {Object} event The event caused by the combobox value changing
     */
    onChangeLocation(event) {
        // Both of these are needed so that the change in this.loc is recognized and this.loc.data has the right value.
        this.loc = event.detail;
        this.loc.data = event.detail.value;
    }

    /**
     * @author Connor Davis
     * @description This method will change year to the year that the user selected
     * @param {Object} event The event caused by the combobox value changing
     */
    onChangeYear(event) {
        this.year = event.detail.value;

        if(this.year > new Date().getFullYear()) {
            this.distribute = true;
        } else {
            this.distribute = false;
        }
    }

    /**
     * @author Connor Davis
     * @description This method will make the Distribute Totals modal box appear when the 'Distribute Totals' button is pressed
     * @param {Object} event 
     */
    onClickDistribute(event) {
        this.showDistribute = true; // Show the box
        this.enterTotals=true; // Make the body be the properties that totals can be entered into
        this.enterWeightsMonth=false; //Make sure this is not showing
        this.enterWeightsPeriod=false; // Make sure this is not showing
        this.varDisButtons=false; // Make sure the wrong buttons aren't showing
        // Set monthWeights to be 100/the amount of goals (12 or 13 depending on if CORO)
        this.monthWeights = [];
        for (let i = 0; i < this.monthGoals.length; i++) {
            this.monthWeights.push(100/this.monthGoals.length);
        }
        // Reset everything else
        this.updatedTotals=[]  
        this.tempTotals = []; 
    }

    /**
     * @author Connor Davis
     * @descripton This function will bring the user to the correct enter Weights page
     * @param {Object} event 
     */
    onClickVariable(event) {
        this.enterTotals=false;
        // Show 13 periods if CORO, show 12 months if not
        if (this.type.data === 'CORO') {

            this.enterWeightsPeriod=true;
        } else {

            this.enterWeightsMonth=true;
        } 
        this.varDisButtons=true; // Show the buttons needed for this functionality
        // Set monthWeights to all 0's
        this.monthWeights = []; 
        for (let i = 0; i < this.monthGoals.length; i++) {
            this.monthWeights.push(0);
        }
    }

    /**
     * @author Connor Davis
     * @description This method will close the modal box if the use clicks 'Cancel'
     * @param {Object} event 
     */
    onClickCancel(event) {
        this.showDistribute = false;
    }

    /**
     * @author Connor Davis
     * @description This method will go back to the enter totals screen from the enter weights screen.
     * @param {Object} event 
     */
    onClickBack(event) {
        // Set enter totals to true, everything else to false. reset the tempTotals variable
        this.enterTotals=true;
        this.enterWeightsMonth=false;
        this.enterWeightsPeriod=false;
        this.varDisButtons=false;
        this.tempTotals=[];
    }

    /**
     * @author Connor Davis
     * @description This method adds up the weights
     * @return totalWeight Returns the value of all the weights added up
     */
    get totalWeights() {
        let totalWeight = 0;
        for (let i = 0; i < this.monthGoals.length; i++) {
            totalWeight += this.monthWeights[i];
        }

        return totalWeight
    }

    /**
     * @author Connor Davis
     * @description This method takes the list of properties that will be changed and makes them more readable by removing '_' and '__c'
     * @return Returns a list of properties that will be changed in a readable way
     */
    get updatedTotalsStrings() {
        let totalsToFormat = []
        for (let i = 0; i < this.updatedTotals.length; i++) {
            totalsToFormat.push(this.updatedTotals[i].replace(/__c/, ''));
            totalsToFormat[i] = totalsToFormat[i].replace(/_/g, ' ');
            totalsToFormat[i] = ' ' + totalsToFormat[i];
        }

        return totalsToFormat
    }

    /**
     * @author Connor Davis
     * @descripton This method will keep track of what properties are being edited and what their values are
     * @param {Object} event 
     */
    onChangeTotal(event) {
        if (!this.updatedTotals.includes(event.srcElement.name)) {
            this.updatedTotals.push(event.srcElement.name);
        }
        this.tempTotals[event.srcElement.name] = Object.values(event.detail);
    }

    /**
     * @author Connor Davis
     * @description This method keeps track of what the weights are as they are edited
     * @param {Object} event 
     */
    onChangeWeight(event) {
        this.monthWeights[event.srcElement.name] = Number(Object.values(event.detail));
    }

    /**
     * @author Connor Davis
     * @description This method will split the rawGoals, which contains both monthly and total goals, into monthGoals, and totals
     */
    splitGoals() {
        for (let i = 0; i < this.rawGoals.length-1; i++) {
            this.monthGoals.push(this.rawGoals[i]);
        }

        this.totals.push(this.rawGoals[this.rawGoals.length-1]);
    }

    // Used for debugging purposes
    get info() {
        console.log('Location: ' + this.loc.data);
        console.log('Year ' + this.year);
        return 0;
    }

    /**
     * @author Connor Davis
     * @description If the selected year is greater than the current year, this method will distribute whatever values are in the totals table into the corresponding columns in the goals table
     * @param {Object} event The event that triggered this function, which is the save button being clicked
     */
    handleDistributeTotals(event) {

        // Make sure they are within a certain range of 100 before allowing them to distribute
        if (this.totalWeights > 100-0.0001 && this.totalWeights < 100+0.0001) {

            // An array that will store the goals we want to update, its contents will be draft values that we will create
            let goalsToUpdate = [];

            // These loops will loop through each month and create an object that is the same as the draft values that are created when editing the table
            for (let i = 0; i < this.monthGoals.length; i++) {
                // Create a temporary variable with the id for the current month
                let tempGoal = { 'Id': this.monthGoals[i]['Id'] };
                // This loop goes through each property that was edited in the totals table, then adds that property and part of its value
                for (let j = 0; j < this.updatedTotals.length; j++) {
                    let field = this.updatedTotals[j];
                    tempGoal[field] = Math.round(this.tempTotals[field] * (this.monthWeights[i]/100));
                }
                // Push the new draft Value to the goalsToUpdate array
                goalsToUpdate.push(tempGoal);
            }

            // Send the draft values to the save function
            this.saveGoals(goalsToUpdate);

            this.showDistribute = false;

          // If the selected year is not greater than the current year, show a warning message
        } else {
            alert('Percent of Sales Covered must be 100%');
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Warning',
                    message: 'Percent of Sales Covered must be 100%',
                    variant: 'warning'
                })
            );
        }
    }

    /**
     * @author Connor Davis
     * @description This method will send the draft values of the table to the save function
     * @param {Object} event The event caused by the user clicking save
     */
    handleSave(event) {

        // Send the draft values to the save function
        this.saveGoals(event.detail.draftValues)
    }

    /**
     * @author Connor Davis
     * @description This method takes in an array of changed values, then updates the necessary records 
     * @param {Object} savedGoals An array of the goals we want to save/change
     */
    saveGoals(savedGoals) {

        // Takes the passed in object and creates recordInputs out of them so they can be used in updateRecord, and stores them in the newGoals variable
        this.newGoals = savedGoals.slice().map(values => {
            let fields = Object.assign({}, values);
            return {fields};
        });

        // Maps each newGoal to an updateRecord call and stores them in promises
        let promises = this.newGoals.map(newGoal => updateRecord(newGoal));
        // Goes through each promise, and if they are all succesful, shows the success message
        Promise.all(promises).then(goals => {
            alert('Store Goals Updated');
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Store Goals Updated',
                    variant: 'success'
                })
            );

            // Clear the new goals
            this.newGoals = [];

            // Refresh the data in the table
            return refreshApex(this.updatedGoals);
        }).catch(error => { 
            alert('Cannot edit goals from past Months/Periods');
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Cannot edit goals from past Months/Periods',
                    variant: 'error'
                })
            );
        });
    }
}