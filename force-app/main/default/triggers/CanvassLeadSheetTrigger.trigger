trigger CanvassLeadSheetTrigger on CNVSS_Canvass_Lead_Sheet__c (after insert, after update) {

    if(trigger.isUpdate) {
        list<CNVSS_Canvass_Lead_Sheet__c> listLeadSheetsToApprove = new list<CNVSS_Canvass_Lead_Sheet__c>();
        for(CNVSS_Canvass_Lead_Sheet__c cls : trigger.new) {
            if(cls.CNVSS_Status__c == 'Approved: DO NOT CONTACT') {
               listLeadSheetsToApprove.add(cls);
            }
        }
        if(listLeadSheetsToApprove.size() > 0)
         	CanvassLeadSheetTriggerHandler.updateCanvassUnitDoNotContact(listLeadSheetsToApprove);
    }
}