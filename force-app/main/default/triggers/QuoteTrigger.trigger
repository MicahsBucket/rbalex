trigger QuoteTrigger on Quote (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

	// Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Quote')) return;
	
	// Before Insert
	//if(Trigger.isInsert && Trigger.isBefore){
		
	//}

	// Before Delete
	if(Trigger.isDelete && Trigger.isBefore){
		system.debug('****Order Is Delete Is Before');
		RMS_QuoteTriggerHandler.OnBeforeDelete(Trigger.oldMap, Trigger.newMap);

	}
	
	// After Insert
	//if(Trigger.isInsert && Trigger.isAfter){
		
	//}
	
		
	// After Update
	//if(Trigger.isUpdate && Trigger.isAfter){
		
	//}
}