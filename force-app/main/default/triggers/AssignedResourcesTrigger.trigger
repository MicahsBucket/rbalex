trigger AssignedResourcesTrigger on AssignedResource (before update, after insert, after update, after delete) {
    
    // Begin logic to filter skip logic by profile        
    if (!UtilityMethods.isTriggerActive('AssignedResource')) {
    	return;
    }
    List<SObject> serviceAppts = new List<SObject>();

    // rForce-1524: If the service resource changes, it was modifed manually or by FSL, and not by the custom scheduling components.
    if (Trigger.isBefore && Trigger.isUpdate) {
        for (AssignedResource ar : Trigger.new) {
            if (Trigger.oldMap.get(ar.Id) != null && ar.ServiceResourceId != Trigger.oldMap.get(ar.Id).ServiceResourceId) {
                ar.Scheduled_Via_Custom_Component__c = false;
            }
        }
    }

    if (Trigger.isAfter) {
        if(Trigger.isInsert) {
            if (UtilityMethods.hasServiceApptTriggerRan() && !System.isFuture() && !System.isBatch()){
                System.debug('>>>>>>>> AFTER INSERT.');
                RMS_FutureRollups.rollupAssignedResourcesToServiceAppts(trigger.newMap.keySet());
            } else {
                System.debug('>>>>>>>> AFTER INSERT 1.');
                update (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            }
        }
        if (Trigger.isUpdate) {
            if (UtilityMethods.hasServiceApptTriggerRan() && !System.isFuture() && !System.isBatch()){
                System.debug('>>>>>>>> AFTER UPDATE.');
                RMS_FutureRollups.rollupAssignedResourcesToServiceAppts(trigger.newMap.keySet());
            } else {
                System.debug('>>>>>>>> AFTER UPDATE 2');
                update (List<SObject>) dlrs.RollupService.rollup(trigger.new);
            }
        }
        if (Trigger.isDelete) {
            if (UtilityMethods.hasServiceApptTriggerRan() && !System.isFuture() && !System.isBatch()){
                RMS_FutureRollups.rollupAssignedResourcesToServiceAppts(trigger.oldMap.keySet());
            } else {
                update (List<SObject>) dlrs.RollupService.rollup(trigger.old);
            }
        }
    }
}