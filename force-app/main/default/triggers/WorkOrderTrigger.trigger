trigger WorkOrderTrigger on WorkOrder (before delete, before insert, before update, after delete, after insert, after undelete, after update) {

    UtilityMethods.setWOTriggerStarted();

    if (!UtilityMethods.isTriggerActive('WorkOrder')) return;

    FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isInsert || Trigger.isUpdate), 'WorkOrder');

    List<SObject> orders = new List<SObject>();

    if (Trigger.isBefore) {
        if (Trigger.isInsert){

            System.debug('>>>>>>>>>> Total Number of SOQL Queries (Workorder) OnBeforeInsert: ' +  Limits.getQueries());
            RMS_addPaintStainOrderItemNotestoWO addPaintStainNotestoWO = new RMS_addPaintStainOrderItemNotestoWO();
            addPaintStainNotestoWO.addPaintStainNotesWO(Trigger.new);
            WorkOrderTriggerHandler.setduration(Trigger.new);
            WorkOrderTriggerHandler.updateMunicipalityFields(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderFromOrderOnInsert(Trigger.new);
            WorkOrderTriggerHandler.setWorkTypeOnWorkOrder(Trigger.new);
            WorkOrderTriggerHandler.assignServiceTerritories(trigger.new, null);
            WorkOrderTriggerHandler.setOperatingHours(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderFromAccount(Trigger.new);
        }
        if (Trigger.isUpdate) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (Workorder) OnBeforeUpdate: ' +  Limits.getQueries());
            WorkOrderTriggerHandler.clearFieldsOnCancelation(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.clearFieldsOnUnscheduling(Trigger.new, Trigger.oldMap);  
            WorkOrderTriggerHandler.workOrderRecordTypeChange(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.updateWorkOrderTypes(Trigger.newMap);
            WorkOrderTriggerHandler.updateMunicipalityFields(Trigger.new);
            RMS_WorkOrderCreationManager.createInstallWorkOrderOnTechMeasureComplete(Trigger.oldMap, Trigger.newMap);
            WorkOrderTriggerHandler.updateWorkOrderFromOrderOnUpdate(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap); 
            WorkOrderTriggerHandler.setOperatingHours(Trigger.new);
            WorkOrderTriggerHandler.updateWorkOrderRescheduleDate(Trigger.new, Trigger.oldMap);  
            WorkOrderTriggerHandler.updateWorkOrderFromAccount(Trigger.new);
            WorkOrderTriggerHandler.setWOCompleteDate(Trigger.new, Trigger.oldMap);
            WorkOrderTriggerHandler.blockOverwritingWithOldRecordVersionInClassic(Trigger.new, Trigger.oldMap);
        }
    }

    if (Trigger.isAfter) {
        
        if(Trigger.isInsert) {
            WorkOrderTriggerHandler.generateWOLIonInsert(Trigger.newMap);
            WorkOrderTriggerHandler.assignSkills(Trigger.new);
            FSLRollupRolldownController.triggerFromWorkOrder(new Map<Id,WorkOrder>(), Trigger.newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_INSERT);
        }
        if (Trigger.isUpdate) {
            WorkOrderTriggerHandler.generateWOLIonInsert(Trigger.newMap);
            RMS_cancelWOManager.closeEvent(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            WorkOrderTriggerHandler.updateSkills(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            FSLRollupRolldownController.triggerFromWorkOrder(Trigger.oldMap, Trigger.newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
        }
    }

    UtilityMethods.setWOTriggerRan();
}