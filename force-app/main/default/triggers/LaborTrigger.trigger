/*******************************************************//**

@trigger LaborTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2016-1/7  Slalom.ADS
    Created.

@see        LaborTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger LaborTrigger on Labor__c (after delete, after insert, after undelete,
                                    after update, before delete, before insert, before update) {

    if (!UtilityMethods.isTriggerActive('Labor__c')) return;
    //HANDLERS AND MANAGERS
    LaborTriggerHandler handler = new LaborTriggerHandler(Trigger.isExecuting,Trigger.size);
    RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
    List<SObject> wkorders = new List<SObject>();

    // Before Insert

    if(Trigger.isInsert && Trigger.isBefore){
        handler.onBeforeInsert(Trigger.new);
    }

    //  Before Update
    /*
    if(Trigger.isUpdate && Trigger.isBefore){
        financialTransactionManager.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
        */
    if(Trigger.isUpdate && Trigger.isBefore){
        Boolean lockPrev = false;
        For (Labor__c labor : Trigger.old){
            if(lockPrev == false){
                lockPrev = labor.Locked__c;
            }
        }
        if(lockPrev){
            UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Update');
        }
    }

    // Before Delete
    //else
    if(Trigger.isDelete && Trigger.isBefore){
        UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Delete');

        financialTransactionManager.onBeforeDeleteLabor(Trigger.old, Trigger.oldMap);
    }

    // After Insert
    else if(Trigger.isInsert && Trigger.isAfter){
        financialTransactionManager.onAfterInsertLabor(Trigger.new, Trigger.newMap);
        wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
    }

    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        financialTransactionManager.onAfterUpdateLabor(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
    }


    //After Delete

    else if(Trigger.isDelete && Trigger.isAfter){
        wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.old);
    }


    // After Undelete

    else if(Trigger.isUnDelete){
        wkorders = (List<SObject>) dlrs.RollupService.rollup(Trigger.new);
    }

    // Try - Catch to catch any dml errors doing the work order rollup and displaying
    // errors on the labor records
    try {
        update wkorders;
    } catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : Trigger.old) {
            obj.addError(e.getDmlMessage(0));
        }
        else for (sObject obj : Trigger.new) {
            obj.addError(e.getDmlMessage(0));
        }
    }
}