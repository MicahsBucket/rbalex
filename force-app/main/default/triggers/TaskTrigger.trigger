/*******************************************************//**

@trigger TaskTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson (Slalom.BLW)

@version    2016-5/10  Slalom.BLW
    Created.


@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger TaskTrigger on Task (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Task')) return;
    
        
    //HANDLERS AND MANAGERS        
    TaskTriggerHandler handler = new TaskTriggerHandler();        
    RMS_assignServiceTaskOwner assignServiceTaskOwner = new RMS_assignServiceTaskOwner();

    //Before Insert
    
    if(Trigger.isInsert && Trigger.isBefore){
        assignServiceTaskOwner.assignServiceTaskOwner(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);  
        assignServiceTaskOwner.populateAssignedTo(Trigger.new, Trigger.newMap);  
    }
    
    //  Before Update
    
    if(Trigger.isUpdate && Trigger.isBefore){
        assignServiceTaskOwner.assignServiceTaskOwner(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);  
    }
        

    // After Update
    if(Trigger.isUpdate && Trigger.isAfter){
        handler.onAfterUpdate(Trigger.oldMap,Trigger.newMap);
    }
}