/*******************************************************//**

@trigger OrderTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2015-10/15  Slalom.ADS
    Created.
    Edited by Ian Fitzpatrick ianf@slalom.com 11/8/2017
    Edited by Mitchell Woloschek mwoloschek@penrodsoftware.com 3/16/2018

@see        OrderTriggerTest

@copyright  (c)2015 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger OrderTrigger on Order (before insert, before update, before delete, after insert, after undelete, after update, after delete) {

    // Set the order trigger to ran
    // this gets checked in the RbAWorkOrderTrigger to prevent recursion when updating records
    UtilityMethods.setOrderTriggerRan();

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Order')) return;

    FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isUpdate), 'Order');
    // FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isInsert || Trigger.isUpdate), 'Order'); // FOR FUTURE REFERENCE: use this line if FSLRollupRolldownController is also called from After Insert

        //HANDLERS AND MANAGERS
    //I can't make this static because it could get called from other classes:
        RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
        RMS_ServiceProductPickup customerPickup = new RMS_ServiceProductPickup();
        List<SObject> accounts = new List<SObject>();

        // Before Insert
        if(Trigger.isInsert && Trigger.isBefore){
      //      System.debug('>>>>>>>>>> Order Is Insert Is Before');
      //      System.debug('>>>>>>>>>> Total Number of SOQL Queries (Order) OnBeforeInsert: ' +  Limits.getQueries());
            RMS_OrderTriggerHandler.OnBeforeInsert(Trigger.new);
        }

        //  Before Update
        if(Trigger.isUpdate && Trigger.isBefore && !RMS_WorkOrderCreationManager.updatingBackOfcRecs){
      //      System.debug('>>>>>>>>>> Order Is Update Is Before');
      //      System.debug('>>>>>>>>>> Total Number of SOQL Queries (Order) OnBeforeUpdate: ' +  Limits.getQueries());
            UtilityMethods.checkLockedByStatus(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Order');
            RMS_WorkOrderCreationManager.createWorkOrderOnOrderActivationFutureHelper(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
            RMS_WorkOrderCreationManager.updatingBackOfcRecs = true;
            RMS_OrderTriggerHandler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        }


        // After Insert
    if(Trigger.isInsert && Trigger.isAfter){
      //  System.debug('>>>>>>>>>> Order Is Insert Is After');
      //  System.debug('>>>>>>>>>> Total Number of SOQL Queries (Order) OnAfterInsert: ' +  Limits.getQueries());
        RMS_WorkOrderCreationManager.createWorkOrderOnOrderCreationFutureHelper(Trigger.new, Trigger.newMap);
        RMS_backOfficeChecklistManager.createBackOfficeChecksOnOrderCreation(Trigger.new, Trigger.newMap);
        accounts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        RMS_OrderTriggerHandler.OnAfterInsert(Trigger.new);
        //RMS_OrderTriggerHandler.UpdateOrderEstiShipDateToSerApp(trigger.newMap, null);
    }

    // After Update
    if(Trigger.isUpdate && Trigger.isAfter  && !RMS_WorkOrderCreationManager.updatingWorkorders){
      //  System.debug('>>>>>>>>>> Order Is Update Is After');
        RMS_WorkOrderCreationManager.createWorkOrderOnOrderSoldOrderBeingAssignedFutureHelper(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        RMS_WorkOrderCreationManager.updatingWorkorders = true;
        financialTransactionManager.onAfterUpdateOrder(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        customerPickup.customerPickup(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        accounts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
        RMS_OrderTriggerHandler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
        //RMS_OrderTriggerHandler.UpdateOrderEstiShipDateToSerApp(trigger.newMap, trigger.oldMap);

    }
    if(Trigger.isUpdate && Trigger.isAfter){
        FSLRollupRolldownController.triggerFromOrder(Trigger.oldMap,Trigger.newMap,FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
    }

    //I am not sure what this is:
    //After Delete
    if(Trigger.isAfter && Trigger.isDelete){
      //  System.debug('>>>>>>>>>> Order Is Delete Is After');
        //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        accounts = (List<SObject>) dlrs.RollupService.rollup(trigger.old);
    }


        // After Undelete

    if(Trigger.isAfter && Trigger.isUnDelete){
      //  System.debug('>>>>>>>>>> Order Is UnDelete Is After');
        //handler.OnUndelete(Trigger.new);
            accounts = (List<SObject>) dlrs.RollupService.rollup(trigger.new);
    }

        // Try - Catch to catch any dml errors doing the account rollup and displaying
        // errors on the order records
    // Does this work?
    try {
      //  System.debug('>>>>>>>>>> Order Is Update Accounts');
        update accounts;
    } catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
            else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }

}