trigger CommUserPaymentPlanTrigger on Comm_User_Payment_Plan__c (after insert, after update) {

	if(Trigger.isAfter){
		if(Trigger.isUpdate || Trigger.isInsert)
			CommissionsManagement.checkPaymentPlans(Trigger.new);
	}

}