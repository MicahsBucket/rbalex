/*******************************************************//**

@trigger ContactTrigger

@brief  trigger framework to secure order of operation

@author  Brianne Wilson (Slalom.BLW)

@version    2016-04/04  Slalom.BLW
Created.
@version    2017-04-07  Slalom.CDK
Revised

@see        ContactTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
Unauthorized use is prohibited.

***********************************************************/

trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after undelete, after update, after delete) {
    if (!UtilityMethods.isTriggerActive('Contact')) return;

    //Trigger to add the Co Applicant details into the contact record. Only run if he contact is saved in the UI (Only one record). This won't process bulk updates
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore && Trigger.new.size() == 1){
        Contact c = Trigger.new[0];
        CFW_ContactManagement.checkForCoApplicant(c);
        CFW_ContactManagement.checkForDefaultSSN(c);
    }

    //HANDLERS AND MANAGERS
    RMS_createContactHistoryManager contactHistoryCreationManager = new RMS_createContactHistoryManager();
    
    // Before Insert
    /*
    if(Trigger.isInsert && Trigger.isBefore){
    }
    */
    //Before Update
    /*
    else if(Trigger.isUpdate && Trigger.isBefore){
    }
    */
    
    
    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
    }
    */
    
    // After Insert 
    if(Trigger.isInsert && Trigger.isAfter){       
        contactHistoryCreationManager.afterContactInsertManager(Trigger.new);              
        
    } 
    
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        contactHistoryCreationManager.afterContactUpdateManager(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);    
    }
    
    /*
    else if(Trigger.isDelete && Trigger.isAfter){
    }
    */


    // After Undelete 
    /*
    else if(Trigger.isUnDelete){
    }
    */
    
}