trigger  OrderFinancingTrigger on Order_Financing__c (after update, after insert,after delete){ 

// Begin logic to filter skip logic by profile    
if (!UtilityMethods.isTriggerActive('Order_Financing__c')) return;

set<Id> recordIds = new set<Id>();

if(Trigger.isInsert && Trigger.isAfter ||Trigger.isUpdate && Trigger.isAfter){
   for(Order_Financing__c OrdFin: Trigger.new){ 
        recordIds.add(OrdFin.Id); 
        system.debug('@@@'+recordIds);
        OrderFinancingTriggerHandler.updateFinanceExpirationDateonOrder(recordIds);
       }
       UtilityMethods.setOrderFinancingTriggerRan();
          }
if(Trigger.isDelete && Trigger.isAfter){
   for(Order_Financing__c OrdFin: Trigger.old){ 
        recordIds.add(OrdFin.Id); 
        system.debug('@@@'+recordIds);
        OrderFinancingTriggerHandler.updateFinanceExpirationDateonOrder(recordIds);
         }
   }
}