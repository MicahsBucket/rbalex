trigger Canvass_Unit_Sharing on CNVSS_Canvass_Unit__c (after insert,after update) {

    //get list of public groups to share with
    
    Map<String,Id> mktGrps = new Map<String,Id>(); // Define a new map
    for (Group g: [select id,DeveloperName,name from group where name like 'CNVSSApex-%']){
        mktGrps.put((g.name).substring(10),g.id);
    }
    
    set<id> updatedIdSet = new set<id>();
    List<CNVSS_Canvass_Unit__Share> removeShare = new List<CNVSS_Canvass_Unit__Share>();
    List<CNVSS_Canvass_Unit__Share> canvassunitShares  = new List<CNVSS_Canvass_Unit__Share>();

if (Trigger.isInsert){
    for(CNVSS_Canvass_Unit__c c_unit : trigger.new){
        if (mktGrps.containsKey(c_unit.market__c)){
            CNVSS_Canvass_Unit__Share cUnitShare = new CNVSS_Canvass_Unit__Share();
            cUnitShare.ParentId = c_unit.Id;
            cUnitShare.UserOrGroupId = mktGrps.get(c_unit.market__c);
            cUnitShare.AccessLevel = 'edit';
            cUnitShare.RowCause = Schema.CNVSS_Canvass_Unit__Share.RowCause.APEX_Sharing_for_Canvass_Market__c;
            canvassunitShares.add(cUnitShare);
        }
                
    }
}else if (Trigger.isUpdate){
    for (CNVSS_Canvass_Unit__c c: Trigger.new) {
        if (  (Trigger.oldMap.get(c.Id).market__c != c.market__c) )  {
            updatedIdSet.add(c.Id);
            if (mktGrps.containsKey(c.market__c)){
                CNVSS_Canvass_Unit__Share cUnitShare = new CNVSS_Canvass_Unit__Share();
                cUnitShare.ParentId = c.Id;
                cUnitShare.UserOrGroupId = mktGrps.get(c.market__c);
                cUnitShare.AccessLevel = 'edit';
                cUnitShare.RowCause = Schema.CNVSS_Canvass_Unit__Share.RowCause.APEX_Sharing_for_Canvass_Market__c;
                canvassunitShares.add(cUnitShare);
            }
            
        }
    }
}
    //remove old APEX sharing
    
    removeShare = [select id,ParentId from CNVSS_Canvass_Unit__Share where ParentId IN: updatedIdSet 
                and UserOrGroupId IN:mktGrps.values()];
    if(removeShare.size() > 0){
        delete removeShare;
    }


    // Insert all of the newly created Share records and capture save result 
    
    Database.SaveResult[] jobShareInsertResult = Database.insert(canvassunitShares,false);
        
    // Error handling 
    CNVSS_Apex_Sharing_Error_Log__c[] errs = new List<CNVSS_Apex_Sharing_Error_Log__c>();

    for(Database.SaveResult sr: jobShareInsertResult){ 
     if (!sr.isSuccess()) { 
           CNVSS_Apex_Sharing_Error_Log__c er = new CNVSS_Apex_Sharing_Error_Log__c(Name=sr.getId(),
                    Object__c='Canvass Unit',Error__c=sr.getErrors()[0].getMessage());

        }
    }
    if (errs.size()>0){
        insert errs;
    } 

}