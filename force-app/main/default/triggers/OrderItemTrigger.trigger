/*******************************************************/
/**
    
    @trigger OrderItemTrigger
    
    @brief  trigger framework to secure order of operation
    
    @author  Anthony Strafaccia (Slalom.ADS)
    
    @version    2016-3/3  Slalom.ADS
    Created.
    
    @see        OrderItemTriggerTest
    
    @copyright  (c)2016 Slalom.  All Rights Reserved.
    Unauthorized use is prohibited.
    
    ***********************************************************/
trigger OrderItemTrigger on OrderItem(after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('OrderItem')) return;

    //HANDLERS AND MANAGERS
    RMS_OrderItemManager orderItemManager = new RMS_OrderItemManager();
    RMS_createCharges createCharges = new RMS_createCharges();
    List <SObject> orderItems = new List <SObject> ();

    // Before Insert
    if(Trigger.isInsert && Trigger.isBefore){
//          orderItemManager.linkNewChildProducts(Trigger.new);
    } 
    //  Before Update
    else  if (Trigger.isUpdate && Trigger.isBefore) {

    }
    // Before Delete
    /*
    else if(Trigger.isDelete && Trigger.isBefore){
        UtilityMethods.checkLocked(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap, 'Delete');
        financialTransactionManager.onBeforeDeletePayment(Trigger.old, Trigger.oldMap);
    }
    */
    // After Insert
    else if (Trigger.isInsert && Trigger.isAfter) {
        orderItemManager.setUpChangeHistoryOnCreate(Trigger.new, Trigger.newMap);
        createCharges.createCharge(Trigger.new, Trigger.newMap);
        RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
    }
        
    // After Update
    else if(Trigger.isUpdate && Trigger.isAfter){
        Set<Id> oldAndNewTriggerIds = new Set<Id>();
        oldAndNewTriggerIds.addAll(trigger.newMap.keySet());
        oldAndNewTriggerIds.addAll(trigger.oldMap.keySet());    
        RMS_FutureRollups.rollupOrderItemsToOrders(oldAndNewTriggerIds);
        orderItemManager.updateChangeHistoryOnUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
    }
    //After Delete
    else if(Trigger.isDelete && Trigger.isAfter){
        RMS_FutureRollups.rollupOrderItemsToOrders(trigger.oldMap.keySet());
    }
    // After Undelete 
    else if(Trigger.isUnDelete){
        RMS_FutureRollups.rollupOrderItemsToOrders(trigger.newMap.keySet());
    }    
    // Try - Catch to catch any dml errors doing the order rollup and displaying
    // errors on the order item records
    try { update orderItems;} 
    catch(System.DmlException e) {
        if (Trigger.isDelete) for (sObject obj : trigger.old) { obj.addError(e.getDmlMessage(0)); }
        else for (sObject obj : trigger.new) { obj.addError(e.getDmlMessage(0)); }
    }
}