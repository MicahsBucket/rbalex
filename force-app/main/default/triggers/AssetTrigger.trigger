/*******************************************************//**

@trigger AssetTrigger

@brief  trigger framework to secure order of operation

@author  Anthony Strafaccia (Slalom.ADS)

@version    2016-1/7  Slalom.ADS
    Created.

@see        AssetTriggerTest

@copyright  (c)2016 Slalom.  All Rights Reserved.
            Unauthorized use is prohibited.

***********************************************************/

trigger AssetTrigger on Asset (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    // Begin logic to filter skip logic by profile    
    if (!UtilityMethods.isTriggerActive('Asset')) return;


    //HANDLERS AND MANAGERS
    RMS_financialTransactionManager financialTransactionManager = new RMS_financialTransactionManager();
        
    // Before Insert
    /*
    if(Trigger.isInsert && Trigger.isBefore){
        handler.onBeforeInsert(Trigger.new);
    }
    */
    //  Before Update
    /*
    if(Trigger.isUpdate && Trigger.isBefore){
        financialTransactionManager.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
        */

    // Before Delete
    //else  
    if(Trigger.isDelete && Trigger.isBefore){
        financialTransactionManager.onBeforeDeleteAsset(Trigger.old, Trigger.oldMap);
    }

    // After Insert
    else if(Trigger.isInsert && Trigger.isAfter){
        financialTransactionManager.onAfterInsertAsset(Trigger.new, Trigger.newMap);
    } 
        
    // After Update 
    else if(Trigger.isUpdate && Trigger.isAfter){
        financialTransactionManager.onAfterUpdateAsset(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
    
                
    //After Delete
    /*
    else if(Trigger.isDelete && Trigger.isAfter){
        handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }
    */
    
    // After Undelete 
    /*
    else if(Trigger.isUnDelete){
        financialTransactionManager.onUndelete(Trigger.new, Trigger.newMap);
    }
    */
}