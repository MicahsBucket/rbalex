/*

	Used to keep Commission Totals YTD_Sales_Total__c synced with YTD_Sales__c formula field.
	This allows YTD_Sales_Total__c to be referenced in a Commission Totals formula 
	field (using previous commissions) while also staying up to date with changes made on 
	the Commission Total object

*/

trigger CommissionTotalsTrigger on Commission_Totals__c (before insert, before update, after insert, after update) {

	if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore){
		for(Commission_Totals__c ct : Trigger.New){

			if(ct.YTD_Sales__c != ct.YTD_Sales_Total__c){
				ct.YTD_Sales_Total__c = ct.YTD_Sales__c;
			}

			if(ct.Negative_Balance_Formula__c != ct.Negative_Balance__c){
				ct.Negative_Balance__c = ct.Negative_Balance_Formula__c;
			}

			if(ct.Previous_Period_Sales_Formula__c != ct.Prev_Sales_This_Period__c){
				ct.Prev_Sales_This_Period__c = ct.Previous_Period_Sales_Formula__c;
			}

		}
	}

	if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){
		CommissionsManagement.checkForTotalsUpdate(Trigger.newMap, Trigger.oldMap);
  }

}