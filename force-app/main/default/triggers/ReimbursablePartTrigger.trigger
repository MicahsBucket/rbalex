/*
* @author Jason Flippen
* @date 01/13/2021
* @description: Master Trigger for the Reimbursable_Part__c object.
*
*              Code Coverage provided by the following Test Class:
*              - ReimbursablePartTriggerHandlerTest
*
* CHANGE HISTORY
* ====================================================================================================
* DATE          NAME                    DESCRIPTION
* 20210113      Jason Flippen           Created
* ====================================================================================================
*/
trigger ReimbursablePartTrigger on Reimbursable_Part__c (after insert, after delete) {

    if (Trigger.isAfter && Trigger.isInsert) {
        ReimbursablePartTriggerHandler.afterInsert(Trigger.new);
    }
    else if (Trigger.isAfter && Trigger.isDelete) {
        ReimbursablePartTriggerHandler.afterDelete(Trigger.old, Trigger.oldMap);
    }
    
}