/**
* @author Calvin O'Keefe, Slalom Consulting
* @group Signature Service Community
* @date 8/03
* @description Updates contact records associated to surveys when surveys are updated
**/
trigger SurveyTrigger on Survey__c (before insert,after update) {

    if(Trigger.isAfter && Trigger.isUpdate){
        SurveyToRForceUpdater.SurveyToRForceContactUpdater(Trigger.new, Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isBefore && Trigger.isInsert){
       SurveyToRForceUpdater.associatingParentActStoreCon(Trigger.new);
    
    }

}