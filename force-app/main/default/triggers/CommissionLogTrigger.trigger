/*

	Used to keep Commission Log Payment_Amount__c synced with Commissions__c formula field
	This allows Payment_Amount__c to be used in a Commission Totals roll-up field, while also staying
	up to date with changes made on the Commission Log object

*/

trigger CommissionLogTrigger on Commission_Log__c (before insert, before update, after update, after insert) {

	if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore){
		for(Commission_Log__c cl : Trigger.New){
			cl.Payment_Amount__c = cl.Commissions__c;	
		}
  }

  if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){
  	CommissionsManagement.checkForLogUpdate(Trigger.New, Trigger.oldMap);
  }

}