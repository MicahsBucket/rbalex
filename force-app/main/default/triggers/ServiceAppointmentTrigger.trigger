trigger ServiceAppointmentTrigger on ServiceAppointment (before insert, before update, after insert, after update) {
    UtilityMethods.setServiceApptTriggerStarted();
    
    if (!UtilityMethods.isTriggerActive('ServiceAppointment') || !UtilityMethods.runServiceApptTrigger) return;

    FSLRollupRolldownController.setTriggerStarted(Trigger.isBefore, (Trigger.isInsert || Trigger.isUpdate), 'ServiceAppointment');

    if (Trigger.isBefore) {
        if(Trigger.isInsert) {
            ServiceAppointmentTriggerHandler.setScheduledViaOnScheduling(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateWorkOrderField(trigger.new);
            ServiceAppointmentTriggerHandler.calculateEndTimeFromDuration(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateAddressFields(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateGanttLabel(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap);
        }
        if (Trigger.isUpdate) {
            ServiceAppointmentTriggerHandler.setScheduledViaOnScheduling(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateWorkOrderField(trigger.new);
            ServiceAppointmentTriggerHandler.calculateEndTimeFromDuration(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateAddressFields(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.populateGanttLabel(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.assignServiceTerritories(trigger.new, trigger.oldMap);
            ServiceAppointmentTriggerHandler.whenAppointmentScheduled(trigger.newMap, trigger.oldMap);
        }
    }
    if (Trigger.isAfter) {
        
        if(Trigger.isInsert) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (ServiceAppointment) OnAfterInsert: ' +  Limits.getQueries());
            FSLRollupRolldownController.triggerFromServiceAppointment(new Map<Id,ServiceAppointment>(), Trigger.newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_INSERT);
        }
        if (Trigger.isUpdate) {
            System.debug('>>>>>>>>>> Total Number of SOQL Queries (ServiceAppointment) OnAfterUpdate: ' +  Limits.getQueries());
            FSLRollupRolldownController.triggerFromServiceAppointment(Trigger.oldMap, Trigger.newMap, FSLRollupRolldownController.TriggerOperationEnum.IS_UPDATE);
        }
    }
    UtilityMethods.setServiceApptTriggerRan();
}